﻿using System;
using System.Collections.Generic;
using System.Text;
using Todo.Models;

namespace Todo.ViewsModels
{
    public class VMFacturaEnviar
    {
        public int nroFactura { get; set; }
        public int idFactura { get; set; }
        public int idEstado { get; set; }
        public DateTime fechaFactura { get; set; }
        public string nroCliente { get; set; }
        public int dniCliente { get; set; }
        public string nombreCliente { get; set; }
        public string domicilioCliente { get; set; }
        public int idLocalidad { get; set; }
        public int idDepartamento { get; set; }
        public int montoFactura { get; set; }
        public int idEstado2 { get; set; }
        public bool anulado { get; set; }
        public int idVendedor { get; set; }
        public string observacion { get; set; }
        public bool revisionAdm { get; set; }
        public int montoAnticipo { get; set; }
        public List<FacturaCuota> listaCuota { get; set; }
        public List<FacturaProducto> listaProducto { get; set; }

    }
}
