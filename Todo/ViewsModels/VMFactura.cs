﻿using System;
using System.Collections.Generic;
using System.Text;
using Todo.Models;

namespace Todo.ViewsModels
{
    public class VMFactura
    {
        public int nroFactura { get; set; }
        public int idFactura { get; set; }
        public int idEstado { get; set; }
        public DateTime fechaFactura { get; set; }
        public string fechaFacturaMostrar { get; set; }
        public string horaFacturaMostrar { get; set; }
        public string nroCliente { get; set; }
        public bool sincronizado { get; set; }
        public string nombreImagenFactura { get; set; }
        public int dniCliente { get; set;  }
        public string nombreCliente { get; set; }
        public string domicilioCliente { get; set; }
        public int idLocalidad { get; set; }
        public int idDepartamento { get; set; }
        public int montoFactura { get; set; }
        public List<VMProducto> listaProductos { get; set; }
        public int nroTalonario { get; set; }
        public string nombreEstado { get; set; }
        public int anticipo { get; set; }
        public bool financiacion { get; set; }
        public int idEstado2 { get; set; }
        public string nombreEstado2 { get; set; }
        public string nombreImagenestado2 { get; set; }
        public int idinterno { get; set; }
        public int montoDisponibleCliente { get; set; }
        public int montoTotalMaximoCliente { get; set; }
        public List<VMFacturaCouta> listaFacturaCuota { get; set; }
        public int totalContado { get; set; }
        public int totalCredito { get; set; }
        public int totalDebito { get; set; }
        public int cantCuotaMasAlta { get; set; }
        public int FormaPago { get; set; }
        public string nombreTipoFactura { get; set; }
        public bool anulado { get; set; }

        public bool revisionAdm { get; set; }
        public string observacion { get; set; }
        public bool nuevoCliente { get; set; }
        public bool puedeAgregarCuotas { get; set; }
        //datos que creo q no usamos
        public int cantProductos { get; set; }
        public string formaDePagoNombre { get; set; }
        public int idFormaPago { get; set; }
        public DateTime fechaPrimerCuota { get; set; }
        public string fechaPrimerCuotaMostrar { get; set; }
        public int cantidadCuotas { get; set; }
        public int idVendedor { get; set; }
        public int valorCuota { get; set; }
        
        //
        public int montoAnticipo { get; set; }
        public List<FacturaCuota>  listaCuota { get; set; }
        public List<FacturaProducto> listaProducto { get; set; }

        //    
    }
}
