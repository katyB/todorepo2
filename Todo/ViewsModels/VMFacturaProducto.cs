﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMFacturaProducto
    {
        public int idFacturaProducto { get; set; }
        public int idFactura { get; set; }
        public int nroTalonario { get; set; }
        public int idProducto { get; set; }
        public int cantidadComprada { get; set; }
        public int precio { get; set; }
    }
}
