﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMcobranzaRecibido
    {
        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public string nombreCliente { get; set; }
        public int idZona { get; set; }
        public decimal saldo { get; set; }
        public string domicilio { get; set; }
        public string cuentaCorriente { get; set; }
        public string nombreEstado { get; set; } //mensaje a mostrar
        public int idEstado { get; set; }

        public decimal monto { get; set; }
        public DateTime fechaCobranza { get; set; }
        public string fechaCobranzaMostrar { get; set; }
        public bool sincronizado { get; set; }
        public string imagenEstado { get; set; }
        public string nroComprobante { get; set; }

        public bool cobrado { get; set; }

        public decimal costoCapital { get; set; }
        public decimal costoOperativo { get; set; }

        public decimal montoPrimerCuota { get; set; }
    }
}
