﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMCobranzaCuentaCorrienterecibido
    {
        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public int idEstado { get; set; }
        public decimal montoComprobante { get; set; }

        public string imagenEstado { get; set; }
        public bool sincronizado { get; set; }
    }
}
