﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
   public  class VMDevolverProducto
    {
        public int idProducto { get; set; }
        public int idDeposito { get; set; }
        public int cantidad { get; set; }
        public string mensaje { get; set; }
        public int codigo { get; set; }
    }
}
