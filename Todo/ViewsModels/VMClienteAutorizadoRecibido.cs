﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMClienteAutorizadoRecibido
    {
        public string mensaje { get; set; }
        public bool autorizado { get; set; }
        public int cantComprasAnteriores { get; set; }
        public int cantReclamosAnteriores { get; set; }
        public decimal montoDisponible { get; set; }
        public decimal montoAutorizado { get; set; }
        public decimal montoTotalMaximo { get; set; }
        public decimal montoTotalMaximoCliente { get; set; }
        public string codigoCliente { get; set; }
        public int idEstado { get; set; }
        public bool tieneConexion { get; set; }
        public bool nuevoCliente { get; set; }
        public int idCliente { get; set; }
    }
}
