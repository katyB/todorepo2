﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMProductosRecibidos
    {
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public string codigoProducto { get; set; }
        public int idDeposito { get; set; }
        public string nombreDeposito { get; set; }
        public int saldo { get; set; } //es la cant que nos envia el Omar
        public int cantidad { get; set; } //no usar
        public int cantidadComprada { get; set; }
        public decimal precioUnitario { get; set; }
        public decimal precioTotal { get; set; }
        public int idinterno { get; set; }
        public int cantidadDevuelta { get; set; }
        public string imagenUltimoProducto { get; set; }
    }
}
