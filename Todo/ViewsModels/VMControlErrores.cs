﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMControlErrores
    {
        public int idControlErrores { get; set; }
        public string codigoControlErrores { get; set; }
        public string nombreError { get; set; }
        public bool sincronizado { get; set; }
        public DateTime fecha { get; set; }
    }
}
