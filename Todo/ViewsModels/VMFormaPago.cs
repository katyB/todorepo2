﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMFormaPago
    {
        public int idFormaPago { get; set; }
        public string nombre { get; set; }
        public int descuento { get; set; }
        public int aumento { get; set; }
        public int montoVenta { get; set; }

        public string formaDePagoNombre { get; set; }
        public bool financiacion { get; set; }
        public bool tieneFinanciacion { get; set; }
        public int cantCuotas { get; set; }
        public int cantidadCuotas { get; set; }
        public int idProducto { get; set; }
        public string codigoProducto { get; set; }
        public string nombreProducto { get; set; }
        public string nombreFormaPago { get; set; }
        public decimal precioProducto { get; set; }
    }
}
