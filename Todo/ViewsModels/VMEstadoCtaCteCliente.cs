﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMEstadoCtaCteCliente
    {
        public int idCliente { get; set; }
        public decimal  montoCuota { get; set; }
        public DateTime fechaVencimiento { get; set; }
    }
}
