﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMProducto
    {
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public string codigoProducto { get; set; }
        public int cantidad { get; set; }
        public int idDeposito { get; set; }
        public string nombreDeposito { get; set; }
        public int cantidadComprada { get; set; }
        public int precioUnitario { get; set; }
        public int precioTotal { get; set; }
        public int preciocuota { get; set; }
        public int cantCuota { get; set; }
        public int idFormaPago { get; set; }
        public string nombreFormaPago { get; set; }
        public int cantidadDevuelta { get; set; }
        public string imagenUltimoProducto { get; set; }
    }
}
