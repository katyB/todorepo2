﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMReclamo
    {
        public int NroReclamo { get; set; }
        public int idReclamo { get; set; }
        public string observacionCbr { get; set; }
        public string observacionVta { get; set; }
        public int nroTalonario { get; set; }
        public int nroFactura { get; set; }
        public bool Activo { get; set; }
        public int idCausa { get; set; }
        public string nombreCausa { get; set; }
        public string causaReclmo { get; set; }
        public int codCliente { get; set; }
        public string nombreCliente { get; set; }
        public int CbrIdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public int idEstado { get; set; }
        public string Estado { get; set; }
        public int CbrSolucion { get; set; }
        public string Solucion { get; set; }
        public DateTime fechaReclamo { get; set; }
        public DateTime fechaInicio { get; set; }
        public string fechaReclamoMostrar { get; set; }
        public bool sincronizado { get; set; }
        public string nroCliente { get; set; }
        public string domicilioCliente { get; set; }
        public string observacionVenta { get; set; }
        public string nombreImagen { get; set; }
        public string Referencia1 { get; set; }
        public string Referencia2 { get; set; }
    }
}
