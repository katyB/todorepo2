﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMFacturaCouta
    {
        public int idFacturaCouta { get; set; }
        public string nombreCuota { get; set; }
        public int monto { get; set; }
        public DateTime fechaVto { get; set; }
    }
}
