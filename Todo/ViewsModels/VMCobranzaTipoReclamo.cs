﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMCobranzaTipoReclamo
    {
        public int idCobranzaTipoReclamo { get; set; }
        public string nombreCobranzaTipoReclamo { get; set; }
    }
}
