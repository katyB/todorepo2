﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMClienteRecibido
    {
        public int idCliente { get; set; }
        public string codigoCliente { get; set; }
        public string nombreCliente { get; set; }
        public string domicilioCliente { get; set; }
        public string localidadCliente { get; set; }
        public int idLocalidad { get; set; }
        public string departamento { get; set; }
        public int idDepartamento { get; set; }
        public int idDomicilio { get; set; }
        public string actividad { get; set; }
        public string tipoDocumento { get; set; }
        public string nroDocumento { get; set; }
        public int idEstado { get; set; }
        public string nombreEstado { get; set; }
        public int idDeposito { get; set; }
        public int ingresoMensual { get; set; }
        public int ingresoComplementario { get; set; }

        public string imagenEstado { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string fechaNacimientoMostrar { get; set; }
        public string codPostal { get; set; }
        public string referencia { get; set; }
        public string telefonoFijo { get; set; }
        public string telefonoCel { get; set; }
        public bool sincronizado { get; set; }
        public int idinterno { get; set; }
        public string trabajo { get; set; }
        public string empresa { get; set; }
        public string domicilioEmpresa { get; set; }
        public string Antiguedad { get; set; }
        public string telefonoEmpresa { get; set; }
        public string nombreConyugue { get; set; }
        public string nombreOtro { get; set; }
        public string dniConyugue { get; set; }
        public string dniOtro { get; set; }
        public string empresaConyugue { get; set; }
        public string empresaOtro { get; set; }
        public int ingresoConyogue { get; set; }
        public int ingresoOtro { get; set; }
        public string antiguedadConyugue { get; set; }
        public string vinculoFamiliarOtro { get; set; }
        public string domicilioAlternativo { get; set; }
        public string departamentoAlternativo { get; set; }
        public int idDepartamentoAlternativo { get; set; }
        public string vivienda { get; set; }
        public DateTime fechaDesdeInquilino { get; set; }
        public DateTime fechaHastaInquilino { get; set; }
        public string tarjetaCredito { get; set; }
        public string estadoCivil { get; set; }
        public int idEstadoCivil { get; set; }
        public string CantidadHijos { get; set; }
        public string telFamiliar { get; set; }
        public string longitud { get; set; }
        public string latitud { get; set; }
        public string genero { get; set; }
        public string mensaje { get; set; }
        public string referenciaVecinal { get; set; }
        public bool nuevoCliente { get; set; }
        public decimal montoDisponible { get; set; }
        public decimal montoAutorizado { get; set; }
        public decimal montoTotalMaximo { get; set; }
        public int cantComprasAnteriores { get; set; }
        public int cantReclamosAnteriores { get; set; }
        public bool autorizado { get; set; }
    }
}
