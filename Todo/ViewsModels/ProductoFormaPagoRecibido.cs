﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
   public class ProductoFormaPagoRecibido
    {
        public int idFormaPago { get; set; }
        public string nombreFormaPago { get; set; }
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public decimal montoVenta { get; set; }
        public int cantidadCuotas { get; set; }
        public bool tieneFinanciacion { get; set; }
    }
}
