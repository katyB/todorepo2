﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
    public class VMCobranzaCuentaCorriente
    {
        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public int idEstado { get; set; }
        public int montoComprobante { get; set; }

    }
}
