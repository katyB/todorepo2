﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.ViewsModels
{
   public  class VMMontoMaximo
    {
        public int porcentajeDescuento { get; set; }
        public int montoMaximo { get; set; }
        public decimal montoTotalMaximo { get; set; }

    }
}
