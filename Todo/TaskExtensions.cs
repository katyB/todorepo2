﻿using System;
using System.Threading.Tasks;

namespace Todo
{
    public static class TaskExtensions
    {
        //Con el fin de iniciar la inicialización asincrónica, evitar el bloqueo de la ejecución y tener la oportunidad de detectar excepciones, 
        //la aplicación de ejemplo usa un método de extensión denominado SafeFireAndForget.

        //El método de extensión SafeFireAndForget proporciona funcionalidad adicional a la clase Task

        public static async void SafeFireAndForget(this Task task,
            bool returnToCallingContext,
            Action<Exception> onException = null)
        {
            try
            {
                await task.ConfigureAwait(returnToCallingContext);
            }

            // if the provided action is not null, catch and
            // pass the thrown exception
            catch (Exception ex) when (onException != null)
            {
                onException(ex);
            }
        }
    }
}
