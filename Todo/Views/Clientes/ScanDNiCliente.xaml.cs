﻿using System;
using System.Collections.Generic;
using System.Linq;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;
using ZXing.Mobile;
using ZXing.Net.Mobile.Forms;

namespace Todo.Views.Clientes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanDNiCliente : ContentPage
    {
        ClienteApp cliente = new ClienteApp();
        VMFactura factura = new VMFactura();
        int flag;
        bool puedeEntrar;

        public ScanDNiCliente(ClienteApp cl, int fl)
        {
            flag = fl;
            cliente = cl;
            InitializeComponent();

        }

        public ScanDNiCliente(VMFactura f, int fl)
        {
            flag = fl;
            factura = f;
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        public async void Escanear(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                var options = new MobileBarcodeScanningOptions
                {
                    PossibleFormats = new List<BarcodeFormat>
            {
                BarcodeFormat.PDF_417
            }
                };

                ZXingScannerPage scanPage;

                scanPage = new ZXingScannerPage(options);

                scanPage.OnScanResult += (result) =>
                {

                    string texto = result.Text;
                    scanPage.IsScanning = false;
                    SepararTexto(texto);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Navigation.PopModalAsync();

                        //if (flag == 7)
                        //{
                        //    //mando 4 en forma de pago xq debo mandar algo y seria manso bardo modificar todo, no hace nada el 4
                        //    await Navigation.PushAsync(new AltaClienteDatosPersonales(cliente, flag, 4));

                        //}
                        //else
                        //{
                        if (flag != 10)
                        {
                            await Navigation.PushAsync(new Facturacion.FormaPago(cliente, flag));
                        }
                        else
                        {
                            await Navigation.PushAsync(new AltaClienteDatosPersonales(factura, cliente, flag));
                        }

                        //}

                    });


                };
                await Navigation.PushModalAsync(scanPage);
                indi.IsRunning = false;
                this.Content.IsEnabled = true;
            }


        }

        public void SepararTexto(string txt)
        {
            string[] words = txt.Split('@');
            int cantidad = words.Count();
            string apellido = "";
            string nombre = "";
            string dni = "";
            string fechaNacimiento = "";
            string genero = "";
            switch (cantidad)
            {
                case 8:
                    apellido = words[1];
                    nombre = words[2];
                    genero = words[3];
                    dni = words[4];
                    fechaNacimiento = words[6];
                    break;
                case 9:
                    apellido = words[1];
                    nombre = words[2];
                    genero = words[3];
                    dni = words[4];
                    fechaNacimiento = words[6];
                    break;
                case 15:
                    apellido = words[4];
                    nombre = words[5];
                    dni = words[1];
                    fechaNacimiento = words[7];
                    genero = words[8];

                    break;
                case 17:
                    apellido = words[4];
                    nombre = words[5];
                    dni = words[1];
                    fechaNacimiento = words[7];
                    genero = words[8];

                    break;
                default:
                    DisplayAlert("","DNI no soportado, por favor comuníquese con administración","ok");
                    Navigation.PopToRootAsync(true);
                    break;
            }
            DateTime fechacl = DateTime.ParseExact(fechaNacimiento, "dd/MM/yyyy", null);
            cliente.nombreCliente = nombre + " " + apellido;

            WS.WSCliente wsCliente = new WS.WSCliente();
            dni = wsCliente.SepararDocumento(dni);
            wsCliente.Dispose();

            cliente.nroDocumento = dni.Trim();
            cliente.fechaNacimiento = fechacl;
            cliente.genero = genero;



        }
    }
}