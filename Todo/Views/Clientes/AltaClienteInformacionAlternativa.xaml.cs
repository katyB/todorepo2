﻿using System;
using System.Collections.Generic;
using Todo.Models;
using Todo.Views.Facturacion;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;
using Rg.Plugins.Popup.Extensions;
using Todo.ViewsModels;
using System.Linq;

namespace Todo.Views.Clientes

{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaClienteInformacionAlternativa : ContentPage
	{
        List<Departamento> listaDepartamentos = new List<Departamento>();
        List<Localidad> listaLocalidades = new List<Localidad>();
        ClienteApp cliente = new ClienteApp();
        VMFactura factura2 = new VMFactura();
        VMAutorizacionCliente consultarEstadoCliente;
        int flag = 0;
        int formaPago;
        int dniConyuge = 0;
        int dniOtro = 0;
        bool puedeEntrar;

        public AltaClienteInformacionAlternativa(ClienteApp cl, int fl, int fp)
        {
            cliente = cl;
            flag = fl;
            formaPago = fp;
            InitializeComponent();

        }

        public AltaClienteInformacionAlternativa(VMFactura f, ClienteApp cl, int fl)
        {
            cliente = cl;
            flag = fl;
            factura2 = f;
            InitializeComponent();

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            puedeEntrar = true;
            this.Content.IsEnabled = true;
            indi.IsRunning = false;
            var lista = await App.Database.GetDepartamentoAsync();
            listaDepartamentos = lista;

            foreach (var item in listaDepartamentos)
            {
                pickerDepartamento.Items.Add(item.nombre);
            }

        }

        public void mensaje()
        {
            MessagingCenter.Subscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado", (page, autorizacion) =>
            {

                MessagingCenter.Unsubscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado");

                if (consultarEstadoCliente.tieneConexion)
                {
                    cliente.montoDisponible = consultarEstadoCliente.montoDisponible;
                    cliente.montoTotalMaximo = consultarEstadoCliente.montoTotalMaximoCliente;

                    CrearFactura();
                }
                else
                {
                    CalcularMonto();
                }
            });
        }

        public async void cambioDniConyugue(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(entryNroDocConyugue.Text))
            {
                if (int.TryParse(entryNroDocConyugue.Text, out int dni))
                {
                    dniConyuge = dni;
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor correcto", "Ok");
                    if (dniConyuge >0)
                    {
                        entryNroDocConyugue.Text = dniConyuge.ToString();
                    }
                    else
                    {
                        entryNroDocConyugue.Text = "";
                    }

                }
            }

        }

        public async void cambioDniOtro(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(entryNroDocOtro.Text))
            {
                if (int.TryParse(entryNroDocOtro.Text, out int dni))
                {
                    dniOtro = dni;
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor correcto", "Ok");
                    if (dniOtro>0)
                    {
                        entryNroDocOtro.Text = dniOtro.ToString();

                    }
                    else
                    {
                        entryNroDocOtro.Text = "";

                    }
                }
            }

        }

        public async void Confirmar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                indi.IsRunning = true;
                if (!string.IsNullOrWhiteSpace(entrydomicilioAlternativo.Text))
                {
                    if (pickerDepartamento.SelectedItem != null)
                    {
                        if (pickerLocalidad.SelectedItem != null)
                        {
                            VerificarConyuge();
                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;
                            puedeEntrar = true;

                            await DisplayAlert("", "Seleccione Localidad alternativo", "Ok");
                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Seleccione Departamento alternativo", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;

                    await DisplayAlert("", "Ingrese Domicilio alternativo", "Ok");
                }
            }

        }

        public async void VerificarConyuge()
        {
            if (string.IsNullOrWhiteSpace(entryNombreYApellidoConyugue.Text) &&
                string.IsNullOrWhiteSpace(entryNroDocConyugue.Text) &&
                string.IsNullOrWhiteSpace(entryEmpresaConyugue.Text) &&
                string.IsNullOrWhiteSpace(entryIngresoConyugue.Text) &&
                string.IsNullOrWhiteSpace(entryAntiguedadConyugue.Text))
            {
                VerificarOtro();
            }
            else
            {

                if (!string.IsNullOrWhiteSpace(entryNombreYApellidoConyugue.Text))
                {

                    if (!string.IsNullOrWhiteSpace(entryNroDocConyugue.Text))
                    {

                        if (!string.IsNullOrWhiteSpace(entryEmpresaConyugue.Text))
                        {

                            if (!string.IsNullOrWhiteSpace(entryIngresoConyugue.Text))
                            {

                                if (!string.IsNullOrWhiteSpace(entryAntiguedadConyugue.Text))
                                {
                                    VerificarOtro();
                                }
                                else
                                {
                                    this.Content.IsEnabled = true;
                                    indi.IsRunning = false;
                                    puedeEntrar = true;

                                    await DisplayAlert("", "Introduzca Antigüedad del Conyugue", "Ok");
                                }
                            }
                            else
                            {
                                this.Content.IsEnabled = true;
                                indi.IsRunning = false;
                                puedeEntrar = true;

                                await DisplayAlert("", "Introduzca Ingreso del Conyugue", "Ok");
                            }
                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;
                            puedeEntrar = true;

                            await DisplayAlert("", "Introduzca Empresa del Conyugue", "Ok");
                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Introduzca DNI del Conyugue", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;

                    await DisplayAlert("", "Introduzca Nombre y Apellido del Conyugue", "Ok");
                }
            }
        }

        public async void DepartamentoCambia(object sender, EventArgs e)
        {
            //metodo para obtner las locadlidades por Departamento
            string departamento = pickerDepartamento.SelectedItem.ToString();
            int idDepartamento = 0;

            foreach (var item in listaDepartamentos)
            {
                if (departamento == item.nombre)
                {
                    idDepartamento = item.idDepartamento;
                }
            }

            listaLocalidades = await App.Database.GetLocalidadesPorDepartamento(idDepartamento);
            pickerLocalidad.Items.Clear();

            foreach (var item in listaLocalidades)
            {
                pickerLocalidad.Items.Add(item.nombre);
            }

        }

        public async void VerificarOtro()
        {
            if (string.IsNullOrWhiteSpace(entryNombreYApellidoOtro.Text) &&
                string.IsNullOrWhiteSpace(entryNroDocOtro.Text) &&
                string.IsNullOrWhiteSpace(entryEmpresaOtro.Text) &&
                string.IsNullOrWhiteSpace(entryIngresoOtro.Text) &&
                string.IsNullOrWhiteSpace(entryVinculoFamiliarOtro.Text))
            {
                GuardarDatos();
            }
            else
            {

                if (!string.IsNullOrWhiteSpace(entryNombreYApellidoOtro.Text))
                {

                    if (!string.IsNullOrWhiteSpace(entryNroDocOtro.Text))
                    {

                        if (!string.IsNullOrWhiteSpace(entryEmpresaOtro.Text))
                        {

                            if (!string.IsNullOrWhiteSpace(entryIngresoOtro.Text))
                            {

                                if (!string.IsNullOrWhiteSpace(entryVinculoFamiliarOtro.Text))
                                {
                                    GuardarDatos();
                                }
                                else
                                {
                                    this.Content.IsEnabled = true;
                                    indi.IsRunning = false;
                                    puedeEntrar = true;

                                    await DisplayAlert("", "Introduzca Víncula Familiar", "Ok");
                                }
                            }
                            else
                            {
                                this.Content.IsEnabled = true;
                                indi.IsRunning = false;
                                puedeEntrar = true;

                                await DisplayAlert("", "Introduzca Ingreso de la persona", "Ok");
                            }
                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;
                            puedeEntrar = true;

                            await DisplayAlert("", "Introduzca Empresa de la persona", "Ok");
                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Introduzca DNI de la persona", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;

                    await DisplayAlert("", "Introduzca Nombre y Apellido de la persona", "Ok");
                }
            }

        }

        public async void CrearFactura()
        {
            // envio el  cliente
            var profiles = Connectivity.ConnectionProfiles;

            if (cliente.nuevoCliente)
            {
                cliente.codigoCliente = "Nuevo0000";
                cliente.nuevoApp = true;

            }

            await App.Database.SaveIClienteAsync(cliente);
            //Flag = 10 es el simulador de ventas, que la factura se guarda en este momento
            if (flag != 10)
            {
                //guardo factura parcial con datos cliente
                var factura = await App.Database.GetFacturaPorDniYestado(cliente.nroDocumento.Trim());

                if (factura == null)
                {
                    factura = new FacturaApp();
                }

                factura.nroCliente = cliente.codigoCliente;
                factura.nombreCliente = cliente.nombreCliente;
                factura.domicilioCliente = cliente.domicilioCliente;
                factura.montoDisponibleCliente = cliente.montoDisponible;
                factura.montoTotalMaximoCliente = cliente.montoTotalMaximo;
                factura.dniCliente = cliente.nroDocumento.Trim();
                factura.idEstado = 14;
                factura.sincronizado = false;
                factura.nombreImagenFactura = "Amarillo.png";
                factura.nombreEstado = "Cliente Ingresado";
                factura.fechaFactura = DateTime.Now;
                factura.nuevoCliente = cliente.nuevoCliente;
                await App.Database.SaveIFacturaAsync(factura);
            }
            else
            {
                factura2.nroCliente = cliente.codigoCliente;
                factura2.nombreCliente = cliente.nombreCliente;
                factura2.domicilioCliente = cliente.domicilioCliente;
                factura2.montoDisponibleCliente = cliente.montoDisponible;
                factura2.montoTotalMaximoCliente = cliente.montoTotalMaximo;
                factura2.nuevoCliente = cliente.nuevoCliente;
                if (int.TryParse(cliente.nroDocumento, out int dni))
                {
                    factura2.dniCliente = dni;
                }

            }

            if (flag != 10)
            {
                await this.Navigation.PushAsync(new AltaClienteResumen(cliente, flag, formaPago));
            }
            else
            {
                GuardarFactura();
            }
        }

        public async void CalcularMonto()
        {
            //sumatoria de todos los ingresos

            var sumatoria = cliente.ingresoMensual + cliente.ingresoOtro + cliente.ingresoConyogue + cliente.ingresoComplementarios;

            //busco el monto maximo validado 

            var montoMaximo = await App.Database.GetMontoMaximoAsync();
            decimal porcentaje = 0;
            decimal monto = 0;
            if (montoMaximo != null)
            {
                decimal p = montoMaximo.porcentajeDescuento;
                porcentaje = (p / 100);
            }
            if (porcentaje != 0)
            {
                monto = sumatoria * porcentaje;

            }
            else
            {
                monto = sumatoria * 0.10m;
            }

            if (monto <= cliente.montoCuotaAutorizadoVeraz)
            {
                cliente.montoDisponible = (int)monto;
            }
            else
            {
                cliente.montoDisponible = cliente.montoCuotaAutorizadoVeraz;
            }
            cliente.montoTotalMaximo = cliente.montoTotalAutorizadoVeraz;
            CrearFactura();
        }

        public async void GuardarDatos()
        {

            cliente.domicilioAlternativo = entrydomicilioAlternativo.Text;
            if (pickerDepartamento.SelectedItem != null)
            {
                cliente.departamentoAlternativo = pickerDepartamento.SelectedItem.ToString();
            }

            if (!string.IsNullOrWhiteSpace(cliente.departamentoAlternativo))
            {
                foreach (var item in listaDepartamentos)
                {
                    if (cliente.departamentoAlternativo == item.nombre)
                    {
                        cliente.idDepartamentoAlternativo = item.id;
                    }
                }
            }

            if (pickerLocalidad.SelectedItem != null)
            {
                cliente.localidadAlternativa = pickerLocalidad.SelectedItem.ToString();
            }

            if (!string.IsNullOrWhiteSpace(cliente.localidadAlternativa))
            {
                foreach (var item in listaLocalidades)
                {
                    if (cliente.localidadAlternativa == item.nombre)
                    {
                        cliente.idLocalidadAlternativa = item.id;
                    }
                }
            }


            //cliente.nroDocumento = "27412889";
            WSCliente wsCliente = new WSCliente();
            cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
            wsCliente.Dispose();

            cliente.nombreConyugue = entryNombreYApellidoConyugue.Text;
            cliente.dniConyugue = entryNroDocConyugue.Text;
            cliente.empresaConyugue = entryEmpresaConyugue.Text;
            if (!string.IsNullOrWhiteSpace(entryIngresoConyugue.Text))
            {
                cliente.ingresoConyogue = Int32.Parse(entryIngresoConyugue.Text);

            }
            else
            {
                cliente.ingresoConyogue = 0;
            }
            cliente.antiguedadConyugue = entryAntiguedadConyugue.Text;
            cliente.nombreOtro = entryNombreYApellidoOtro.Text;
            cliente.dniOtro = entryNroDocOtro.Text;
            cliente.empresaOtro = entryEmpresaOtro.Text;
            if (!string.IsNullOrWhiteSpace(entryIngresoOtro.Text))
            {
                cliente.ingresoOtro = Int32.Parse(entryIngresoOtro.Text);

            }
            else
            {
                cliente.ingresoOtro = 0;
            }
            cliente.vinculoFamiliarOtro = entryVinculoFamiliarOtro.Text;
            cliente.idEstado = 7;
            cliente.nombreEstado = "Derivado a Administración";
            cliente.imagenEstado = "DerivadoAdmin.png";
            cliente.sincronizado = false;


            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                mensaje();
                WSCliente clientes = new WSCliente();
                var respuesta = await clientes.AltaUnCliente(cliente);

                if (respuesta != null)
                {
                    consultarEstadoCliente = respuesta;
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await Navigation.PushPopupAsync(new PopUpAutorizacion(respuesta, 0));
                }
                else
                {

                    CalcularMonto();
                    //if (flag != 10)
                    //{
                    //    await this.Navigation.PushAsync(new AltaClienteResumen(cliente, flag, formaPago));

                    //}
                    //else
                    //{
                    //    GuardarFactura();
                    //}
                }


            }
            else
            {

                CalcularMonto();
                //if (flag != 10)
                //{
                //    await this.Navigation.PushAsync(new AltaClienteResumen(cliente, flag, formaPago));

                //}
                //else
                //{
                //    GuardarFactura();
                //}
            }




            


        }

        public async void GuardarFactura()
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            try
            {


                if (factura2.nuevoCliente)
                {
                    foreach (var item in factura2.listaProductos)
                    {
                        if(item.cantCuota > 6)
                        {
                            //Aca debe enviar a la vista AltaFacturaResumenProduct

                            await DisplayAlert("", "Clientes nuevos no están autorizados para comprar en más de seis cuotas.", "ok");

                            await Navigation.PopToRootAsync();
                        }
                    }
                }


                var hayFinanciacion = false;
                var PrimerCuota = 0m;
                if (factura2.listaFacturaCuota.Count > 0)
                {
                    PrimerCuota = factura2.listaFacturaCuota[1].monto;
                    hayFinanciacion = true;
                }
                //cambiar por varibale de la api 

                var montoCliente = factura2.montoDisponibleCliente * (decimal)1.20;


                if (montoCliente >= PrimerCuota || hayFinanciacion == false)
                {


                    var monto = factura2.montoDisponibleCliente;

                    //listado de talonarios
                    var listadoTalonario = await App.Database.GetTalonariosAsync();

                    

                    if (listadoTalonario.Count > 0)
                    {
                        factura2.fechaFactura = DateTime.Now;
                        factura2.fechaFacturaMostrar = factura2.fechaFactura.ToString("dd/MM/yyyy");
                        factura2.horaFacturaMostrar = factura2.fechaFactura.ToString("HH:mm");

                        //aca va el gaurdado de datos

                        FacturaApp f = new FacturaApp
                        {
                            fechaFactura = factura2.fechaFactura,
                            fechaFacturaMostrar = factura2.fechaFacturaMostrar,
                            horaFacturaMostrar = factura2.horaFacturaMostrar,
                            nroFactura = factura2.nroFactura,
                            idFactura = factura2.idFactura,
                            idEstado = 13,
                            nroCliente = factura2.nroCliente,
                            sincronizado = false,
                            nombreImagenFactura = "Ingresado.png",
                            nombreEstado = "Ingresado",
                            nombreCliente = factura2.nombreCliente,
                            domicilioCliente = factura2.domicilioCliente,
                            dniCliente = factura2.dniCliente.ToString().Trim(),
                            montoFactura = factura2.montoFactura,
                            financiacion = factura2.financiacion,
                            anticipo = factura2.anticipo,
                            totalDebito = factura2.totalDebito,
                            totalCredito = factura2.totalCredito,
                            totalContado = factura2.totalContado,
                            formaPago = factura2.FormaPago,
                            revisionAdm = true,
                            observacion = factura2.observacion,
                            nuevoCliente = factura2.nuevoCliente
                        };
                        var current = Connectivity.NetworkAccess;
                        if (current == NetworkAccess.Internet)
                        {
                            f.revisionAdm = false;
                        }

                        var talonario1 = listadoTalonario.FirstOrDefault();
                        talonario1.facturado = true;
                        DateTime fecha = DateTime.Now;
                        talonario1.fecha = fecha;
                        factura2.nroFactura = talonario1.nroFactura;
                        factura2.idFactura = talonario1.idFactura;
                        factura2.nombreTipoFactura = talonario1.nombreTipoFactura; //nombre del tipo factura
                        f.nroFactura = talonario1.nroFactura;
                        f.idFactura = talonario1.idFactura;
                        f.nombreTipoFactura = talonario1.nombreTipoFactura;
                        await App.Database.SaveITalonarioAsync(talonario1);

                        //modificamos el cliente que facturo


                        string dni = factura2.dniCliente.ToString().Trim();
                        ClienteApp cl = await App.Database.GetClientePorDni(dni);
                        if (cl != null)
                        {
                            cl.idEstado = 7;
                            cl.imagenEstado = "DerivadoAdmin.png";
                            cl.nombreEstado = "Derivado a Administración";

                            var montoFactura = f.montoFactura;

                            if (hayFinanciacion)
                            {
                                decimal totalFinanciado = 0m;
                                cl.montoDisponible = (cl.montoDisponible - factura2.listaFacturaCuota[1].monto);
                                foreach (var item in factura2.listaFacturaCuota)
                                {
                                    totalFinanciado += item.monto;
                                }

                                f.totalFinanciado = (int)totalFinanciado;
                            }

                            await App.Database.SaveIClienteAsync(cl);
                        }



                        //Put cliente modificado
                        var profiles = Connectivity.ConnectionProfiles;
                        if (current == NetworkAccess.Internet)
                        {
                            WSCliente clientes = new WSCliente();
                            var respuesta = await clientes.ModificarUnCliente(cl);

                        }
                        else
                        {
                            var respuesta = "Cliente modificado, necesita autorización";
                        }
                        await App.Database.SaveIFacturaAsync(f);



                        var fTemporal = await App.Database.GetFacturaPorDniYestado(factura2.dniCliente.ToString().Trim());

                        if (fTemporal != null)
                        {
                            await App.Database.DeleteUnaFacturasync(fTemporal);
                        }

                        foreach (var item in factura2.listaProductos)
                        {
                            //Guardo Factura Productos
                            FacturaProducto fp = new FacturaProducto
                            {
                                idFactura = f.idFactura,
                                nroFactura = f.nroFactura,
                                idProducto = item.idProducto,
                                cantidadComprada = item.cantidadComprada,
                                precio = item.precioUnitario,
                                idFormaPago = item.idFormaPago,
                                nombreProducto = item.nombreProducto,
                                precioTotal = item.precioTotal

                            };
                            await App.Database.SaveIFacturaProductoAsync(fp);


                            Models.Producto p = await App.Database.GetProductoAsync(item.idProducto);
                            p.saldo = p.saldo - item.cantidadComprada;
                            await App.Database.SaveIProductoAsync(p);

                        }



                        foreach (var item in factura2.listaFacturaCuota)
                        {
                            //Guardo Factura Cuota
                            FacturaCuota fc = new FacturaCuota
                            {
                                idFactura = f.idFactura,
                                nroFactura = f.nroFactura,
                                fechaVto = item.fechaVto,
                                monto = item.monto,
                                nombreCuota = item.nombreCuota,
                                sincronizado = false,
                            };

                            await App.Database.SaveIFacturaCuotaAsync(fc);
                        }

                        if (current == NetworkAccess.Internet)
                        {
                            WSCliente clientes = new WSCliente();
                            WSFactura factura = new WSFactura();
                            await factura.EnviarFctura();
                        }


                        await this.Navigation.PushAsync(new AltaFacturaImprimir(factura2, flag));
                        indi.IsRunning = false;
                        this.Content.IsEnabled = true;

                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Monto disponible superado", "Ok");
                    //await Navigation.PopToRootAsync();

                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.InsertPageBefore(new ListaFacturas(), this);
                    this.Navigation.InsertPageBefore(new AltaFacturaProductos(factura2, 11, 2), this);
                    this.Navigation.InsertPageBefore(new AltaFacturaResumenProductos(factura2, 11), this);
                    await Navigation.PopAsync();


                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "101-ConfirmarFacturacion";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                puedeEntrar = true;

            }
        }

        private async void CambiaIngresoConyugue(object sender, TextChangedEventArgs e)
        {
            if (entryIngresoConyugue.Text != "")
            {
                if (int.TryParse(entryIngresoConyugue.Text, out int ingConyugue))
                {
                    if (ingConyugue < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        entryIngresoConyugue.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    entryIngresoConyugue.Text = "";
                }
            }
        }

        private async void CambiaIngresoOtro(object sender, TextChangedEventArgs e)
        {
            if (entryIngresoOtro.Text != "")
            {
                if (int.TryParse(entryIngresoOtro.Text, out int ingOtro))
                {
                    if (ingOtro < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        entryIngresoOtro.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    entryIngresoOtro.Text = "";
                }
            }
        }

    }
}