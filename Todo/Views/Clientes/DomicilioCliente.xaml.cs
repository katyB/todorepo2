﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.Views.Facturacion;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Clientes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DomicilioCliente : ContentPage
    {
        List<Departamento> listaDepartamentos = new List<Departamento>();
        List<Localidad> listaLocalidades = new List<Localidad>();
        ClienteApp cliente = new ClienteApp();
        int flag;
        bool primeraVez;
        int formaPago;
        bool puedeEntrar;
        public DomicilioCliente(ClienteApp cl, int fl, int fp)
        {
            flag = fl;
            primeraVez = true;
            formaPago = fp;
            cliente = cl;
            InitializeComponent();
            CargarDatos();
        }

        public async void CargarDatos()
        {
            listaDepartamentos = await App.Database.GetDepartamentoAsync();

            foreach (var item in listaDepartamentos)
            {
                pickerDepartamento.Items.Add(item.nombre);
            }

            entryNombreYApellido.Text = cliente.nombreCliente;

            WSCliente wsCliente = new WSCliente();
            cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
            wsCliente.Dispose();

            entryNroDoc.Text = cliente.nroDocumento;
            entryfechaNacimiento.Text = cliente.fechaNacimiento.ToString("dd/MM/yyyy");
            //genero.Text = cliente.genero;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            puedeEntrar = true;
            if (primeraVez == false)
            {
                indi.IsRunning = false;
            }
            primeraVez = false;

        }

        public async void DepartamentoCambia(object sender, EventArgs e)
        {
            //metodo para obtner las locadlidades por Departamento
            string departamento = pickerDepartamento.SelectedItem.ToString();
            int idDepartamento = 0;

            foreach (var item in listaDepartamentos)
            {
                if (departamento == item.nombre)
                {
                    idDepartamento = item.idDepartamento;
                }
            }

            listaLocalidades = await App.Database.GetLocalidadesPorDepartamento(idDepartamento);
            pickerLocalidad.Items.Clear();

            foreach (var item in listaLocalidades)
            {
                pickerLocalidad.Items.Add(item.nombre);
            }

        }

        public async void Continuar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                indi.IsRunning = true;

                if (!string.IsNullOrWhiteSpace(entryDomicilio.Text))
                {
                    if (pickerDepartamento.SelectedItem != null)
                    {
                        if (pickerLocalidad.SelectedItem != null)
                        {
                            var controlGPS = false;

                            try
                            {
                                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                                var location = await Geolocation.GetLocationAsync(request);

                                if (location != null)
                                {
                                    cliente.latitud = location.Latitude;
                                    cliente.longitud = location.Longitude;
                                }
                                controlGPS = true;
                            }
                            catch (FeatureNotSupportedException fnsEx)
                            {
                                this.Content.IsEnabled = true;
                                indi.IsRunning = false;
                                puedeEntrar = true;

                                await DisplayAlert("", "GPS no soportado, no se guardaran las coordenadas", "Ok");
                                controlGPS = true;
                            }

                            catch (FeatureNotEnabledException fneEx)
                            {
                                this.Content.IsEnabled = true;
                                puedeEntrar = true;

                                indi.IsRunning = false;

                                await DisplayAlert("", "GPS desactivado, por favor encienda el gps", "Ok");
                                controlGPS = false;
                            }
                            catch (PermissionException pEx)
                            {
                                this.Content.IsEnabled = true;
                                puedeEntrar = true;

                                indi.IsRunning = false;

                                await DisplayAlert("", "Acepte los permisos de GPS para guardar las coordenadas", "Ok");
                                controlGPS = false;
                            }
                            catch (Exception ex)
                            {
                                this.Content.IsEnabled = true;
                                puedeEntrar = true;

                                indi.IsRunning = false;

                                await DisplayAlert("", "No se pudo obtener las coordenadas", "Ok");
                                controlGPS = false;
                            }

                            if (controlGPS)
                            {
                                cliente.domicilioCliente = entryDomicilio.Text;
                                if (pickerDepartamento.SelectedItem != null)
                                {
                                    cliente.nombreDepartamento = pickerDepartamento.SelectedItem.ToString();
                                    if (cliente.nombreDepartamento != null)
                                    {
                                        foreach (var item in listaDepartamentos)
                                        {
                                            if (cliente.nombreDepartamento == item.nombre)
                                            {
                                                cliente.idDepartamento = item.idDepartamento;
                                            }
                                        }
                                    }
                                }
                                if (pickerLocalidad.SelectedItem != null)
                                {
                                    cliente.localidadCliente = pickerLocalidad.SelectedItem.ToString();
                                    if (cliente.localidadCliente != null)
                                    {
                                        foreach (var item in listaLocalidades)
                                        {
                                            if (cliente.localidadCliente == item.nombre)
                                            {
                                                cliente.idLocalidad = item.idLocalidad;
                                            }
                                        }
                                    }
                                }

                                cliente.idEstado = 7;
                                cliente.nombreEstado = "Derivado a Administración";
                                cliente.imagenEstado = "DerivadoAdmin.png";
                                cliente.sincronizado = false;

                                var current = Connectivity.NetworkAccess;
                                var profiles = Connectivity.ConnectionProfiles;

                                if (cliente.nuevoCliente)
                                {

                                    cliente.codigoCliente = "Nuevo0000";
                                    cliente.nuevoApp = true;

                                }

                                await App.Database.SaveIClienteAsync(cliente);


                                if (current == NetworkAccess.Internet)
                                {
                                    WSCliente clientes = new WSCliente();
                                    var respuesta = await clientes.AltaUnCliente(cliente);
                                }

                                this.Content.IsEnabled = true;
                                indi.IsRunning = false;
                                await Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));

                            }
                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;
                            puedeEntrar = true;

                            await DisplayAlert("", "Ingrese Localidad", "Ok");
                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Ingrese Departamento", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;
                    await DisplayAlert("", "Ingrese Domicilio", "Ok");
                }
            }
 
        }
    }
}