﻿using System;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Clientes
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaClienteIngresos : ContentPage
	{
        ClienteApp cliente = new ClienteApp();
        VMFactura factura = new VMFactura();
        int flag = 0;
        int flag2 = 0;
        int formaPago;
        bool puedeEntrar;

        public AltaClienteIngresos(ClienteApp cl, int fl, int fp)
        {
            cliente = cl;
            InitializeComponent();
            flag2 = fl;
            formaPago = fp;
        }

        public AltaClienteIngresos(VMFactura f, ClienteApp cl, int fl)
        {
            cliente = cl;
            factura = f;
            InitializeComponent();
            flag2 = fl;
        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        public void alTocarChange(object sender, EventArgs e)
        {
            if (flag == 0)
            {
                flag = 1;
                frameIngresos.IsVisible = true;
            }
            else
            {
                if (flag == 1)
                {
                    flag = 0;
                    frameIngresos.IsVisible = false;
                    entryIngresoAlternativo.Text = "";
                    entryActividad2.Text = "";

                }
            }
        }

        public async void GuardarDatos()
        {

            cliente.actividad = entryActividad.Text;
            cliente.empresa = entryEmpresa.Text;
            if (!string.IsNullOrWhiteSpace(entryIngreso.Text))
            {
                cliente.ingresoMensual = Int32.Parse(entryIngreso.Text);

            }
            else
            {
                cliente.ingresoMensual = 0;
            }
            cliente.domicilioEmpresa = entrydomicilio.Text;
            cliente.Antiguedad = entryAntiguedad.Text;
            cliente.telefonoEmpresa = entryTelEmpresa.Text;


            if (flag == 1)
            {

                cliente.actividad2 = entryActividad2.Text;
                if (!string.IsNullOrWhiteSpace(entryIngresoAlternativo.Text))
                {
                    cliente.ingresoComplementarios = Int32.Parse(entryIngresoAlternativo.Text);

                }
                else
                {
                    cliente.ingresoComplementarios = 0;
                }
            }

            if(flag2 != 10)
            {
                await this.Navigation.PushAsync(new AltaClienteInformacionAlternativa(cliente, flag2, formaPago));

            }
            else
            {
                await this.Navigation.PushAsync(new AltaClienteInformacionAlternativa(factura, cliente, flag2));
            }
        }

        public async void Continuar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                if (!string.IsNullOrWhiteSpace(entryActividad.Text))
                {
                    if (!string.IsNullOrWhiteSpace(entryEmpresa.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(entryIngreso.Text))
                        {
                            if (check.IsChecked)
                            {
                                if (!string.IsNullOrWhiteSpace(entryIngresoAlternativo.Text))
                                {
                                    if (!string.IsNullOrWhiteSpace(entryActividad2.Text))
                                    {
                                        GuardarDatos();
                                    }
                                    else
                                    {
                                        puedeEntrar = true;

                                        await DisplayAlert("", "Introduzca Actividad Complementaria", "Ok");
                                    }
                                }
                                else
                                {
                                    puedeEntrar = true;

                                    await DisplayAlert("", "Introduzca Ingreso Complementario ", "Ok");
                                }
                            }
                            else
                            {
                                GuardarDatos();
                            }

                        }
                        else
                        {
                            puedeEntrar = true;

                            await DisplayAlert("", "Introduzca Ingresos mensuales", "Ok");
                        }
                    }
                    else
                    {
                        puedeEntrar = true;

                        await DisplayAlert("", "Introduzca Nombre de la Empresa", "Ok");
                    }
                }
                else
                {
                    puedeEntrar = true;

                    await DisplayAlert("", "Introduzca Actividad Principal", "Ok");
                }
            }
 

        }

        private async void CambiaIngresoMensual(object sender, TextChangedEventArgs e)
        {
            if (entryIngreso.Text != "")
            {
                if (int.TryParse(entryIngreso.Text, out int ingMensual))
                {
                    if (ingMensual < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        entryIngreso.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    entryIngreso.Text = "";
                }
            }
        }

        private async void CambiaIngresoComplementario(object sender, TextChangedEventArgs e)
        {
            if (entryIngresoAlternativo.Text != "")
            {
                if (int.TryParse(entryIngresoAlternativo.Text, out int ingComplementario))
                {
                    if (ingComplementario < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        entryIngresoAlternativo.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    entryIngresoAlternativo.Text = "";
                }
            }
        }

    }
}