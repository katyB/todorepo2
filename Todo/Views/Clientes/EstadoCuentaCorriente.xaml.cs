﻿using Forms9Patch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Clientes
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EstadoCuentaCorriente : ContentPage
	{
        ClienteApp cliente = new ClienteApp();
        List<VMEstadoCtaCteCliente> listaEstadoCtaCte = new List<VMEstadoCtaCteCliente>();
        int flag;
        bool puedeEntrar;

        public EstadoCuentaCorriente (ClienteApp cl, int f)
		{
			InitializeComponent ();
            cliente = cl;
            CargarDatos();
            flag = f;

        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        public async void CargarDatos()
        {
            WSCliente clientes = new WSCliente();
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                var idCliente = cliente.idCliente;
                if(idCliente != 0)
                {
                    listaEstadoCtaCte = await clientes.GetClienteCtacte<WSCliente>(idCliente);

                    nombreCliente.Text = cliente.nombreCliente;
                    codigoCliente.Text = cliente.codigoCliente;
                    direccionCliente.Text = cliente.domicilioCliente;

                    var PrimerCuota = listaEstadoCtaCte.FirstOrDefault();

                    if (PrimerCuota != null)
                    {

                        var size = Device.GetNamedSize(NamedSize.Medium, typeof(Xamarin.Forms.Label));
                        gridCuotas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        gridCuotas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        gridCuotas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                        var Titulocuota = new Xamarin.Forms.Label { Text = "Monto", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                        var TituloFechaVencimiento = new Xamarin.Forms.Label { Text = "Vencimiento", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                        gridCuotas.Children.Add(Titulocuota, 0, 0);
                        gridCuotas.Children.Add(TituloFechaVencimiento, 1, 0);

                        for (int i = 0; i < listaEstadoCtaCte.Count; i++)
                        {
                            gridCuotas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                            string monto = listaEstadoCtaCte[i].montoCuota.ToString();
                            string fVenc = listaEstadoCtaCte[i].fechaVencimiento.ToString("dd/MM/yy");

                            var cuota = new Xamarin.Forms.Label { Text = monto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                            var FechaVencimiento = new Xamarin.Forms.Label { Text = fVenc, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

                            gridCuotas.Children.Add(cuota, 0, i + 1);
                            gridCuotas.Children.Add(FechaVencimiento, 1, i + 1);
                        }

                    }
                    else
                    {

                        var size = Device.GetNamedSize(NamedSize.Medium, typeof(Xamarin.Forms.Label));
                        gridCuotas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        gridCuotas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                        var Mensaje = new Xamarin.Forms.Label { Text = "No posee compras anteriores", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                        gridCuotas.Children.Add(Mensaje, 0, 0);
                    }
                }
                else
                {
                    await DisplayAlert("", "Consulta no disponible en este momento.", "ok");
                    if (flag == 0)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        if (flag == 1)
                        {
                            this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                            await this.Navigation.PopAsync();

                        }
                    }
                }
            }
            else
            {
                await DisplayAlert("", "Conectese a internet por favor. Intenete nuevamente en unos minutos.", "ok");
                if (flag == 0)
                {
                    await this.Navigation.PopAsync();
                }
                else
                {
                    if (flag == 1)
                    {
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        await this.Navigation.PopAsync();

                    }
                }
            }



        }

        protected override bool OnBackButtonPressed()
        {
            if (flag == 0)
            {
                this.Navigation.PopAsync();
            }
            else
            {

                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    this.Navigation.PopAsync();

            }

            return true;
        }


        public async void Imprimir(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                DateTime fecha = DateTime.Now;

                Forms9Patch.WebViewPrintEffect.ApplyTo(webView);
                var htmlSource = new HtmlWebViewSource();
                string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
                html += " <table style = 'width: 100 %'>";
                html += "<tr>";
                html += "<td><strong>Fecha: </strong>" + fecha.ToString("dd/MM/yy") + "</td>";
                html += "<td><strong>Hora: </strong>" + fecha.ToString("HH:mm:ss") + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td><strong>Cliente: </strong>" + cliente.nombreCliente + "</td>";
                html += "</tr>";
                html += "</table>";

                var PrimerCuota = listaEstadoCtaCte.FirstOrDefault();

                if (PrimerCuota != null)
                {

                    html += " <table style = 'width: 100 %'>";
                    html += "<tr>";
                    html += "<th>Monto</th>";
                    html += "<th>Vencimiento</th>";
                    html += "</tr>";
                    foreach (var item in listaEstadoCtaCte)
                    {
                        html += "<tr>";
                        html += "<td>$" + item.montoCuota + "</td>";
                        html += "<td>" + item.fechaVencimiento.ToString("dd/MM/yy") + "</td>";
                        html += "</tr>";
                    }

                    html += "</table>";
                }
                else
                {

                    html += " <table style = 'width: 100 %'>";
                    html += "<tr colspan=2>";
                    html += "<td><strong>No posee compras anteriores.</strong></td>";
                    html += "</tr>";

                    html += "</table>";
                }


                htmlSource.Html = html;
                webView.Source = htmlSource;

                var estado = await Permissions.RequestAsync<Permissions.StorageWrite>();

                if (await webView.ToPdfAsync(DateTime.Now.ToString("ddMMyyyyhhmmss")) is ToFileResult pdfResult)
                {

                    var status9 = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();
                    var status10 = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
                    await Share.RequestAsync(new ShareFileRequest
                    {
                        Title = "Archivo Pdf",
                        File = new ShareFile(pdfResult.Result)
                    });

                    this.Content.IsEnabled = true;
                    if (flag == 0)
                    {
                        await this.Navigation.PopAsync();
                    }
                    else
                    {
                        if (flag == 1)
                        {
                            this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                            await this.Navigation.PopAsync();

                        }
                    }

                }
                else
                {
                    this.Content.IsEnabled = true;
                    puedeEntrar = true;
                }
            }
 

        }

    }
}