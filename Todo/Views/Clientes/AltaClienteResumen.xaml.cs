﻿using System;
using Todo.Models;
using Todo.Views.Facturacion;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Clientes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AltaClienteResumen : ContentPage
    {
        ClienteApp cliente = new ClienteApp();
        int flag;
        int formaPago;
        bool puedeEntrar;

        public AltaClienteResumen(ClienteApp cl, int fl, int fp)
        {
            flag = fl;
            formaPago = fp;
            cliente = cl;
            InitializeComponent();
            CargarDatos();
        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }

        public void CargarDatos()
        {
            //cliente.nroDocumento = "27412889";
            WS.WSCliente wsCliente = new WS.WSCliente();
            cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
            wsCliente.Dispose();

            nombre.Text = cliente.nombreCliente;
            dni.Text = "DNI: " + cliente.nroDocumento;
            //fechaNacimiento.Text = cliente.fechaNacimientoMostrar;
            estCivil.Text = cliente.estadoCivil;
            cantHijos.Text = cliente.cantidadHijo;
            Domicilio.Text = cliente.domicilioCliente;
            localidad.Text = cliente.localidadCliente;
            departamento.Text = cliente.nombreDepartamento;
            referencia.Text = cliente.referencia;
            vivienda.Text = cliente.vivienda;
            if (!string.IsNullOrWhiteSpace(cliente.telefonoFijo))
            {
                telefonoFijo.Text = "Fijo: "+cliente.telefonoFijo;
                telefonoFijo.IsVisible = true;
            }
            if (!string.IsNullOrWhiteSpace(cliente.telefono))
            {
                telefonoCel.Text = "Cel: "+cliente.telefono;
                telefonoCel.IsVisible = true;
            }
            if (!string.IsNullOrWhiteSpace(cliente.telFamiliar))
            {
                telefonoFam.Text = "Familiar: "+cliente.telFamiliar;
                telefonoFam.IsVisible = true;
            }
            if(cliente.latitud != 0 && cliente.longitud != 0)
            {
                gps.Text = "Si";
            }
            else
            {
                gps.Text = "No";
            }

            TarjetaCredito.Text = cliente.tarjeta;


            ActividadPrincipal.Text = cliente.actividad;
            Empresa.Text = cliente.empresa;
            ingreso.Text = "$" + cliente.ingresoMensual;
            if (!string.IsNullOrWhiteSpace(cliente.domicilioEmpresa))
            {
                domicilioEmpresa.Text = cliente.domicilioEmpresa;
                labelDomicilioEmpresa.IsVisible = true;
            }

            if (!string.IsNullOrWhiteSpace(cliente.Antiguedad))
            {
                antiguedad.Text = cliente.Antiguedad;
                labelAntiguedad.IsVisible = true;
            }

            if (cliente.ingresoComplementarios > 0)
            {
                ingresoComplementario.Text = "$"+ cliente.ingresoComplementarios;
                SLingresoComplementario.IsVisible = true;
            }
            if (!string.IsNullOrWhiteSpace(cliente.actividad2))
            {
                actividad2.Text = cliente.actividad2;
                slActividad.IsVisible = true;
            }

        }

        public async void Continuar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                //if (flag == 0 || flag == 1 || flag == 2)
                //{
                //    await this.Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));
                //}
                //else
                //{
                //    if (flag == 7 || flag == 4)
                //    {
                //        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                //        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                //        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                //        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                //        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                //        await Navigation.PushAsync(new HomeCliente(cliente,7));
                //    }
                //}

                await this.Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));
            }
 
        }


    }
}