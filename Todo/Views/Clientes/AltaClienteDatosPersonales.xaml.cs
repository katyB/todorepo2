﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using Todo.Models;
using Todo.Views.Facturacion;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;

namespace Todo.Views.Clientes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AltaClienteDatosPersonales : ContentPage
    {
        List<Departamento> listaDepartamentos = new List<Departamento>();
        List<Localidad> listaLocalidades = new List<Localidad>();
        List<EstadoCivil> listaEstadoCivil = new List<EstadoCivil>();
        List<TipoVivienda> listaTipoVivienda = new List<TipoVivienda>();
        List<TarjetaCredito> listaTarjetaCredito = new List<TarjetaCredito>();
        ClienteApp cliente = new ClienteApp();
        //FacturaTemporal facturaTemporal = new FacturaTemporal();
        VMAutorizacionCliente consultarEstadoCliente;
        int cantidadHijos = 0;
        int flag;
        bool primeraVez;
        int formaPago;
        VMFactura factura = new VMFactura();
        bool existe = false;
        bool puedeEntrar;
        public AltaClienteDatosPersonales(ClienteApp cl, int fl, int fp)
        {
            flag = fl;
            primeraVez = true;
            formaPago = fp;
            cliente = cl;
            InitializeComponent();
            CargaEstadoCivil();
            CargarTipoVivienda();
            CargaTarjetaCredito();
            ValidarCliente();


        }

        public AltaClienteDatosPersonales(VMFactura f,ClienteApp cl, int fl)
        {
            flag = fl;
            primeraVez = true;
            cliente = cl;
            factura = f;
            InitializeComponent();
            CargaEstadoCivil();
            CargarTipoVivienda();
            CargaTarjetaCredito();
            ValidarCliente();


        }

        public async void ValidarCliente()
        {
            indi.IsVisible = true;
            indi.IsRunning = true;

            if(flag == 10)
            {
                WSCliente wsCliente = new WSCliente();
                cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
                wsCliente.Dispose();
                var verificarCliente = await App.Database.GetClientePorDni(cliente.nroDocumento);
                if (verificarCliente != null)
                {
                    existe = true;
                    verificarCliente.nombreCliente = cliente.nombreCliente;
                    verificarCliente.nroDocumento = cliente.nroDocumento.Trim();
                    verificarCliente.fechaNacimiento = cliente.fechaNacimiento;
                    verificarCliente.genero = cliente.genero;
                    cliente = verificarCliente;
                    controlarMorosidad();

                }
                else
                {
                    controlarMorosidad();
                }
            }
            else
            {
                //var current = Connectivity.NetworkAccess;


                if (cliente.idEstado != 2)
                {
                    indi.IsRunning = false;
                    //Si no es estado 2 (moroso) cargo los datos no editables.
                    mostraFormulario.IsVisible = true;
                    CargarDatos();
                }
                else
                {
                    indi.IsRunning = false;
                    //Si es moroso lo vuelvo al inicio
                    await this.Navigation.PopToRootAsync();
                }

              
            }

        }

        public async void controlarMorosidad()
        {
            WSCliente clientes = new WSCliente();


            var nroDocuemento = cliente.nroDocumento;
            var genero = cliente.genero;
            var nombre = cliente.nombreCliente;

            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                mensaje();
                consultarEstadoCliente = await clientes.ValidarEstadoCliente(nroDocuemento, genero, nombre);
                if (consultarEstadoCliente != null)
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await Navigation.PushPopupAsync(new PopUpAutorizacion(consultarEstadoCliente, 1));
                }
                else
                {
                    if (existe == false)
                    {
                        cliente.nuevoCliente = true;
                    }
                    var montoMaximo = await App.Database.GetMontoMaximoAsync();
                    cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                    cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Sin acceso a internet, administración deberá aprobar esta solicitud.", "Ok");
                    mostraFormulario.IsVisible = true;
                    CargarDatos();
                }

            }
            else
            {
                if (existe == false)
                {
                    cliente.nuevoCliente = true;
                }
                var montoMaximo = await App.Database.GetMontoMaximoAsync();
                cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Sin acceso a internet, administración deberá aprobar esta solicitud.", "Ok");

                mostraFormulario.IsVisible = true;
                CargarDatos();

            }
        }

        public async void CargarDatos()
        {
                listaDepartamentos = await App.Database.GetDepartamentoAsync();

                foreach (var item in listaDepartamentos)
                {
                    pickerDepartamento.Items.Add(item.nombre);
                }

                entryNombreYApellido.Text = cliente.nombreCliente;

            WSCliente wsCliente = new WSCliente();
            cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
            wsCliente.Dispose();

            entryNroDoc.Text = cliente.nroDocumento;
                entryfechaNacimiento.Text = cliente.fechaNacimiento.ToString("dd/MM/yyyy");
                //genero.Text = cliente.genero;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            puedeEntrar = true;
            if (primeraVez == false)
            {
                indi.IsRunning = false;
            }
            primeraVez = false;

        }

        public void mensaje()
        {
            MessagingCenter.Subscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado", async (page, autorizacion) =>
            {
                MessagingCenter.Unsubscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado");

                if (consultarEstadoCliente.tieneConexion)
                {
                    cliente.idEstado = consultarEstadoCliente.idEstado;
                    cliente.nuevoCliente = consultarEstadoCliente.nuevoCliente;
                    cliente.codigoCliente = consultarEstadoCliente.codigoCliente;
                    cliente.montoCuotaAutorizadoVeraz = consultarEstadoCliente.montoDisponible;
                    cliente.montoTotalAutorizadoVeraz = consultarEstadoCliente.montoTotalMaximoCliente;
                    cliente.idCliente = consultarEstadoCliente.idCliente;
                    if (cliente.idEstado == 2 || cliente.idEstado == 7)
                    {
                        await DisplayAlert("", "Cliente moroso, no se le puede vender financiado", "ok");
                        await Navigation.PopToRootAsync();
                    }
                    else
                    {
                        mostraFormulario.IsVisible = true;
                        CargarDatos();
                    }
                }
                else
                {
                    if (existe == false)
                    {
                        cliente.nuevoCliente = true;
                    }
                    var montoMaximo = await App.Database.GetMontoMaximoAsync();
                    cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                    cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    mostraFormulario.IsVisible = true;
                    CargarDatos();
                }

            });
        }

        public async void cambioCantHijos(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(entryCantHijos.Text))
            {
                if (int.TryParse(entryCantHijos.Text, out int hijos))
                {
                    cantidadHijos = hijos;
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    if(cantidadHijos > 0)
                    {
                        entryCantHijos.Text = cantidadHijos.ToString();
                    }
                    else
                    {
                        entryCantHijos.Text = "";
                    }

                }
            }

        }

        public void CargarTipoVivienda()
        {
            TipoVivienda tv1 = new TipoVivienda
            {
                idTipoVivienda = 1,
                nombreTipoVivienda = "Propietario"
            };
            TipoVivienda tv2 = new TipoVivienda
            {
                idTipoVivienda = 2,
                nombreTipoVivienda = "Inquilino"
            };
            listaTipoVivienda.Add(tv1);
            listaTipoVivienda.Add(tv2);

            foreach (var item in listaTipoVivienda)
            {
                pickerVivienda.Items.Add(item.nombreTipoVivienda);
            }
        }

        public void CargaEstadoCivil()
        {
            EstadoCivil ec1 = new EstadoCivil
            {
                idEstadoCivil = 1,
                nombreEstadoCivil = "Soltero"
            };
            EstadoCivil ec2 = new EstadoCivil
            {
                idEstadoCivil = 2,
                nombreEstadoCivil = "Casado"
            };
            EstadoCivil ec3 = new EstadoCivil
            {
                idEstadoCivil = 3,
                nombreEstadoCivil = "Divorciado"
            };
            EstadoCivil ec4 = new EstadoCivil
            {
                idEstadoCivil = 4,
                nombreEstadoCivil = "Viudo"
            };
            listaEstadoCivil.Add(ec1);
            listaEstadoCivil.Add(ec2);
            listaEstadoCivil.Add(ec3);
            listaEstadoCivil.Add(ec4);

            foreach (var item in listaEstadoCivil)
            {
                pickerEstadoCivil.Items.Add(item.nombreEstadoCivil);
            }
        }

        public void CargaTarjetaCredito()
        {
            TarjetaCredito tj1 = new TarjetaCredito
            {
                idTarjetaCredito = 4,
                nombreTarjetaCredito = "Ninguna"
            };
            TarjetaCredito tj2 = new TarjetaCredito
            {
                idTarjetaCredito = 2,
                nombreTarjetaCredito = "Visa"
            };
            TarjetaCredito tj3 = new TarjetaCredito
            {
                idTarjetaCredito = 1,
                nombreTarjetaCredito = "Master"
            };
            TarjetaCredito tj4 = new TarjetaCredito
            {
                idTarjetaCredito = 3,
                nombreTarjetaCredito = "Naranja"
            };
            TarjetaCredito tj5 = new TarjetaCredito
            {
                idTarjetaCredito = 3,
                nombreTarjetaCredito = "Cabal"
            };
            TarjetaCredito tj6 = new TarjetaCredito
            {
                idTarjetaCredito = 4,
                nombreTarjetaCredito = "Otro"
            };

            listaTarjetaCredito.Add(tj1);
            listaTarjetaCredito.Add(tj2);
            listaTarjetaCredito.Add(tj3);
            listaTarjetaCredito.Add(tj4);
            listaTarjetaCredito.Add(tj5);
            listaTarjetaCredito.Add(tj6);

            foreach (var item in listaTarjetaCredito)
            {
                pickerTarjCred.Items.Add(item.nombreTarjetaCredito);
            }
        }

        public async void DepartamentoCambia(object sender, EventArgs e)
        {
            //metodo para obtner las locadlidades por Departamento
            string departamento = pickerDepartamento.SelectedItem.ToString();
            int idDepartamento = 0;

            foreach (var item in listaDepartamentos)
            {
                if(departamento == item.nombre)
                {
                    idDepartamento = item.idDepartamento;
                }
            }

            listaLocalidades = await App.Database.GetLocalidadesPorDepartamento(idDepartamento);
            pickerLocalidad.Items.Clear();

            foreach (var item in listaLocalidades)
            {
                pickerLocalidad.Items.Add(item.nombre);
            }

        }

        public void TipoViviendaCambia(object sender, EventArgs e)
        {
            if (pickerVivienda.SelectedItem.ToString() == "Propietario")
            {
                slFechasInquilino.IsVisible = false;
                //frameVivienda.HeightRequest = 200;

            }
            else
            {
                if (pickerVivienda.SelectedItem.ToString() == "Inquilino")
                {
                    slFechasInquilino.IsVisible = true;
                    //frameVivienda.HeightRequest = 220;
                }
            }
        }


        public async void Continuar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                indi.IsRunning = true;

                if (pickerEstadoCivil.SelectedItem != null)
                {
                    if (!string.IsNullOrWhiteSpace(entryCantHijos.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(entryDomicilio.Text))
                        {
                            if (pickerDepartamento.SelectedItem != null)
                            {
                                if (pickerLocalidad.SelectedItem != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(entryReferencia.Text))
                                    {
                                        if (!string.IsNullOrWhiteSpace(entryReferenciaVecinal.Text))
                                        {
                                            if (pickerVivienda.SelectedItem != null)
                                            {
                                                if (!string.IsNullOrWhiteSpace(entryTelFijo.Text) || !string.IsNullOrWhiteSpace(entryTelCel.Text) || !string.IsNullOrWhiteSpace(entryTelFam.Text))
                                                {
                                                    if (pickerTarjCred.SelectedItem != null)
                                                    {

                                                        var controlGPS = false;

                                                        try
                                                        {
                                                            var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                                                            var location = await Geolocation.GetLocationAsync(request);

                                                            if (location != null)
                                                            {
                                                                cliente.latitud = location.Latitude;
                                                                cliente.longitud = location.Longitude;
                                                            }
                                                            controlGPS = true;
                                                        }
                                                        catch (FeatureNotSupportedException fnsEx)
                                                        {
                                                            this.Content.IsEnabled = true;
                                                            indi.IsRunning = false;
                                                            puedeEntrar = true;
                                                            await DisplayAlert("", "GPS no soportado, no se guardaran las coordenadas", "Ok");
                                                            controlGPS = true;
                                                        }

                                                        catch (FeatureNotEnabledException fneEx)
                                                        {
                                                            this.Content.IsEnabled = true;

                                                            indi.IsRunning = false;
                                                            puedeEntrar = true;

                                                            await DisplayAlert("", "GPS desactivado, por favor encienda el gps", "Ok");
                                                            controlGPS = false;
                                                        }
                                                        catch (PermissionException pEx)
                                                        {
                                                            this.Content.IsEnabled = true;

                                                            indi.IsRunning = false;
                                                            puedeEntrar = true;

                                                            await DisplayAlert("", "Acepte los permisos de GPS para guardar las coordenadas", "Ok");
                                                            controlGPS = false;
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            this.Content.IsEnabled = true;

                                                            indi.IsRunning = false;
                                                            puedeEntrar = true;

                                                            await DisplayAlert("", "No se pudo obtener las coordenadas", "Ok");
                                                            controlGPS = false;
                                                        }

                                                        if (controlGPS)
                                                        {

                                                            if (pickerEstadoCivil.SelectedItem != null)
                                                            {
                                                                cliente.estadoCivil = pickerEstadoCivil.SelectedItem.ToString();
                                                            }

                                                            cliente.cantidadHijo = entryCantHijos.Text;
                                                            cliente.domicilioCliente = entryDomicilio.Text;
                                                            if (pickerDepartamento.SelectedItem != null)
                                                            {
                                                                cliente.nombreDepartamento = pickerDepartamento.SelectedItem.ToString();
                                                                if (cliente.nombreDepartamento != null)
                                                                {
                                                                    foreach (var item in listaDepartamentos)
                                                                    {
                                                                        if (cliente.nombreDepartamento == item.nombre)
                                                                        {
                                                                            cliente.idDepartamento = item.id;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (pickerLocalidad.SelectedItem != null)
                                                            {
                                                                cliente.localidadCliente = pickerLocalidad.SelectedItem.ToString();
                                                                if (cliente.localidadCliente != null)
                                                                {
                                                                    foreach (var item in listaLocalidades)
                                                                    {
                                                                        if (cliente.localidadCliente == item.nombre)
                                                                        {
                                                                            cliente.idLocalidad = item.id;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            cliente.referencia = entryReferencia.Text;
                                                            cliente.referenciaVecinal = entryReferenciaVecinal.Text;
                                                            if (pickerVivienda.SelectedItem != null)
                                                            {
                                                                cliente.vivienda = pickerVivienda.SelectedItem.ToString();
                                                            }

                                                            cliente.telefonoFijo = entryTelFijo.Text;
                                                            cliente.telefono = entryTelCel.Text;
                                                            cliente.telFamiliar = entryTelFam.Text;
                                                            if (pickerTarjCred.SelectedItem != null)
                                                            {
                                                                cliente.tarjeta = pickerTarjCred.SelectedItem.ToString();
                                                            }
                                                            cliente.fechaDesdeInquilino = entryfechDesde.Date;
                                                            cliente.fechaHastaInquilino = entryfechHasta.Date;

                                                            this.Content.IsEnabled = true;
                                                            indi.IsRunning = false;

                                                            if (flag != 10)
                                                            {
                                                                await this.Navigation.PushAsync(new AltaClienteIngresos(cliente, flag, formaPago));

                                                            }
                                                            else
                                                            {
                                                                await this.Navigation.PushAsync(new AltaClienteIngresos(factura, cliente, flag));
                                                            }
                                                        }

                                                    }

                                                    else
                                                    {
                                                        this.Content.IsEnabled = true;
                                                        indi.IsRunning = false;
                                                        puedeEntrar = true;

                                                        await DisplayAlert("", "Seleccione una tarjeta de crédito.", "Ok");
                                                    }
                                                }
                                                else
                                                {
                                                    this.Content.IsEnabled = true;
                                                    indi.IsRunning = false;
                                                    puedeEntrar = true;

                                                    await DisplayAlert("", "Ingrese algún número de teléfono", "Ok");
                                                }
                                            }
                                            else
                                            {
                                                this.Content.IsEnabled = true;
                                                indi.IsRunning = false;
                                                puedeEntrar = true;

                                                await DisplayAlert("", "Ingrese Tipo de Vivienda", "Ok");
                                            }
                                        }
                                        else
                                        {
                                            this.Content.IsEnabled = true;
                                            indi.IsRunning = false;
                                            puedeEntrar = true;

                                            await DisplayAlert("", "Ingrese Referencia Vecinal", "Ok");
                                        }
                                    }
                                    else
                                    {
                                        this.Content.IsEnabled = true;
                                        indi.IsRunning = false;
                                        puedeEntrar = true;

                                        await DisplayAlert("", "Ingrese Referencia", "Ok");
                                    }
                                }
                                else
                                {
                                    this.Content.IsEnabled = true;
                                    indi.IsRunning = false;
                                    puedeEntrar = true;

                                    await DisplayAlert("", "Ingrese Localidad", "Ok");
                                }
                            }
                            else
                            {
                                this.Content.IsEnabled = true;
                                indi.IsRunning = false;
                                puedeEntrar = true;

                                await DisplayAlert("", "Ingrese Departamento", "Ok");
                            }
                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;
                            puedeEntrar = true;

                            await DisplayAlert("", "Ingrese Domicilio", "Ok");
                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        puedeEntrar = true;

                        await DisplayAlert("", "Ingrese Cantidad de Hijos", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;

                    await DisplayAlert("", "IngreseEstado Civil", "Ok");
                }
            }
 

        }


    }
}