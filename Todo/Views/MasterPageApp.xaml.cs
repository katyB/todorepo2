﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Views.Cobranzas;
using Todo.Views.Facturacion;
using Todo.Views.Reclamo;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace Todo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPageApp : MasterDetailPage
    {
        public MasterPageApp()
        {
            InitializeComponent();
            Init();
        }

        private void OnLabelClicked(object sender, SelectedItemChangedEventArgs e)
        {
            App.Current.Logout();
        }

        private void Init()
        {
            //var nombreUsuario = App.Current.Properties["usuarioLogueado"].ToString();
            var nombreUsuario = Preferences.Get("usuarioLogueado", "");
            this.Title = nombreUsuario;
            nombreMenu.Text = nombreUsuario;

            List<Menu> menu = new List<Menu> {
                new Menu{MenuTitle = "Home", MenuImage = "home.png",idMenu = 1 },
                new Menu{MenuTitle = "Venta", MenuImage = "venta.png",idMenu = 2},
                new Menu{ MenuTitle = "Reclamo", MenuImage = "reclamo.png",idMenu = 3 },
                new Menu{MenuTitle = "Cliente", MenuImage = "clientes.png",idMenu = 4},
                new Menu{MenuTitle = "Cobranza", MenuImage = "cobranzas.png",idMenu = 5}

            };


            ListMenu.ItemsSource = menu;
            ListMenu.HeightRequest = 60 * menu.Count + 30;
            List<Menu> menuSalir = new List<Menu>{
                new Menu { MenuTitle = "Salir", MenuImage = "salir.png", idMenu = 0 }
            };

            salir.ItemsSource = menuSalir;
            salir.HeightRequest = 50;
            Detail = new NavigationPage(new HomeView());
            Detail.BackgroundColor = Color.FromHex("#f8f4fc");
        }

        public class Menu
        {

            public string MenuTitle { get; set; }
            public string MenuImage { get; set; }
            public int idMenu { get; set; }
        }

        private async void ListMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var menu = e.SelectedItem as Menu;
            
            if(menu!= null) {
                switch (menu.idMenu)
                {
                    case 1:
                        ListMenu.SelectedItem = null;
                        IsPresented = false;
                        await (this.Detail as NavigationPage).PushAsync(new HomeView());
                        break;
                    case 2:
                        ListMenu.SelectedItem = null;
                        IsPresented = false;
                        await (this.Detail as NavigationPage).PushAsync(new ListaFacturas());
                        break;
                    case 3:
                        ListMenu.SelectedItem = null;
                        IsPresented = false;
                        await (this.Detail as NavigationPage).PushAsync(new ListaReclamos());
                        break;
                    case 4:
                        ListMenu.SelectedItem = null;
                        IsPresented = false;
                        var listadoClientes = await App.Database.GetClientesAsync();
                        if (listadoClientes.Count > 0)
                        {
                            await (this.Detail as NavigationPage).PushAsync(new BuscarCliente(1));
                        }
                        else
                        {
                            await DisplayAlert("", "No posee clientes disponibles. Por favor, comuníquese con administración", "Ok");
                        }
                        break;
                    case 5:
                        ListMenu.SelectedItem = null;
                        IsPresented = false;
                        await (this.Detail as NavigationPage).PushAsync(new ListaCobranza());
                        break;
                }
            }

        }
    }
}