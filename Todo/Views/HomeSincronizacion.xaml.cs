﻿//using Android.Widget;
using System;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;
using Xamarin.Forms.Xaml;


namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeSincronizacion : ContentPage
	{
		public HomeSincronizacion ()
		{
			InitializeComponent ();
		}

        async void ClickMantenimiento(object sender, EventArgs e)
        {
            Sincronizacion sin = new Sincronizacion();
            var res = sin.btnSincronizarParcial();
            if (res.IsCompleted)
            {
                await DisplayAlert("", "Necesita conexión Wifi para sincronizar", "Ok");
            }
            else
            {
                await DisplayAlert("", "Sincronizando..", "Ok");
            }  
        }

        async void ClickSincronizar(object sender, EventArgs e)
        {
            Sincronizacion sin = new Sincronizacion();
            var  res = sin.btnSincronizarClick();
            if (res.IsCompleted)
            {
                await DisplayAlert("", "Necesita conexión Wifi para sincronizar", "Ok");
            }
            else
            {
                await DisplayAlert("", "Sincronizando..", "Ok");
            }
        }
    }
}