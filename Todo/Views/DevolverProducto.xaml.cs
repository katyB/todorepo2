﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DevolverProducto : ContentPage
	{
        Models.Producto producto = new Models.Producto();
        int cantProductos;

        public DevolverProducto (Models.Producto p)
		{
            producto = p;
            producto.saldo = producto.cantidad;
            InitializeComponent();
            cargarDatos();
        }

        public void cargarDatos()
        {
            nombre.Text = producto.nombreProducto;
            codigo.Text = producto.codigoProducto;
            stock.Text = producto.saldo.ToString(); //lo cambie por saldo. No usams cantidad

            cantidad.Text = "1";
            cantProductos = 1;
            menosBN.IsVisible = true;
            menosC.IsVisible = false;
            if (cantProductos == producto.saldo)
            {
                masC.IsVisible = false;
                masBN.IsVisible = true;
            }
            else
            {
                masC.IsVisible = true;
                masBN.IsVisible = false;
            }

        }

        public void AumentarCantidad(object sender, EventArgs e)
        {
            if (producto.saldo > cantProductos)
            {
                if (cantProductos == 1)
                {
                    menosC.IsVisible = true;
                    menosBN.IsVisible = false;
                }

                cantProductos += 1;
                cantidad.Text = cantProductos.ToString();
                if (cantProductos == producto.saldo)
                {
                    masBN.IsVisible = true;
                    masC.IsVisible = false;
                }
            }

        }

        public void DisminuirCantidad(object sender, EventArgs e)
        {
            if (cantProductos > 1)
            {
                if (cantProductos == producto.saldo)
                {
                    masBN.IsVisible = false;
                    masC.IsVisible = true;
                }
                cantProductos -= 1;
                cantidad.Text = cantProductos.ToString();
                if (cantProductos == 1)
                {
                    menosC.IsVisible = false;
                    menosBN.IsVisible = true;
                }
            }
        }

        public async void DevolverProductos(object sender, EventArgs e)
        {
            producto.cantidadDevuelta = cantProductos;

            //devolvemos a la api los productos
            var current = Connectivity.NetworkAccess;
            WSProducto productos = new WSProducto();

            if (current == NetworkAccess.Internet)
            {
                var idProducto = producto.idProducto;
                var cantidad = producto.cantidadDevuelta;
                var mensaje = await productos.DevolverProductos(idProducto, cantidad);

                //modifico el stock local
                Models.Producto p = await App.Database.GetProductoAsync(producto.idProducto);
                p.saldo = p.saldo - producto.cantidadDevuelta;
                await App.Database.SaveIProductoAsync(p);

                await DisplayAlert("", mensaje, "Ok");
            }
            else
            {
                await DisplayAlert("", "Sin acceso a internet, administración deberá aprobar esta solicitud.", "Ok");
            }

            //await Navigation.PopAsync();
            await this.Navigation.PopAsync();

        }
    }
}