﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopUpSincronizarClientesPorLocalidad : PopupPage
    {
        List<Departamento> listaDepartamentos = new List<Departamento>();
        List<Localidad> listaLocalidades = new List<Localidad>();

        public PopUpSincronizarClientesPorLocalidad ()
		{
			InitializeComponent ();
            BuscarDepartamentos();
		}

        public async void BuscarDepartamentos()
        {
            listaDepartamentos = await App.Database.GetDepartamentoAsync();

            foreach (var item in listaDepartamentos)
            {
                pickerDepartamento.Items.Add(item.nombre);
            }

        }

        public async void DepartamentoCambia(object sender, EventArgs e)
        {
            //metodo para obtner las locadlidades por Departamento
            string departamento = pickerDepartamento.SelectedItem.ToString();
            int idDepartamento = 0;

            foreach (var item in listaDepartamentos)
            {
                if (departamento == item.nombre)
                {
                    idDepartamento = item.idDepartamento;
                }
            }

            listaLocalidades = await App.Database.GetLocalidadesPorDepartamento(idDepartamento);
            pickerLocalidad.Items.Clear();

            foreach (var item in listaLocalidades)
            {
                pickerLocalidad.Items.Add(item.nombre);
            }

        }

        async void Confirmar(object sender, EventArgs e)
        {
            if (pickerDepartamento.SelectedItem != null)
            {
                if (pickerLocalidad.SelectedItem != null)
                {
                    int idLocalidad = 0;

                    string localidad = pickerLocalidad.SelectedItem.ToString();
                    if (localidad != null)
                    {
                        foreach (var item in listaLocalidades)
                        {
                            if (localidad == item.nombre)
                            {
                                idLocalidad = item.id;
                            }
                        }
                    }
                    await Navigation.PopPopupAsync();
                    MessagingCenter.Send(this, "Sincronizar Clientes por Localidad", idLocalidad);



                }
                else
                {
                    await DisplayAlert("", "Ingrese Localidad", "Ok");
                }
            }
            else
            {
                await DisplayAlert("", "Ingrese Departamento", "Ok");
            }

        }

        async void Cancelar(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
            MessagingCenter.Send(this, "Sincronizar Clientes por Localidad", -1);

        }


        protected override bool OnBackgroundClicked()
        {
            return false;
        }

    }
}