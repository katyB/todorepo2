﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Todo.Models;
using Todo.Views.Clientes;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Producto
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TotalCompraSV : ContentPage
    {
        bool hayFinanciacion = false;
        VMFactura factura = new VMFactura();
        decimal totalFactura = 0m;
        decimal totalContado = 0m;
        decimal totalDebito = 0m;
        decimal totalCredito = 0m;
        int flag;

        public TotalCompraSV (VMFactura f, int fl)
		{
            factura = f;
            InitializeComponent();
            this.Content.IsEnabled = false;
            flag = fl;
            CargarDatosFactura();
            this.Content.IsEnabled = true;
        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            indi.IsRunning = false;
            DatePickerFechaCuota.MinimumDate = DateTime.Now.Date;
            bool masSeisCuotas = false;
            foreach (var item in factura.listaProductos)
            {
                if (item.cantCuota > 6)
                {
                    masSeisCuotas = true;
                }
            }


            if (masSeisCuotas)
            {
                DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(30);

            }
            else
            {
                DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(120);
            }

        }

        public void CargarDatosFactura()
        {

            if (flag == 1)
            {
                totalContado = 0m;
                totalDebito = 0m;
                totalCredito = 0m;

                //Calculo los montos totales, debito. credito y contado

                foreach (var item in factura.listaProductos)
                {
                    //Calculo monto total
                    totalFactura = totalFactura + item.precioTotal;

                    // Calculo monto Contado Y lo hago visible si existe
                    if (item.idFormaPago == 6)
                    {
                        if (labelContado.IsVisible == false)
                        {
                            labelContado.IsVisible = true;
                            labelNombreContado.IsVisible = true;
                        }
                        totalContado += item.precioTotal;
                    }
                    //Calculo monto Credito Y lo hago visible si existe

                    if (item.idFormaPago == 9)
                    {
                        if (labelCredito.IsVisible == false)
                        {
                            labelCredito.IsVisible = true;
                            labelNombreCredito.IsVisible = true;
                        }
                        totalCredito += item.precioTotal;
                    }

                    //Calculo monto Debito Y lo hago visible si existe
                    if (item.idFormaPago == 10)
                    {
                        if (labelDebito.IsVisible == false)
                        {
                            labelDebito.IsVisible = true;
                            labelNombreDebito.IsVisible = true;
                        }
                        totalDebito += item.precioTotal;
                    }

                    //Verifico si existira financiacion
                    if (hayFinanciacion == false)
                    {
                        if (item.idFormaPago != 10 && item.idFormaPago != 9 && item.idFormaPago != 6)
                        {
                            hayFinanciacion = true;

                        }
                    }

                }
                //Hago visible los datos de la financiacion y inicializo la fecha primer cuota y el anticipo

                if (hayFinanciacion)
                {
                    DisplayAlert("", "Recuerde cambiar la fecha de la Primer Cuota", "ok");

                    //DateTime fechaPrimerCuota = DateTime.Now.Date.AddDays(15);
                    DateTime fechaPrimerCuota = DateTime.Now.Date;
                    DatePickerFechaCuota.MinimumDate = fechaPrimerCuota;
                    bool masSeisCuotas = false;
                    foreach (var item in factura.listaProductos)
                    {
                        if (item.cantCuota > 6)
                        {
                            masSeisCuotas = true;
                        }
                    }


                    if (masSeisCuotas)
                    {
                        DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(30);

                    }
                    else
                    {
                        DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(120);
                    }
                    DatePickerFechaCuota.Date = fechaPrimerCuota;
                    Anticipo.Text = "0";
                    DatePickerFechaCuotaTitulo.IsVisible = true;
                    DatePickerFechaCuota.IsVisible = true;
                    labelAnticipo.IsVisible = true;
                    Anticipo.IsVisible = true;
                    gridTabla.IsVisible = true;
                    frame.IsVisible = true;



                }
                else
                {
                    grid.VerticalOptions = LayoutOptions.CenterAndExpand;
                    var medium = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                    labelContado.FontSize = medium;
                    labelNombreContado.FontSize = medium;
                    labelDebito.FontSize = medium;
                    labelNombreDebito.FontSize = medium;
                    labelCredito.FontSize = medium;
                    labelNombreCredito.FontSize = medium;
                    grid.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    frame.IsVisible = true;
                    grid2.HeightRequest = 60;

                }

                //calculo monto de cada cuota y genero la lista de FacturaCuota

                DateTime fecha = DatePickerFechaCuota.Date;
                factura.listaFacturaCuota = new List<VMFacturaCouta>();
                for (int i = 0; i < factura.cantCuotaMasAlta; i++)
                {
                    VMFacturaCouta fc = new VMFacturaCouta();
                    fc.nombreCuota = (i + 1).ToString();
                    if (i == 0)
                    {
                        fc.fechaVto = fecha;
                    }
                    else
                    {
                        fecha = fecha.AddMonths(1);
                        fc.fechaVto = fecha;
                    }
                    decimal montoCuota = 0m;
                    foreach (var item in factura.listaProductos)
                    {
                        if (item.cantCuota > 0)
                        {
                            if (i + 1 <= item.cantCuota)
                            {
                                montoCuota += item.preciocuota;
                            }
                        }
                    }
                    fc.monto = (int)montoCuota;
                    factura.listaFacturaCuota.Add(fc);
                }

                //Cargo los valoresde monto total, credito, debito y contado en la vista

                labelMontoFactura.Text = "$" + totalFactura.ToString();
                labelContado.Text = "$" + totalContado.ToString();
                labelCredito.Text = "$" + totalCredito.ToString();
                labelDebito.Text = "$" + totalDebito.ToString();

                //cargo los valores de cuotas en la tabla
                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(TituloCuota, 0, 0);
                gridTabla.Children.Add(TituloMonto, 1, 0);
                gridTabla.Children.Add(TituloFecha, 2, 0);


                for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
                {
                    gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                    var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                    gridTabla.Children.Add(Cuota, 0, i + 1);
                    gridTabla.Children.Add(Monto, 1, i + 1);
                    gridTabla.Children.Add(Fecha, 2, i + 1);
                }

                flag = 0;
            }
        }

        public void CambioFecha(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            if (factura.listaFacturaCuota != null && flag == 0)
            {
                gridTabla.Children.Clear();
                gridTabla.RowDefinitions.Clear();
                gridTabla.ColumnDefinitions.Clear();

                DateTime fecha = DatePickerFechaCuota.Date;

                for (int i = 0; i < factura.listaFacturaCuota.Count(); i++)
                {
                    if (i == 0)
                    {
                        factura.listaFacturaCuota[i].fechaVto = fecha;
                    }
                    else
                    {
                        fecha = fecha.AddMonths(1);
                        factura.listaFacturaCuota[i].fechaVto = fecha;
                    }
                }

                //cargo los valores de cuotas en la tabla
                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(TituloCuota, 0, 0);
                gridTabla.Children.Add(TituloMonto, 1, 0);
                gridTabla.Children.Add(TituloFecha, 2, 0);


                for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
                {
                    gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                    var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                    gridTabla.Children.Add(Cuota, 0, i + 1);
                    gridTabla.Children.Add(Monto, 1, i + 1);
                    gridTabla.Children.Add(Fecha, 2, i + 1);
                }
            }
            this.Content.IsEnabled = true;
        }

        public async void cambioAnticipo(object sender, EventArgs e)
        {
            if (factura.listaFacturaCuota != null && flag == 0)
            {
                if (!string.IsNullOrWhiteSpace(Anticipo.Text))
                {
                    if (int.TryParse(Anticipo.Text, out int anticipo))
                    {
                        if (anticipo >= 0)
                        {
                            if (anticipo <= totalFactura)
                            {
                                CalcularCuotas(anticipo);
                            }
                            else
                            {
                                await DisplayAlert("", "El anticipo no puede ser mayor al monto de la factura", "Ok");
                                Anticipo.Text = "0";
                                factura.anticipo = 0;
                                CalcularCuotas(0);

                            }
                        }
                        else
                        {
                            await DisplayAlert("", "Ingrese un anticipo mayor a 0", "Ok");
                            Anticipo.Text = "0";
                            CalcularCuotas(0);
                        }

                    }
                    else
                    {
                        await DisplayAlert("", "Ingrese un valor entero", "Ok");
                        Anticipo.Text = "0";
                        factura.anticipo = 0;
                        CalcularCuotas(0);
                    }

                }
                else
                {
                    Anticipo.Text = "";
                    factura.anticipo = 0;
                    CalcularCuotas(0);
                }

            }

        }

        public void CalcularCuotas(int anticipo)
        {
            decimal porcentaje = 1 - (anticipo / totalFactura);
            factura.anticipo = anticipo;
            Thread.Sleep(2);
            gridTabla.Children.Clear();
            gridTabla.RowDefinitions.Clear();
            gridTabla.ColumnDefinitions.Clear();
            for (int i = 0; i < factura.listaFacturaCuota.Count(); i++)
            {
                decimal montoCuota = 0m;
                foreach (var item in factura.listaProductos)
                {
                    if (item.cantCuota > 0)
                    {
                        if (i + 1 <= item.cantCuota)
                        {
                            montoCuota += item.preciocuota * porcentaje;
                            montoCuota = decimal.Round(montoCuota, 2);
                        }
                    }
                }
                factura.listaFacturaCuota[i].monto = (int)montoCuota;
            }

            //cargo los valores de cuotas en la tabla
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
            var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
            var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

            gridTabla.Children.Add(TituloCuota, 0, 0);
            gridTabla.Children.Add(TituloMonto, 1, 0);
            gridTabla.Children.Add(TituloFecha, 2, 0);


            for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
            {
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(Cuota, 0, i + 1);
                gridTabla.Children.Add(Monto, 1, i + 1);
                gridTabla.Children.Add(Fecha, 2, i + 1);
            }
        }

        public async void Confirmar(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            factura.FormaPago = 2;
            factura.totalContado = (int)totalContado;
            factura.totalCredito = (int)totalCredito;
            factura.totalDebito = (int)totalDebito;
            factura.observacion = observacion.Text;
            factura.montoFactura = (int)totalFactura;



            var montoInformacion = await App.Database.GetInfoMontoAsync();
            if (montoInformacion != null)
            {
                var montoFinanciado = factura.montoFactura - factura.totalContado - factura.totalCredito - factura.totalDebito;
                if (montoFinanciado > montoInformacion.limiteVenta)
                {
                    if (!string.IsNullOrEmpty(montoInformacion.mensaje))
                    {
                        await DisplayAlert("", montoInformacion.mensaje, "ok");

                    }
                    else
                    {
                        await DisplayAlert("", "Monto excedido, solicite autorización.", "ok");

                    }
                }
            }
            else
            {
                await DisplayAlert("", "Venta sujeta a verificación.", "ok");

            }

            await this.Navigation.PushAsync(new ScanDNiCliente(factura, 10));
            indi.IsRunning = false;
            this.Content.IsEnabled = true;

 
        }

    }
}