﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Todo.Models;
using Todo.Views.Facturacion;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Producto
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscadorProductosSV : ContentPage
	{

        List<VMProducto> listaProductos = new List<VMProducto>();
        List<VMProducto> listaProductosSeleccionados = new List<VMProducto>();
        VMProducto productoSeleccionado = new VMProducto();
        VMFactura factura = new VMFactura();

        public BuscadorProductosSV()
        {
            InitializeComponent();
            BuscarProductos();
            Mensaje();
        }

        async void BuscarProductos()
        {
            var listadoBDlocalProductos = await App.Database.GetProductoAsync();

            foreach (var db in listadoBDlocalProductos)
            {
                VMProducto items = new VMProducto();
                items.idProducto = db.idProducto;
                items.codigoProducto = db.codigoProducto;
                items.nombreProducto = db.nombreProducto;
                items.idDeposito = db.idDeposito;
                items.cantidad = db.saldo; // el saldo tiene la cantidad disponble de productos
                if (db.saldo == 1)
                {
                    items.imagenUltimoProducto = "Amarillo.png";
                }
                listaProductos.Add(items);
            }

            listaProductos.OrderBy(i => i.nombreProducto);
            listViewProductos.ItemsSource = listaProductos.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
            frameProductos.HeightRequest = listaProductos.Count * 60 + 10;
        }

        public void OnTextChanged(object sender, EventArgs e)
        {
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            SearchBar searchBar = (SearchBar)sender;
            string texto = reg.Replace(searchBar.Text.Normalize(NormalizationForm.FormD).ToLower(), "");
            if (texto.Count() > 1)
            {
                var lista = Filtrar(texto);
                listViewProductos.ItemsSource = lista;
                var altura = lista.Count * 95 + 10;
                if (altura < 400)
                {
                    frameProductos.HeightRequest = 195;

                }
                else
                {
                    frameProductos.HeightRequest = altura;

                }
            }
            if (texto.Count() == 0)
            {
                listViewProductos.ItemsSource = listaProductos.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
                var altura = listaProductos.Count * 95 + 10;
                if (altura < 400)
                {
                    frameProductos.HeightRequest = 195;

                }
                else
                {
                    frameProductos.HeightRequest = altura;

                }
            }

        }

        public List<VMProducto> Filtrar(string busqueda)
        {
            List<VMProducto> listaFiltrada = new List<VMProducto>();
            foreach (var item in listaProductos)
            {
                Regex reg = new Regex("[^a-zA-Z0-9 ]");
                var nombreProduto = reg.Replace(item.nombreProducto.Normalize(NormalizationForm.FormD).ToLower(), "");

                if (nombreProduto.Contains(busqueda) || item.codigoProducto.ToString().Contains(busqueda))
                {
                    VMProducto p = new VMProducto();
                    p.idProducto = item.idProducto;
                    p.codigoProducto = item.codigoProducto;
                    p.nombreProducto = item.nombreProducto;
                    p.nombreDeposito = item.nombreDeposito;
                    p.idDeposito = item.idDeposito;
                    p.cantidad = item.cantidad;
                    listaFiltrada.Add(p);
                    listaFiltrada = listaFiltrada.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
                }

            }
            return listaFiltrada;
        }

        public void Mensaje()
        {
            MessagingCenter.Subscribe<ResumenCompraSV, VMProducto>(this, "Eliminar Producto", (page, producto) => {

                listaProductosSeleccionados.Remove(producto);

                listViewProductos.ItemsSource = listaProductos.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();

            });


            MessagingCenter.Subscribe<CantidadProductosSV, VMProducto>(this, "Producto con cantidad", (page, producto) => {

                listaProductosSeleccionados.Add(producto);

            });

            MessagingCenter.Subscribe<CantidadProductosSV, VMProducto>(this, "Producto con cantidad editado desde producto", (page, producto) => {

                foreach (var item in listaProductosSeleccionados)
                {
                    if (item.idProducto == producto.idProducto)
                    {
                        item.cantidadComprada = producto.cantidadComprada;
                    }
                }

            });


        }

        public async void Seleccionar(object sender, ItemTappedEventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            var p = e.Item as VMProducto;
            if (p.cantidad > 0)
            {
                productoSeleccionado = p;

                var edita = 0;

                foreach (var item in listaProductosSeleccionados)
                {
                    if (item.idProducto == p.idProducto)
                    {
                        edita = 1;
                        p.cantidadComprada = item.cantidadComprada;

                    }
                }

                if (edita == 0)
                {

                    await this.Navigation.PushAsync(new CantidadProductosSV(p));
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                }
                else
                {

                    await this.Navigation.PushAsync(new CantidadProductosSV(p, 2));
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                }

            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Producto sin Stock", "Ok");
            }

        }


        public async void Continuar(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            if (listaProductosSeleccionados.Count > 0)
            {
                factura.listaProductos = listaProductosSeleccionados;

                await this.Navigation.PushAsync(new ResumenCompraSV(factura));
                this.Content.IsEnabled = true;
                indi.IsRunning = false;

            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Seleccione  al menos un producto", "Ok");
            }


        }

        async void sync(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;


            var idDeposito = Preferences.Get("idDeposito", 0);
            WSProducto producto = new WSProducto();
            WSFormaPago formaPago = new WSFormaPago();

            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                MessagingCenter.Subscribe<PopUpSincronizarProductos, bool>(this, "Sincronizar Productos Cerrado", async (page, respuesta) =>
                {
                    MessagingCenter.Unsubscribe<PopUpSincronizarProductos, bool>(this, "Sincronizar Productos Cerrado");
                    this.Content.IsEnabled = false;
                    indi.IsRunning = true;
                    if (respuesta)
                    {

                        //productos
                        string urlproducto = "http://biancogestion.com:3630/api/Producto/BuscarProductoPorDeposito?";
                        //string urlproducto = "http://biancogestion.com/api/Producto/BuscarProductoPorDeposito?";
                        var respuestaProductos = await producto.GetProductos<WSProducto>(urlproducto + "idDeposito=" + idDeposito);
                        if (respuestaProductos != null)
                        {
                            foreach (var element in respuestaProductos)
                            {
                                await App.Database.SaveIProductoAsync(element);
                            }

                        }

                        //Forma de pago de los productos
                        var urlFP = "http://biancogestion.com:3630/api/Producto/BuscarProductoConFormaPagoPorDeposito?";
                        //var urlFP = "http://biancogestion.com/api/Producto/BuscarProductoConFormaPagoPorDeposito?";
                        var listadoProductos = await App.Database.GetProductoAsync();
                        if (listadoProductos.Count() != 0)
                        {
                            await App.Database.DeleteProductoFormaDePagosync();
                            var respuestaWSFormaDePago = await formaPago.Get<WSFormaPago>(urlFP + "idDeposito=" + idDeposito);
                            foreach (var item in respuestaWSFormaDePago)
                            {
                                await App.Database.SaveIProductoFormaDePagoAsync(item);
                            }
                        }
                        string msj = "Sincronización Finalizada";

                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        await Navigation.PushPopupAsync(new PopUpMensaje(msj));
                        BuscarProductos();
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                    }

                });

                this.Content.IsEnabled = true;
                indi.IsRunning = false;

                await Navigation.PushPopupAsync(new PopUpSincronizarProductos());
            }
            else
            {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("", "Sin acceso a internet, no puede sincronizar sus productos", "ok");
            }
        }
    }
}