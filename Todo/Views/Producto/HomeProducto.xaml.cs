﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Producto
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeProducto : ContentPage
	{
		public HomeProducto ()
		{
			InitializeComponent ();
		}

        async void ClickProducto(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new BuscarProductoView());
        }

        async void ClickSimulador(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new BuscadorProductosSV());
        }
    }
}