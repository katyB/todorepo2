﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Todo.Models;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Producto
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResumenCompraSV : ContentPage
	{
        VMFactura factura = new VMFactura();
        List<FormaPago> listaMediosDePago = new List<FormaPago>();
        List<VMProducto> listaProductos = new List<VMProducto>();
        HttpClient client = new HttpClient();
        WSFormaPago formaPago = new WSFormaPago();

        public ResumenCompraSV (VMFactura f)
        { 
            factura = f;
            InitializeComponent();
            this.Content.IsEnabled = false;
            Mensaje();
            cargarDatos();
            this.Content.IsEnabled = true;
        }

        async void cargarDatos()
        {
            //Defino columnas
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

            //Creo tabla      
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            for (int i = 0; i < factura.listaProductos.Count(); i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                string nombreProd;
                if (factura.listaProductos[i].nombreProducto.Length > 50)
                {
                    nombreProd = factura.listaProductos[i].nombreProducto.Substring(0, 50) + "...";
                }
                else
                {
                    nombreProd = factura.listaProductos[i].nombreProducto;
                }

                var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                //forma pago por Producto del listado de lo seleccionado
                int precioCuota = 0;
                int totalParcial = 0;
                Picker pickercoutas = new Picker { StyleId = i.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };

                //forma de pago
                var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[i].idProducto);
                respuestaFormaPago = respuestaFormaPago.OrderBy(r => r.nombreFormaPago).ToList();

                var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                var precioDeCuota = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

                foreach (var ind in respuestaFormaPago)
                {
                    pickercoutas.Items.Add(ind.nombreFormaPago);

                    if (ind.idFormaPago == 1)
                    {

                        pickercoutas.SelectedItem = ind.nombreFormaPago;
                        factura.listaProductos[i].precioUnitario = ind.montoVenta;
                        factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                        factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                        CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                        totalParcial = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                        factura.listaProductos[i].precioTotal = totalParcial;
                        if (ind.tieneFinanciacion)
                        {
                            precioCuota = (int)totalParcial / ind.cantidadCuotas;
                            factura.listaProductos[i].preciocuota = precioCuota;
                            factura.listaProductos[i].cantCuota = ind.cantidadCuotas;
                        }
                        else
                        {
                            factura.listaProductos[i].preciocuota = precioCuota;
                            factura.listaProductos[i].cantCuota = 0;
                            factura.listaProductos[i].precioTotal = totalParcial;
                        }

                        precioDeCuota.Text = "$" + precioCuota.ToString();
                    }
                }
                pickercoutas.SelectedIndexChanged += this.cambioPicker;
                ImageButton edit = new ImageButton
                {
                    StyleId = i.ToString(),
                    Source = "Edit.png",
                    BackgroundColor = Color.Transparent,
                    HeightRequest = 30,
                    WidthRequest = 30
                };
                edit.Clicked += Edit;

                ImageButton delete = new ImageButton
                {
                    StyleId = i.ToString(),
                    Source = "Rojo.png",
                    BackgroundColor = Color.Transparent,
                    HeightRequest = 30,
                    WidthRequest = 30
                };
                delete.Clicked += Delete;


                StackLayout slVertical = new StackLayout();
                StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
                StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };

                slVertical.Children.Add(nombre);
                slHorizontal1.Children.Add(CantYPrecioUnitario);
                slHorizontal1.Children.Add(edit);
                slHorizontal1.Children.Add(delete);
                slHorizontal2.Children.Add(pickercoutas);
                slHorizontal2.Children.Add(precioDeCuota);

                slVertical.Children.Add(slHorizontal1);
                slVertical.Children.Add(slHorizontal2);

                grid.Children.Add(slVertical, 0, i);
            }

            frame.IsVisible = true;
            grid.IsVisible = true;
        }

        public async void Edit(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            var button = sender as ImageButton;

            int id = Convert.ToInt32(button.StyleId);

            VMProducto p = new VMProducto();
            p.idProducto = factura.listaProductos[id].idProducto;
            p.cantidadComprada = factura.listaProductos[id].cantidadComprada;
            p.cantidad = factura.listaProductos[id].cantidad;
            p.codigoProducto = factura.listaProductos[id].codigoProducto;
            p.nombreProducto = factura.listaProductos[id].nombreProducto;
            p.idDeposito = factura.listaProductos[id].idDeposito;
            p.nombreDeposito = factura.listaProductos[id].nombreDeposito;

            await this.Navigation.PushAsync(new CantidadProductosSV(p, 1));
            this.Content.IsEnabled = true;
        }

        public void Delete(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            //Elimino elemtno de la listaProducto
            var button = sender as ImageButton;
            int id = Convert.ToInt32(button.StyleId);
            MessagingCenter.Send(this, "Eliminar Producto", factura.listaProductos[id]);
            regenerarTabla();
        }

        public async void cambioPicker(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

            var precioDeCuota = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

            var picker = sender as Picker;
            var id = Int32.Parse(picker.StyleId);
            var nombrePicker = picker.SelectedItem.ToString();
            string nombreProd;
            if (factura.listaProductos[id].nombreProducto.Length > 50)
            {
                nombreProd = factura.listaProductos[id].nombreProducto.Substring(0, 50) + "...";
            }
            else
            {
                nombreProd = factura.listaProductos[id].nombreProducto;
            }
            var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };

            //forma de pago
            var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[id].idProducto);
            respuestaFormaPago = respuestaFormaPago.OrderBy(r => r.nombreFormaPago).ToList();

            int precioCuota = 0;
            int totalParcial = 0;
            Picker pickercoutas = new Picker { StyleId = id.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };
            foreach (var ind in respuestaFormaPago)
            {
                pickercoutas.Items.Add(ind.nombreFormaPago);
                if (ind.nombreFormaPago == nombrePicker)
                {
                    pickercoutas.SelectedItem = ind.nombreFormaPago;
                    factura.listaProductos[id].precioUnitario = ind.montoVenta;
                    CantYPrecioUnitario.Text = factura.listaProductos[id].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[id].precioUnitario.ToString();
                    totalParcial = factura.listaProductos[id].precioUnitario * factura.listaProductos[id].cantidadComprada;
                    factura.listaProductos[id].precioTotal = totalParcial;
                    factura.listaProductos[id].idFormaPago = ind.idFormaPago;
                    factura.listaProductos[id].nombreFormaPago = ind.nombreFormaPago;

                    if (ind.tieneFinanciacion)
                    {
                        precioCuota = (int)totalParcial / ind.cantidadCuotas;
                        factura.listaProductos[id].preciocuota = precioCuota;
                        factura.listaProductos[id].cantCuota = ind.cantidadCuotas;
                    }
                    else
                    {
                        precioCuota = totalParcial;
                        factura.listaProductos[id].preciocuota = precioCuota;
                        factura.listaProductos[id].cantCuota = 0;
                    }
                    precioDeCuota.Text = "$" + precioCuota.ToString();
                }
            }
            pickercoutas.SelectedIndexChanged += this.cambioPicker;

            foreach (var control in grid.Children)
            {
                if (Grid.GetRow(control) == id && Grid.GetColumn(control) == 0)
                {
                    grid.Children.Remove(control);
                    break;
                }
            }

            ImageButton edit = new ImageButton
            {
                StyleId = id.ToString(),
                Source = "Edit.png",
                BackgroundColor = Color.Transparent,
                HeightRequest = 30,
                WidthRequest = 30
            };
            edit.Clicked += Edit;

            ImageButton delete = new ImageButton
            {
                StyleId = id.ToString(),
                Source = "Rojo.png",
                BackgroundColor = Color.Transparent,
                HeightRequest = 30,
                WidthRequest = 30
            };
            delete.Clicked += Delete;

            StackLayout slVertical = new StackLayout();
            StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
            StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };

            slVertical.Children.Add(nombre);
            slHorizontal1.Children.Add(CantYPrecioUnitario);
            slHorizontal1.Children.Add(edit);
            slHorizontal1.Children.Add(delete);
            slHorizontal2.Children.Add(pickercoutas);
            slHorizontal2.Children.Add(precioDeCuota);

            slVertical.Children.Add(slHorizontal1);
            slVertical.Children.Add(slHorizontal2);

            grid.Children.Add(slVertical, 0, id);
            this.Content.IsEnabled = true;
        }


        async void Continuar(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            int cuotaAlta = 0;


            //
            //      Guardar producto en factura temporal y averiguo cuota mas alta
            //

            foreach (var item in factura.listaProductos)
            {
                //cuota mas alta
                if (cuotaAlta < item.cantCuota)
                {
                    cuotaAlta = item.cantCuota;
                }

            }
            factura.cantCuotaMasAlta = cuotaAlta;


            await this.Navigation.PushAsync(new TotalCompraSV(factura, 1));
            this.Content.IsEnabled = true;
        }

        public void Mensaje()
        {
            MessagingCenter.Subscribe<CantidadProductosSV, VMProducto>(this, "Producto con cantidad editado desde resumen", (page, producto) =>
            {
                this.Content.IsEnabled = false;
                foreach (var item in factura.listaProductos)
                {
                    if (item.idProducto == producto.idProducto)
                    {
                        item.cantidadComprada = producto.cantidadComprada;
                    }
                }
                regenerarTabla();
            });
        }

        public async void regenerarTabla()
        {
            this.Content.IsEnabled = false;
            if (factura.listaProductos.Count > 0)
            {
                //Regenero la tabla
                grid.Children.Clear();
                grid.RowDefinitions.Clear();
                grid.ColumnDefinitions.Clear();


                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                for (int i = 0; i < factura.listaProductos.Count(); i++)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                    string nombreProd;
                    if (factura.listaProductos[i].nombreProducto.Length > 50)
                    {
                        nombreProd = factura.listaProductos[i].nombreProducto.Substring(0, 50) + "...";
                    }
                    else
                    {
                        nombreProd = factura.listaProductos[i].nombreProducto;
                    }

                    var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };

                    //forma pago por Producto del listado de lo seleccionado
                    int precioCuota = 0;
                    int totalParcial = 0;
                    Picker pickercoutas = new Picker { StyleId = i.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };

                    //forma de pago
                    var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[i].idProducto);
                    respuestaFormaPago = respuestaFormaPago.OrderBy(r => r.nombreFormaPago).ToList();

                    var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

                    var precioDeCuota = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

                    foreach (var ind in respuestaFormaPago)
                    {
                        pickercoutas.Items.Add(ind.nombreFormaPago);

                        if (ind.idFormaPago == factura.listaProductos[i].idFormaPago)
                        {

                            pickercoutas.SelectedItem = ind.nombreFormaPago;
                            factura.listaProductos[i].precioUnitario = ind.montoVenta;
                            factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                            factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                            CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                            totalParcial = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                            factura.listaProductos[i].precioTotal = totalParcial;
                            if (ind.tieneFinanciacion)
                            {
                                precioCuota = (int)totalParcial / ind.cantidadCuotas;
                                factura.listaProductos[i].preciocuota = precioCuota;
                                factura.listaProductos[i].cantCuota = ind.cantidadCuotas;
                            }
                            else
                            {
                                factura.listaProductos[i].preciocuota = precioCuota;
                                factura.listaProductos[i].cantCuota = 0;
                                factura.listaProductos[i].precioTotal = totalParcial;
                            }

                            precioDeCuota.Text = "$" + precioCuota.ToString();
                        }
                    }
                    pickercoutas.SelectedIndexChanged += this.cambioPicker;
                    ImageButton edit = new ImageButton
                    {
                        StyleId = i.ToString(),
                        Source = "Edit.png",
                        BackgroundColor = Color.Transparent,
                        HeightRequest = 30,
                        WidthRequest = 30
                    };
                    edit.Clicked += Edit;

                    ImageButton delete = new ImageButton
                    {
                        StyleId = i.ToString(),
                        Source = "Rojo.png",
                        BackgroundColor = Color.Transparent,
                        HeightRequest = 30,
                        WidthRequest = 30
                    };
                    delete.Clicked += Delete;


                    StackLayout slVertical = new StackLayout();
                    StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
                    StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };

                    slVertical.Children.Add(nombre);
                    slHorizontal1.Children.Add(CantYPrecioUnitario);
                    slHorizontal1.Children.Add(edit);
                    slHorizontal1.Children.Add(delete);
                    slHorizontal2.Children.Add(pickercoutas);
                    slHorizontal2.Children.Add(precioDeCuota);

                    slVertical.Children.Add(slHorizontal1);
                    slVertical.Children.Add(slHorizontal2);

                    grid.Children.Add(slVertical, 0, i);
                    this.Content.IsEnabled = true;
                }
            }
            else
            {
                await this.Navigation.PopAsync();
                this.Content.IsEnabled = true;
            }
        }

    }
}