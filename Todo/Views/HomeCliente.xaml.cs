﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Models;
using Todo.Views.Clientes;
using Todo.Views.Facturacion;
using Todo.Views.Reclamo;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeCliente : ContentPage
	{
        ClienteApp cliente;
        int flag = 0;
         public HomeCliente (ClienteApp cl)
		{
            cliente = cl;
			InitializeComponent ();
            nombreCliente.Title = cliente.nombreCliente;
            montoDisponible.Text = "$ "+cliente.montoTotalMaximo.ToString();

        }

        public HomeCliente(ClienteApp cl, int fl)
        {

            cliente = cl;
            InitializeComponent();
            nombreCliente.Title = cliente.nombreCliente;
            montoDisponible.Text = "$ " + cliente.montoTotalMaximo.ToString();
            flag = fl;

        }



        async void ClickFacturacion(object sender, EventArgs e)
        {
            var listadoTalonario = await App.Database.GetTalonariosAsync();
            if (listadoTalonario.Count > 0)
            {

                var listadoProductos = await App.Database.GetProductoAsync();
                if (listadoProductos.Count > 0)
                {

                    if(flag == 0)
                    {
                        await this.Navigation.PushAsync(new ScanDNiCliente(cliente, 2));

                    }
                    else
                    {
                        if(flag == 7)
                        {
                            await this.Navigation.PushAsync(new Facturacion.FormaPago(cliente, 7));

                        }
                    }
                }
                else
                {
                    await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con administración", "Ok");
                }


            }
            else
            {
                await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
            }

        }

        void ClickReclamos(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new AltaReclamoFacturaView(cliente));
        }

        void ClickEstadoCuenta(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, 0));
        }




    }
}