﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;
using Todo.Models;
using Xamarin.Essentials;
using Todo.WS;

namespace Todo.Views.Cobranzas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AvisoCobranza : ContentPage
    {
        VMCobranza cobranza = new VMCobranza();
        List<CobranzaTipoReclamo> listaTipoReclamo = new List<CobranzaTipoReclamo>();

        public AvisoCobranza(VMCobranza c)
        {
            InitializeComponent();
            cobranza = c;
            BindingContext = c;
            CargarDatos();
        }

        public async void CargarDatos()
        {
            listaTipoReclamo = await App.Database.GetCobranzaTipoReclamoAsync();
            foreach (var item in listaTipoReclamo)
            {
                TipoAviso.Items.Add(item.nombreInforme);
            }
        }

        public async void Confirmar(object sender, EventArgs e)
        {
            indi.IsRunning = true;
            this.Content.IsEnabled = false;
            if (TipoAviso.SelectedItem != null)
            {
                if (!string.IsNullOrWhiteSpace(observacionAviso.Text))
                {
                    CobranzaReclamo aviso = new CobranzaReclamo
                    {
                        codCliente = cobranza.codCliente,
                        fechaCobranzaReclamo = DateTime.Now,
                        idCliente = cobranza.idCliente,
                        idEstado = 7,
                        nombreCliente = cobranza.nombreCliente,
                        observacion = observacionAviso.Text,
                        sincronizado = false
                    };

                    foreach (var item in listaTipoReclamo)
                    {
                        if (item.nombreInforme == TipoAviso.SelectedItem.ToString())
                        {
                            aviso.nombreTipoReclamoCobranza = item.nombreInforme;
                            aviso.idTipoReclamoCobranza = item.idCobranzaTipoReclamo;
                            aviso.idTipoInforme = item.idTipoInforme;
                        }
                    }

                    var unReclamoCobranza = App.Database.SaveCobranzaReclamoAsync(aviso);
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                        WSClienteCobranza cobranza = new WSClienteCobranza();
                        await cobranza.EnviarUnReclamoDeCobranza(aviso);
                    }

                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    await Navigation.PopAsync();
                    indi.IsRunning = false;
                    this.Content.IsEnabled = true;
                }
                else
                {
                    indi.IsRunning = false;
                    this.Content.IsEnabled = true;

                    await DisplayAlert("", "Ingrese una observación", "Ok");
                }
            }
            else
            {
                indi.IsRunning = false;
                this.Content.IsEnabled = true;

                await DisplayAlert("", "Seleccione un Tipo de Aviso", "Ok");
            }

        }
    }
}