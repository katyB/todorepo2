﻿using Forms9Patch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Cobranzas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FinalizarDiaCobranza : ContentPage
	{
        List<Cobranza> listaCobranza = new List<Cobranza>();
        public FinalizarDiaCobranza ()
		{
			InitializeComponent ();
            CargarDatos();
        }

        public async void CargarDatos()
        {

            //var nombreUsuario = App.Current.Properties["usuarioLogueado"].ToString();
            var nombreUsuario = Preferences.Get("usuarioLogueado", "");
            cobrador.Text = nombreUsuario;

            //var nZona = App.Current.Properties["nroZona"].ToString();
            //nroZona.Text = nZona.ToString();


            fecha.Text = DateTime.Now.Date.ToString("dd/MM/yy");

            int totalClientes = await App.Database.GetCantClientesCobrados();
            cantCuentas.Text = totalClientes.ToString();

            decimal totalCobrado = await App.Database.GetTotalCobrado();
            cantCobrado.Text = "$"+totalCobrado.ToString();

            //servicio para obtener la lista de nroCuenta y cuanto pago
            listaCobranza = await App.Database.GetCobranzaDeldia();

            var listaRecibosSinUsar = await App.Database.GetTalonariosCobranzaAsync();

            //Crear Tabla
            var size = Device.GetNamedSize(NamedSize.Medium, typeof(Xamarin.Forms.Label));
            gridCuentas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
            gridCuentas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
            gridCuentas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            var TituloCuenta = new Xamarin.Forms.Label { Text = "Cuenta", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
            var TituloMonto = new Xamarin.Forms.Label { Text = "Monto", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
            gridCuentas.Children.Add(TituloCuenta, 0, 0);
            gridCuentas.Children.Add(TituloMonto, 1, 0);


            for (int i = 0; i < listaCobranza.Count; i++)
            {
                gridCuentas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                string c = listaCobranza[i].codCliente;
                string m = "$"+listaCobranza[i].monto;

                var cuenta = new Xamarin.Forms.Label { Text = c, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var monto = new Xamarin.Forms.Label { Text = m, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridCuentas.Children.Add(cuenta, 0, i + 1);
                gridCuentas.Children.Add(monto, 1, i + 1);
            }
        }

        public async void Imprimir(object sender, EventArgs e)
        {

            Forms9Patch.WebViewPrintEffect.ApplyTo(webView);
            var htmlSource = new HtmlWebViewSource();
            var fechaTicket = DateTime.Now;
            string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
            html += " <table style = 'width: 100 %'>";
            html += "<tr colspan=2>";
            html += "<td cols><strong>Vendedor: </strong>" + cobrador.Text + "</td>";
            html += "</tr>";
            //html += "<tr colspan=2>";
            //html += "<td cols><strong>Zona: </strong>" + nroZona.Text + "</td>";
            //html += "</tr>";
            html += "<tr>";
            html += "<td><strong>Fecha: </strong>" + fechaTicket.ToString("dd/MM/yyyy") + "</td>";
            html += "<td><strong>Hora: </strong>" + fechaTicket.ToString("HH:mm:ss") + "</td>";
            html += "</tr>";

            html += "</table>";


            html += " <table style = 'width: 100 %'>";
            html += "<tr>";
            html += "<th>Cuenta</th>";
            html += "<th>Monto</th>";
            html += "</tr>";
            foreach (var item in listaCobranza)
            {
                html += "<tr>";
                html += "<td>"+ item.codCliente + "</td>";
                html += "<td>$" + item.monto + "</td>";
                html += "</tr>";
            }
            html += "</table>";


            html += " <table style = 'width: 100 %'>";
            html += "<tr colspan=2>";
            html += "<td><strong>Cuentas Cobradas: </strong>" + cantCuentas.Text + "</td>";
            html += "</tr>";
            html += "<tr colspan=2>";
            html += "<td cols><strong>Total Cobrado: </strong>" + cantCobrado.Text + "</td>";
            html += "</tr>";
            html += "</table>";

            htmlSource.Html = html;
            webView.Source = htmlSource;

            var estado = await Permissions.RequestAsync<Permissions.StorageWrite>();

            if (await webView.ToPdfAsync(fechaTicket.ToString("ddMMyyyyhhmmss")) is ToFileResult pdfResult)
            {
                await Share.RequestAsync(new ShareFileRequest
                {
                    Title = "Archivo Pdf",
                    File = new ShareFile(pdfResult.Result)
                });


                await this.Navigation.PopAsync();


            }

        }
    }
}