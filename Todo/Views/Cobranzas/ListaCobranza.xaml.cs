﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.Models;
using Todo.ViewsModels;

namespace Todo.Views.Cobranzas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListaCobranza : ContentPage
    {
        List<Cobranza> listaCobranza = new List<Cobranza>();

        public ListaCobranza ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ObtenerListaCobranza();


        }
        public async void ObtenerListaCobranza()
        {


            //lista de clientes a cobrar
            listaCobranza = await App.Database.GetCobranzaNotDoneAsync();

            //listaCobranza.Add(c);
            foreach (var item in listaCobranza)
            {
                var listaCuentaCteXCliente = await App.Database.GetCobranzaCuentaCorrienteCodAsync(item.codCliente);
                var PrimerCuota = listaCuentaCteXCliente.FirstOrDefault();
                item.montoPrimerCuota = PrimerCuota.montoComprobante;
                item.fechaCobranzaMostrar = item.fechaCobranza.ToString("dd/MM/yy");
            }
            listaCobranza = listaCobranza.OrderBy(x => x.nombreCliente).ToList();
            listViewCobranza.ItemsSource = listaCobranza;
            if (listaCobranza.Count != 0)
            {
                frameCobranza.IsVisible = true;
                frameCobranza.HeightRequest = listaCobranza.Count * 45 + 45;
                frameCobranza.IsVisible = true;

            }
            else
            {
                frameCobranza.IsVisible = false;
            }
        }

        async void cobranzaElegida(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {

                Cobranza c = (Cobranza)e.SelectedItem;
                c.fechaCobranzaMostrar = c.fechaCobranza.ToString("dd/MM/yyyy");
                VMCobranza cobranza = new VMCobranza
                {
                    
                    codCliente = c.codCliente,
                    nombreCliente = c.nombreCliente,
                    monto = c.monto,
                    fechaCobranza = c.fechaCobranza,
                    fechaCobranzaMostrar = c.fechaCobranzaMostrar,
                    saldo = c.saldo,
                    imagenEstado = c.imagenEstado,
                    domicilio = c.domicilio,
                    idCliente = c.idCliente, 
                    idEstado = c.idEstado,
                    nombreEstado = c.nombreEstado,
                    idInterno = c.idInterno,
                    nroComprobante = c.nroComprobante,
                    cobrado = c.cobrado,
                    idZona = c.idZona,
                    costoOperativo = c.costoOperativo,
                    costoCapital = c.costoCapital
                };


                //lista de cuotas a cobrar por cliente (enviar el codido del cliente)
                var listaCuentaCteXCliente = await App.Database.GetCobranzaCuentaCorrienteCodAsync(cobranza.codCliente);
                List<VMCobranzaCuentaCorriente> listaCC = new List<VMCobranzaCuentaCorriente>();
                

                foreach (var item in listaCuentaCteXCliente)
                {
                    VMCobranzaCuentaCorriente cc = new VMCobranzaCuentaCorriente {
                        codCliente = item.codCliente,
                        fechaVencimiento = item.fechaVencimiento,
                        idCliente = item.idCliente,
                        idEstado = item.idEstado,
                        montoComprobante = item.montoComprobante
                       
                        
                    };
                    listaCC.Add(cc);
                }

                cobranza.cuentaCorriente = new List<VMCobranzaCuentaCorriente>();
                cobranza.cuentaCorriente = listaCC;

                if(cobranza.idEstado == 7)
                {
                    await Navigation.PushAsync(new CobranzaImprimir(cobranza, 1));
                }
                else
                {
                    var listadoTalonario = await App.Database.GetTalonariosCobranzaAsync();
                    if (listadoTalonario.Count > 0)
                    {
                        await Navigation.PushAsync(new HomeCobranza(cobranza));

                    }
                    else
                    {
                        await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    }
                }

            }
        }

        public async void CerrarDia(object sender, EventArgs e)
        {

            await this.Navigation.PushAsync(new FinalizarDiaCobranza());

        }

        void OnTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;
            string texto = searchBar.Text;
            if (texto.Count() > 2)
            {
                var lista = Filtrar(texto);
                listViewCobranza.ItemsSource = Filtrar(texto).OrderBy(i => i.nombreCliente).ToList();
                //OrdenarPorDistancia(lista);
            }
            if (texto.Count() == 0)
            {
                //if (controlGPS)
                //{
                //    searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();

                //}
                //else
                //{
                listViewCobranza.ItemsSource = listaCobranza;

                //}
            }
        }

        List<Cobranza> Filtrar(string busqueda)
        {


            List<Cobranza> listaFiltrada = new List<Cobranza>();
            foreach (var item in listaCobranza)
            {

                if (item.nombreCliente == null)
                {
                    item.nombreCliente = "";
                }
                if (item.codCliente == null)
                {
                    item.codCliente = "";
                }

                if (item.domicilio == null)
                {
                    item.domicilio = "";
                }

                if (item.nombreCliente.ToLower().Contains(busqueda.ToLower())
                    || item.nombreCliente.ToLower().Contains(busqueda.ToLower())
                    || item.codCliente.ToLower().Contains(busqueda.ToLower())
                    || item.domicilio.ToLower().Contains(busqueda.ToLower()))
                {
                    Cobranza c = new Cobranza
                    {
                        cobrado = item.cobrado,
                        codCliente = item.codCliente,
                        cuentaCorriente = item.cuentaCorriente,
                        domicilio = item.domicilio,
                        fechaCobranza = item.fechaCobranza,
                        fechaCobranzaMostrar = item.fechaCobranzaMostrar,
                        idCliente = item.idCliente,
                        idEstado = item.idEstado,
                        idInterno = item.idInterno,
                        idZona = item.idZona,
                        imagenEstado = item.imagenEstado,
                        monto = item.monto,
                        nombreCliente = item.nombreCliente,
                        nombreEstado = item.nombreEstado,
                        nroComprobante = item.nroComprobante,
                        saldo = item.saldo,
                        sincronizado = item.sincronizado,
                        costoCapital = item.costoCapital,
                        costoOperativo = item.costoOperativo
                    };

                    listaFiltrada.Add(c);
                }

            }
            return listaFiltrada;
        }

    }
}