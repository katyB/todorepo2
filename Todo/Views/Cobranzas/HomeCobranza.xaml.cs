﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.Views.Clientes;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Cobranzas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeCobranza : ContentPage
    {
        VMCobranza cobranza = new VMCobranza();

        public HomeCobranza(VMCobranza c)
        {
            InitializeComponent();
            cobranza = c;
            nombreCliente.Title = c.nombreCliente;

        }

        void ClickCobranza(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new AltaCobranza(cobranza));
        }

        void ClickEstadoCuenta(object sender, EventArgs e)
        {
            ClienteApp cliente = new ClienteApp();
            cliente.idCliente = cobranza.idCliente;
            cliente.nombreCliente = cobranza.nombreCliente;
            cliente.codigoCliente = cobranza.codCliente;
            cliente.domicilioCliente = cobranza.domicilio;
            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, 0));
        }
    }
}