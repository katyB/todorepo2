﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Cobranzas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AltaCobranza : ContentPage
    {
        VMCobranza cobranza = new VMCobranza();
        public AltaCobranza(VMCobranza c)
        {
            cobranza = c;
            BindingContext = c;

            InitializeComponent();
            CargarDatos();
        }


        protected override async void OnAppearing()
        {
            indi.IsRunning = false;
            this.Content.IsEnabled = true;
        }

        public void CargarDatos()
        {
            var PrimerCuota = cobranza.cuentaCorriente.FirstOrDefault();

            if (PrimerCuota != null)
            {
                monto.Text = "$" + PrimerCuota.montoComprobante;

                var size = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                gridCuotas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                gridCuotas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                gridCuotas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var Titulocuota = new Label { Text = "Monto", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                var TituloFechaVencimiento = new Label { Text = "Vencimiento", TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                gridCuotas.Children.Add(Titulocuota, 0, 0);
                gridCuotas.Children.Add(TituloFechaVencimiento, 1, 0);

                for (int i = 0; i < cobranza.cuentaCorriente.Count; i++)
                {
                    gridCuotas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                    string monto = cobranza.cuentaCorriente[i].montoComprobante.ToString();
                    string fVenc = cobranza.cuentaCorriente[i].fechaVencimiento.ToString("dd/MM/yy");

                    var cuota = new Label { Text = monto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                    var FechaVencimiento = new Label { Text = fVenc, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

                    gridCuotas.Children.Add(cuota, 0, i + 1);
                    gridCuotas.Children.Add(FechaVencimiento, 1, i + 1);
                }

            }
            else
            {
                DisplayAlert("", "No existen datos de cuotas, por favor comuniquese con administración.", "ok");
            }
        }

        public async void Confirmar(object sender, EventArgs e)
        {
            indi.IsRunning = true;
            this.Content.IsEnabled = false;
            if (!string.IsNullOrWhiteSpace(costoCapital.Text))
            {
                if (int.TryParse(costoCapital.Text, out int costoCap))
                {

                    string cOP = costoOperativo.Text;

                    if (string.IsNullOrWhiteSpace(cOP))
                    {
                        cOP = "0";
                    }

                    if (int.TryParse(cOP, out int costoOP))
                    {
                        var listadoTalonario = await App.Database.GetTalonariosCobranzaAsync();
                        if (listadoTalonario.Count > 0)
                        {
                            var monto = costoCap + costoOP;
                            cobranza.costoCapital = costoCap;
                            cobranza.costoOperativo = costoOP;
                            cobranza.monto = monto;
                            cobranza.saldo = cobranza.saldo - monto;
                            cobranza.fechaCobranza = DateTime.Now;
                            cobranza.fechaCobranzaMostrar = cobranza.fechaCobranza.ToString("dd/MM/yyyy");

                            Cobranza c = new Cobranza
                            {
                                idInterno = cobranza.idInterno,
                                codCliente = cobranza.codCliente,
                                idCliente = cobranza.idCliente,
                                nombreCliente = cobranza.nombreCliente,
                                saldo = cobranza.saldo,
                                fechaCobranza = cobranza.fechaCobranza,
                                fechaCobranzaMostrar = cobranza.fechaCobranzaMostrar,
                                monto = cobranza.monto,
                                nombreEstado = cobranza.nombreEstado,
                                domicilio = cobranza.domicilio,
                                costoCapital = cobranza.costoCapital,
                                costoOperativo = cobranza.costoOperativo

                            };

                            var talonario = listadoTalonario.FirstOrDefault();
                            talonario.usado = true;
                            talonario.fecha = cobranza.fechaCobranza;

                            await App.Database.SaveITalonarioCobranzaAsync(talonario);

                            c.nroComprobante = talonario.numeroTalonario;
                            cobranza.nroComprobante = talonario.numeroTalonario;
                            c.idEstado = 7;
                            c.imagenEstado = "DerivadoAdmin.png";
                            c.nombreEstado = "Derivado a Administración";
                            c.cobrado = true;
                            await App.Database.SaveICobranzaAsync(c);

                            //Sincronizacion automatica de clientes cobranza
                            var current = Connectivity.NetworkAccess;
                            var profiles = Connectivity.ConnectionProfiles;
                            if (current == NetworkAccess.Internet)
                            {
                                WSClienteCobranza clienteCobranza = new WSClienteCobranza();
                                await clienteCobranza.EnviarUnClienteCobranza(c);
                                await Navigation.PushAsync(new CobranzaImprimir(cobranza, 0));
                                indi.IsRunning = false;
                                this.Content.IsEnabled = true;
                            }
                            else
                            {
                                await Navigation.PushAsync(new CobranzaImprimir(cobranza, 0));
                                indi.IsRunning = false;
                                this.Content.IsEnabled = true;
                            }
                        }
                        else
                        {
                            indi.IsRunning = false;
                            this.Content.IsEnabled = true;

                            await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                        }
                    }
                    else
                    {
                        indi.IsRunning = false;
                        this.Content.IsEnabled = true;

                    }
                }
                else
                {
                    indi.IsRunning = false;
                    this.Content.IsEnabled = true;
                }
            }
            else
            {
                indi.IsRunning = false;
                this.Content.IsEnabled = true;

                await DisplayAlert("", "Ingrese el costo capital a pagar", "Ok");
            }
        }


        public async void Aviso(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AvisoCobranza(cobranza));
        }


        private async void ControlarCostoCapital(object sender, TextChangedEventArgs e)
        {
            if (costoCapital.Text != "")
            {
                if (int.TryParse(costoCapital.Text, out int monto))
                {
                    if (monto < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        costoCapital.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    costoCapital.Text = "";
                }
            }
        }

        private async void ControlarCostoOperativo(object sender, TextChangedEventArgs e)
        {
            if (costoOperativo.Text != "")
            {
                if (int.TryParse(costoOperativo.Text, out int monto))
                {
                    if (monto < 0)
                    {
                        await DisplayAlert("", "Ingrese un valor mayor a 0", "Ok");
                        costoOperativo.Text = "";
                    }
                }
                else
                {
                    await DisplayAlert("", "Ingrese un valor entero", "Ok");
                    costoOperativo.Text = "";
                }
            }

        }
    }
}