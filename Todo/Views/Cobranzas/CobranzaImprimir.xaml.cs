﻿using Forms9Patch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Cobranzas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CobranzaImprimir : ContentPage
    {
        VMCobranza cobranza = new VMCobranza();
        int flag;
        public CobranzaImprimir(VMCobranza c, int fl)
        {
            flag = fl;
            cobranza = c;
            InitializeComponent();
            BindingContext = cobranza;
            CargarDatos();

        }

        public void CargarDatos()
        {
            Fecha.Text = cobranza.fechaCobranza.ToString("dd/MM/yy");
            Hora.Text = cobranza.fechaCobranza.ToString("HH:mm:ss");
        }

        protected override bool OnBackButtonPressed()
        {
            if (flag == 0)
            {
                this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                this.Navigation.PopAsync();
            }
            else
            {
                if (flag == 1)
                {
                    this.Navigation.PopAsync();
                }

            }

            return true;
        }

        public async void Imprimir(object sender, EventArgs e)
        {

            Forms9Patch.WebViewPrintEffect.ApplyTo(webView);
            var htmlSource = new HtmlWebViewSource();
            string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
            html += " <table style = 'width: 100 %'>";
            html += "<tr colspan=2>";
            html += "<td><strong>Comprobante: </strong>" + cobranza.nroComprobante + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><strong>Fecha: </strong>" + cobranza.fechaCobranza.ToString("dd/MM/yy") + "</td>";
            html += "<td><strong>Hora: </strong>" + cobranza.fechaCobranza.ToString("HH:mm:ss") + "</td>";
            html += "</tr>";
            html += "<tr colspan=2>";
            html += "<td><strong>Cliente: </strong>" + cobranza.nombreCliente + "</td>";
            html += "</tr>";
            html += "<tr colspan=2>";
            html += "<td><strong>Monto Pagado: </strong>$" + cobranza.monto + "</td>";
            html += "</tr>";
            html += "<tr colspan=2>";
            html += "<td><strong>Monto Adeudado: </strong>$" + cobranza.saldo + "</td>";
            html += "</tr>";
            html += "</table>";

            htmlSource.Html = html;
            webView.Source = htmlSource;

            if (await webView.ToPdfAsync(DateTime.Now.ToString("ddMMyyyyhhmmss")) is ToFileResult pdfResult)
            {
                await Share.RequestAsync(new ShareFileRequest
                {
                    Title = "Archivo Pdf",
                    File = new ShareFile(pdfResult.Result)
                });

                if (flag == 0)
                {
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    await this.Navigation.PopAsync();
                }
                else
                {
                    if (flag == 1)
                    {
                        await this.Navigation.PopAsync();
                    }

                }

            }
        }
    }
}