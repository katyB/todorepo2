﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;
using Xamarin.Essentials;
using Forms9Patch;
using Todo.ViewsModels;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscarProductoView : ContentPage
	{
        List<Models.Producto> listaProductos = new List<Models.Producto>();
        Models.Producto productoSeleccionado = new Models.Producto();

        public BuscarProductoView ()
		{
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            listaProductos = new List<Models.Producto>();
            productoSeleccionado = null;
            SLgrid.IsVisible = false;
            botonDevolver.IsVisible = false;
            var listadoBDlocalProductos = await App.Database.GetProductoAsync(); //serv que valida que los productos tengan saldo

            if (listadoBDlocalProductos.Count() != 0)
            {
                foreach (var db in listadoBDlocalProductos)
                {
                    Models.Producto items = new Models.Producto();
                    items.idProducto = db.idProducto;
                    items.codigoProducto = db.codigoProducto;
                    items.nombreProducto = db.nombreProducto;
                    items.idDeposito = db.idDeposito;
                    items.cantidad = db.saldo; // el saldo tiene la cantidad disponble de productos
                    if (db.saldo == 1)
                    {
                        items.imagenUltimoProducto = "Amarillo.png";
                    }
                    listaProductos.Add(items);
                }

                listaProductos.OrderBy(i => i.nombreProducto);
                searchResults.ItemsSource = listaProductos.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
            }
            else
            {
                await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con Administración.", "Ok");

                await this.Navigation.PushAsync(new HomeView());
            }
        }

        void OnTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;
            string texto = searchBar.Text;
            if (texto.Count() > 2)
            {
                searchResults.ItemsSource = Filtrar(texto).Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
            }
            if (texto.Count() == 0)
            {
                searchResults.ItemsSource = listaProductos.Where(i => i.cantidad > 0).OrderBy(i => i.nombreProducto).ToList();
            }

        }

        List<Models.Producto> Filtrar(string busqueda)
        {
            List<Models.Producto> listaFiltrada = new List<Models.Producto>();
            foreach (var item in listaProductos)
            {
                if (item.nombreProducto.ToLower().Contains(busqueda.ToLower()) || item.codigoProducto.ToString().Contains(busqueda))
                {
                    Models.Producto p = new Models.Producto();
                    p.idProducto = item.idProducto;
                    p.codigoProducto = item.codigoProducto;
                    //p.cantidad = item.saldo; //ya no es cantidad es saldo donde envia la cantidad de productos.
                    p.cantidad = item.cantidad; //cambio introducido por omar porque lo anterior no funciona
                    if (item.saldo == 1)
                    {
                        p.imagenUltimoProducto = "Amarillo.png";
                    }
                    p.nombreProducto = item.nombreProducto;
                    listaFiltrada.Add(p);
                }

            }
            return listaFiltrada;
        }

        async void Seleccionar(object sender, EventArgs e)
        {
            productoSeleccionado = (Models.Producto)searchResults.SelectedItem;
            Xamarin.Forms.Grid grid = new Xamarin.Forms.Grid { VerticalOptions = LayoutOptions.CenterAndExpand };

            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            var medium = Device.GetNamedSize(NamedSize.Small, typeof(Xamarin.Forms.Label));
            var labelNombre = new Xamarin.Forms.Label { Text = "Producto:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var nombre = new Xamarin.Forms.Label { Text = productoSeleccionado.nombreProducto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var labelCodigo = new Xamarin.Forms.Label { Text = "Código:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var codigo = new Xamarin.Forms.Label { Text = productoSeleccionado.codigoProducto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var labelCantidad = new Xamarin.Forms.Label { Text = "Stock:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var cantidad = new Xamarin.Forms.Label { Text = productoSeleccionado.cantidad.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };


            //forma pago del Producto

            var formasPago = await App.Database.GetFormaPagoProductosAsync();
            var respuesta = await App.Database.GetFormaPagoProductoAsync(productoSeleccionado.idProducto);

            respuesta = respuesta.OrderBy(r => r.nombreFormaPago).ToList();

            if (SLgrid.Children.Count() > 0)
            {
                SLgrid.Children.RemoveAt(0);
            }

            grid.Children.Add(labelNombre, 0, 0);
            grid.Children.Add(nombre, 1, 0);
            grid.Children.Add(labelCodigo, 0, 1);
            grid.Children.Add(codigo, 1, 1);
            grid.Children.Add(labelCantidad, 0, 2);
            grid.Children.Add(cantidad, 1, 2);

            SLgrid.Children.Add(grid);
            for (int i = 0; i < respuesta.Count(); i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var Left = new Xamarin.Forms.Label { Text = respuesta[i].nombreFormaPago,FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };

                int precioCuota = 0;
                if (respuesta[i].cantidadCuotas > 0)
                {
                    precioCuota = (int)(respuesta[i].montoVenta / respuesta[i].cantidadCuotas);
                }
                else
                {
                    precioCuota = respuesta[i].montoVenta;
                }

                var Rigth = new Xamarin.Forms.Label { Text = "$"+ precioCuota.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
                grid.Children.Add(Left, 0, i + 3);
                grid.Children.Add(Rigth, 1, i + 3);
            }
            grid.IsVisible = true;
            SLgrid.IsVisible = true;
            botonDevolver.IsVisible = true;
        }

        public async void Devolver(object sender, EventArgs e)
        {
            if(productoSeleccionado.codigoProducto != null)
            {
                await Navigation.PushAsync(new DevolverProducto(productoSeleccionado));
            }
            else
            {
                await DisplayAlert("", "Seleccione un producto", "Ok");
            }
        }

        async void GenerarPdf(object sender, EventArgs e)
        {

            Forms9Patch.WebViewPrintEffect.ApplyTo(webView);

            var listadoProductosConSaldo = await App.Database.GetProductosConSaldo();
            var fecha = DateTime.Now;
            var nombreUsuario = Preferences.Get("usuarioLogueado", "");
            var htmlSource = new HtmlWebViewSource();
            string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
            html += " <table style = 'width: 100 %'>";
            html += "<tr colspan=2>";
            html += "<td cols><strong>Vendedor: </strong>" + nombreUsuario + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><strong>Fecha: </strong>" + fecha.ToString("dd/MM/yyyy") + "</td>";
            html += "<td><strong>Hora: </strong>" + fecha.ToString("HH:mm:ss") + "</td>";
            html += "</tr>";

            html += "</table>";


            html += " <table style = 'width: 100 %'>";
            html += "<tr>";
            html += "<th>Código</th>";
            html += "<th>Producto</th>";
            html += "<th>Stock</th>";
            html += "</tr>";
            var Cantidad = 0;
            foreach (var item in listadoProductosConSaldo)
            {
                Cantidad = Cantidad + item.saldo;
                html += "<tr>";
                html += "<td style='text-align:center'>" + item.codigoProducto + "</td>";
                html += "<td>" + item.nombreProducto + "</td>";
                html += "<td style='text-align:center'>" + item.saldo + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += " <table style = 'width: 100 %'>";
            html += "<tr>";
            html += "<td><b>Cantidad Productos: </b><td>";
            html += "<td>"+Cantidad+"<td>";
            html += "</table>";
            htmlSource.Html = html;
            webView.Source = htmlSource;


            var current = Connectivity.NetworkAccess;

            var estado = await Permissions.RequestAsync<Permissions.StorageWrite>();
            string nombre = fecha.ToString("ddMMyyyyssmmhh");
            if (await webView.ToPdfAsync(nombre) is ToFileResult pdfResult)
            {
                await Share.RequestAsync(new ShareFileRequest
                {
                    Title = nombre,
                    File = new ShareFile(pdfResult.Result)
                });


            }
        }
    }
}