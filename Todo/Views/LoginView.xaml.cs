﻿using System;
using System.Net.Http;
using Todo.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;
using Xamarin.Essentials;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : ContentPage
	{
        public static string usuario = "";
        public static string password = "";
        ILoginManager iml = null;

        private const string Url = "http://biancogestion.com:3630/api/Usuario/Verificar?";
        private const string Urldeposito = "http://biancogestion.com:3630/api/Vendedor/BuscarDepositoPorVendedor?";
        //private const string Url = "http://biancogestion.com/api/Usuario/Verificar?";
        //private const string Urldeposito = "http://biancogestion.com/api/Vendedor/BuscarDepositoPorVendedor?";

        static HttpClient _client = new HttpClient();

        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
            var currentVersion = VersionTracking.CurrentVersion;
            version.Text = currentVersion;
        }



        public LoginView(ILoginManager ilm)
        {
            InitializeComponent();
            iml = ilm;
        }

        async void btnLoginClick(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            usuario = Username.Text;
            password = Password.Text;
            if (!string.IsNullOrEmpty(usuario))
            {
                if (!string.IsNullOrEmpty(password)) {

                    WSUser client = new WSUser();
                    var result = await client.Get<WSUser>(Url + "usuario=" + usuario + "&password=" + password);

                    if (result != null)
                    {
                        Preferences.Set("Name", result.ToString());
                        Preferences.Set("vendedor", result.ToString());
                        Preferences.Set("IsLoggedIn", true);

                        var idusuarioLogueado = Preferences.Get("idusuarioLogueado", 0);

                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        iml.ShowMainPage();
                    }
                    else
                    {
                        await DisplayAlert("", "Usuario y/o contraseña incorrecto. Por favor intente otra vez", "Ok");

                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Por favor ingrese su contraseña", "ok");
                }

            }
            else {
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                await DisplayAlert("","Por favor ingrese su usuario","ok");
            }

          
 

        }
    }
}