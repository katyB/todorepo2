﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.Views.Facturacion;
using Todo.ViewsModels;
using Xamarin.Essentials;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Informacion : ContentPage
	{
		public Informacion ()
		{
			InitializeComponent ();
            CargarDatos();

        }

        async void CargarDatos()
        {
            //Número Versión
            var currentVersion = VersionTracking.CurrentVersion;
            nroVersion.Text = currentVersion;

            //clientes facturacion 
            var lista = await App.Database.GetClientesAsync();
            var cantidadClientes = lista.Count();
            cantClientes.Text = cantidadClientes.ToString();

            //productos
            var listadoBDlocalProductos = await App.Database.GetProductoAsync();
            var cantidadProductos = 0;
            foreach (var item in listadoBDlocalProductos)
            {
                cantidadProductos = cantidadProductos + item.saldo;
            }
            cantProductos.Text = cantidadProductos.ToString();

            //talonarios de facturacion 
            var listadoBDlocalFacturas = await App.Database.GetTalonariosAsync();
            var cantidadTalonariosFacturacion = listadoBDlocalFacturas.Count();
            cantTalonarios.Text = cantidadTalonariosFacturacion.ToString();

            //clientes cobranza
            var listadoBDlocalClienteCobranza = await App.Database.GetCobranzaAsync();
            var cantidadClientesCobranza = listadoBDlocalClienteCobranza.Count();
            cantClientesCobranza.Text = cantidadClientesCobranza.ToString();

            //talonario cobranza
            var listadoBDlocaltalonarioCobranza = await App.Database.GetTalonariosCobranzaAsync();
            var cantidadTalonariosCobranza = listadoBDlocaltalonarioCobranza.Count();
            cantTalonariosCobranza.Text = cantidadTalonariosCobranza.ToString();

            //reclamos
            var listadoBDlocalReclamos = await App.Database.GetItemsAsync();
            var cantidadReclamos = listadoBDlocalReclamos.Count();
            cantReclamo.Text = cantidadReclamos.ToString();

            //departamentos
            var listadoDepartamentos = await App.Database.GetDepartamentoAsync();
            var cantidadDepartamentos = listadoDepartamentos.Count();
            cantDepartamentos.Text = cantidadDepartamentos.ToString();


            //localidades
            var listadoLocalidades = await App.Database.GetLocalidadAsync();
            var cantidadLoclidades = listadoLocalidades.Count();
            cantLocalidad.Text = cantidadLoclidades.ToString();


            //Productos a Cargar
            var idDeposito = Preferences.Get("idDeposito", 0);
            var cantidad = await App.Database.GetInformacionAsync(idDeposito);
            int prodACargar = cantidad.productoCargar;
            productosCargar.Text = prodACargar.ToString();
            if(prodACargar <0)
            {
                productosCargar.TextColor = Color.FromHex("#d9534f");

            }
            else
            {
                productosCargar.TextColor = Color.FromHex("#5cb85c");

            }

        }
    }
}