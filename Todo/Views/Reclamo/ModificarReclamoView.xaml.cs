﻿using System;
using System.Collections.Generic;
using System.Linq;
using Todo.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;

namespace Todo.Views.Reclamo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModificarReclamoView : ContentPage
	{
        List<EstadoReclamo> estadoLista;
        List<Solucion> solucionLista;
        TodoItem reclamo;


        public ModificarReclamoView (TodoItem item)
		{
            reclamo = item;
			InitializeComponent();
            BindingContext = item;
            BuscarEstados();
            BuscarSoluciones();
            setValores();
		}

        private void setValores()
        {

            if (string.IsNullOrWhiteSpace(reclamo.Prioridad))
            {
                PrioridadReclamo.Text = "Sin prioridad asignada";
            }

        }

        public  void BuscarEstados()
        {
            //Aca iria la  conexion con la base de datos si existe la tabla Estados
            estadoLista = new List<EstadoReclamo>();

            //estadoLista = await App.Database.GetEstadosAsync();
            //estadoLista.RemoveAt(2);

            EstadoReclamo estado3 = new EstadoReclamo();
            estado3.idEstado = 3;
            estado3.nombreEstado = "En Proceso";
            estadoLista.Add(estado3);
            EstadoReclamo estado5 = new EstadoReclamo();
            estado5.idEstado = 5;
            estado5.nombreEstado = "Derivado a Admin";
            estadoLista.Add(estado5);
        }

        public void BuscarSoluciones()
        {
            //Aca iria la  conexion con la base de datos si existe la tabla Soluciones
            solucionLista = new List<Solucion>();
            //solucionLista = await App.Database.GetSolucionAsync();

            Solucion sol1 = new Solucion
            {
                CbrSolucion = 1,
                nombre = "Fallado",
                orden = 1
            };
            solucionLista.Add(sol1);
            Solucion sol2 = new Solucion
            {
                CbrSolucion = 2,
                nombre = "No puede pagar",
                orden = 2
            };
            solucionLista.Add(sol2);
            Solucion sol3 = new Solucion
            {
                CbrSolucion = 3,
                nombre = "Zona",
                orden = 3
            };
            solucionLista.Add(sol3);
            Solucion sol4 = new Solucion
            {
                CbrSolucion = 4,
                nombre = "Firma conforme - Pago",
                orden = 4
            };
            solucionLista.Add(sol4);
            Solucion sol5 = new Solucion
            {
                CbrSolucion = 5,
                nombre = "Firma conforme - Comp de pago",
                orden = 5
            };
            solucionLista.Add(sol5);
            Solucion sol6 = new Solucion
            {
                CbrSolucion = 6,
                nombre = "Firma conforme - Descuento por falla",
                orden = 6
            };
            solucionLista.Add(sol6);
            Solucion sol7 = new Solucion
            {
                CbrSolucion = 7,
                nombre = "No esta conforme",
                orden = 7
            };
            solucionLista.Add(sol7);
            Solucion sol8 = new Solucion
            {
                CbrSolucion = 8,
                nombre = "Mala venta - Dom de transito",
                orden = 8
            };
            solucionLista.Add(sol8);
            Solucion sol9 = new Solucion
            {
                CbrSolucion = 9,
                nombre = "Mala venta - Mala referencia",
                orden = 9
            };
            solucionLista.Add(sol9);
            Solucion sol10 = new Solucion
            {
                CbrSolucion = 10,
                nombre = "Mala venta - Apertura de boleta",
                orden = 10
            };
            solucionLista.Add(sol10);
            Solucion sol11 = new Solucion
            {
                CbrSolucion = 11,
                nombre = "No resuelto",
                orden = 11
            };
            solucionLista.Add(sol11);
            Solucion sol12 = new Solucion
            {
                CbrSolucion = 12,
                nombre = "Mala cobranza",
                orden = 12
            };
            solucionLista.Add(sol12);
            Solucion sol13 = new Solucion
            {
                CbrSolucion = 13,
                nombre = "Ya esta resuelto",
                orden = 13
            };
            solucionLista.Add(sol13);
            Solucion sol14 = new Solucion
            {
                CbrSolucion = 14,
                nombre = "Debe pagar",
                orden = 14
            };
            solucionLista.Add(sol14);

            foreach (var item in solucionLista)
            {
                pickerSolucion.Items.Add(item.nombre);
            }

            if (reclamo.CbrSolucion > 0)
            {
                foreach (var item in solucionLista)
                {
                    if (reclamo.CbrSolucion == item.CbrSolucion)
                    {
                        pickerSolucion.SelectedItem = item.nombre;
                    }
                }

            }
        }

        protected override async void OnAppearing()
        {
            indi.IsRunning = false;
            this.Content.IsEnabled = true;
        }

        async void OnSaveClicked(object sender, EventArgs e)
        {
            indi.IsRunning = true;
            this.Content.IsEnabled = false;
            var todoItem = (TodoItem)BindingContext;
            todoItem.idEstado = 5;
            todoItem.Estado = "Derivado a Admin";
            todoItem.nombreImagen = "DerivadoAdmin.png";

            var itemSolucion = pickerSolucion.SelectedItem;
            foreach (var item in solucionLista)
            {
                if (item.nombre == itemSolucion.ToString())
                {
                    todoItem.CbrSolucion = item.CbrSolucion;
                    todoItem.Solucion = item.nombre;
                }
            }
            await App.Database.SaveItemAsync(todoItem);
            await Navigation.PopAsync();
            indi.IsRunning = false;
            this.Content.IsEnabled = true;
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;
            //reclamos
            //if (current == NetworkAccess.Internet)
            if (profiles.Contains(ConnectionProfile.WiFi)== true)
            {
                WSReclamo cliente = new WSReclamo();
                WSEstado estado = new WSEstado();
                WSCliente clientes = new WSCliente();
                //enviar datos
                await cliente.PutReclamo();
            }
            else
            {

            }
        }

        async void OnDeleteClicked(object sender, EventArgs e)
        {
            var todoItem = (TodoItem)BindingContext;
            await App.Database.DeleteItemAsync(todoItem);
            await Navigation.PopAsync();
        }
    }
}