﻿using System;
using System.Collections.Generic;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Reclamo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaReclamoDatosView : ContentPage
	{
        TodoItem reclamo;
        List<Prioridad> prioridadLista;
        List<CausaReclamo> causaLista;

        public AltaReclamoDatosView (TodoItem item)
		{
            reclamo = item;

            InitializeComponent();
            nroTalonarioReclamo.Text = "";
            BindingContext = item;
            BuscarPrioridad();
            BuscarCausa();

        }

        private void BuscarPrioridad()
        {
            //Aca iria la  conexion con la base de datos si existe la tabla prioridad

            prioridadLista = new List<Prioridad>();
            Prioridad pri1 = new Prioridad
            {
                CbrIdPrioridad = 1,
                nombre = "Alta"
            };
            prioridadLista.Add(pri1);
            Prioridad pri2 = new Prioridad
            {
                CbrIdPrioridad = 2,
                nombre = "Media"
            };
            prioridadLista.Add(pri2);
            Prioridad pri3 = new Prioridad
            {
                CbrIdPrioridad = 3,
                nombre = "Baja"
            };
            prioridadLista.Add(pri3);



            foreach (var item in prioridadLista)
            {
                pickerPrioridad.Items.Add(item.nombre);
            }
        }

        private void BuscarCausa()
        {
            //Aca iria la  conexion con la base de datos si existe la tabla causa

            causaLista = new List<CausaReclamo>();
            CausaReclamo cau1 = new CausaReclamo
            {
                idCausa = 1,
                nombreCausa = "Cambio"
            };
            causaLista.Add(cau1);
            CausaReclamo cau2 = new CausaReclamo
            {
                idCausa = 2,
                nombreCausa = "Devolución"
            };
            causaLista.Add(cau2);
            CausaReclamo cau3 = new CausaReclamo
            {
                idCausa = 3,
                nombreCausa = "Retirar"
            };
            causaLista.Add(cau3);

            foreach (var item in causaLista)
            {
                pickerCausa.Items.Add(item.nombreCausa);
            }
        }

        async void OnSaveClicked(object sender, EventArgs e)
        {
            if(pickerPrioridad.SelectedItem == null )
            {
                await DisplayAlert("", "Se debe elegir una prioridad", "Ok");
                
            }
            else
            {
                if (pickerCausa.SelectedItem == null) 
                {
                    await DisplayAlert("", "Se debe elegir una causa", "Ok");
                    
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(observacionReclamo.Text))
                    {
                        await DisplayAlert("", "La observación del reclamo no puede estar vacía", "Ok");
                    }
                    else
                    {
                        if(string.IsNullOrWhiteSpace(nroTalonarioReclamo.Text) || nroTalonarioReclamo.Text == "0")
                        {
                            await DisplayAlert("", "El número de talonario no puede estar vacío o igual a 0", "Ok");
                        }
                        else
                        {
                            indi.IsRunning = true;
                            var item = (TodoItem)BindingContext;

                            foreach (var item2 in causaLista)
                            {
                                if (item2.nombreCausa == item.causaReclmo)
                                {
                                    reclamo.idCausa = item2.idCausa;
                                    reclamo.nombreCausa = item.causaReclmo;
                                }
                            }
                            foreach (var item3 in prioridadLista)
                            {
                                if (item3.nombre == item.Prioridad)
                                {
                                    reclamo.CbrIdPrioridad = item3.CbrIdPrioridad;
                                    reclamo.Prioridad = item.Prioridad;
                                }
                            }
                            reclamo.observacionVta = item.observacionVta;
                            reclamo.nroTalonario = item.nroTalonario;
                            reclamo.Estado = "Ingresado";
                            reclamo.Activo = true;
                            reclamo.Solucion = null;
                            reclamo.nombreImagen = "Ingresado.png";
                            reclamo.idEstado = 1;
                            var fecha = DateTime.Now.Date;
                            reclamo.fechaInicio = fecha.Date;
                            reclamo.fechaReclamoMostrar = reclamo.fechaInicio.ToString("dd/MM/yyyy");
                            await App.Database.SaveItemAsync(reclamo);
                            indi.IsRunning = false;
                            await this.Navigation.PopToRootAsync();
                        }
                    }
                }
            }

        }
    }
}