﻿using System;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Reclamo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaReclamoFacturaView : ContentPage
	{
        TodoItem reclamo;

		public AltaReclamoFacturaView (ClienteApp cliente)
		{
            TodoItem todoItem = new TodoItem
            {
                nroCliente = cliente.codigoCliente,
                domicilioCliente = cliente.domicilioCliente,
                nombreCliente = cliente.nombreCliente
            };
            reclamo = todoItem;
            BindingContext = todoItem;
			InitializeComponent ();
            nroFacturaReclamo.Text = "";
		}

        async void btnContinuar2(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nroFacturaReclamo.Text) || nroFacturaReclamo.Text == "0")
            {
                await DisplayAlert("", "El número de la factura no puede estar vacío o igual a 0", "Ok");
            }
            else
            {
                var item2 = (TodoItem)BindingContext;
                reclamo.nroFactura = item2.nroFactura;

                await this.Navigation.PushAsync(new AltaReclamoDatosView(reclamo));
            }


        }

    }
}