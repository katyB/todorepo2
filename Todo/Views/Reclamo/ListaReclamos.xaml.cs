﻿using System;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Reclamo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListaReclamos : ContentPage
    {
        public ListaReclamos()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            //listView.ItemsSource = await App.Database.GetItemsAsync();
            var lista = await App.Database.GetItemsAsync();
            listView.ItemsSource = lista;
            if(lista.Count != 0)
            {
                frameReclamos.HeightRequest = lista.Count * 45 + 45;
            }
            else
            {
                frameReclamos.IsVisible = false;
            }



        }

        void OnItemAdded(object sender, EventArgs e)
        {
            int flag = 0;
            Navigation.PushAsync(new BuscarCliente(flag)
            {

                BindingContext = new ClienteApp()
            });
        }

        void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {


            if (e.SelectedItem != null)
            {
                TodoItem item = (TodoItem)e.SelectedItem;
                Navigation.PushAsync(new ModificarReclamoView(item));
            }
        }
    }
}
