﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Todo.Models;
using Todo.Views.Clientes;
using Todo.Views.Facturacion;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscarCliente : ContentPage
	{
        int flag;
        List<ClienteApp> listaClientes = new List<ClienteApp>();
        ClienteApp clienteSeleccionado = new ClienteApp();
        bool controlGPS = false;
        public BuscarCliente(int num)
        {
            flag = num;
            InitializeComponent();
            if (flag == 1)
            {
                //si es home cliente no puedo dar alta
                this.ToolbarItems.RemoveAt(0);
            }
            ObtenerTipoBusqueda();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.Content.IsEnabled = true;

            ObtenerListaClientes();

        }

        public void ObtenerTipoBusqueda()
        {


            tipoBusqueda.Items.Add("Nombre");
            tipoBusqueda.Items.Add("Código");
            tipoBusqueda.Items.Add("DNI");
            tipoBusqueda.Items.Add("Dirección");
            tipoBusqueda.Items.Add("Localidad");
            tipoBusqueda.Items.Add("Departamento");
            tipoBusqueda.SelectedItem = "Nombre";
        }

        public async void ObtenerListaClientes()
        {
            var lista = await App.Database.GetClientesAsync();
            if (lista.Count > 0)
            {
                listaClientes = lista;
                //searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();
                ObtenerDistancias(listaClientes);

            }
            else
            {
                buscador.IsVisible = false;
            }
        }



        public async void ObtenerDistancias(List<ClienteApp> listaClientes)
        {

            Location miPosicion = new Location();
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    miPosicion = location;
                }
                controlGPS = true;
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("", "GPS no soportado, no se guardaran las coordenadas", "Ok");
                searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();
                controlGPS = true;
            }

            catch (FeatureNotEnabledException fneEx)
            {
                await DisplayAlert("", "GPS desactivado, por favor encienda el gps", "Ok");
                searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();
                controlGPS = false;
            }
            catch (PermissionException pEx)
            {
                await DisplayAlert("", "Acepte los permisos de GPS para guardar las coordenadas", "Ok");
                searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();
                controlGPS = false;
            }
            catch (Exception ex)
            {

                await DisplayAlert("", "No se pudo obtener las coordenadas", "Ok");
                searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();
                controlGPS = false;
            }

            if (controlGPS)
            {
                foreach (var item in listaClientes)
                {

                    if (item.longitud == 0 || item.latitud == 0)
                    {
                        item.distanciaMetrosTexto = "Sin Datos";
                        item.distanciaMetros = 2147483647;

                    }
                    else
                    {
                        Location posicion = new Location(item.latitud, item.longitud);
                        int distancia = Convert.ToInt32(Location.CalculateDistance(miPosicion, posicion, DistanceUnits.Kilometers) * 1000);
                        item.distanciaMetrosTexto = distancia.ToString("N0", new CultureInfo("is-IS")) + "m";
                        item.distanciaMetros = distancia;
                    }
                }

                searchResults.ItemsSource = listaClientes.OrderBy(i => i.distanciaMetros).ToList();
            }
            else
            {
                searchResults.ItemsSource = listaClientes.OrderBy(i => i.nombreCliente).ToList();

            }
        }

        void OnTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;
            string texto = searchBar.Text;
            if(texto.Count() > 2)
            {
                var lista = Filtrar(texto);
                //searchResults.ItemsSource = Filtrar(texto).OrderBy(i => i.nombreCliente).ToList();
                if (controlGPS)
                {
                    searchResults.ItemsSource = lista.OrderBy(i => i.distanciaMetros).ToList();

                }
                else
                {
                    searchResults.ItemsSource = lista.OrderBy(i => i.nombreCliente).ToList();

                }
            }
            if(texto.Count() == 0)
            {
                if (controlGPS)
                {
                    searchResults.ItemsSource = listaClientes.OrderBy(i => i.distanciaMetros).ToList();

                }
                else
                {
                    searchResults.ItemsSource = Filtrar(texto).OrderBy(i => i.nombreCliente).ToList();

                }
            }
        }
   
        List<ClienteApp> Filtrar (string busqueda)
        {

            var tBusqueda = tipoBusqueda.SelectedItem.ToString();
            List<ClienteApp> listaFiltrada = new List<ClienteApp>();

            if (tBusqueda == "Nombre")
            {
                foreach (var item in listaClientes)
                {
                    if (item.nombreCliente == null)
                    {
                        item.nombreCliente = "";
                    }

                    if (item.nombreCliente.ToLower().Contains(busqueda.ToLower()))
                    {
                        ClienteApp c = new ClienteApp
                        {
                            idClienteApp = item.idClienteApp,
                            idCliente = item.idCliente,
                            codigoCliente = item.codigoCliente,
                            nombreCliente = item.nombreCliente,
                            domicilioCliente = item.domicilioCliente,
                            montoDisponible = item.montoDisponible,
                            montoTotalMaximo = item.montoTotalMaximo,
                            idEstado = item.idEstado,
                            imagenEstado = item.imagenEstado,
                            actividad = item.actividad,
                            actividad2 = item.actividad2,
                            Antiguedad = item.Antiguedad,
                            antiguedadConyugue = item.antiguedadConyugue,
                            departamentoAlternativo = item.departamentoAlternativo,
                            cantidadHijo = item.cantidadHijo,
                            nroDocumento = item.nroDocumento,
                            localidadCliente = item.localidadCliente,
                            nombreDepartamento = item.nombreDepartamento,
                            distanciaMetros = item.distanciaMetros,
                            distanciaMetrosTexto= item.distanciaMetrosTexto,
                            latitud = item.latitud,
                            longitud = item.longitud
                        };

                        listaFiltrada.Add(c);
                    }

                }

            }
            else
            {
                if (tBusqueda == "Código")
                {
                    foreach (var item in listaClientes)
                    {
                        if (item.codigoCliente == null)
                        {
                            item.codigoCliente = "";
                        }

                        if (item.codigoCliente.ToString().ToLower().Contains(busqueda.ToLower()))
                        {
                            ClienteApp c = new ClienteApp
                            {
                                idClienteApp = item.idClienteApp,
                                idCliente = item.idCliente,
                                codigoCliente = item.codigoCliente,
                                nombreCliente = item.nombreCliente,
                                domicilioCliente = item.domicilioCliente,
                                montoDisponible = item.montoDisponible,
                                montoTotalMaximo = item.montoTotalMaximo,
                                idEstado = item.idEstado,
                                imagenEstado = item.imagenEstado,
                                actividad = item.actividad,
                                actividad2 = item.actividad2,
                                Antiguedad = item.Antiguedad,
                                antiguedadConyugue = item.antiguedadConyugue,
                                departamentoAlternativo = item.departamentoAlternativo,
                                cantidadHijo = item.cantidadHijo,
                                nroDocumento = item.nroDocumento,
                                localidadCliente = item.localidadCliente,
                                nombreDepartamento = item.nombreDepartamento
                            };

                            listaFiltrada.Add(c);
                        }
                    }
                }
                else
                {
                    if (tBusqueda == "DNI")
                    {
                        foreach (var item in listaClientes)
                        {
                            if (item.nroDocumento == null)
                            {
                                item.nroDocumento = "";
                            }

                            if (item.nroDocumento.ToLower().Contains(busqueda.ToLower()))
                            {
                                ClienteApp c = new ClienteApp
                                {
                                    idClienteApp = item.idClienteApp,
                                    idCliente = item.idCliente,
                                    codigoCliente = item.codigoCliente,
                                    nombreCliente = item.nombreCliente,
                                    domicilioCliente = item.domicilioCliente,
                                    montoDisponible = item.montoDisponible,
                                    montoTotalMaximo = item.montoTotalMaximo,
                                    idEstado = item.idEstado,
                                    imagenEstado = item.imagenEstado,
                                    actividad = item.actividad,
                                    actividad2 = item.actividad2,
                                    Antiguedad = item.Antiguedad,
                                    antiguedadConyugue = item.antiguedadConyugue,
                                    departamentoAlternativo = item.departamentoAlternativo,
                                    cantidadHijo = item.cantidadHijo,
                                    nroDocumento = item.nroDocumento,
                                    localidadCliente = item.localidadCliente,
                                    nombreDepartamento = item.nombreDepartamento
                                };

                                listaFiltrada.Add(c);
                            }
                        }
                    }
                    else
                    {
                        if (tBusqueda == "Dirección")
                        {
                            foreach (var item in listaClientes)
                            {
                                if (item.domicilioCliente == null)
                                {
                                    item.domicilioCliente = "";
                                }

                                if (item.domicilioCliente.ToLower().Contains(busqueda.ToLower()))
                                {
                                    ClienteApp c = new ClienteApp
                                    {
                                        idClienteApp = item.idClienteApp,
                                        idCliente = item.idCliente,
                                        codigoCliente = item.codigoCliente,
                                        nombreCliente = item.nombreCliente,
                                        domicilioCliente = item.domicilioCliente,
                                        montoDisponible = item.montoDisponible,
                                        montoTotalMaximo = item.montoTotalMaximo,
                                        idEstado = item.idEstado,
                                        imagenEstado = item.imagenEstado,
                                        actividad = item.actividad,
                                        actividad2 = item.actividad2,
                                        Antiguedad = item.Antiguedad,
                                        antiguedadConyugue = item.antiguedadConyugue,
                                        departamentoAlternativo = item.departamentoAlternativo,
                                        cantidadHijo = item.cantidadHijo,
                                        nroDocumento = item.nroDocumento,
                                        localidadCliente = item.localidadCliente,
                                        nombreDepartamento = item.nombreDepartamento
                                    };
                                    listaFiltrada.Add(c);
                                }
                            }
                        }
                        else
                        {
                            if (tBusqueda == "Localidad")
                            {
                                foreach (var item in listaClientes)
                                {
                                    if (item.localidadCliente == null)
                                    {
                                        item.localidadCliente = "";
                                    }

                                    if (item.localidadCliente.ToLower().Contains(busqueda.ToLower()))
                                    {
                                        ClienteApp c = new ClienteApp
                                        {
                                            idClienteApp = item.idClienteApp,
                                            idCliente = item.idCliente,
                                            codigoCliente = item.codigoCliente,
                                            nombreCliente = item.nombreCliente,
                                            domicilioCliente = item.domicilioCliente,
                                            montoDisponible = item.montoDisponible,
                                            montoTotalMaximo = item.montoTotalMaximo,
                                            idEstado = item.idEstado,
                                            imagenEstado = item.imagenEstado,
                                            actividad = item.actividad,
                                            actividad2 = item.actividad2,
                                            Antiguedad = item.Antiguedad,
                                            antiguedadConyugue = item.antiguedadConyugue,
                                            departamentoAlternativo = item.departamentoAlternativo,
                                            cantidadHijo = item.cantidadHijo,
                                            nroDocumento = item.nroDocumento,
                                            localidadCliente = item.localidadCliente,
                                            nombreDepartamento = item.nombreDepartamento
                                        };
                                        listaFiltrada.Add(c);
                                    }
                                }  
                            }
                            else
                            {
                                if (tBusqueda == "Departamento")
                                {
                                    foreach (var item in listaClientes)
                                    {
                                        if (item.nombreDepartamento == null)
                                        {
                                            item.nombreDepartamento = "";
                                        }

                                        if (item.nombreDepartamento.ToLower().Contains(busqueda.ToLower()))
                                        {
                                            ClienteApp c = new ClienteApp
                                            {
                                                idClienteApp = item.idClienteApp,
                                                idCliente = item.idCliente,
                                                codigoCliente = item.codigoCliente,
                                                nombreCliente = item.nombreCliente,
                                                domicilioCliente = item.domicilioCliente,
                                                montoDisponible = item.montoDisponible,
                                                montoTotalMaximo = item.montoTotalMaximo,
                                                idEstado = item.idEstado,
                                                imagenEstado = item.imagenEstado,
                                                actividad = item.actividad,
                                                actividad2 = item.actividad2,
                                                Antiguedad = item.Antiguedad,
                                                antiguedadConyugue = item.antiguedadConyugue,
                                                departamentoAlternativo = item.departamentoAlternativo,
                                                cantidadHijo = item.cantidadHijo,
                                                nroDocumento = item.nroDocumento,
                                                localidadCliente = item.localidadCliente,
                                                nombreDepartamento = item.nombreDepartamento
                                            };

                                            listaFiltrada.Add(c);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return listaFiltrada;
        }

        async void btnContinuar1(object sender, EventArgs e)
        {

            if (clienteSeleccionado.idClienteApp > 0) {

                //Vengo del facturar (flag = 0 )

                if (flag == 0)
                {
                    var cl = await App.Database.GetClientePorDni(clienteSeleccionado.nroDocumento);

                    await Navigation.PushAsync(new ScanDNiCliente(cl, 1)
                    {

                        BindingContext = new ClienteApp()
                    });

                }
                else
                {
                    //Toque Home Cliente (flag = 1)

                    if (flag == 1)
                    {
                        await Navigation.PushAsync(new HomeCliente(clienteSeleccionado));
                    }
                }


            }
            else
            {
                await DisplayAlert("", "Seleccione un cliente", "Ok");
            }

        }

        void Seleccionar(object sender, EventArgs e)
        {
            clienteSeleccionado = (ClienteApp)searchResults.SelectedItem;
            //nombreClienteSel.Text = clienteSeleccionado.nombreCliente;
            //codigoClienteSel.Text = clienteSeleccionado.codigoCliente;
            //imagenClienteSel.Source = clienteSeleccionado.imagenEstado;
            //domicilioClienteSel.Text = clienteSeleccionado.domicilioCliente;
            ////if (controlGPS)
            ////{
            //    //if(clienteSeleccionado.longitud != 0 && clienteSeleccionado.latitud != 0)
            //    //{
            //    //    distanciaClienteSel.Text = "Distancia: " + clienteSeleccionado.distanciaMetros + "m";
            //    //}
            //    //else
            //    //{
            //    //    distanciaClienteSel.Text = "Distancia: Sin datos";

            //    //}
            ////}
            //telClienteSel.Text = "Teléfono: "+clienteSeleccionado.telefono;
            //localidadClienteSel.Text = clienteSeleccionado.localidadCliente;
            //departamentoClienteSel.Text = clienteSeleccionado.nombreDepartamento;
            //dniClienteSel.Text = "DNI: "+clienteSeleccionado.nroDocumento;
            //montoClienteSel.Text = "Monto Disponible: $" + clienteSeleccionado.montoTotalMaximo;
            //mensajeSel.Text = clienteSeleccionado.mensajeEstado; //mensaje que envia Omar segun la situacion de c/ cliente
            //frameClienteSel.IsVisible = true;
        }

        void OnItemAdded(object sender, EventArgs e)
        {
            ClienteApp cl = new ClienteApp();

            //alta desde facturar (flag = 0)
            if (flag == 0)
            {
                Navigation.PushAsync(new ScanDNiCliente(cl, 0));
            }
            //else
            //{

            //    //home desde facturar (flag = 1)
            //    if (flag == 1)
            //    {
            //        Navigation.PushAsync(new ScanDNiCliente(cl, 7));
            //    }
            //}
        }

        void CambiaPicker(object sender, EventArgs e)
        {
            searchbar.Text = "";
        }

        //async void sync(object sender, EventArgs e)
        //{
        //    this.Content.IsEnabled = false;
        //    indi.IsRunning = true;

        //    var current = Connectivity.NetworkAccess;
        //    if (current == NetworkAccess.Internet)
        //    {
        //        MessagingCenter.Subscribe<PopUpSincronizarClientesPorLocalidad, int>(this, "Sincronizar Clientes por Localidad", async (page, respuesta) =>
        //        {
        //            MessagingCenter.Unsubscribe<PopUpSincronizarClientesPorLocalidad, int>(this, "Sincronizar Clientes por Localidad");
        //            this.Content.IsEnabled = false;
        //            indi.IsRunning = true;
        //            if (respuesta != -1)
        //            {
        //                //Aca va la sincronizacion



        //                string msj = "Sincronización Finalizada";
        //                this.Content.IsEnabled = true;
        //                indi.IsRunning = false;
        //                await Navigation.PushPopupAsync(new PopUpMensaje(msj));
        //                ObtenerListaClientes();
        //            }
        //            else
        //            {if (respuesta == 0)
        //                {
        //                    await DisplayAlert("","Ocurrió un error inesperado","ok");
        //                }
        //                else
        //                {
        //                    this.Content.IsEnabled = true;
        //                    indi.IsRunning = false;
        //                }

        //            }

        //        });

        //        this.Content.IsEnabled = true;
        //        indi.IsRunning = false;

        //        await Navigation.PushPopupAsync(new PopUpSincronizarClientesPorLocalidad());
        //    }
        //    else
        //    {
        //        this.Content.IsEnabled = true;
        //        indi.IsRunning = false;
        //        await DisplayAlert("", "Sin acceso a internet, no puede sincronizar clientes por Localidad", "ok");
        //    }
        //}
    }
}





