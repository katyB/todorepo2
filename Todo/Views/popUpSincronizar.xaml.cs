﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class popUpSincronizar : PopupPage
    {
        float progressmaxSinc = 14;
        bool istimerRunning = true;
        float progress;
        decimal porcentaje;
        int counter;
        int flag;

        public popUpSincronizar ()
		{
			InitializeComponent ();
            mensaje();
            progress = 0;
            porcentaje = 0;
            counter = 0;
            progressLabel.Text = porcentaje + "%";

        }

        public void mensaje()
        {

            //comentario

            MessagingCenter.Subscribe<Sincronizacion, int>(this, "Avance", (page, avance) =>
            {
                if(avance != -1)
                {
                    counter += avance;

                    progress = counter / progressmaxSinc;
                    porcentaje = Math.Truncate((decimal)(counter / progressmaxSinc * 100));
                    progressbar.ProgressTo(progress, 500, Easing.Linear);
                    progressLabel.Text = porcentaje + "%";
                    //counteLabel.Text = counter.ToString();
                    if (progress >= 1)
                    {
                        MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");

                        istimerRunning = false;

                        if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
                        {
                            Navigation.PopPopupAsync();
                        }
                    }
                }
                else
                {
                    MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");
                    ProgressFrame.IsVisible = false;
                    ErrorFrame.IsVisible = true;
                }

            });
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }
        protected override bool OnBackButtonPressed()
        {
            MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");
            if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
            {
                Navigation.PopPopupAsync();
            }
            return true;
        }


        public async void Ok(object sender, EventArgs e)
        {
            if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
            {
                await Navigation.PopPopupAsync();
            }
        }
    }
}