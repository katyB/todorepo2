﻿using System;
using Todo.Views.Cobranzas;
using Todo.Views.Facturacion;
using Todo.Views.Reclamo;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;
using Xamarin.Essentials;
using Rg.Plugins.Popup.Extensions;
using Todo.Views.Producto;
using System.Timers;

namespace Todo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : ContentPage
    {
        private bool _canClose = true;
        private string nombreUsuario;
        private string nombreDeposito;
        public static string NombreUsuario;
        WSDeposito deposito = new WSDeposito();
        bool puedeEntrar;

        public HomeView()
        {
            InitializeComponent();
            modificarNavigationBar();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            indi.IsRunning = false;
            this.Content.IsEnabled = true;
            puedeEntrar = true;
            //var tiempo = Preferences.Get("Tiempo", "");
            //DisplayAlert("", "Se cierra en " + tiempo + " segundos", "ok");
        }

        async public void modificarNavigationBar()
        {
            nombreUsuario = Preferences.Get("usuarioLogueado", "");
            navigationBar.Title = nombreUsuario;
        }

        void btnLogoutClick(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                App.Current.Logout();

            }
        }

        void ClickFacturacion(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                this.Navigation.PushAsync(new ListaFacturas());

            }
        }

        void ClickCobranza(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                this.Navigation.PushAsync(new ListaCobranza());

            }
        }

        void ClickReclamos(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                this.Navigation.PushAsync(new ListaReclamos());

            }
        }

        void ClickInformacion(object sender, EventArgs e)
        {
            if (puedeEntrar){
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                this.Navigation.PushAsync(new Informacion());

            }
        }

        async void ClickCliente(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                var listadoClientes = await App.Database.GetClientesAsync();
                //await this.Navigation.PushAsync(new BuscarCliente(1));
                if (listadoClientes.Count == 0)
                {
                    await DisplayAlert("", "No posee clientes disponibles. Por favor, comuníquese con administración", "Ok");
                    await this.Navigation.PushAsync(new BuscarCliente(1));
                }
                else
                {
                    await this.Navigation.PushAsync(new BuscarCliente(1));

                }

            }


        }

        async void ClickProducto(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                indi.IsRunning = true;
                this.Content.IsEnabled = false;
                var listaProductos = await App.Database.GetProductoAsync();
                if (listaProductos.Count > 0)
                {
                    await this.Navigation.PushAsync(new HomeProducto());
                }
                else
                {
                    await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con Administración.", "Ok");
                    puedeEntrar = true;
                }

            }

        }

        async void ClickMantenimiento(object sender, EventArgs e)
        {

            var answer = await DisplayAlert("", "¿Esta seguro que desea mantener? No podrá consultar las facturas emitidas", "Si", "No");
            if (answer)
            {
                Sincronizacion sin = new Sincronizacion();
                var res = sin.btnSincronizarParcial();
                if (res.IsCompleted)
                {
                    await DisplayAlert("", "Necesita conexión Wifi para sincronizar", "Ok");
                }
                else
                {
                    await Navigation.PushPopupAsync(new popupMantenimiento());
                }
            }
        }

        async void ClickSincronizar(object sender, EventArgs e)
        {
            Sincronizacion sin = new Sincronizacion();
            var res = sin.btnSincronizarClick();
            if (res.IsCompleted)
            {
                await DisplayAlert("", "Necesita conexión Wifi para sincronizar", "Ok");
            }
            else
            {
                await Navigation.PushPopupAsync(new popUpSincronizar());
            }

        }

        protected override bool OnBackButtonPressed()
        {
            if(this.Navigation.NavigationStack.Count == 1)
            {
                CerrarSesion();

            }
            else
            {
                this.Navigation.PopAsync();
            }
            return true;
        }

        private async void CerrarSesion()
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                var answer = await DisplayAlert("", "¿Desea cerrar sesión?", "Si", "No");
                if (answer)
                {
                    App.Current.Logout();
                }
                else
                {
                    puedeEntrar = true;
                }
            }

        }




    }
}