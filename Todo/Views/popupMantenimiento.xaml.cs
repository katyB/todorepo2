﻿ using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;

using System.Linq;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class popupMantenimiento : PopupPage
    {
        float progressmaxMant = 19;
        bool istimerRunning = true;
        float progress;
        decimal porcentaje;
        int counter;


        public popupMantenimiento ()
		{
			InitializeComponent ();
            mensaje();
            progress = 0;
            porcentaje = 0;
            counter = 0;
            progressLabel.Text = porcentaje + "%";

        }

        public void mensaje()
        {

            MessagingCenter.Subscribe<Sincronizacion, int>(this, "Avance", (page, avance) =>
            {
                if(avance != -1)
                {
                    counter += avance;

                    progress = counter / progressmaxMant;
                    porcentaje = Math.Truncate((decimal)(counter / progressmaxMant * 100));
                    progressbar.ProgressTo(progress, 500, Easing.Linear);
                    progressLabel.Text = porcentaje + "%";
                    //counteLabel.Text = counter.ToString();

                    if (progress >= 1)
                    {
                        MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");

                        istimerRunning = false;

                        if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
                        {
                            Navigation.PopPopupAsync();
                        }
                    }
                }
                else
                {
                    MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");
                    ProgressFrame.IsVisible = false;
                    ErrorFrame.IsVisible = true;

                }

            });
        }
        protected override bool OnBackgroundClicked()
        {
            return false;
        }

        protected override bool OnBackButtonPressed()
        {
            MessagingCenter.Unsubscribe<Sincronizacion, int>(this, "Avance");
            if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
            {
                Navigation.PopPopupAsync();
            }
            return true;
        }


        public async void Ok(object sender, EventArgs e)
        {
            if (Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopupStack.Any())
            {
                await Navigation.PopPopupAsync();
            }
        }
    }
}
