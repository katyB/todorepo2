﻿using System;
using System.Linq;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Forms9Patch;
using Xamarin.Essentials;
using Todo.Views.Clientes;
using Todo.Models;
using System.IO;
using System.Text;
using Todo.Imprimir;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaFacturaImprimir : ContentPage
	{
        ClienteApp cliente = new ClienteApp();
        VMFactura factura = new VMFactura();
        int flag;
        int bandera = 1;
        string texto;
        bool puedeEntrar;

        public AltaFacturaImprimir(VMFactura f, int fl)
		{

            flag = fl;
            factura = f;
            InitializeComponent ();
            this.Content.IsEnabled = false;
            BindingContext = factura;

            string nombreVendedor = Preferences.Get("usuarioLogueado", ""); 
            string telefonoVendedor  = Preferences.Get("telefono", "");

            CargarDatos();
            Forms9Patch.WebViewPrintEffect.ApplyTo(webView);
            var htmlSource = new HtmlWebViewSource();
            string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
                html += " <table style = 'width: 100 %'>";
            html += "<tr>";
            html += "<td><strong>Nro factura: </strong>"+ factura.nroFactura + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><strong>Fecha: </strong>" + factura.fechaFacturaMostrar + "</td>";
            html += "<td><strong>Hora: </strong>" + factura.horaFacturaMostrar + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td colspan=2><strong>Vendedor: </strong>" + nombreVendedor + "</td>";
            html += "</tr>";
                html += "<tr>";
                html += "<td colspan=2><strong>Teléfono Vendedor: </strong>" + telefonoVendedor + "</td>";
                html += "</tr>";

            html += "<tr>";
            html += "<td colspan=2><strong>Nombre: </strong>" + factura.nombreCliente + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td colspan=2><strong>Dni: </strong>" + factura.dniCliente + "</td>";
            html += "</tr>";
            html += "</table>";
                html += "<table style='width: 100 % '>";

            foreach (var item in factura.listaProductos)
            {
                html += "<tr>";
                html += "<td>" + item.cantidadComprada + "x <strong>" + item.nombreProducto + "</strong></td>";
                html += "<td> $" + item.precioTotal + "</td>";
                html += "</tr>";
            }
            html += "<tr></tr>";
            html += "<tr>";
            html += "<td><strong> Total: </strong></td>";
            html += "<td><strong>$" + factura.montoFactura + "</strong></td>";
            html += "</tr>";
            html += "</table>";



            if (factura.totalContado > 0 || factura.anticipo > 0 || factura.totalDebito > 0 || factura.totalCredito > 0)
            {
                html += " <table style = 'width: 100 %'>";
                if (factura.totalContado > 0 || factura.anticipo > 0)
                {
                    html += "<tr>";
                    if (factura.totalContado > 0)
                    {
                        html += "<td><strong>Total Contado: $</strong> " + factura.totalContado + "</td>";
                    }
                    if (factura.anticipo > 0)
                    {
                        html += "<td><strong>Anticipo: $</strong> " + factura.anticipo + "</td>";
                    }
                    html += "</tr>";
                }
                if (factura.totalDebito > 0 || factura.totalCredito > 0)
                {
                    html += "<tr>";
                    if (factura.totalDebito > 0)
                    {
                        html += "<td><strong>Total Débito: $</strong> " + factura.totalDebito + "</td>";

                    }
                    if (factura.totalCredito > 0)
                    {
                        html += "<td><strong>Total Crédito: $</strong> " + factura.totalCredito + "</td>";
                    }
                    html += "</tr>";


                }
                html += "</table>";
            }



            if (factura.FormaPago == 2)
            {
                html += "<table>";
                html += "<tr>";
                html += "<th style='text-align: center;'> Cuota </th>";
                html += "<th style='text-align: center;'> Monto </th>";
                html += "<th style='text-align: center;'> Fecha </th>";
                html += "</tr>";
                foreach (var item in factura.listaFacturaCuota)
                {
                    html += "<tr>";
                    html += "<td style='text-align: center;'> " + item.nombreCuota + "</td>";
                    html += "<td  style='text-align: center;'>" + item.monto + "</td>";
                    html += "<td style='text-align: center;'>" + item.fechaVto.ToString("dd/MM/yyyy") + "</td>";
                    html += "</tr>";
                }
                html += "</table>";
            }

            htmlSource.Html = html;
            webView.Source = htmlSource;
            texto = html;
            this.Content.IsEnabled = true;
        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        protected override bool OnBackButtonPressed()
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                var current = Connectivity.NetworkAccess;

                switch (flag)
                {
                    case 0:
                        //Facure desde el alta financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);

                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }

                        break;
                    case 1:
                        //Seleccionar cliente desde facturar financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 2:
                        //facturar desde Home Cliente financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 3:
                        //Facure desde el alta contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);

                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }

                        break;
                    case 4:
                        //Seleccionar cliente desde Facturar contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 5:
                        // ver factura desde el lsitadop factura
                        this.Navigation.PopAsync();
                        break;
                    case 6:
                        //facturar desde Home Cliente contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 7:
                        //Alta desde home cliente financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);

                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 8:
                        //Alta desde home cliente contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 9:
                        //Retomar Factura
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 10:
                        //Facturar desde simulador
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.InsertPageBefore(new ListaFacturas(), this);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {

                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 11:
                        //Monto Superado en el simulador
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {

                            this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;

                }
            }
            return true;
        }

        public async void CargarDatos()
        {

            if(factura.anulado == false)
            {
                if (factura.fechaFactura.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                {
                    conAnular.IsVisible = true;
                }
                else
                {
                    sinAnular.IsVisible = true;
                }
            }


            //Busco datos cliente
            cliente = await App.Database.GetClientePorDni(factura.dniCliente.ToString());

            //Cargo título factura
            tituloFactura.Title = "Factura n° " + factura.nroFactura;

            //Verifico si el domicilio es nulo
            if(factura.domicilioCliente == null)
            {
                domicilioCliente.IsVisible = false;
                labelDomicilio.IsVisible = false;
            }

            //Cargo elemntos en el listview de los productos
            ProductosListview.ItemsSource = factura.listaProductos;
            ProductosListview.HeightRequest = 50 * factura.listaProductos.Count();

            //Verifico si hay contado

            if(factura.totalContado > 0)
            {
                labelContado.IsVisible = true;
                contado.IsVisible = true;
            }
            if(factura.FormaPago == 1 || factura.FormaPago == 2 )
            {
                //Verifico si hay crédito

                if (factura.totalCredito > 0)
                {
                    labelCredito.IsVisible = true;
                    credito.IsVisible = true;
                }


                //Verifico si hay débito

                if (factura.totalDebito > 0)
                {
                    labeldebito.IsVisible = true;
                    debito.IsVisible = true;
                }

                if(factura.FormaPago == 2)
                {
                    //Verifico si hay anticpo

                    if (factura.anticipo > 0)
                    {
                        labelAnticipo.IsVisible = true;
                        anticipo.IsVisible = true;
                    }

                    //Creo la tabla de facturaCuota
                    if (factura.listaFacturaCuota.Count > 0)
                    {
                        gridTabla.IsVisible = true;

                        var size = Device.GetNamedSize(NamedSize.Medium, typeof(Xamarin.Forms.Label));
                        gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                        var TituloCuota = new Xamarin.Forms.Label { Text = "Cuota", TextColor = Xamarin.Forms.Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                        var TituloMonto = new Xamarin.Forms.Label { Text = "Monto", TextColor = Xamarin.Forms.Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                        var TituloFecha = new Xamarin.Forms.Label { Text = "Fecha", TextColor = Xamarin.Forms.Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                        gridTabla.Children.Add(TituloCuota, 0, 0);
                        gridTabla.Children.Add(TituloMonto, 1, 0);
                        gridTabla.Children.Add(TituloFecha, 2, 0);


                        for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
                        {
                            gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                            var Cuota = new Xamarin.Forms.Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Xamarin.Forms.Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                            var Monto = new Xamarin.Forms.Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Xamarin.Forms.Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                            var Fecha = new Xamarin.Forms.Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Xamarin.Forms.Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                            gridTabla.Children.Add(Cuota, 0, i + 1);
                            gridTabla.Children.Add(Monto, 1, i + 1);
                            gridTabla.Children.Add(Fecha, 2, i + 1);
                        }
                    }
                }


            }
        }

        public void botonConAnular_Clicked(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                ShareButton_Clicked(sender, e);

            }
        }

        public void botonSinAnular_Clicked(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                ShareButton_Clicked(sender, e);

            }
        }

            async void ShareButton_Clicked(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            var current = Connectivity.NetworkAccess;

            var estado = await Permissions.RequestAsync<Permissions.StorageWrite>();

            if (await webView.ToPdfAsync(factura.nroFactura.ToString()) is ToFileResult pdfResult)
            {
                await Share.RequestAsync(new ShareFileRequest
                {
                    Title = "Archivo Pdf",
                    File = new ShareFile(pdfResult.Result)
                });


                //DependencyService.Get<IPrintService>().Print(webView, "GIS Succinctly.pdf");


                //webView.Print("nombre.pdf");

                switch (flag)
                {
                    case 0:
                        //Alta cliente desde facturar financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {

                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;

                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 1:
                        //Seleccionar cliente desde Facturar financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 2:
                        //facturar desde Home Cliente financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 3:
                        //Facture desde el alta contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }

                        break;
                    case 4:
                        //Seleccionar cliente desde Facturar contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {

                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 5:
                        this.Content.IsEnabled = true;
                        await this.Navigation.PopAsync();
                        break;
                    case 6:
                        //facturar desde Home Cliente contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 7:
                        //Alta desde home cliente financiado
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 8:
                        //Alta desde home cliente contado/debito/credito
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 9:
                        //Retomar Factura
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 10:
                        //Facturar desde simulador
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.InsertPageBefore(new ListaFacturas(), this);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                    case 11:
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                        if (current == NetworkAccess.Internet && cliente != null)
                        {
                            await this.Navigation.PushAsync(new EstadoCuentaCorriente(cliente, bandera));
                            this.Content.IsEnabled = true;
                        }
                        else
                        {

                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        break;
                }

            }
        }

        public async void AnularFactura(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                //var listadoTalonario = await App.Database.GetTalonariosAsync();
                //if (listadoTalonario.Count > 0)
                //{
                //    var talonario1 = listadoTalonario.FirstOrDefault();
                //    talonario1.anulado = true;
                //    DateTime fecha = DateTime.Now;
                //    talonario1.fecha = fecha;
                //    await App.Database.SaveITalonarioAsync(talonario1);
                //    await DisplayAlert("", "Factura n°" + talonario1.nroFactura + " anulada", "Ok");
                //    cancelarFacturaTemporal(talonario1.idFactura);
                //    await this.Navigation.PopToRootAsync();



                //}
                //else
                //{
                //    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                //}

                this.Content.IsEnabled = false;
                var talonario = await App.Database.GetTalonario(factura.idFactura);
                var facturaModelo = await App.Database.GetFacturaPorIdAsync(factura.idFactura);
                if (talonario != null && facturaModelo != null)
                {
                    talonario.anulado = true;
                    DateTime fecha = DateTime.Now;
                    talonario.fecha = fecha;
                    await App.Database.SaveITalonarioAsync(talonario);

                    facturaModelo.anulado = true;
                    facturaModelo.nombreImagenFactura = "Rojo.png";
                    facturaModelo.sincronizado = false;
                    facturaModelo.idEstado = 13;
                    facturaModelo.montoFactura = 0;
                    await App.Database.SaveIFacturaAsync(facturaModelo);
                    await Navigation.PopAsync();
                    this.Content.IsEnabled = true;
                    foreach (var item in factura.listaProductos)
                    {
                        Models.Producto p = await App.Database.GetProductoAsync(item.idProducto);
                        p.saldo = p.saldo + item.cantidadComprada;
                        await App.Database.SaveIProductoAsync(p);
                    }
                }
                else
                {
                    await DisplayAlert("", "Número de factura inexistente", "ok");
                    await Navigation.PopAsync();
                    this.Content.IsEnabled = true;
                }
            }


        }

    }
}






