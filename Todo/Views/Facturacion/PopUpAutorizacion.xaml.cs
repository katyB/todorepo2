﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;


namespace Todo.Views.Facturacion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpAutorizacion : PopupPage
    {
        VMAutorizacionCliente autorizacion = new VMAutorizacionCliente();
        int flag;
        public PopUpAutorizacion(VMAutorizacionCliente aut,int fl)
        {
            InitializeComponent();
            autorizacion = aut;
            flag = fl;
        }

        protected override void OnAppearing()
        {
            var size = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
            var Mensaje = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.StartAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
            if(autorizacion != null)
            {
                if (!string.IsNullOrWhiteSpace(autorizacion.mensaje))
                {
                    Mensaje.Text = autorizacion.mensaje;

                    slMensaje.Children.Add(Mensaje);
                    if (autorizacion.tieneConexion)
                    {

                        if (autorizacion.autorizado || flag == 1)
                        {
                            var Monto = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                            var cantReclamos = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                            var cantCompras = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                            Monto.Text = "Monto Autorizado: $" + autorizacion.montoTotalMaximoCliente;


                            cantCompras.Text = "Compras Anteriores: " + autorizacion.cantComprasAnteriores;
                            cantReclamos.Text = "Reclamos: " + autorizacion.cantReclamosAnteriores;

                            slMensaje.Children.Add(Monto);
                            slMensaje.Children.Add(cantCompras);
                            slMensaje.Children.Add(cantReclamos);
                        }
                    }
                }
                else
                {
                    Mensaje.Text = "Error al sincronizar, necesita autorización.";
                    slMensaje.Children.Add(Mensaje);


                }

            }
            else
            {
                Mensaje.Text = "Error al sincronizar, necesita autorización.";
                slMensaje.Children.Add(Mensaje);

            }

        }

        protected override bool OnBackgroundClicked()
        {
            MessagingCenter.Send(this, "PopUp Cerrado", autorizacion);
            return false;
        }

        public async void Ok(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "PopUp Cerrado", autorizacion);
            await Navigation.PopPopupAsync();
        }
    }
}