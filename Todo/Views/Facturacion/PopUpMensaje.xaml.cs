﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopUpMensaje : PopupPage
    {
		public PopUpMensaje (string msj)
		{
			InitializeComponent ();
            mensaje.Text = msj;
		}

        async void Ok(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }


    }
}