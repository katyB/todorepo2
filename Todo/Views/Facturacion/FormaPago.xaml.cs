﻿using System;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.Views.Clientes;
using Xamarin.Essentials;
using Todo.WS;
using Rg.Plugins.Popup.Extensions;
using Todo.ViewsModels;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormaPago : ContentPage
	{
        ClienteApp cliente = new ClienteApp();
        int flag;
        int formaPago;
        VMAutorizacionCliente consultarEstadoCliente;
        bool puedeEntrar;

        public FormaPago (ClienteApp cl, int f)
		{
			InitializeComponent ();
            this.Content.IsEnabled = false;
            indi.IsRunning = true;
            cliente = cl;
            flag = f;

            if (flag == 0 || flag == 3)
            {
                verificarExitencia();

            }
            else
            {
                controlarMorosidad();
            }

        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        public async void verificarExitencia()
        {
            WSCliente wsCliente = new WSCliente();
            cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
            wsCliente.Dispose();
            var verificarCliente = await App.Database.GetClientePorDni(cliente.nroDocumento);
            if (verificarCliente != null)
            {
                verificarCliente.nombreCliente = cliente.nombreCliente;
                verificarCliente.nroDocumento = cliente.nroDocumento.Trim();
                verificarCliente.fechaNacimiento = cliente.fechaNacimiento;
                verificarCliente.genero = cliente.genero;
                cliente = verificarCliente;
                flag = 1;
                controlarMorosidad();
            }
            else
            {
                controlarMorosidad();
            }
        }

        async void ClickEfectivo(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                var listadoTalonario = await App.Database.GetTalonariosAsync();
                if (listadoTalonario.Count > 0)
                {

                    var listadoProductos = await App.Database.GetProductoAsync();
                    if (listadoProductos.Count > 0)
                    {
                        cambioFlag();
                        if (flag == 3 || flag == 4 || flag == 6 || flag == 8)
                        {
                            formaPago = 0;
                            ////alta
                            //if (flag == 3)
                            //{
                            //    //Aca debo crear cliente y guardarlo para obtener codigo
                            //    cliente.idEstado = 7;
                            //    cliente.nombreEstado = "Derivado a Administración";
                            //    cliente.imagenEstado = "DerivadoAdmin.png";
                            //    cliente.sincronizado = false;
                            //    cliente.localidadCliente = "";
                            //    cliente.nombreDepartamento = "";
                            //    cliente.domicilioCliente = "";
                            //    var current = Connectivity.NetworkAccess;
                            //    var profiles = Connectivity.ConnectionProfiles;

                            //    cliente.codigoCliente = "Nuevo0000";
                            //    cliente.nuevoApp = true;
                            //    await App.Database.SaveIClienteAsync(cliente);
                            //}

                            ////
                            ////      Guardar cliente en factura temporal
                            ////
                            //FacturaTemporal facturaTemporal = new FacturaTemporal();
                            //facturaTemporal.nombreCliente = cliente.nombreCliente;
                            //facturaTemporal.dniCliente = cliente.nroDocumento;
                            //await App.Database.SaveFacturaTemporalAsync(facturaTemporal);
                            ////

                            //await Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));

                            await Navigation.PushAsync(new DomicilioCliente(cliente, flag, formaPago));
                            this.Content.IsEnabled = true;


                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;

                        await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con administración", "Ok");
                        puedeEntrar = true;
                    }


                }
                else
                {
                    this.Content.IsEnabled = true;

                    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    puedeEntrar = true;
                }
            }
 


        }

        async void ClickTarjeta(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                var listadoTalonario = await App.Database.GetTalonariosAsync();
                if (listadoTalonario.Count > 0)
                {

                    var listadoProductos = await App.Database.GetProductoAsync();
                    if (listadoProductos.Count > 0)
                    {
                        cambioFlag();

                        //cliente.nroDocumento = "27412889";
                        //WSCliente wsCliente = new WSCliente();
                        //cliente.nroDocumento = wsCliente.SepararDocumento(cliente.nroDocumento);
                        //wsCliente.Dispose();

                        if (flag == 3 || flag == 4 || flag == 6 || flag == 8)
                        {
                            formaPago = 1;
                            //Aca debo crear cliente y guardarlo para obtener codigo

                            //if (flag == 3)
                            //{
                            //    cliente.idEstado = 7;
                            //    cliente.nombreEstado = "Derivado a Administración";
                            //    cliente.imagenEstado = "DerivadoAdmin.png";
                            //    cliente.sincronizado = false;
                            //    var current = Connectivity.NetworkAccess;
                            //    var profiles = Connectivity.ConnectionProfiles;
                            //    cliente.localidadCliente = "";
                            //    cliente.nombreDepartamento = "";
                            //    cliente.domicilioCliente = "";
                            //    cliente.codigoCliente = "Nuevo0000";
                            //    cliente.nuevoApp = true;
                            //    await App.Database.SaveIClienteAsync(cliente);

                            //}

                            ////
                            ////      Guardar cliente en factura temporal
                            ////
                            //FacturaTemporal facturaTemporal = new FacturaTemporal();
                            //facturaTemporal.nombreCliente = cliente.nombreCliente;
                            //facturaTemporal.dniCliente = cliente.nroDocumento;
                            //await App.Database.SaveFacturaTemporalAsync(facturaTemporal);
                            ////

                            //await Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));
                            await Navigation.PushAsync(new DomicilioCliente(cliente, flag, formaPago));
                            this.Content.IsEnabled = true;

                        }



                    }
                    else
                    {
                        this.Content.IsEnabled = true;

                        await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con administración", "Ok");
                        puedeEntrar = true;
                    }


                }
                else
                {
                    this.Content.IsEnabled = true;

                    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    puedeEntrar = true;

                }
            }
        }

        async void ClickFinanciacion(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                var listadoTalonario = await App.Database.GetTalonariosAsync();
                if (listadoTalonario.Count > 0)
                {

                    var listadoProductos = await App.Database.GetProductoAsync();
                    if (listadoProductos.Count > 0)
                    {

                        formaPago = 2;

                        await Navigation.PushAsync(new AltaClienteDatosPersonales(cliente, flag, formaPago));
                        this.Content.IsEnabled = true;

                        //if (flag == 7)
                        //{
                        //    await Navigation.PushAsync(new AltaFacturaProductos(cliente, flag, formaPago));

                        //}
                        //else
                        //{
                        //    await Navigation.PushAsync(new AltaClienteDatosPersonales(cliente, flag, formaPago));

                        //}
                    }
                    else
                    {
                        this.Content.IsEnabled = true;

                        await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con administración", "Ok");
                        puedeEntrar = true;
                    }


                }
                else
                {
                    this.Content.IsEnabled = true;

                    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    puedeEntrar = true;
                }
            }
 

        }

        public void mensaje()
        {
            MessagingCenter.Subscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado", async (page, autorizacion) =>
            {
                MessagingCenter.Unsubscribe<PopUpAutorizacion, VMAutorizacionCliente>(this, "PopUp Cerrado");

                if (consultarEstadoCliente.tieneConexion)
                {
                    cliente.idEstado = consultarEstadoCliente.idEstado;
                    cliente.nuevoCliente = consultarEstadoCliente.nuevoCliente;
                    cliente.codigoCliente = consultarEstadoCliente.codigoCliente;
                    cliente.montoCuotaAutorizadoVeraz = consultarEstadoCliente.montoDisponible;
                    cliente.montoTotalAutorizadoVeraz = consultarEstadoCliente.montoTotalMaximoCliente;
                    cliente.idCliente = consultarEstadoCliente.idCliente;
                    if (cliente.idEstado == 2 || cliente.idEstado == 7)
                    {
                        financiado.IsVisible = false;
                        grid.ColumnDefinitions.RemoveAt(2);
                    }
                }
                else
                {
                    if (flag == 0 || flag == 3)
                    {
                        cliente.nuevoCliente = true;
                    }
                    var montoMaximo = await App.Database.GetMontoMaximoAsync();
                    cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                    cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                }



            });
        }

        public async void controlarMorosidad()
        {
            WSCliente clientes = new WSCliente();
            var nroDocuemento = cliente.nroDocumento;
            var genero = cliente.genero;
            var nombre = cliente.nombreCliente;

            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                mensaje();
                consultarEstadoCliente = await clientes.ValidarEstadoCliente(nroDocuemento, genero, nombre);
                if(consultarEstadoCliente != null)
                {
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await Navigation.PushPopupAsync(new PopUpAutorizacion(consultarEstadoCliente, 1));
                }
                else
                {
                    if (flag == 0 || flag == 3)
                    {
                        cliente.nuevoCliente = true;
                    }
                    var montoMaximo = await App.Database.GetMontoMaximoAsync();
                    cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                    cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    await DisplayAlert("", "Sin acceso a internet, administración deberá aprobar esta solicitud.", "Ok");
                }

            }
            else
            {


                if(flag == 0 || flag == 3)
                {
                    cliente.nuevoCliente = true;
                }
                var montoMaximo = await App.Database.GetMontoMaximoAsync();
                cliente.montoCuotaAutorizadoVeraz = montoMaximo.montoMaximo;
                cliente.montoTotalAutorizadoVeraz = montoMaximo.montoTotalMaximo;
                this.Content.IsEnabled = true;
                indi.IsRunning = false;
                if (cliente.idEstado == 2 || cliente.idEstado == 7)
                {
                    financiado.IsVisible = false;
                    grid.ColumnDefinitions.RemoveAt(2);
                }
                await DisplayAlert("", "Sin acceso a internet, administración deberá aprobar esta solicitud.", "Ok");

            }



        }

        public void cambioFlag()
        {
            switch (flag)
            {
                case 0:
                    flag = 3;
                    break;
                case 1:
                    flag = 4;
                    break;
                case 2:
                    flag = 6;
                    break;
                case 7:
                    flag = 8;
                    break;
            }
        }

        //protected override bool OnBackButtonPressed()
        //{
        //    cancelarFacturaTemporal();
        //    this.Navigation.PopAsync();
        //    return true;
        //}

        //public async void cancelarFacturaTemporal()
        //{
        //    //cliente.nroDocumento = "27412889";
        //    //WSCliente wsCliente = new WSCliente();
        //    //cliente.nroDocumento= wsCliente.SepararDocumento(cliente.nroDocumento);
        //    //wsCliente.Dispose();
        //    FacturaTemporal facturaTemporal = await App.Database.GetFacturaTemporalPorCliente(cliente.nroDocumento);
        //    if(facturaTemporal != null)
        //    {
        //        facturaTemporal.cancelado = true;
        //        await App.Database.SaveFacturaTemporalAsync(facturaTemporal);
        //    }

        //}

    }
}