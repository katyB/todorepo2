﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Todo.Models;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;
using Xamarin.Essentials;
using System.Threading;
using Todo.WS;


namespace Todo.Views.Facturacion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AltaFacturaTotalCompra : ContentPage
    {
        bool hayFinanciacion = false;
        VMFactura factura = new VMFactura();
        decimal totalFactura = 0;
        decimal totalContado = 0;
        decimal totalDebito = 0;
        decimal totalCredito = 0;
        int flag;
        int flag2;
        bool puedeEntrar;
        bool contolCuotaMasAlta;
        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            indi.IsRunning = false;
            //DateTime fechaPrimerCuota = DateTime.Now.Date.AddDays(15);
            //DatePickerFechaCuota.MinimumDate = fechaPrimerCuota;
            //DatePickerFechaCuota.Date = fechaPrimerCuota;
            DatePickerFechaCuota.MinimumDate = DateTime.Now.Date;

            bool masSeisCuotas = false;
            foreach (var item in factura.listaProductos)
            {
                if (item.cantCuota > 6)
                {
                    masSeisCuotas = true;
                }
            }


            if (masSeisCuotas)
            {
                DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(30);

            }
            else
            {
                DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(120);
            }

            Anticipo.Text = "0";
            factura.anticipo = 0;
            puedeEntrar = true;

        }

        public AltaFacturaTotalCompra(VMFactura f, int fl, int fl2)
        {
            flag2 = fl2;
            factura = f;
            InitializeComponent();
            this.Content.IsEnabled = false;
            flag = fl;
            contolCuotaMasAlta = true;
            CargarDatosFactura();
            this.Content.IsEnabled = true;

        }

        public void CargarDatosFactura()
        {        
            if(flag == 1)
            {
                totalContado = 0;
                totalDebito = 0;
                totalCredito = 0;
 
                //Calculo los montos totales, debito. credito y contado

                foreach (var item in factura.listaProductos)
                {
                    //Calculo monto total
                    totalFactura = totalFactura + item.precioTotal;

                    // Calculo monto Contado Y lo hago visible si existe
                    if (item.idFormaPago == 6)
                    {
                        if (labelContado.IsVisible == false)
                        {
                            labelContado.IsVisible = true;
                            labelNombreContado.IsVisible = true;
                        }
                        totalContado += item.precioTotal;
                    }
                    //Calculo monto Credito Y lo hago visible si existe

                    if (item.idFormaPago == 9)
                    {
                        if (labelCredito.IsVisible == false)
                        {
                            labelCredito.IsVisible = true;
                            labelNombreCredito.IsVisible = true;
                        }
                        totalCredito += item.precioTotal;
                    }

                    //Calculo monto Debito Y lo hago visible si existe
                    if (item.idFormaPago == 10)
                    {
                        if (labelDebito.IsVisible == false)
                        {
                            labelDebito.IsVisible = true;
                            labelNombreDebito.IsVisible = true;
                        }
                        totalDebito += item.precioTotal;
                    }

                    //Verifico si existira financiacion
                    if (hayFinanciacion == false)
                    {
                        if (item.idFormaPago != 10 && item.idFormaPago != 9 && item.idFormaPago != 6)
                        {
                            hayFinanciacion = true;

                        }
                    }

                }
                //Hago visible los datos de la financiacion y inicializo la fecha primer cuota y el anticipo

                if (hayFinanciacion)
                {
                    DisplayAlert("", "Recuerde cambiar la fecha de la Primer Cuota", "ok");

                    //DateTime fechaPrimerCuota = DateTime.Now.Date.AddDays(15);
                    DateTime fechaPrimerCuota = DateTime.Now.Date;
                    DatePickerFechaCuota.MinimumDate = fechaPrimerCuota;

                    bool masSeisCuotas = false;
                    foreach (var item in factura.listaProductos)
                    {
                        if (item.cantCuota > 6)
                        {
                            masSeisCuotas = true;
                        }
                    }


                    if (masSeisCuotas)
                    {
                        DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(30);

                    }
                    else
                    {
                        DatePickerFechaCuota.MaximumDate = DateTime.Now.Date.AddDays(120);
                    }
                    DatePickerFechaCuota.Date = fechaPrimerCuota;
                    Anticipo.Text = "0";
                    DatePickerFechaCuotaTitulo.IsVisible = true;
                    DatePickerFechaCuota.IsVisible = true;
                    labelAnticipo.IsVisible = true;
                    Anticipo.IsVisible = true;
                    gridTabla.IsVisible = true;
                    frame.IsVisible = true;



                }
                else
                {
                    grid.VerticalOptions = LayoutOptions.CenterAndExpand;
                    var medium = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                    labelContado.FontSize = medium;
                    labelNombreContado.FontSize = medium;
                    labelDebito.FontSize = medium;
                    labelNombreDebito.FontSize = medium;
                    labelCredito.FontSize = medium;
                    labelNombreCredito.FontSize = medium;
                    grid.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    frame.IsVisible = true;
                    grid2.HeightRequest = 60;

                }

                //calculo monto de cada cuota y genero la lista de FacturaCuota
                if (factura.puedeAgregarCuotas)
                {
                    DateTime fecha = DatePickerFechaCuota.Date;
                    factura.listaFacturaCuota = new List<VMFacturaCouta>();
                    for (int i = 0; i < factura.cantCuotaMasAlta; i++)
                    {
                        VMFacturaCouta fc = new VMFacturaCouta();
                        fc.nombreCuota = (i + 1).ToString();
                        if (i == 0)
                        {
                            fc.fechaVto = fecha;
                        }
                        else
                        {
                            fecha = fecha.AddMonths(1);
                            fc.fechaVto = fecha;
                        }
                        int montoCuota = 0;
                        foreach (var item in factura.listaProductos)
                        {
                            if (item.cantCuota > 0)
                            {
                                if (i + 1 <= item.cantCuota)
                                {
                                    montoCuota += item.preciocuota;
                                }
                            }
                        }
                        fc.monto = montoCuota;
                        factura.listaFacturaCuota.Add(fc);
                    }
                    factura.puedeAgregarCuotas = false;
                }
 
               //Cargo los valoresde monto total, credito, debito y contado en la vista

                labelMontoFactura.Text = "$" + totalFactura.ToString();
                labelContado.Text = "$" + totalContado.ToString();
                labelCredito.Text = "$" + totalCredito.ToString();
                labelDebito.Text = "$" + totalDebito.ToString();

                //cargo los valores de cuotas en la tabla
                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(TituloCuota, 0, 0);
                gridTabla.Children.Add(TituloMonto, 1, 0);
                gridTabla.Children.Add(TituloFecha, 2, 0);


                for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
                {
                    gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                    var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                    gridTabla.Children.Add(Cuota, 0, i + 1);
                    gridTabla.Children.Add(Monto, 1, i + 1);
                    gridTabla.Children.Add(Fecha, 2, i + 1);
                }

                flag = 0;
            }
        }


        public void CambioFecha(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            if (factura.listaFacturaCuota != null && flag == 0)
            {
                gridTabla.Children.Clear();
                gridTabla.RowDefinitions.Clear();
                gridTabla.ColumnDefinitions.Clear();

                DateTime fecha = DatePickerFechaCuota.Date;

                for (int i = 0; i < factura.listaFacturaCuota.Count(); i++)
                {
                    if (i == 0)
                    {
                        factura.listaFacturaCuota[i].fechaVto = fecha;
                    }
                    else
                    {
                        fecha = fecha.AddMonths(1);
                        factura.listaFacturaCuota[i].fechaVto = fecha;
                    }
                }

                //cargo los valores de cuotas en la tabla
                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(TituloCuota, 0, 0);
                gridTabla.Children.Add(TituloMonto, 1, 0);
                gridTabla.Children.Add(TituloFecha, 2, 0);


                for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
                {
                    gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                    var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                    var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                    gridTabla.Children.Add(Cuota, 0, i + 1);
                    gridTabla.Children.Add(Monto, 1, i + 1);
                    gridTabla.Children.Add(Fecha, 2, i + 1);
                }
            }
            this.Content.IsEnabled = true;
        }

        public async void cambioAnticipo(object sender, EventArgs e)
        {
            if (factura.listaFacturaCuota != null && flag == 0)
            {
                if (!string.IsNullOrWhiteSpace(Anticipo.Text))
                {
                    if (int.TryParse(Anticipo.Text, out int anticipo))
                    {
                        if (anticipo >= 0)
                        {
                            if (anticipo <= totalFactura)
                            {
                                CalcularCuotas(anticipo);
                            }
                            else
                            {
                                await DisplayAlert("", "El anticipo no puede ser mayor al monto de la factura", "Ok");
                                Anticipo.Text = "0";
                                factura.anticipo = 0;
                                CalcularCuotas(0);

                            }
                        }
                        else
                        {
                            await DisplayAlert("", "Ingrese un anticipo mayor a 0", "Ok");
                            Anticipo.Text = "0";
                            CalcularCuotas(0);
                        }


                    }
                    else {
                        await DisplayAlert("", "Ingrese un valor entero", "Ok");
                        Anticipo.Text = "0";
                        factura.anticipo = 0;
                        CalcularCuotas(0);

                    }
                }
                else
                {
                    Anticipo.Text = "";
                    factura.anticipo = 0;
                    CalcularCuotas(0);
                }
 
            }
 
        }

        public void CalcularCuotas(int anticipo)
        {
            decimal porcentaje = 1 - (anticipo / totalFactura);
            factura.anticipo = anticipo;
            Thread.Sleep(2);
            gridTabla.Children.Clear();
            gridTabla.RowDefinitions.Clear();
            gridTabla.ColumnDefinitions.Clear();
            for (int i = 0; i < factura.listaFacturaCuota.Count(); i++)
            {
                decimal montoCuota = 0m;
                foreach (var item in factura.listaProductos)
                {
                    if (item.cantCuota > 0)
                    {
                        if (i + 1 <= item.cantCuota)
                        {
                            montoCuota += item.preciocuota * porcentaje;
                            montoCuota = decimal.Round(montoCuota, 2);
                        }
                    }
                }
                factura.listaFacturaCuota[i].monto = (int)montoCuota;
            }

            //cargo los valores de cuotas en la tabla
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            var TituloCuota = new Label { Text = "Cuota", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
            var TituloMonto = new Label { Text = "Monto", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
            var TituloFecha = new Label { Text = "Fecha", TextColor = Color.Black, FontAttributes = FontAttributes.Bold, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

            gridTabla.Children.Add(TituloCuota, 0, 0);
            gridTabla.Children.Add(TituloMonto, 1, 0);
            gridTabla.Children.Add(TituloFecha, 2, 0);


            for (int i = 0; i < factura.listaFacturaCuota.Count; i++)
            {
                gridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                var Cuota = new Label { Text = factura.listaFacturaCuota[i].nombreCuota, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var Monto = new Label { Text = "$" + factura.listaFacturaCuota[i].monto.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };
                var Fecha = new Label { Text = factura.listaFacturaCuota[i].fechaVto.ToString("dd/MM/yyyy"), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.CenterAndExpand, FontSize = size };

                gridTabla.Children.Add(Cuota, 0, i + 1);
                gridTabla.Children.Add(Monto, 1, i + 1);
                gridTabla.Children.Add(Fecha, 2, i + 1);
            }
        }

        public async void Confirmar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                indi.IsRunning = true;
                try
                {

                    var PrimerCuota = 0m;
                    if (factura.listaFacturaCuota.Count > 0)
                    {
                        PrimerCuota = factura.listaFacturaCuota[1].monto;
                    }

                    //Agregar porcentaje de Api

                    var montoCliente = factura.montoDisponibleCliente * (decimal)1.20;

                    if (montoCliente >= PrimerCuota || hayFinanciacion == false)
                    {



                            factura.montoFactura = (int)totalFactura;



                        var monto = factura.montoDisponibleCliente;

                        //listado de talonarios
                        var listadoTalonario = await App.Database.GetTalonariosAsync();

                        if (listadoTalonario.Count > 0)
                        {
                            factura.FormaPago = 2;
                            factura.fechaFactura = DateTime.Now;
                            factura.totalContado = (int)totalContado;
                            factura.totalCredito = (int)totalCredito;
                            factura.totalDebito = (int)totalDebito;
                            factura.fechaFacturaMostrar = factura.fechaFactura.ToString("dd/MM/yyyy");
                            factura.horaFacturaMostrar = factura.fechaFactura.ToString("HH:mm");
                            factura.observacion = observacion.Text;


                            var montoInformacion = await App.Database.GetInfoMontoAsync();
                            if(montoInformacion != null)
                            {
                                var montoFinanciado = factura.montoFactura - factura.totalContado - factura.totalCredito - factura.totalDebito;
                                if (montoFinanciado > montoInformacion.limiteVenta)
                                {
                                    if (!string.IsNullOrEmpty(montoInformacion.mensaje))
                                    {
                                        await DisplayAlert("", montoInformacion.mensaje, "ok");

                                    }
                                    else
                                    {
                                        await DisplayAlert("", "Monto excedido, solicite autorización.", "ok");

                                    }
                                }
                            }
                            else
                            {
                                await DisplayAlert("", "Venta sujeta a verificación.", "ok");

                            }


                            //aca va el gaurdado de datos
                            FacturaApp f = new FacturaApp
                            {
                                fechaFactura = factura.fechaFactura,
                                fechaFacturaMostrar = factura.fechaFacturaMostrar,
                                horaFacturaMostrar = factura.horaFacturaMostrar,
                                nroFactura = factura.nroFactura,
                                idFactura = factura.idFactura,
                                idEstado = 13,
                                nroCliente = factura.nroCliente,
                                sincronizado = false,
                                nombreImagenFactura = "Ingresado.png",
                                nombreEstado = "Ingresado",
                                nombreCliente = factura.nombreCliente,
                                domicilioCliente = factura.domicilioCliente,
                                dniCliente = factura.dniCliente.ToString().Trim(),
                                montoFactura = factura.montoFactura,
                                financiacion = factura.financiacion,
                                anticipo = factura.anticipo,
                                totalDebito = factura.totalDebito,
                                totalCredito = factura.totalCredito,
                                totalContado = factura.totalContado,
                                formaPago = factura.FormaPago,
                                revisionAdm = true,
                                observacion = factura.observacion,
                                nuevoCliente = factura.nuevoCliente
                            };
                            var current = Connectivity.NetworkAccess;
                            if (current == NetworkAccess.Internet)
                            {
                                f.revisionAdm = false;
                            }

                            var talonario1 = listadoTalonario.FirstOrDefault();
                            talonario1.facturado = true;
                            DateTime fecha = DateTime.Now;
                            talonario1.fecha = fecha;
                            factura.nroFactura = talonario1.nroFactura;
                            factura.idFactura = talonario1.idFactura;
                            factura.nombreTipoFactura = talonario1.nombreTipoFactura; //nombre del tipo factura
                            f.nroFactura = talonario1.nroFactura;
                            f.idFactura = talonario1.idFactura;
                            f.nombreTipoFactura = talonario1.nombreTipoFactura;

                            await App.Database.SaveITalonarioAsync(talonario1);

                            //modificamos el cliente que facturo
                            //string idCliente = factura.nroCliente;
                            string dni = factura.dniCliente.ToString().Trim();
                            ClienteApp cl = await App.Database.GetClientePorDni(dni);
                            if (cl != null)
                            {
                                cl.idEstado = 7;
                                cl.imagenEstado = "DerivadoAdmin.png";
                                cl.nombreEstado = "Derivado a Administración";

                                var montoFactura = f.montoFactura;

                                if (hayFinanciacion)
                                {
                                    decimal totalFinanciado = 0m;
                                    cl.montoDisponible = (cl.montoDisponible - factura.listaFacturaCuota[1].monto);
                                    foreach (var item in factura.listaFacturaCuota)
                                    {
                                        totalFinanciado += item.monto;
                                    }

                                    f.totalFinanciado = (int)totalFinanciado;
                                }

                                await App.Database.SaveIClienteAsync(cl);
                            }



                            //Put cliente modificado
                            var profiles = Connectivity.ConnectionProfiles;
                            if (current == NetworkAccess.Internet)
                            {
                                WSCliente clientes = new WSCliente();
                                var respuesta = await clientes.ModificarUnCliente(cl);

                            }


                            await App.Database.SaveIFacturaAsync(f);



                            var fTemporal = await App.Database.GetFacturaPorDniYestado(factura.dniCliente.ToString().Trim());

                            if (fTemporal != null)
                            {
                                await App.Database.DeleteUnaFacturasync(fTemporal);
                            }

                            foreach (var item in factura.listaProductos)
                            {
                                //Guardo Factura Productos
                                FacturaProducto fp = new FacturaProducto
                                {
                                    idFactura = f.idFactura,
                                    nroFactura = f.nroFactura,
                                    idProducto = item.idProducto,
                                    cantidadComprada = item.cantidadComprada,
                                    precio = item.precioUnitario,
                                    idFormaPago = item.idFormaPago,
                                    nombreProducto = item.nombreProducto,
                                    precioTotal = item.precioTotal

                                };
                                await App.Database.SaveIFacturaProductoAsync(fp);


                                Models.Producto p = await App.Database.GetProductoAsync(item.idProducto);
                                p.saldo = p.saldo - item.cantidadComprada;
                                await App.Database.SaveIProductoAsync(p);

                            }


                            foreach (var item in factura.listaFacturaCuota)
                            {
                                //Guardo Factura Cuota
                                FacturaCuota fc = new FacturaCuota
                                {
                                    idFactura = f.idFactura,
                                    nroFactura = f.nroFactura,
                                    fechaVto = item.fechaVto,
                                    monto = item.monto,
                                    nombreCuota = item.nombreCuota,
                                    sincronizado = false,
                                };

                                await App.Database.SaveIFacturaCuotaAsync(fc);
                            }


                            if (current == NetworkAccess.Internet)
                            {
                                WSCliente clientes = new WSCliente();
                                WSFactura factura = new WSFactura();
                                //await clientes.EnviarCliente();
                                await factura.EnviarFctura();
                            }


                            await this.Navigation.PushAsync(new AltaFacturaImprimir(factura, flag2));
                            indi.IsRunning = false;
                            this.Content.IsEnabled = true;

                        }
                        else
                        {
                            this.Content.IsEnabled = true;
                            indi.IsRunning = false;

                            await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                                                    puedeEntrar = true;

                        }
                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        indi.IsRunning = false;
                        await DisplayAlert("", "Monto disponible superado", "Ok");
                        puedeEntrar = true;


                    }

                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "101-ConfirmarFacturacion";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    await App.Database.SaveIControlErroresAsync(control);
                    this.Content.IsEnabled = true;
                    indi.IsRunning = false;
                    puedeEntrar = true;

                }
            }
 
        }


    }
}