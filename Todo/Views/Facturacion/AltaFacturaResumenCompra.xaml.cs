﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Todo.WS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltaFacturaResumenCompra : ContentPage
	{
        VMFactura factura = new VMFactura();
        List<FormaPago> listaMediosDePago = new List<FormaPago>();
        List<VMProducto> listaProductos = new List<VMProducto>();
        HttpClient client = new HttpClient();
        WSFormaPago formaPago = new WSFormaPago();
        int flag;
        int formaDePago;
        int totalFactura;
        int totalDebito;
        int totalCredito;
        bool puedeEntrar;
        public AltaFacturaResumenCompra (VMFactura f, int fl, int fp)
		{
			InitializeComponent ();
            this.Content.IsEnabled = false;
            factura = f;
            flag = fl;
            formaDePago = fp;
            Mensaje();
            cargarDatos();
            this.Content.IsEnabled = true;
        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }

        public void Mensaje()
        {
            MessagingCenter.Subscribe<AltaFacturaCantidad, VMProducto>(this, "Producto con cantidad editado desde resumenCompra", (page, producto) =>
            {
                foreach (var item in factura.listaProductos)
                {
                    if (item.idProducto == producto.idProducto)
                    {
                        item.cantidadComprada = producto.cantidadComprada;
                    }
                }
                regenerarTabla();
            });
        }

        async void cargarDatos()
        {
            //Defino columnas
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

            //Creo tabla      
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            for (int i = 0; i < factura.listaProductos.Count(); i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                string nombreProd;
                if (factura.listaProductos[i].nombreProducto.Length > 50)
                {
                    nombreProd = factura.listaProductos[i].nombreProducto.Substring(0, 50) + "...";
                }
                else
                {
                    nombreProd = factura.listaProductos[i].nombreProducto;
                }

                var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                var precio = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };


                ImageButton edit = new ImageButton
                {
                    StyleId = i.ToString(),
                    Source = "Edit.png",
                    BackgroundColor = Color.Transparent,
                    HeightRequest = 30,
                    WidthRequest = 30
                };
                edit.Clicked += Edit;

                ImageButton delete = new ImageButton
                {
                    StyleId = i.ToString(),
                    Source = "Rojo.png",
                    BackgroundColor = Color.Transparent,
                    HeightRequest = 30,
                    WidthRequest = 30
                };
                delete.Clicked += Delete;

                StackLayout slVertical = new StackLayout();
                StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
                StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };


                slVertical.Children.Add(nombre);
                slHorizontal1.Children.Add(CantYPrecioUnitario);
                slHorizontal1.Children.Add(edit);
                slHorizontal1.Children.Add(delete);

                var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[i].idProducto);
                respuestaFormaPago = respuestaFormaPago.OrderBy(o => o.nombreFormaPago).ToList();

                if (formaDePago == 0)
                {
                    //contado

                    foreach (var ind in respuestaFormaPago)
                    {
                        if (ind.idFormaPago == 6)
                        {
                            factura.listaProductos[i].precioUnitario = ind.montoVenta;
                            factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                            factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                            CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                            factura.listaProductos[i].precioTotal = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                            factura.listaProductos[i].cantCuota = 0;


                            precio.Text = "$" + factura.listaProductos[i].precioTotal.ToString();
                        }
                    }

                    slHorizontal2.Children.Add(precio);

                }
                else
                {
                    //DebitoCredito

                    Picker pickercoutas = new Picker { StyleId = i.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };




                    foreach (var ind in respuestaFormaPago)
                    {
                        if (ind.idFormaPago == 9)
                        {
                            pickercoutas.Items.Add(ind.nombreFormaPago);

                            pickercoutas.SelectedItem = ind.nombreFormaPago;

                            factura.listaProductos[i].precioUnitario = ind.montoVenta;
                            factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                            factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                            CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                            factura.listaProductos[i].precioTotal = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                            factura.listaProductos[i].cantCuota = 0;


                            precio.Text = "$" + factura.listaProductos[i].precioTotal.ToString();
                        }
                        else
                        {
                            if (ind.idFormaPago == 10)
                            {
                                pickercoutas.Items.Add(ind.nombreFormaPago);
                            }
                        }
                    }

                    pickercoutas.SelectedIndexChanged += this.cambioPicker;

                    slHorizontal2.Children.Add(pickercoutas);
                    slHorizontal2.Children.Add(precio);

                }

                slVertical.Children.Add(slHorizontal1);
                slVertical.Children.Add(slHorizontal2);

                grid.Children.Add(slVertical, 0, i);

                calcularTotal();
            }

            frame.IsVisible = true;
            grid.IsVisible = true;

        }

        public async void Edit(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            var button = sender as ImageButton;

            int id = Convert.ToInt32(button.StyleId);

            VMProducto p = new VMProducto();
            p.idProducto = factura.listaProductos[id].idProducto;
            p.cantidadComprada = factura.listaProductos[id].cantidadComprada;
            p.cantidad = factura.listaProductos[id].cantidad;
            p.codigoProducto = factura.listaProductos[id].codigoProducto;
            p.nombreProducto = factura.listaProductos[id].nombreProducto;
            p.idDeposito = factura.listaProductos[id].idDeposito;
            p.nombreDeposito = factura.listaProductos[id].nombreDeposito;

            await this.Navigation.PushAsync(new AltaFacturaCantidad(p, 3));
            this.Content.IsEnabled = true;
        }

        public void Delete(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            //Elimino elemtno de la listaProducto
            var button = sender as ImageButton;
            int id = Convert.ToInt32(button.StyleId);
            MessagingCenter.Send(this, "Eliminar Producto ResumenCompra", factura.listaProductos[id]);
            regenerarTabla();

        }

        public async void cambioPicker(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

            var precio = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };

            var picker = sender as Picker;
            var id = Int32.Parse(picker.StyleId);
            var nombrePicker = picker.SelectedItem.ToString();
            string nombreProd;
            if (factura.listaProductos[id].nombreProducto.Length > 50)
            {
                nombreProd = factura.listaProductos[id].nombreProducto.Substring(0, 50) + "...";
            }
            else
            {
                nombreProd = factura.listaProductos[id].nombreProducto;
            }
            var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };

            //forma de pago
            var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[id].idProducto);
            respuestaFormaPago = respuestaFormaPago.OrderBy(r => r.nombreFormaPago).ToList();

            Picker pickercoutas = new Picker { StyleId = id.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };
            foreach (var ind in respuestaFormaPago)
            {

                if (ind.idFormaPago == 9 || ind.idFormaPago == 10)
                {
                    pickercoutas.Items.Add(ind.nombreFormaPago);
                    if (ind.nombreFormaPago == nombrePicker)
                    {
                        pickercoutas.SelectedItem = ind.nombreFormaPago;

                        factura.listaProductos[id].precioUnitario = ind.montoVenta;
                        factura.listaProductos[id].idFormaPago = ind.idFormaPago;
                        factura.listaProductos[id].nombreFormaPago = ind.nombreFormaPago;
                        CantYPrecioUnitario.Text = factura.listaProductos[id].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[id].precioUnitario.ToString();
                        factura.listaProductos[id].precioTotal = factura.listaProductos[id].precioUnitario * factura.listaProductos[id].cantidadComprada;
                        factura.listaProductos[id].cantCuota = 0;

                        precio.Text = "$" + factura.listaProductos[id].precioTotal.ToString();
                    }
                }



            }
            pickercoutas.SelectedIndexChanged += this.cambioPicker;

            foreach (var control in grid.Children)
            {
                if (Grid.GetRow(control) == id && Grid.GetColumn(control) == 0)
                {
                    grid.Children.Remove(control);
                    break;
                }
            }

            ImageButton edit = new ImageButton
            {
                StyleId = id.ToString(),
                Source = "Edit.png",
                BackgroundColor = Color.Transparent,
                HeightRequest = 30,
                WidthRequest = 30
            };
            edit.Clicked += Edit;

            ImageButton delete = new ImageButton
            {
                StyleId = id.ToString(),
                Source = "Rojo.png",
                BackgroundColor = Color.Transparent,
                HeightRequest = 30,
                WidthRequest = 30
            };
            delete.Clicked += Delete;

            StackLayout slVertical = new StackLayout();
            StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
            StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };

            slVertical.Children.Add(nombre);
            slHorizontal1.Children.Add(CantYPrecioUnitario);
            slHorizontal1.Children.Add(edit);
            slHorizontal1.Children.Add(delete);
            slHorizontal2.Children.Add(pickercoutas);
            slHorizontal2.Children.Add(precio);

            slVertical.Children.Add(slHorizontal1);
            slVertical.Children.Add(slHorizontal2);

            grid.Children.Add(slVertical, 0, id);

            calcularTotal();
        }

        public async void regenerarTabla()
        {
            this.Content.IsEnabled = false;
            if (factura.listaProductos.Count > 0)
            {
                //Regenero la tabla
                grid.Children.Clear();
                grid.RowDefinitions.Clear();
                grid.ColumnDefinitions.Clear();


                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });

                var size = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                for (int i = 0; i < factura.listaProductos.Count(); i++)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                    string nombreProd;
                    if (factura.listaProductos[i].nombreProducto.Length > 50)
                    {
                        nombreProd = factura.listaProductos[i].nombreProducto.Substring(0, 50) + "...";
                    }
                    else
                    {
                        nombreProd = factura.listaProductos[i].nombreProducto;
                    }

                    var nombre = new Label { Text = nombreProd, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size, FontAttributes = FontAttributes.Bold };
                    var CantYPrecioUnitario = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };
                    var precio = new Label { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = size };


                    ImageButton edit = new ImageButton
                    {
                        StyleId = i.ToString(),
                        Source = "Edit.png",
                        BackgroundColor = Color.Transparent,
                        HeightRequest = 30,
                        WidthRequest = 30
                    };
                    edit.Clicked += Edit;

                    ImageButton delete = new ImageButton
                    {
                        StyleId = i.ToString(),
                        Source = "Rojo.png",
                        BackgroundColor = Color.Transparent,
                        HeightRequest = 30,
                        WidthRequest = 30
                    };
                    delete.Clicked += Delete;

                    StackLayout slVertical = new StackLayout();
                    StackLayout slHorizontal1 = new StackLayout { Orientation = StackOrientation.Horizontal };
                    StackLayout slHorizontal2 = new StackLayout { Orientation = StackOrientation.Horizontal };

                    slVertical.Children.Add(nombre);
                    slHorizontal1.Children.Add(CantYPrecioUnitario);
                    slHorizontal1.Children.Add(edit);
                    slHorizontal1.Children.Add(delete);


                    //forma de pago
                    var respuestaFormaPago = await App.Database.GetFormaPagoProductoAsync(factura.listaProductos[i].idProducto);
                    respuestaFormaPago = respuestaFormaPago.OrderBy(r => r.nombreFormaPago).ToList();

                    if (formaDePago == 0)
                    {
                        //Contado

                        foreach (var ind in respuestaFormaPago)
                        {
                            if (ind.idFormaPago == 6)
                            {
                                factura.listaProductos[i].precioUnitario = ind.montoVenta;
                                factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                                factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                                CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                                factura.listaProductos[i].precioTotal = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                                factura.listaProductos[i].cantCuota = 0;


                                precio.Text = "$" + factura.listaProductos[i].precioTotal.ToString();
                            }
                        }

                        slHorizontal2.Children.Add(precio);


                    }
                    else
                    {
                        if(formaDePago == 1)
                        {
                            //DebitoCredito

                            Picker pickercoutas = new Picker { StyleId = i.ToString(), FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Picker)) };

                            foreach (var ind in respuestaFormaPago)
                            {
                                if (ind.idFormaPago == 9 || ind.idFormaPago == 10)
                                {
                                    pickercoutas.Items.Add(ind.nombreFormaPago);

                                    if (ind.idFormaPago == factura.listaProductos[i].idFormaPago)
                                    {
                                        pickercoutas.SelectedItem = ind.nombreFormaPago;
                                        factura.listaProductos[i].precioUnitario = ind.montoVenta;
                                        factura.listaProductos[i].idFormaPago = ind.idFormaPago;
                                        factura.listaProductos[i].nombreFormaPago = ind.nombreFormaPago;
                                        CantYPrecioUnitario.Text = factura.listaProductos[i].cantidadComprada.ToString() + "x " + "$" + factura.listaProductos[i].precioUnitario.ToString();
                                        factura.listaProductos[i].precioTotal = factura.listaProductos[i].precioUnitario * factura.listaProductos[i].cantidadComprada;
                                        factura.listaProductos[i].cantCuota = 0;
                                        precio.Text = "$" + factura.listaProductos[i].precioTotal.ToString();
                                    }
                                }
                            }

                            pickercoutas.SelectedIndexChanged += this.cambioPicker;

                            slHorizontal2.Children.Add(pickercoutas);
                            slHorizontal2.Children.Add(precio);

                        }
                    }

                    slVertical.Children.Add(slHorizontal1);
                    slVertical.Children.Add(slHorizontal2);

                    grid.Children.Add(slVertical, 0, i);
                }


                calcularTotal();
            }
            else
            {
                await this.Navigation.PopAsync();
                this.Content.IsEnabled = true;
            }
        }

        public void calcularTotal()
        {
            totalFactura = 0;
            totalDebito = 0;
            totalCredito = 0;

            foreach (var item in factura.listaProductos)
            {
                //Calculo monto total
                totalFactura = totalFactura + item.precioTotal;

                if (formaDePago == 1)
                {
                    //Calculo monto Credito Y lo hago visible si existe

                    if (item.idFormaPago == 9)
                    {
                        totalCredito += item.precioTotal;
                    }

                    //Calculo monto Debito Y lo hago visible si existe
                    if (item.idFormaPago == 10)
                    {
                        totalDebito += item.precioTotal;
                    }
                }


            }

            labelMontoFactura.Text = "$" + totalFactura;
            if (formaDePago == 1)
            {
                if(totalCredito > 0)
                {
                    labelNombreCredito.IsVisible = true;
                    labelCredito.Text = "$" + totalCredito;
                    labelCredito.IsVisible = true;
                }
                else
                {
                    labelNombreCredito.IsVisible = false;
                    labelCredito.Text = "$0";
                    labelCredito.IsVisible = false;
                }
                if (totalDebito > 0)
                {
                    labelNombreDebito.IsVisible = true;
                    labelDebito.Text = "$" + totalDebito;
                    labelDebito.IsVisible = true;
                }
                else
                {
                    labelNombreDebito.IsVisible = false;
                    labelDebito.Text = "$0";
                    labelDebito.IsVisible = false;
                }
            }
            this.Content.IsEnabled = true;
        }

        async void Confirmar(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                indi.IsRunning = true;

                factura.montoFactura = totalFactura;
                factura.FormaPago = formaDePago;
                //listado de talonarios
                var listadoTalonario = await App.Database.GetTalonariosAsync();

                if (listadoTalonario.Count > 0)
                {

                    factura.fechaFactura = DateTime.Now;
                    if (formaDePago == 0)
                    {
                        factura.totalContado = totalFactura;
                    }
                    else
                    {
                        if (formaDePago == 1)
                        {
                            factura.totalCredito = totalCredito;
                            factura.totalDebito = totalDebito;
                        }
                    }
                    factura.fechaFacturaMostrar = factura.fechaFactura.ToString("dd/MM/yyyy");
                    factura.horaFacturaMostrar = factura.fechaFactura.ToString("HH:mm");
                    factura.financiacion = false;
                    factura.observacion = observacion.Text;

                    //aca va el gaurdado de datos
                    FacturaApp f = new FacturaApp
                    {
                        fechaFactura = factura.fechaFactura,
                        fechaFacturaMostrar = factura.fechaFacturaMostrar,
                        horaFacturaMostrar = factura.horaFacturaMostrar,
                        nroFactura = factura.nroFactura,
                        idFactura = factura.idFactura,
                        idEstado = 13,
                        nroCliente = factura.nroCliente,
                        sincronizado = false,
                        nombreImagenFactura = "Ingresado.png",
                        nombreEstado = "Ingresado",
                        nombreCliente = factura.nombreCliente,
                        domicilioCliente = factura.domicilioCliente,
                        dniCliente = factura.dniCliente.ToString(),
                        montoFactura = factura.montoFactura,
                        financiacion = factura.financiacion,
                        totalDebito = factura.totalDebito,
                        totalCredito = factura.totalCredito,
                        totalContado = factura.totalContado,
                        formaPago = factura.FormaPago,
                        revisionAdm = true,
                        observacion = factura.observacion,
                        nuevoCliente = factura.nuevoCliente

                    };
                    var current = Connectivity.NetworkAccess;
                    if (current == NetworkAccess.Internet)
                    {
                        f.revisionAdm = false;
                    }

                    var talonario1 = listadoTalonario.FirstOrDefault();
                    talonario1.facturado = true;
                    DateTime fecha = DateTime.Now;
                    talonario1.fecha = fecha;
                    factura.nroFactura = talonario1.nroFactura;
                    factura.idFactura = talonario1.idFactura;
                    factura.nombreTipoFactura = talonario1.nombreTipoFactura; //nombre del tipo factura
                    f.nroFactura = talonario1.nroFactura;
                    f.idFactura = talonario1.idFactura;
                    f.nombreTipoFactura = talonario1.nombreTipoFactura;

                    await App.Database.SaveITalonarioAsync(talonario1);

                    var montoFactura = f.montoFactura;



                    await App.Database.SaveIFacturaAsync(f);

               

                    foreach (var item in factura.listaProductos)
                    {
                        //Guardo Factura Productos
                        FacturaProducto fp = new FacturaProducto
                        {
                            idFactura = f.idFactura,
                            nroFactura = f.nroFactura,
                            idProducto = item.idProducto,
                            cantidadComprada = item.cantidadComprada,
                            precio = item.precioUnitario,
                            idFormaPago = item.idFormaPago,
                            nombreProducto = item.nombreProducto,
                            precioTotal = item.precioTotal

                        };
                        await App.Database.SaveIFacturaProductoAsync(fp);


                        Models.Producto p = await App.Database.GetProductoAsync(item.idProducto);
                        p.saldo = p.saldo - item.cantidadComprada;
                        await App.Database.SaveIProductoAsync(p);

                    }

               

                    if (current == NetworkAccess.Internet)
                    {

                        WSCliente clientes = new WSCliente();
                        WSFactura factura = new WSFactura();
                        //await clientes.EnviarCliente();
                        await factura.EnviarFctura();
                    }

                    indi.IsRunning = false;
                    await this.Navigation.PushAsync(new AltaFacturaImprimir(factura, flag));
                    this.Content.IsEnabled = true;

                }
                else
                {
                    this.Content.IsEnabled = true;

                    indi.IsRunning = false;

                    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                    puedeEntrar = true;
                }
            }

        }
    }
}