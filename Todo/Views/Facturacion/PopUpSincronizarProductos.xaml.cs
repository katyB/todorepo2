﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopUpSincronizarProductos : PopupPage
    {
		public PopUpSincronizarProductos ()
		{
			InitializeComponent ();
		}


        async void Si(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "Sincronizar Productos Cerrado", true);
            await Navigation.PopPopupAsync();
        }

        async void No(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "Sincronizar Productos Cerrado", false);
            await Navigation.PopPopupAsync();
        }


        protected override bool OnBackgroundClicked()
        {
            return false;
        }
    }
}