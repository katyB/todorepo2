﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.ViewsModels;
using Forms9Patch;
using Xamarin.Essentials;
using Todo.WS;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FinalizarDia : ContentPage
	{
        WSDeposito deposito = new WSDeposito();
        bool puedeEntrar;

        public FinalizarDia ()
		{
			InitializeComponent ();
            CargarDatos();
		}

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }


        public async void CargarDatos() {
            indi.IsRunning = true;
            this.Content.IsEnabled = false;
            var nombreUsuario = Preferences.Get("usuarioLogueado", "");
            vendedor.Text = nombreUsuario;

            var nombreDeposito = Preferences.Get("nombreDeposito", "");
            if(nombreDeposito.Length > 10)
            {
                depositoVendedor.Text = nombreDeposito.Substring(10);
            }
            else
            {
                depositoVendedor.Text = nombreDeposito;
            }

            fecha.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            int clBianco = await App.Database.GetCantClientesFacturados();

            int clNuevos = await App.Database.GetCantClientesFacturadosNuevos();

            int clTotales = clBianco + clNuevos;
            clientesBianco.Text = clBianco.ToString();
            clientesNuevos.Text = clNuevos.ToString();
            clientesTotal.Text = clTotales.ToString();

            int cantVendido = await App.Database.GetCantProductosVendidos();
            cantProdVendidos.Text = cantVendido.ToString();

            int facturasAnuladas = await App.Database.GetCantFacturasAnuladas();
            cantFacturasAnuladas.Text = facturasAnuladas.ToString();

            int facturasEmitidas = await App.Database.GetCantFacturasEmitidas();
           cantFacturas.Text = facturasEmitidas.ToString();

            var talonarioDesde = await App.Database.GetPrimerNrroFactura();

            if (talonarioDesde != null) {
                int desdeNro = talonarioDesde.nroFactura;
                desdeFacturas.Text = desdeNro.ToString();
            }
            else
            {
                desdeFacturas.Text = "     ";
            }

            var talonarioHasta = await App.Database.GetUltimoNrroFactura();
            if(talonarioHasta != null)
            {
                int hastaNro = talonarioHasta.nroFactura;
                hastaFacturas.Text = hastaNro.ToString();
            }
            else
            {
                hastaFacturas.Text = "     ";
            }

            var cantAnticipo = await App.Database.GetCantAnticipo();
            totalAnticipo.Text = "$" + cantAnticipo;

            var cantContado = await App.Database.GetCantContado();
            totalContado.Text = "$" + cantContado;

            var cantDebito = await App.Database.GetCantDebito();
            totalDebito.Text = "$" + cantDebito;

            var cantCredito = await App.Database.GetCantCredito();
            totalCredito.Text = "$" + cantCredito;

            var cantFinanciado = await App.Database.GetCantFinanaciado();
            totalFinanciado.Text = "$" + cantFinanciado;

            var montoVendido = await App.Database.GetMontoVendido();
            totalVendido.Text = "$"+ montoVendido;
            indi.IsRunning = false;
            this.Content.IsEnabled = true;
        }

        public async void Imprimir(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                Forms9Patch.WebViewPrintEffect.ApplyTo(webView);
                var htmlSource = new HtmlWebViewSource();
                var fechaTicket = DateTime.Now;
                string html = @"
                <!DOCTYPE html>
                <html>
                <body>";
                html += " <table style = 'width: 100 %'>";
                html += "<tr colspan=2>";
                html += "<td cols><strong>Vendedor: </strong>" + vendedor.Text + "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td><strong>Fecha: </strong>" + fechaTicket.ToString("dd/MM/yyyy") + "</td>";
                html += "<td><strong>Hora: </strong>" + fechaTicket.ToString("HH:mm:ss") + "</td>";
                html += "</tr>";
                html += "</table>";

                html += "Cantidad Clientes <br/>";

                html += " <table style = 'width: 100 %'>";
                html += "<tr>";
                html += "<td><strong>Bianco: </strong>" + clientesBianco.Text + "</td>";
                html += "<td><strong>Nuevos: </strong>" + clientesNuevos.Text + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td cols><strong>Clientes Totales: </strong>" + clientesTotal.Text + "</td>";
                html += "</tr>";
                html += "</table>";

                html += "Facturas <br/>";

                html += " <table style = 'width: 100 %'>";
                html += "<tr colspan=2>";
                html += "<td cols><strong>Productos Vendidos: </strong>" + cantProdVendidos.Text + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td><strong>Facturas Emitidas: </strong>" + cantFacturas.Text + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td><strong>Facturas Anuladas: </strong>" + cantFacturasAnuladas.Text + "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td><strong>Desde: </strong>" + desdeFacturas.Text + "</td>";
                html += "<td><strong>Hasta: </strong>" + hastaFacturas.Text + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td><strong>Contado: </strong>" + totalContado.Text + "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td><strong>Débito: </strong>" + totalDebito.Text + "</td>";
                html += "<td><strong>Crédito: </strong>" + totalCredito.Text + "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td><strong>Anticipo: </strong>" + totalAnticipo.Text + "</td>";
                html += "<td><strong>Financiado: </strong>" + totalFinanciado.Text + "</td>";
                html += "</tr>";
                html += "<tr colspan=2>";
                html += "<td cols><strong>Total Vendido: </strong>" + totalVendido.Text + "</td>";
                html += "</tr>";
                html += "</table>";

                htmlSource.Html = html;
                webView.Source = htmlSource;

                var estado = await Permissions.RequestAsync<Permissions.StorageWrite>();

                if (await webView.ToPdfAsync(fechaTicket.ToString("ddMMyyyyhhmmss")) is ToFileResult pdfResult)
                {
                    await Share.RequestAsync(new ShareFileRequest
                    {
                        Title = "Archivo Pdf",
                        File = new ShareFile(pdfResult.Result)
                    });


                    await this.Navigation.PopAsync();
                    this.Content.IsEnabled = true;


                }
            }


        }
    }
}