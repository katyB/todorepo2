﻿using System;
using System.Linq;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Todo.WS;
using Todo.Models;

namespace Todo.Views.Facturacion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AltaFacturaCantidad : ContentPage
    {
        VMProducto producto = new VMProducto();
        int cantProductos = 1;
        int flag = 0;
        bool puedeEntrar;

        public AltaFacturaCantidad(VMProducto p)
        {
            producto.idProducto = p.idProducto;
            producto.idDeposito = p.idDeposito;
            producto.cantidad = p.cantidad;
            producto.codigoProducto = p.codigoProducto;
            producto.nombreDeposito = p.nombreDeposito;
            producto.nombreProducto = p.nombreProducto;
            InitializeComponent();
            this.Content.IsEnabled = false;

            BuscarPrecios();
            cargarDatos();
            this.Content.IsEnabled = true;

        }

        public AltaFacturaCantidad(VMProducto p, int f)
        {
            flag = f;
            producto.idProducto = p.idProducto;
            producto.idDeposito = p.idDeposito;
            producto.cantidad = p.cantidad;
            producto.codigoProducto = p.codigoProducto;
            producto.nombreDeposito = p.nombreDeposito;
            producto.nombreProducto = p.nombreProducto;
            producto.cantidadComprada = p.cantidadComprada;
            InitializeComponent();
            this.Content.IsEnabled = false;

            BuscarPrecios();

            cantidad.Text = producto.cantidadComprada.ToString();
            cantProductos = producto.cantidadComprada;

            if(cantProductos == producto.cantidad)
            {
                masC.IsVisible = false;
                masBN.IsVisible = true;
            }
            else
            {
                masC.IsVisible = true;
                masBN.IsVisible = false;
            }

            if(cantProductos == 1)
            {
                menosBN.IsVisible = true;
                menosC.IsVisible = false;
            }
            else
            {
                menosBN.IsVisible = false;
                menosC.IsVisible = true;
            }
            this.Content.IsEnabled = true;

        }

        protected override void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
        }

        public void cargarDatos()
        {
            cantidad.Text = "1";
            cantProductos = 1;
            menosBN.IsVisible = true;
            menosC.IsVisible = false;
            if (cantProductos == producto.cantidad)
            {
                masC.IsVisible = false;
                masBN.IsVisible = true;
            }
            else
            {
                masC.IsVisible = true;
                masBN.IsVisible = false;
            }

        }

        public void AumentarCantidad(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            if (producto.cantidad > cantProductos)
            {
                if(cantProductos == 1)
                {
                    menosC.IsVisible = true;
                    menosBN.IsVisible = false;
                }

                cantProductos += 1;

                cantidad.Text = cantProductos.ToString();
                if(cantProductos == producto.cantidad)
                {
                    masBN.IsVisible = true;
                    masC.IsVisible = false;
                }
            }
            this.Content.IsEnabled = true;

        }

        public void DisminuirCantidad(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;
            if (cantProductos > 1)
            {
                if (cantProductos == producto.cantidad)
                {
                    masBN.IsVisible = false;
                    masC.IsVisible = true;
                }
                cantProductos -= 1;
                producto.cantidadComprada = cantProductos;
                cantidad.Text = cantProductos.ToString();
                if (cantProductos == 1)
                {
                    menosC.IsVisible = false;
                    menosBN.IsVisible = true;
                }
            }
            this.Content.IsEnabled = true;
        }

        public async void AgregarProducto(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;
                if (flag == 0)
                {
                    producto.cantidadComprada = cantProductos;
                    MessagingCenter.Send(this, "Producto con cantidad", producto);
                    await this.Navigation.PopAsync();
                    this.Content.IsEnabled = true;
                }
                else
                {
                    if (flag == 1)
                    {
                        producto.cantidadComprada = cantProductos;
                        MessagingCenter.Send(this, "Producto con cantidad editado desde resumen", producto);
                        await this.Navigation.PopAsync();
                        this.Content.IsEnabled = true;
                    }
                    else
                    {
                        if (flag == 2)
                        {
                            producto.cantidadComprada = cantProductos;
                            MessagingCenter.Send(this, "Producto con cantidad editado desde producto", producto);
                            await this.Navigation.PopAsync();
                            this.Content.IsEnabled = true;
                        }
                        else
                        {
                            if (flag == 3)
                            {
                                producto.cantidadComprada = cantProductos;
                                MessagingCenter.Send(this, "Producto con cantidad editado desde resumenCompra", producto);
                                await this.Navigation.PopAsync();
                                this.Content.IsEnabled = true;
                            }
                            else
                            {
                                this.Content.IsEnabled = true;
                                puedeEntrar = true;


                            }

                        }
                    }
                }
            }


        }

        public async void BuscarPrecios()
        {
            Grid grid = new Grid { VerticalOptions = LayoutOptions.CenterAndExpand };

            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            var medium = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            var labelNombre = new Label { Text = "Producto:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var nombre = new Label { Text = producto.nombreProducto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var labelCodigo = new Label { Text = "Código:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var codigo = new Label { Text = producto.codigoProducto, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var labelCantidad = new Label { Text = "Cantidad:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
            var cantidad = new Label { Text = producto.cantidad.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };


            //forma pago del Producto
            
            var formasPago = await App.Database.GetFormaPagoProductosAsync();
            var respuesta = await App.Database.GetFormaPagoProductoAsync(producto.idProducto);
            respuesta = respuesta.OrderBy(r => r.nombreFormaPago).ToList();


            if (SLgrid.Children.Count() > 0)
            {
                SLgrid.Children.RemoveAt(0);
            }

            grid.Children.Add(labelNombre, 0, 0);
            grid.Children.Add(nombre, 1, 0);
            grid.Children.Add(labelCodigo, 0, 1);
            grid.Children.Add(codigo, 1, 1);
            grid.Children.Add(labelCantidad, 0, 2);
            grid.Children.Add(cantidad, 1, 2);

            SLgrid.Children.Add(grid);
            for (int i = 0; i < respuesta.Count(); i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                var Left = new Label { Text = respuesta[i].nombreFormaPago, FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };

                int precioCuota = 0;
                if (respuesta[i].cantidadCuotas > 0)
                {
                    precioCuota = (int)(respuesta[i].montoVenta / respuesta[i].cantidadCuotas);
                }
                else
                {
                    precioCuota = respuesta[i].montoVenta;
                }
                var Rigth = new Label { Text = "$" + precioCuota.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand, FontSize = medium };
                grid.Children.Add(Left, 0, i + 3);
                grid.Children.Add(Rigth, 1, i + 3);
            }
            grid.IsVisible = true;
        }
}
}
