﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModificarFacturaView : ContentPage
	{
        VMFactura factura = new VMFactura();
        FacturaApp facturaAGuardar = new FacturaApp();
        List<EstadoFactura> listaEstados = new List<EstadoFactura>();

		public ModificarFacturaView (VMFactura f)
		{
            factura = f;
            facturaAGuardar.anticipo = f.anticipo;
            facturaAGuardar.cantidadCuotas = f.cantidadCuotas;
            facturaAGuardar.cantProductos = f.cantProductos;
            facturaAGuardar.domicilioCliente = f.domicilioCliente;
            facturaAGuardar.fechaFactura = f.fechaFactura;
            facturaAGuardar.fechaFacturaMostrar = f.fechaFacturaMostrar;
            facturaAGuardar.horaFacturaMostrar = f.horaFacturaMostrar;
            facturaAGuardar.fechaPrimerCuota = f.fechaPrimerCuota;
            facturaAGuardar.fechaPrimerCuotaMostrar = f.fechaPrimerCuotaMostrar;
            facturaAGuardar.financiacion = f.financiacion;
            facturaAGuardar.formaDePagoNombre = f.formaDePagoNombre;
            facturaAGuardar.idEstado = f.idEstado;
            facturaAGuardar.idFactura = f.idFactura;
            facturaAGuardar.idFormaPago = f.idFormaPago;
            facturaAGuardar.montoFactura = f.montoFactura;
            facturaAGuardar.nombreCliente = f.nombreCliente;
            facturaAGuardar.nombreEstado = f.nombreEstado;
            facturaAGuardar.nombreImagenFactura = f.nombreImagenFactura;
            facturaAGuardar.nroCliente = f.nroCliente;
            facturaAGuardar.nroFactura = f.nroFactura;
            //facturaAGuardar.nroTalonario = f.nroTalonario;
            facturaAGuardar.observacion = f.observacion;
            facturaAGuardar.sincronizado = f.sincronizado;
            facturaAGuardar.valorCuota = f.valorCuota;
            InitializeComponent ();
            CargarDatos();
		}

        public void CargarDatos()
        {
            if (factura.sincronizado == true)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

                var valorCuotaLabel = new Label { Text = "Valor Cuota:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand};
                var valorCuota = new Label { Text = "$" + factura.valorCuota.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand };
                grid.Children.Add(valorCuotaLabel, 0, 8);
                grid.Children.Add(valorCuota, 1, 8);
                var fechaPrimerCuotaLabel = new Label { Text = "Fecha Primer Cuota:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand};
                var fechaPrimerCuota = new Label { Text = factura.fechaPrimerCuotaMostrar.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand};
                grid.Children.Add(fechaPrimerCuotaLabel, 0, 9);
                grid.Children.Add(fechaPrimerCuota, 1, 9);
                if(factura.anticipo > 0)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                    var AnticipoLabel = new Label { Text = "Anticipo:", FontAttributes = FontAttributes.Bold, TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand,};
                    var Anticipo = new Label { Text = "$" + factura.anticipo.ToString(), TextColor = Color.Black, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.StartAndExpand};
                    grid.Children.Add(AnticipoLabel, 0, 10);
                    grid.Children.Add(Anticipo, 1, 10);
                }
            }

            ProductosListview.ItemsSource = factura.listaProductos;

            ProductosListview.HeightRequest = 50 * factura.listaProductos.Count();
        }



        async void OnSaveClicked(object sender, EventArgs e)
        { 

            var itemEstado = pickerEstado.SelectedItem;
            foreach (var item in listaEstados)
            {
                if (item.nombreEstadoFactura == itemEstado.ToString())
                {
                    facturaAGuardar.idEstado = item.idEstadoFactura;
                    facturaAGuardar.nombreEstado = item.nombreEstadoFactura;
                    facturaAGuardar.nombreImagenFactura = "DerivadoAdmin2.png";
                    

                }
            }

            facturaAGuardar.observacion = observacionEditor.Text;

            await App.Database.SaveIFacturaAsync(facturaAGuardar);
            await Navigation.PopAsync();
            //var current = Connectivity.NetworkAccess;
            //var profiles = Connectivity.ConnectionProfiles;
            ////reclamos
            ////if (current == NetworkAccess.Internet)
            //if (profiles.Contains(ConnectionProfile.WiFi) == true)
            //{
            //    WSReclamo cliente = new WSReclamo();
            //    WSEstado estado = new WSEstado();
            //    WSCliente clientes = new WSCliente();
            //    //enviar datos
            //    await cliente.PutReclamo();
            //}
            //else
            //{
            //}
        }
    }
}