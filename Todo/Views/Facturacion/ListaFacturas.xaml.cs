﻿using System;
using System.Collections.Generic;
using System.Linq;
using Todo.Models;
using Todo.Views.Clientes;
using Todo.ViewsModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Todo.Views.Facturacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListaFacturas : ContentPage
	{
        List<VMFactura> listaFacturas = new List<VMFactura>();
        bool puedeEntrar;

        public ListaFacturas()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            this.Content.IsEnabled = true;
            puedeEntrar = true;
            base.OnAppearing();
            var listaFacturas = await App.Database.GetFacturaNotDoneAsync();
            foreach (var item in listaFacturas)
            {
                item.fechaFacturaMostrar = item.fechaFactura.ToString("dd/MM/yy");
                item.horaFacturaMostrar = item.fechaFactura.ToString("HH:mm");
            }
            listaFacturas = listaFacturas.OrderByDescending(x => x.fechaFactura).ToList();
            listViewFacturas.ItemsSource = listaFacturas;
            if(listaFacturas.Count!= 0)
            {
                frameFacturas.IsVisible = true;
                frameFacturas.HeightRequest = listaFacturas.Count * 45 + 45;
                frameFacturas.IsVisible = true;

            }
            else
            {
                frameFacturas.IsVisible = false;
            }

        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            if (puedeEntrar)
            {
                puedeEntrar = false;
                this.Content.IsEnabled = false;

                var listadoTalonario = await App.Database.GetTalonariosAsync();
                if (listadoTalonario.Count > 0)
                {
                    var listadoProductos = await App.Database.GetProductoAsync();
                    if (listadoProductos.Count > 0)
                    {
                        await Navigation.PushAsync(new BuscarCliente(0));
                        this.Content.IsEnabled = true;

                    }
                    else
                    {
                        this.Content.IsEnabled = true;
                        puedeEntrar = true;
                        await DisplayAlert("", "Sin stock de productos. Por favor, comuníquese con administración", "Ok");
                    }
                }
                else
                {
                    this.Content.IsEnabled = true;
                    puedeEntrar = true;
                    await DisplayAlert("", "Sin talonarios disponibles. Por favor, comuníquese con administración", "Ok");
                }
            }
        }

        async void facturaElegida(object sender, SelectedItemChangedEventArgs e)
        {
            this.Content.IsEnabled = false;

            if (e.SelectedItem != null)
            {
                FacturaApp f = (FacturaApp)e.SelectedItem;
                f.fechaFacturaMostrar = f.fechaFactura.ToString("dd/MM/yyyy");
                VMFactura factura = new VMFactura
                {
                    anticipo = f.anticipo,
                    cantidadCuotas = f.cantidadCuotas,
                    cantProductos = f.cantProductos,
                    domicilioCliente = f.domicilioCliente,
                    fechaFactura = f.fechaFactura,
                    fechaFacturaMostrar = f.fechaFacturaMostrar,
                    horaFacturaMostrar = f.horaFacturaMostrar,
                    fechaPrimerCuota = f.fechaPrimerCuota,
                    fechaPrimerCuotaMostrar = f.fechaPrimerCuotaMostrar,
                    financiacion = f.financiacion,
                    formaDePagoNombre = f.formaDePagoNombre,
                    idEstado = f.idEstado,
                    idEstado2 = f.idEstado2,
                    idFactura = f.idFactura,
                    nroFactura = f.nroFactura,
                    idFormaPago = f.idFormaPago,
                    idinterno = f.idinterno,
                    montoFactura = f.montoFactura,
                    nombreCliente = f.nombreCliente,
                    nroCliente = f.nroCliente,
                    totalDebito = f.totalDebito,
                    totalCredito = f.totalCredito,
                    totalContado = f.totalContado,
                    FormaPago = f.formaPago,
                    anulado = f.anulado,
                    observacion = f.observacion,
                    montoDisponibleCliente = f.montoDisponibleCliente
                    
                };

                factura.dniCliente = Int32.Parse(f.dniCliente);
                //llamada al metodo obtener los productos comprados en esa factura

                List<VMProducto> listaProductosFactura = new List<VMProducto>();
                List<FacturaProducto> listaProductos = await App.Database.GetListFacturasProductosAsync(f.nroFactura);

                foreach (var item in listaProductos)
                {
                    VMProducto p = new VMProducto();
                    p.idProducto = item.idProducto;
                    p.cantidadComprada = item.cantidadComprada;
                    p.precioUnitario = item.precio;
                    p.idFormaPago = item.idFormaPago;
                    p.nombreProducto = item.nombreProducto;
                    p.precioTotal = item.precioTotal;

                    listaProductosFactura.Add(p);
                }

                factura.listaProductos = new List<VMProducto>();
                factura.listaProductos = listaProductosFactura;


                //llamada al metodo obtener la factura Cuota comprados en esa factura

                List<FacturaCuota> listaCuota = await App.Database.GetFacturaCuotaAsync(f.nroFactura);
                List<VMFacturaCouta> listaFacturaCuota = new List<VMFacturaCouta>();


                foreach (var item in listaCuota)
                {
                    VMFacturaCouta c = new VMFacturaCouta();
                    c.nombreCuota = item.nombreCuota;
                    c.monto = item.monto;
                    c.fechaVto = item.fechaVto;


                    listaFacturaCuota.Add(c);
                }

                factura.listaFacturaCuota = new List<VMFacturaCouta>();
                factura.listaFacturaCuota = listaFacturaCuota;



                if (factura.idEstado == 13)
                {

                    await Navigation.PushAsync(new AltaFacturaImprimir(factura,5));
                    this.Content.IsEnabled = true;

                }
                else
                {
                    if (factura.idEstado == 14)
                    {

                        await Navigation.PushAsync(new AltaFacturaProductos(factura, 9, 2));
                        this.Content.IsEnabled = true;

                    }
                }
            }
            this.Content.IsEnabled = true;

        }

        public async void CerrarDia(object sender, EventArgs e)
        {
            this.Content.IsEnabled = false;

            await this.Navigation.PushAsync(new FinalizarDia());
            this.Content.IsEnabled = true;

        }

    }
}