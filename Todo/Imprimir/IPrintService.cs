﻿using System.Collections.Generic;
using System.IO;

namespace Todo.Imprimir
{
    public interface IPrintService
    {
        IList<string> GetDeviceList();
        void Print(Stream inputStream, string fileName);
    }
}
