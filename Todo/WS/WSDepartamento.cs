﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSDepartamento
    {
        //buscar listado de departamentos
        public async Task<List<Departamento>> GetDepartamentos<T>(string url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try { 
                var content = await response.Content.ReadAsStringAsync();
                var lista = JsonConvert.DeserializeObject<List<Departamento>>(content);
                return lista;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "009-departamento";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "009-departamento";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }

        //listado de localidades
        public async Task<List<Localidad>> GetLocalidades<T>(string url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var lista = JsonConvert.DeserializeObject<List<Localidad>>(content);
                    return lista;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "008-localidad";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "008-localidad";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }
    }
}
