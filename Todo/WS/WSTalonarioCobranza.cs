﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSTalonarioCobranza
    {
        HttpClient client = new HttpClient();

        // metodo get para obtener el listado de talonarios de cobranza
        public async Task<List<TalonarioCobranza>> GetListaTalonarioCobranza<T>(string Url)
        {
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {

                var content = await response.Content.ReadAsStringAsync();
                var listado = JsonConvert.DeserializeObject<List<TalonarioCobranza>>(content);
                return listado;
            }
            else
            {
                return null;
            }
        }
    }
}
