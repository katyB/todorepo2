﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSMedioPago
    {
        HttpClient client = new HttpClient();

        // metodo get 
        public async Task<List<FormaPago>> GetMedioPago<T>(string Url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Xamarin.Essentials.Preferences.Get("idusuarioLogueado", 0);
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<FormaPago>>(content);
                    return listado;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "015-medioPago";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "015-medioPago";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }
    }
}
