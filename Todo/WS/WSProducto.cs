﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSProducto
    {
        HttpClient client = new HttpClient();

        //buscar listado de productos asignados
        public async Task<List<Producto>> GetProductos<T>(string Url)
        {
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var productosRecibidos = JsonConvert.DeserializeObject<List<VMProductosRecibidos>>(content);

                List<Producto> listadoproductos = new List<Producto>();
                foreach (var item in productosRecibidos)
                {
                    Producto producto = new Producto {
                        nombreProducto = item.nombreProducto,
                        idProducto = item.idProducto,
                        codigoProducto = item.codigoProducto,
                        saldo = item.saldo,
                        cantidad = item.cantidad,
                        precioUnitario = (int)item.precioUnitario,
                        precioTotal = (int)item.precioTotal,
                    };
                    listadoproductos.Add(producto);
                }

                return listadoproductos;
            }
            else
            {
                return null;
            }
        }

        //devolver productos
        public async Task<String> DevolverProductos(int idProducto, int cantidad)
        {
            var idDeposito = Preferences.Get("idDeposito", 0);
            var current = Connectivity.NetworkAccess;
            var urlDevolverProducto = "http://biancogestion.com:3630/api/Producto/cambiarDeposito";
            //var urlDevolverProducto = "http://biancogestion.com/api/Producto/cambiarDeposito";
            VMCliente element = new VMCliente();
            if (current == NetworkAccess.Internet)
            {
                try
                {
                    string sContentType = "application/json";

                    JObject parameters = new JObject();
                    parameters.Add("idProducto", (idProducto).ToString());
                    parameters.Add("cantidad", (cantidad).ToString());
                    parameters.Add("idDeposito", (idDeposito).ToString());

                    var respuesta = await client.PutAsync(urlDevolverProducto, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                    var json = await respuesta.Content.ReadAsStringAsync();
                    var producto = JsonConvert.DeserializeObject<VMDevolverProducto>(json);

                    if (respuesta.IsSuccessStatusCode)
                    {
                        element.mensaje = producto.mensaje;
                        return element.mensaje;
                    }
                    else
                    {
                        var mensaje1 = "Error del conexión. Por favor, intente nuevamene en unos minutos";
                        return mensaje1;
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    var mensaje1 = "Error  de conexion, intente nuevamene en unos minutos.";
                    return mensaje1;
                }

            }
            else
            {
                var mensaje = "Error  de conexion";
                return mensaje;
            }

        }

    }
}
