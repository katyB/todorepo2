﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSInformacion
    {

        public async Task<Informacion> GetInformacion<T>(string url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var informacion = JsonConvert.DeserializeObject<Informacion>(content);
                    return informacion;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "021- Informacion";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "021- Informacion";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }

        public async Task<InfoMonto> GetMontoSuperado<T>()
        {
            string url = "http://biancogestion.com/api/Cliente/calcularLimiteVenta";

            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var informacionMontoSuperado = JsonConvert.DeserializeObject<InfoMonto>(content);
                    return informacionMontoSuperado;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "025- informacionMontoSuperado";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "025- informacionMontoSuperado";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }


    }
}

