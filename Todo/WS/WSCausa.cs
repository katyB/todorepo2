﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSCausa
    {
        string urlCausa = "http://biancogestion.com:3630//api/Reclamo/ListarCausa";
        //string urlCausa = "http://biancogestion.com/api/Reclamo/ListarCausa";

        //buscar listado de estdos desde el buho
        public async Task<List<CausaReclamo>> GetCausas<T>()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(urlCausa);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listado = JsonConvert.DeserializeObject<List<CausaReclamo>>(content);
                    return listado;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "002-causaReclamo";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "002-causaReclamo";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }
    }
}
