﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSControlErrores
    {
        public async Task EnviarControlErrores()
        {
            HttpClient client = new HttpClient();

            var listado = await App.Database.GetControlErrores();

            var url = "http://biancogestion.com:3630/api/General/GrabarErrorApp";
            //var url = "http://biancogestion.com/api/General/GrabarErrorApp";

            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {

                    if (element.sincronizado == false )
                    {
                        string sContentType = "application/json";

                        ViewsModels.VMControlErrores model = new ViewsModels.VMControlErrores();
                        model.codigoControlErrores = element.codigoControlErrores;
                        model.fecha = element.fecha;
                        model.nombreError = element.nombreError;
                        
                            var jsonstring = JsonConvert.SerializeObject(model);

                            var response = await client.PostAsync(url, new StringContent(jsonstring, Encoding.UTF8, sContentType));

                            try
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                element.sincronizado = true;
                                await App.Database.SaveIControlErroresAsync(element);
                            }
                            }
                            catch (Exception ex)
                            {
                                var error = ex.Message;
                            }
                    }
                }
            }
        }
    }
}
