﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSCliente: IDisposable
    {
        HttpClient client = new HttpClient();

        string urlunicaApp = "http://biancogestion.com:3630/api/Cliente/ActualizarClienteApp";
        //string urlunicaApp = "http://biancogestion.com/api/Cliente/ActualizarClienteApp";

        //buscar listado de clientes desde el buho
        public async Task<List<ClienteApp>> GetClientes<T>(String url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);
            var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var lista = JsonConvert.DeserializeObject<List<VMClienteRecibido>>(content);

                    List<ClienteApp> listaclientes = new List<ClienteApp>();

                    foreach (var item in lista)
                    {
                        ClienteApp cliente = new ClienteApp
                        {
                            codigoCliente = item.codigoCliente,
                            idCliente = item.idCliente,
                            nombreCliente = item.nombreCliente,
                            domicilioCliente = item.domicilioCliente,
                            localidadCliente = item.localidadCliente,
                            latitud = Convert.ToDouble(item.latitud),
                            longitud = Convert.ToDouble(item.longitud),
                            telefono = item.telefonoCel,
                            montoTotalMaximo = (int)item.montoTotalMaximo,
                            mensajeEstado = item.mensaje,
                            codPostal = item.codPostal,
                            nroDocumento =item.nroDocumento,
                            idEstado = item.idEstado,
                            nombreEstado = item.nombreEstado,
                            idDeposito = item.idDeposito,
                            montoDisponible = (int)item.montoDisponible,
                            idLocalidad = item.idLocalidad,
                            idDepartamento = item.idDepartamento,
                        };

                        listaclientes.Add(cliente);
                    }
                    return listaclientes;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "003-clientes";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "003-clientes";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }

            //validar estado de cliente en api conectada a veraz
         public async Task<VMAutorizacionCliente> ValidarEstadoCliente(string nroDocumento, string genero, string nombre)
        {
            var current = Connectivity.NetworkAccess;
            var urlvalidarCliente = "http://biancogestion.com:3630/api/Cliente/VerificarClienteApp?";
            //var urlvalidarCliente = "http://biancogestion.com/api/Cliente/VerificarClienteApp?";
            VMCliente element = new VMCliente();

            //nroDocumento = "27412889";
            nroDocumento = SepararDocumento(nroDocumento);

            VMAutorizacionCliente autorizacion = new VMAutorizacionCliente();
            if (current == NetworkAccess.Internet)
            {
                try
                {
                    var respuesta = await client.GetAsync(urlvalidarCliente + "nroDocumento=" + nroDocumento + "&sexo=" + genero + "&apellidoNombre=" + nombre);

                    var json = await respuesta.Content.ReadAsStringAsync();
                    var cliente = JsonConvert.DeserializeObject<VMClienteRecibido>(json);

                    if (respuesta.IsSuccessStatusCode)
                    {                   
                        //vm con los datos que enviamos
                        autorizacion.mensaje = cliente.mensaje;
                        autorizacion.cantComprasAnteriores = cliente.cantComprasAnteriores;
                        autorizacion.cantReclamosAnteriores = cliente.cantReclamosAnteriores;
                        autorizacion.montoTotalMaximoCliente = (int)cliente.montoTotalMaximo;
                        autorizacion.montoDisponible = (int)cliente.montoAutorizado;
                        autorizacion.autorizado = cliente.autorizado;
                        if (string.IsNullOrWhiteSpace(cliente.codigoCliente))
                        {
                            cliente.codigoCliente = "0";
                        }
                        autorizacion.codigoCliente = cliente.codigoCliente;
                        autorizacion.idEstado = cliente.idEstado;
                        autorizacion.tieneConexion = true;
                        autorizacion.nuevoCliente = cliente.nuevoCliente;
                        autorizacion.idCliente = cliente.idCliente;
                    }
                    else
                    {
                        var error = "No se puede acceder a la api, error 500";
                        ControlErrores control = new ControlErrores();
                        DateTime fecha = DateTime.Now;
                        control.codigoControlErrores = "007-clienteVeraz";
                        control.sincronizado = false;
                        control.nombreError = error;
                        control.fecha = fecha;
                        await App.Database.SaveIControlErroresAsync(control);
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "007-clienteVeraz";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    await App.Database.SaveIControlErroresAsync(control);

                    var mensaje = "Error  de conexion, intente de nuevo.";
                    element.mensaje = mensaje;
                    autorizacion.autorizado = false;
                    autorizacion.tieneConexion = false;
                    autorizacion.codigoCliente = "";
                }

            }
            else
            {
                var mensaje = "Error  de conexion, intente de nuevo.";
                element.mensaje = mensaje;
                autorizacion.autorizado = false;
                autorizacion.tieneConexion = false;
                autorizacion.codigoCliente = "";
           
            }
            return autorizacion;
        }

        //enviar cliente nuevos
        public async Task EnviarCliente()
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);
            var listado = await App.Database.GetClientesAsync();

            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    if (element.sincronizado == false & element.idEstado == 7)
                    {
                        try
                        {
                            //element.nroDocumento = "27412889";
                            //element.nroDocumento = SepararDocumento(element.nroDocumento);

                            string sContentType = "application/json";
                            JObject parameters = new JObject();
                            parameters.Add("nroCliente", (element.codigoCliente).ToString());
                            parameters.Add("nombreCliente", (element.nombreCliente).ToString());
                            parameters.Add("nroDocumento", element.nroDocumento.ToString());
                            parameters.Add("idUsuario", idUsuario.ToString());
                            parameters.Add("idEstado", (element.idEstado).ToString());

                            if (element.domicilioCliente != null)
                            {
                                parameters.Add("domicilio", (element.domicilioCliente).ToString());
                            }
                            else
                            {
                                parameters.Add("domicilio", "");
                            }
                            if (element.domicilioCliente != null)
                            {
                                parameters.Add("domicilioCliente", (element.domicilioCliente).ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioCliente", "");
                            }

                            if (element.domicilioCliente != null)
                            {
                                parameters.Add("domicilioCliente", (element.domicilioCliente).ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioCliente", "");
                            }

                            parameters.Add("idLocalidad", (element.idLocalidad).ToString());
                            
                            parameters.Add("idDepartamento", (element.idDepartamento).ToString());

                            parameters.Add("latitud", (element.latitud).ToString());

                            parameters.Add("longitud", (element.longitud).ToString());
 
                            parameters.Add("montoDisponible", element.montoDisponible.ToString());

                            parameters.Add("ingresoMensual", element.ingresoMensual.ToString());

                            parameters.Add("ingresoComplementario", element.ingresoComplementarios.ToString());

                            parameters.Add("ingresoOtro", element.ingresoOtro.ToString());

                            parameters.Add("ingresoConyuge", element.ingresoConyogue.ToString());

                            if (element.actividad != null)
                            {

                                parameters.Add("actividad", element.actividad.ToString());
                            }
                            else
                            {
                                parameters.Add("actividad", "");
                            }

                            parameters.Add("idLocalidadOtro", element.idLocalidadAlternativa.ToString());
                            parameters.Add("idDepartamentoOtro", element.idDepartamentoAlternativo.ToString());
                            if (element.domicilioAlternativo != null)
                            {

                                parameters.Add("domicilioOtro", element.domicilioAlternativo.ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioOtro", "");
                            }

                            parameters.Add("idDepartamentoLaboral", element.idDepartamentoAlternativo.ToString()); //ver


                            if (element.genero != null)
                            {
                                parameters.Add("sexo", element.genero.ToString());
                            }
                            else
                            {
                                parameters.Add("sexo", "");
                            }

                            if (element.domicilioEmpresa != null)
                            {
                                parameters.Add("domicilioLaboral", element.domicilioEmpresa.ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioLaboral", "");
                            }

                            if (element.actividad2 != null)
                            {
                                parameters.Add("actividad2", element.actividad2.ToString());
                            }
                            else
                            {
                                parameters.Add("actividad2", "");
                            }
                            if (element.empresa != null)
                            {
                                parameters.Add("empresa", element.empresa.ToString());
                            }
                            else
                            {
                                parameters.Add("empresa", "");
                            }
                            if (element.Antiguedad != null)
                            {
                                parameters.Add("antiguedad", element.Antiguedad.ToString());
                            }
                            else
                            {
                                parameters.Add("antiguedad", "");
                            }
                            if (element.fechaNacimiento != null)
                            {
                                parameters.Add("fechaNacimiento", element.fechaNacimiento.ToString());
                            }
                            else
                            {
                                parameters.Add("fechaNacimiento", "");
                            }
                            if (element.estadoCivil != null)
                            {
                                parameters.Add("estadoCivil", element.estadoCivil.ToString());
                            }
                            else
                            {
                                parameters.Add("estadoCivil", "");
                            }
                            if (element.cantidadHijo != null)
                            {
                                parameters.Add("cantidadHijos", element.cantidadHijo.ToString());
                            }
                            else
                            {
                                parameters.Add("cantidadHijos", "");
                            }
                            if (element.referencia != null)
                            {

                                parameters.Add("referencia", element.referencia.ToString());
                            }
                            else
                            {
                                parameters.Add("referencia", "");
                            }
                            if (element.vivienda != null)
                            {

                                parameters.Add("vivienda", element.vivienda.ToString());
                            }
                            else
                            {
                                parameters.Add("vivienda", "");
                            }
                            if (element.telefono != null)
                            {

                                parameters.Add("telefono", element.telefono.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono", "");
                            }
                            if (element.telefonoFijo != null)
                            {

                                parameters.Add("telefono1", element.telefonoFijo.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono1", "");
                            }
                            if (element.telFamiliar != null)
                            {

                                parameters.Add("telefono2", element.telFamiliar.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono2", "");
                            }
                            if (element.telefonoEmpresa != null)
                            {

                                parameters.Add("telefono3", element.telefonoEmpresa.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono3", "");
                            }
                            if (element.tarjeta != null)
                            {

                                parameters.Add("tarjetaCredito", element.tarjeta.ToString());
                            }
                            else
                            {
                                parameters.Add("tarjetaCredito", "");
                            }
                            if (element.nombreConyugue != null)
                            {

                                parameters.Add("nombreConyuge", element.nombreConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("nombreConyuge", "");
                            }

                            if (element.dniConyugue != null)
                            {

                                parameters.Add("dniConyuge", element.dniConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("dniConyuge", "");
                            }
                            if (element.empresaConyugue != null)
                            {

                                parameters.Add("empresaConyuge", element.empresaConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("empresaConyuge", "");
                            }
                            if (element.antiguedadConyugue != null)
                            {

                                parameters.Add("antiguedadConyuge", element.antiguedadConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("antiguedadConyuge", "");
                            }
                            if (element.vinculoFamiliarOtro != null)
                            {

                                parameters.Add("nombreVinculo", element.vinculoFamiliarOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("nombreVinculo", "");
                            }

                            if (element.dniOtro != null)
                            {

                                parameters.Add("dniOtroVinculo", element.dniOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("dniOtroVinculo", "");
                            }
                            if (element.empresaOtro != null)
                            {

                                parameters.Add("empresaOtroVinculo", element.empresaOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("empresaOtroVinculo", "");
                            }

                            if (element.referenciaVecinal != null)
                            {

                                parameters.Add("referenciaVecinal", element.referenciaVecinal.ToString());
                            }
                            else
                            {

                                parameters.Add("referenciaVecinal", "");
                            }

                            var respuesta = await client.PutAsync(urlunicaApp, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                            var json = await respuesta.Content.ReadAsStringAsync();
                            var cliente = JsonConvert.DeserializeObject<VMClienteAutorizadoRecibido>(json);


                            if (respuesta.IsSuccessStatusCode)
                            {
                                element.sincronizado = true;
                                element.mensaje = cliente.mensaje;
                                element.codigoCliente = cliente.codigoCliente.ToString();
                                element.idEstado = cliente.idEstado;
                                element.montoDisponible = (int)cliente.montoDisponible;
                                element.montoTotalMaximo = (int)cliente.montoTotalMaximoCliente;
                                element.nuevoApp = true;
                                await App.Database.SaveIClienteAsync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveIClienteAsync(element);

                            }
                        }


                        catch (Exception ex)
                        {
                            var error = ex.Message;
                            ControlErrores control = new ControlErrores();
                            DateTime fecha = DateTime.Now;
                            control.codigoControlErrores = "016-altaCliente";
                            control.sincronizado = false;
                            control.nombreError = error;
                            control.fecha = fecha;
                            control.idUsuario = idUsuario;
                            control.version = version;
                            await App.Database.SaveIControlErroresAsync(control);
           
                        }
                    }
                }
            }
            else
            {

            }
        }

        //enviar clientes modificados
        public async Task ModificarCliente()
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);
            var listado = await App.Database.GetClientesAsync();

            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    if (element.sincronizado == false & element.idEstado == 7)
                    {
                        try
                        {
                            //element.nroDocumento = "27412889";
                            element.nroDocumento = SepararDocumento(element.nroDocumento);

                            string sContentType = "application/json";
                            JObject parameters = new JObject();
                            parameters.Add("nroCliente", (element.codigoCliente).ToString());
                            parameters.Add("nombreCliente", (element.nombreCliente).ToString());
                            parameters.Add("nroDocumento", element.nroDocumento.ToString());
                            parameters.Add("idUsuario", idUsuario.ToString());
                            parameters.Add("idEstado", (element.idEstado).ToString());

                            if (element.domicilioCliente != null)
                            {
                                parameters.Add("domicilio", (element.domicilioCliente).ToString());
                            }
                            else
                            {
                                parameters.Add("domicilio", "");
                            }

                            if (element.domicilioCliente != null)
                            {
                                parameters.Add("domicilioCliente", (element.domicilioCliente).ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioCliente", "");
                            }
                            parameters.Add("idLocalidad", (element.idLocalidad).ToString());

                            parameters.Add("idDepartamento", (element.idDepartamento).ToString());

                            parameters.Add("latitud", (element.latitud).ToString());

                            parameters.Add("longitud", (element.longitud).ToString());

                            parameters.Add("montoDisponible", element.montoDisponible.ToString());

                            parameters.Add("ingresoMensual", element.ingresoMensual.ToString());

                            parameters.Add("ingresoComplementario", element.ingresoComplementarios.ToString());

                            parameters.Add("ingresoOtro", element.ingresoOtro.ToString());

                            parameters.Add("ingresoConyuge", element.ingresoConyogue.ToString());

                            if (element.actividad != null)
                            {

                                parameters.Add("actividad", element.actividad.ToString());
                            }
                            else
                            {
                                parameters.Add("actividad", "");
                            }

                            parameters.Add("idDepartamentoLaboral", element.idDepartamentoAlternativo.ToString()); //ver
                            parameters.Add("idLocalidadOtro", element.idLocalidadAlternativa.ToString());
                            parameters.Add("idDepartamentoOtro", element.idDepartamentoAlternativo.ToString());
                            if (element.domicilioAlternativo != null)
                            {

                                parameters.Add("domicilioOtro", element.domicilioAlternativo.ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioOtro", "");
                            }


                            if (element.genero != null)
                            {
                                parameters.Add("sexo", element.genero.ToString());
                            }
                            else
                            {
                                parameters.Add("sexo", "");
                            }

                            if (element.domicilioEmpresa != null)
                            {
                                parameters.Add("domicilioLaboral", element.domicilioEmpresa.ToString());
                            }
                            else
                            {
                                parameters.Add("domicilioLaboral", "");
                            }

                            if (element.actividad2 != null)
                            {
                                parameters.Add("actividad2", element.actividad2.ToString());
                            }
                            else
                            {
                                parameters.Add("actividad2", "");
                            }
                            if (element.empresa != null)
                            {
                                parameters.Add("empresa", element.empresa.ToString());
                            }
                            else
                            {
                                parameters.Add("empresa", "");
                            }
                            if (element.Antiguedad != null)
                            {
                                parameters.Add("antiguedad", element.Antiguedad.ToString());
                            }
                            else
                            {
                                parameters.Add("antiguedad", "");
                            }
                            if (element.fechaNacimiento != null)
                            {
                                parameters.Add("fechaNacimiento", element.fechaNacimiento.ToString());
                            }
                            else
                            {
                                parameters.Add("fechaNacimiento", "");
                            }
                            if (element.estadoCivil != null)
                            {
                                parameters.Add("estadoCivil", element.estadoCivil.ToString());
                            }
                            else
                            {
                                parameters.Add("estadoCivil", "");
                            }
                            if (element.cantidadHijo != null)
                            {
                                parameters.Add("cantidadHijos", element.cantidadHijo.ToString());
                            }
                            else
                            {
                                parameters.Add("cantidadHijos", "");
                            }
                            if (element.referencia != null)
                            {

                                parameters.Add("referencia", element.referencia.ToString());
                            }
                            else
                            {
                                parameters.Add("referencia", "");
                            }
                            if (element.vivienda != null)
                            {

                                parameters.Add("vivienda", element.vivienda.ToString());
                            }
                            else
                            {
                                parameters.Add("vivienda", "");
                            }
                            if (element.telefono != null)
                            {

                                parameters.Add("telefono", element.telefono.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono", "");
                            }
                            if (element.telefonoFijo != null)
                            {

                                parameters.Add("telefono1", element.telefonoFijo.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono1", "");
                            }
                            if (element.telFamiliar != null)
                            {

                                parameters.Add("telefono2", element.telFamiliar.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono2", "");
                            }
                            if (element.telefonoEmpresa != null)
                            {

                                parameters.Add("telefono3", element.telefonoEmpresa.ToString());
                            }
                            else
                            {
                                parameters.Add("telefono3", "");
                            }
                            if (element.tarjeta != null)
                            {

                                parameters.Add("tarjetaCredito", element.tarjeta.ToString());
                            }
                            else
                            {
                                parameters.Add("tarjetaCredito", "");
                            }
                            if (element.nombreConyugue != null)
                            {

                                parameters.Add("nombreConyuge", element.nombreConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("nombreConyuge", "");
                            }

                            if (element.dniConyugue != null)
                            {

                                parameters.Add("dniConyuge", element.dniConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("dniConyuge", "");
                            }
                            if (element.empresaConyugue != null)
                            {

                                parameters.Add("empresaConyuge", element.empresaConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("empresaConyuge", "");
                            }
                            if (element.antiguedadConyugue != null)
                            {

                                parameters.Add("antiguedadConyuge", element.antiguedadConyugue.ToString());
                            }
                            else
                            {
                                parameters.Add("antiguedadConyuge", "");
                            }
                            if (element.vinculoFamiliarOtro != null)
                            {

                                parameters.Add("nombreVinculo", element.vinculoFamiliarOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("nombreVinculo", "");
                            }

                            if (element.dniOtro != null)
                            {

                                parameters.Add("dniOtroVinculo", element.dniOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("dniOtroVinculo", "");
                            }
                            if (element.empresaOtro != null)
                            {

                                parameters.Add("empresaOtroVinculo", element.empresaOtro.ToString());
                            }
                            else
                            {
                                parameters.Add("empresaOtroVinculo", "");
                            }

                            if (element.referenciaVecinal != null)
                            {

                                parameters.Add("referenciaVecinal", element.referenciaVecinal.ToString());
                            }
                            else
                            {

                                parameters.Add("referenciaVecinal", "");
                            }

                            var respuesta = await client.PutAsync(urlunicaApp, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));


                            if (respuesta.IsSuccessStatusCode)
                            {

                                var json = await respuesta.Content.ReadAsStringAsync();
                                var cliente = JsonConvert.DeserializeObject<VMClienteAutorizadoRecibido>(json);
                                element.sincronizado = true;
                                element.mensaje = cliente.mensaje;
                                element.codigoCliente = cliente.codigoCliente.ToString();
                                element.idEstado = cliente.idEstado;
                                element.montoDisponible = (int)cliente.montoDisponible;
                                element.montoTotalMaximo = (int)cliente.montoTotalMaximoCliente;
                                await App.Database.SaveIClienteAsync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveIClienteAsync(element);
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = ex.Message;
                            ControlErrores control = new ControlErrores();
                            DateTime fecha = DateTime.Now;
                            control.codigoControlErrores = "004-modificarCliente";
                            control.sincronizado = false;
                            control.nombreError = error;
                            control.fecha = fecha;
                            control.idUsuario = idUsuario;
                            control.version = version;
                            await App.Database.SaveIControlErroresAsync(control);
                           
                        }
                    }
               
                }
            }
        }

        //enviar un  cliente modificado
        public async Task<VMAutorizacionCliente> ModificarUnCliente(ClienteApp element)
        {
            var current = Connectivity.NetworkAccess;
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);
            VMAutorizacionCliente autorizacion = new VMAutorizacionCliente();
            if (current == NetworkAccess.Internet)
            {
                if (element.sincronizado == false & element.idEstado == 7)
                {
                    try
                    {
                        //element.nroDocumento = "27412889";
                        element.nroDocumento = SepararDocumento(element.nroDocumento);

                        string sContentType = "application/json";
                        JObject parameters = new JObject();
                        parameters.Add("nroCliente", (element.codigoCliente).ToString());
                        parameters.Add("nombreCliente", (element.nombreCliente).ToString());                        
                        parameters.Add("nroDocumento", element.nroDocumento.ToString());
                        parameters.Add("idUsuario", idUsuario.ToString());
                        parameters.Add("idEstado", (element.idEstado).ToString());

                        if (element.domicilioCliente != null)
                        {
                            parameters.Add("domicilio", (element.domicilioCliente).ToString());
                        }
                        else
                        {
                            parameters.Add("domicilio", "");
                        }

                        parameters.Add("idLocalidad", (element.idLocalidad).ToString());

                        parameters.Add("idDepartamento", (element.idDepartamento).ToString());

                        parameters.Add("latitud", (element.latitud).ToString());

                        parameters.Add("longitud", (element.longitud).ToString());

                        parameters.Add("montoDisponible", element.montoDisponible.ToString());

                        parameters.Add("ingresoMensual", element.ingresoMensual.ToString());

                        parameters.Add("ingresoComplementario", element.ingresoComplementarios.ToString());

                        parameters.Add("ingresoOtro", element.ingresoOtro.ToString());

                        parameters.Add("ingresoConyuge", element.ingresoConyogue.ToString());

                        if (element.actividad != null)
                        {

                            parameters.Add("actividad", element.actividad.ToString());
                        }
                        else
                        {
                            parameters.Add("actividad", "");
                        }

                        parameters.Add("idDepartamentoLaboral", element.idDepartamentoAlternativo.ToString()); //ver

                        parameters.Add("idLocalidadOtro", element.idLocalidadAlternativa.ToString());
                        parameters.Add("idDepartamentoOtro", element.idDepartamentoAlternativo.ToString());
                        if (element.domicilioAlternativo != null)
                        {

                            parameters.Add("domicilioOtro", element.domicilioAlternativo.ToString());
                        }
                        else
                        {
                            parameters.Add("domicilioOtro", "");
                        }


                        if (element.genero != null)
                        {
                            parameters.Add("sexo", element.genero.ToString());
                        }
                        else
                        {
                            parameters.Add("sexo", "");
                        }

                        if (element.domicilioEmpresa != null)
                        {
                            parameters.Add("domicilioLaboral", element.domicilioEmpresa.ToString());
                        }
                        else
                        {
                            parameters.Add("domicilioLaboral", "");
                        }

                        if (element.actividad2 != null)
                        {
                            parameters.Add("actividad2", element.actividad2.ToString());
                        }
                        else
                        {
                            parameters.Add("actividad2", "");
                        }
                        if (element.empresa != null)
                        {
                            parameters.Add("empresa", element.empresa.ToString());
                        }
                        else
                        {
                            parameters.Add("empresa", "");
                        }
                        if (element.Antiguedad != null)
                        {
                            parameters.Add("antiguedad", element.Antiguedad.ToString());
                        }
                        else
                        {
                            parameters.Add("antiguedad", "");
                        }
                        if (element.fechaNacimiento != null)
                        {
                            parameters.Add("fechaNacimiento", element.fechaNacimiento.ToString());
                        }
                        else
                        {
                            parameters.Add("fechaNacimiento", "");
                        }
                        if (element.estadoCivil != null)
                        {
                            parameters.Add("estadoCivil", element.estadoCivil.ToString());
                        }
                        else
                        {
                            parameters.Add("estadoCivil", "");
                        }
                        if (element.cantidadHijo != null)
                        {
                            parameters.Add("cantidadHijos", element.cantidadHijo.ToString());
                        }
                        else
                        {
                            parameters.Add("cantidadHijos", "");
                        }
                        if (element.referencia != null)
                        {

                            parameters.Add("referencia", element.referencia.ToString());
                        }
                        else
                        {
                            parameters.Add("referencia", "");
                        }
                        if (element.vivienda != null)
                        {

                            parameters.Add("vivienda", element.vivienda.ToString());
                        }
                        else
                        {
                            parameters.Add("vivienda", "");
                        }
                        if (element.telefono != null)
                        {

                            parameters.Add("telefono", element.telefono.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono", "");
                        }
                        if (element.telefonoFijo != null)
                        {

                            parameters.Add("telefono1", element.telefonoFijo.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono1", "");
                        }
                        if (element.telFamiliar != null)
                        {

                            parameters.Add("telefono2", element.telFamiliar.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono2", "");
                        }
                        if (element.telefonoEmpresa != null)
                        {

                            parameters.Add("telefono3", element.telefonoEmpresa.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono3", "");
                        }
                        if (element.tarjeta != null)
                        {

                            parameters.Add("tarjetaCredito", element.tarjeta.ToString());
                        }
                        else
                        {
                            parameters.Add("tarjetaCredito", "");
                        }
                        if (element.nombreConyugue != null)
                        {

                            parameters.Add("nombreConyuge", element.nombreConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("nombreConyuge", "");
                        }

                        if (element.dniConyugue != null)
                        {

                            parameters.Add("dniConyuge", element.dniConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("dniConyuge", "");
                        }
                        if (element.empresaConyugue != null)
                        {

                            parameters.Add("empresaConyuge", element.empresaConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("empresaConyuge", "");
                        }
                        if (element.antiguedadConyugue != null)
                        {

                            parameters.Add("antiguedadConyuge", element.antiguedadConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("antiguedadConyuge", "");
                        }
                        if (element.vinculoFamiliarOtro != null)
                        {

                            parameters.Add("nombreVinculo", element.vinculoFamiliarOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("nombreVinculo", "");
                        }

                        if (element.dniOtro != null)
                        {

                            parameters.Add("dniOtroVinculo", element.dniOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("dniOtroVinculo", "");
                        }
                        if (element.empresaOtro != null)
                        {

                            parameters.Add("empresaOtroVinculo", element.empresaOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("empresaOtroVinculo", "");
                        }

                        if (element.referenciaVecinal != null)
                        {

                            parameters.Add("referenciaVecinal", element.referenciaVecinal.ToString());
                        }
                        else
                        {

                            parameters.Add("referenciaVecinal", "");
                        }


                        var respuesta = await client.PutAsync(urlunicaApp, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                        var json = await respuesta.Content.ReadAsStringAsync();
                        var cliente = JsonConvert.DeserializeObject<VMClienteAutorizadoRecibido>(json);

                        if (respuesta.IsSuccessStatusCode)
                        {

                            element.sincronizado = true;
                            element.mensaje = cliente.mensaje;
                            element.codigoCliente = cliente.codigoCliente.ToString();
                            element.idEstado = cliente.idEstado;
                            element.montoDisponible = (int)cliente.montoDisponible;
                            element.montoTotalMaximo = (int)cliente.montoTotalMaximoCliente;
                            element.nuevoCliente = cliente.nuevoCliente;
                            await App.Database.SaveIClienteAsync(element);
                            //vm
                            autorizacion.mensaje = cliente.mensaje;
                            autorizacion.cantComprasAnteriores = cliente.cantComprasAnteriores;
                            autorizacion.cantReclamosAnteriores = cliente.cantReclamosAnteriores;
                            autorizacion.montoDisponible = (int)cliente.montoDisponible;
                            autorizacion.montoTotalMaximoCliente = (int)cliente.montoTotalMaximoCliente;
                            autorizacion.autorizado = cliente.autorizado;
                            autorizacion.codigoCliente = cliente.codigoCliente.ToString();
                            autorizacion.tieneConexion = true;
                            autorizacion.nuevoCliente = cliente.nuevoCliente;
                            return autorizacion;
                        }
                        else
                        {
                            element.sincronizado = false;
                            await App.Database.SaveIClienteAsync(element);
                            //vm
                            autorizacion.mensaje = "Error al sincronizar, necesita autorización";
                            autorizacion.cantComprasAnteriores = 0;
                            autorizacion.cantReclamosAnteriores = 0;
                            autorizacion.autorizado = false;
                            autorizacion.codigoCliente = "";
                            autorizacion.tieneConexion = false;
                            return autorizacion;

                        }
                    }

                    catch (Exception ex)
                    {
                        var error = ex.Message;
                        ControlErrores control = new ControlErrores();

                        DateTime fecha = DateTime.Now;
                        control.codigoControlErrores = "017-AltaCliente";
                        control.sincronizado = false;
                        control.nombreError = error;
                        control.fecha = fecha;
                        control.idUsuario = idUsuario;
                        control.version = version;
                        await App.Database.SaveIControlErroresAsync(control);

                        //element.nroDocumento = "27412889";
                        //element.nroDocumento = SepararDocumento(element.nroDocumento);

                        element.sincronizado = false;
                        await App.Database.SaveIClienteAsync(element);
                        //vm
                        autorizacion.mensaje = "Error al sincronizar, necesita autorización";
                        autorizacion.cantComprasAnteriores = 0;
                        autorizacion.cantReclamosAnteriores = 0;
                        autorizacion.autorizado = false;
                        autorizacion.codigoCliente = "";
                        autorizacion.tieneConexion = false;
                        return autorizacion;

                    }
                }
                else
                {
                    autorizacion.mensaje = "Cliente ingresado, necesita autorización";
                    autorizacion.cantComprasAnteriores = 0;
                    autorizacion.cantReclamosAnteriores = 0;
                    autorizacion.autorizado = false;
                    autorizacion.codigoCliente = "";
                    autorizacion.tieneConexion = false;
                    return autorizacion;
                }

            }
            else
            {
                autorizacion.mensaje = "Necesita internet para sincronizar";
                autorizacion.cantComprasAnteriores = 0;
                autorizacion.cantReclamosAnteriores = 0;
                autorizacion.autorizado = false;
                autorizacion.codigoCliente = "";
                autorizacion.tieneConexion = false;
                return autorizacion;
            }

        }

        //alta de un cliente
        public async Task<VMAutorizacionCliente> AltaUnCliente(ClienteApp element)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            var current = Connectivity.NetworkAccess;
            VMAutorizacionCliente autorizacion = new VMAutorizacionCliente();
            if (current == NetworkAccess.Internet)
            {
                if (element.sincronizado == false & element.idEstado == 7)
                {
                    try
                    {
                        //element.nroDocumento = "27412889";
                        element.nroDocumento = SepararDocumento(element.nroDocumento);

                        string sContentType = "application/json";
                        JObject parameters = new JObject();
                        parameters.Add("nroCliente", (element.codigoCliente).ToString());
                        parameters.Add("nombreCliente", (element.nombreCliente).ToString());                        
                        parameters.Add("nroDocumento", element.nroDocumento.ToString());
                        parameters.Add("idUsuario", idUsuario.ToString());
                        parameters.Add("idEstado", (element.idEstado).ToString());

                        if (element.domicilioCliente != null)
                        {
                            parameters.Add("domicilio", (element.domicilioCliente).ToString());
                        }
                        else
                        {
                            parameters.Add("domicilio", "");
                        }

                        parameters.Add("idLocalidad", (element.idLocalidad).ToString());

                        parameters.Add("idDepartamento", (element.idDepartamento).ToString());

                        parameters.Add("latitud", (element.latitud).ToString());

                        parameters.Add("longitud", (element.longitud).ToString());

                        parameters.Add("montoDisponible", element.montoDisponible.ToString());

                        parameters.Add("ingresoMensual", element.ingresoMensual.ToString());

                        parameters.Add("ingresoComplementario", element.ingresoComplementarios.ToString());

                        parameters.Add("ingresoOtro", element.ingresoOtro.ToString());

                        parameters.Add("ingresoConyuge", element.ingresoConyogue.ToString());

                        if (element.actividad != null)
                        {

                            parameters.Add("actividad", element.actividad.ToString());
                        }
                        else
                        {
                            parameters.Add("actividad", "");
                        }

                        parameters.Add("idLocalidadOtro", element.idLocalidadAlternativa.ToString());
                        parameters.Add("idDepartamentoOtro", element.idDepartamentoAlternativo.ToString());
                        if (element.domicilioAlternativo != null)
                        {

                            parameters.Add("domicilioOtro", element.domicilioAlternativo.ToString());
                        }
                        else
                        {
                            parameters.Add("domicilioOtro", "");
                        }

                        parameters.Add("idDepartamentoLaboral", element.idDepartamentoAlternativo.ToString()); //ver
                                                                                                               //


                        if (element.genero != null)
                        {
                            parameters.Add("sexo", element.genero.ToString());
                        }
                        else
                        {
                            parameters.Add("sexo", "");
                        }

                        if (element.domicilioEmpresa != null)
                        {
                            parameters.Add("domicilioLaboral", element.domicilioEmpresa.ToString());
                        }
                        else
                        {
                            parameters.Add("domicilioLaboral", "");
                        }

                        if (element.actividad2 != null)
                        {
                            parameters.Add("actividad2", element.actividad2.ToString());
                        }
                        else
                        {
                            parameters.Add("actividad2", "");
                        }
                        if (element.empresa != null)
                        {
                            parameters.Add("empresa", element.empresa.ToString());
                        }
                        else
                        {
                            parameters.Add("empresa", "");
                        }
                        if (element.Antiguedad != null)
                        {
                            parameters.Add("antiguedad", element.Antiguedad.ToString());
                        }
                        else
                        {
                            parameters.Add("antiguedad", "");
                        }
                        if (element.fechaNacimiento != null)
                        {
                            parameters.Add("fechaNacimiento", element.fechaNacimiento.ToString());
                        }
                        else
                        {
                            parameters.Add("fechaNacimiento", "");
                        }
                        if (element.estadoCivil != null)
                        {
                            parameters.Add("estadoCivil", element.estadoCivil.ToString());
                        }
                        else
                        {
                            parameters.Add("estadoCivil", "");
                        }
                        if (element.cantidadHijo != null)
                        {
                            parameters.Add("cantidadHijos", element.cantidadHijo.ToString());
                        }
                        else
                        {
                            parameters.Add("cantidadHijos", "");
                        }
                        if (element.referencia != null)
                        {

                            parameters.Add("referencia", element.referencia.ToString());
                        }
                        else
                        {
                            parameters.Add("referencia", "");
                        }
                        if (element.vivienda != null)
                        {

                            parameters.Add("vivienda", element.vivienda.ToString());
                        }
                        else
                        {
                            parameters.Add("vivienda", "");
                        }
                        if (element.telefono != null)
                        {

                            parameters.Add("telefono", element.telefono.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono", "");
                        }
                        if (element.telefonoFijo != null)
                        {

                            parameters.Add("telefono1", element.telefonoFijo.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono1", "");
                        }
                        if (element.telFamiliar != null)
                        {

                            parameters.Add("telefono2", element.telFamiliar.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono2", "");
                        }
                        if (element.telefonoEmpresa != null)
                        {

                            parameters.Add("telefono3", element.telefonoEmpresa.ToString());
                        }
                        else
                        {
                            parameters.Add("telefono3", "");
                        }
                        if (element.tarjeta != null)
                        {

                            parameters.Add("tarjetaCredito", element.tarjeta.ToString());
                        }
                        else
                        {
                            parameters.Add("tarjetaCredito", "");
                        }
                        if (element.nombreConyugue != null)
                        {

                            parameters.Add("nombreConyuge", element.nombreConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("nombreConyuge", "");
                        }

                        if (element.dniConyugue != null)
                        {

                            parameters.Add("dniConyuge", element.dniConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("dniConyuge", "");
                        }
                        if (element.empresaConyugue != null)
                        {

                            parameters.Add("empresaConyuge", element.empresaConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("empresaConyuge", "");
                        }
                        if (element.antiguedadConyugue != null)
                        {

                            parameters.Add("antiguedadConyuge", element.antiguedadConyugue.ToString());
                        }
                        else
                        {
                            parameters.Add("antiguedadConyuge", "");
                        }
                        if (element.vinculoFamiliarOtro != null)
                        {

                            parameters.Add("nombreVinculo", element.vinculoFamiliarOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("nombreVinculo", "");
                        }

                        if (element.dniOtro != null)
                        {

                            parameters.Add("dniOtroVinculo", element.dniOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("dniOtroVinculo", "");
                        }
                        if (element.empresaOtro != null)
                        {

                            parameters.Add("empresaOtroVinculo", element.empresaOtro.ToString());
                        }
                        else
                        {
                            parameters.Add("empresaOtroVinculo", "");
                        }

                        if (element.referenciaVecinal != null)
                        {

                            parameters.Add("referenciaVecinal", element.referenciaVecinal.ToString());
                        }
                        else
                        {

                            parameters.Add("referenciaVecinal", "");
                        }


                        var respuesta = await client.PutAsync(urlunicaApp, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                        if (respuesta.IsSuccessStatusCode)
                        {
                            var json = await respuesta.Content.ReadAsStringAsync();
                            var cliente = JsonConvert.DeserializeObject<VMClienteAutorizadoRecibido>(json);
                            element.sincronizado = true;
                            element.mensaje = cliente.mensaje;
                            element.codigoCliente = cliente.codigoCliente.ToString();
                            element.idEstado = cliente.idEstado;
                            element.montoDisponible = (int)cliente.montoAutorizado;
                            element.montoTotalMaximo = (int)cliente.montoTotalMaximo;
                            element.nuevoCliente = cliente.nuevoCliente;
                            element.idCliente = cliente.idCliente;
                            //element.nroDocumento = "27412889";
                            //element.nroDocumento = SepararDocumento(element.nroDocumento);

                            await App.Database.SaveIClienteAsync(element);
                            //vm
                            autorizacion.mensaje = cliente.mensaje;
                            autorizacion.cantComprasAnteriores = cliente.cantComprasAnteriores;
                            autorizacion.cantReclamosAnteriores = cliente.cantReclamosAnteriores;
                            autorizacion.montoDisponible = (int)cliente.montoAutorizado;
                            autorizacion.montoTotalMaximoCliente = (int)cliente.montoTotalMaximo;
                            autorizacion.autorizado = cliente.autorizado;
                            autorizacion.codigoCliente = cliente.codigoCliente.ToString();
                            autorizacion.tieneConexion = true;
                            autorizacion.nuevoCliente = cliente.nuevoCliente;
                            autorizacion.idCliente = cliente.idCliente;
                            return autorizacion;
                        }
                        else
                        {
                            element.sincronizado = false;

                            //element.nroDocumento = "27412889";
                            //element.nroDocumento = SepararDocumento(element.nroDocumento);

                            await App.Database.SaveIClienteAsync(element);
                            //vm
                            autorizacion.mensaje = "Error al sincronizar, necesita autorización";
                            autorizacion.cantComprasAnteriores = 0;
                            autorizacion.cantReclamosAnteriores = 0;
                            autorizacion.autorizado = false;
                            autorizacion.codigoCliente = "";
                            autorizacion.tieneConexion = false;
                            return autorizacion;

                        }
                    }

                    catch (Exception ex)
                    {
                        var error = ex.Message;
                        ControlErrores control = new ControlErrores();
                        DateTime fecha = DateTime.Now;
                        control.codigoControlErrores = "015-AltaCliente";
                        control.sincronizado = false;
                        control.nombreError = error;
                        control.fecha = fecha;
                        control.idUsuario = idUsuario;
                        control.version = version;
                        await App.Database.SaveIControlErroresAsync(control);

                        var mensaje = "Error  de conexion, intente de nuevo.";
                        element.mensaje = mensaje;
                        autorizacion.autorizado = false;
                        autorizacion.tieneConexion = false;
                        autorizacion.codigoCliente = "";
                        return autorizacion;
                    }
                }
                else
                {
                    autorizacion.mensaje = "Cliente ingresado, necesita autorización";
                    autorizacion.cantComprasAnteriores = 0;
                    autorizacion.cantReclamosAnteriores = 0;
                    autorizacion.autorizado = false;
                    autorizacion.codigoCliente = "";
                    autorizacion.tieneConexion = false;
                    return autorizacion;
                }

            }
            else
            {
                autorizacion.mensaje = "Necesita internet para sincronizar";
                autorizacion.cantComprasAnteriores = 0;
                autorizacion.cantReclamosAnteriores = 0;
                autorizacion.autorizado = false;
                autorizacion.codigoCliente = "";
                autorizacion.tieneConexion = false;
                return autorizacion;
            }

     
        }

        //consultar la cuenta corriente del cliente
        public async Task<List<VMEstadoCtaCteCliente>> GetClienteCtacte<T>(int idCliente)
        {
            String url = "http://biancogestion.com:3630/api/Cliente/listarEstadoCuenta?";
            //String url = "http://biancogestion.com/api/Cliente/listarEstadoCuenta?";
            try
            {
                var response = await client.GetAsync(url + "idCliente=" + idCliente);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listaclientes = JsonConvert.DeserializeObject<List<VMEstadoCtaCteCliente>>(content);
                    return listaclientes;
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "006-clienteCtaCte";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);          
            }
            return null;
        }

        //saca la F o M del dni para que se pueda convertir en int
        public string SepararDocumento(string nroDocumento)
        {
            string dni = "";
            dni = nroDocumento.Replace('F',' ');
            dni = dni.Replace('M', ' ');
            dni = dni.Trim();
            return dni;
        }

        bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //dispose managed resources
                }
            }
            //dispose unmanaged resources
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}



