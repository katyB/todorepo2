﻿//using Java.Util.Prefs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSEstado
    {

        string urlEstado = "http://biancogestion.com:3630/api/Reclamo/ListarApp";
        //string urlEstado = "http://biancogestion.com/api/Reclamo/ListarApp";

        //buscar listado de estdos desde el buho
        public async Task<List<EstadoReclamo>> GetEstados<T>()
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Xamarin.Essentials.Preferences.Get("idusuarioLogueado", 0);
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(urlEstado);
            if (response.IsSuccessStatusCode)
            {
                try { 
                var content = await response.Content.ReadAsStringAsync();
                var listaclientes = JsonConvert.DeserializeObject<List<EstadoReclamo>>(content);
                return listaclientes;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "010-estado";
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "010-estado";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }
    }
}
