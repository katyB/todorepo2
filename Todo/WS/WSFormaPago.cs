﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSFormaPago
    {
        HttpClient client = new HttpClient();

        // metodo get 
        public async Task<List<ProductoFormaDePago>> Get<T>(string Url)
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Xamarin.Essentials.Preferences.Get("idusuarioLogueado", 0);
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {
                try { 
                var content = await response.Content.ReadAsStringAsync();
                var listadoRecibido = JsonConvert.DeserializeObject<List<ViewsModels.ProductoFormaPagoRecibido>>(content);
                List<ProductoFormaDePago> listadoFPproductos = new List<ProductoFormaDePago>();
                    foreach (var item in listadoRecibido)
                    {
                        ProductoFormaDePago productoFormaPago = new ProductoFormaDePago
                        {
                            idFormaPago = item.idFormaPago,
                            nombreFormaPago = item.nombreFormaPago,
                            idProducto = item.idProducto,
                            nombreProducto = item.nombreProducto,
                            montoVenta = (int)item.montoVenta,
                            cantidadCuotas = item.cantidadCuotas,
                            tieneFinanciacion = item.tieneFinanciacion,
                        };
                        listadoFPproductos.Add(productoFormaPago);
                    }

                return listadoFPproductos;
                }
                catch (Exception ex)
                {
                var error = ex.Message;
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "016-FormaPago";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "016-FormaPago";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                control.idUsuario = idUsuario;
                control.version = version;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }       
        
        //trae el listado de productos con su listado de  forma de pago

        public async Task<List<FormaPago>> GetProductoconListaFP()
        {
            var url = "http://biancogestion.com:3630/api/Producto/BuscarProductoConFormaPagoPorDeposito?";
            //var url = "http://biancogestion.com/api/Producto/BuscarProductoConFormaPagoPorDeposito?";

            //var idDeposito = App.Current.Properties["idDeposito"];
            var idDeposito = Preferences.Get("idDeposito", 0);

            var response = await client.GetAsync(url+"idDeposito="+idDeposito);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var listado = JsonConvert.DeserializeObject<List<FormaPago>>(content);
                return listado;
            }
            else
            {
                return null;
            }
        }
    }
}
