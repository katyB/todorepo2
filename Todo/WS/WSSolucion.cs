﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSSolucion
    {
        string urlCausa = "http://biancogestion.com:3630/api/Reclamo/ListarSolucion";
        //string urlCausa = "http://biancogestion.com/api/Reclamo/ListarSolucion";

        //buscar listado de estdos desde el buho
        public async Task<List<Solucion>> GetSolucion<T>()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(urlCausa);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var listado = JsonConvert.DeserializeObject<List<Solucion>>(content);
                return listado;
            }
            else
            {
                return null;
            }
        }
    }
}
