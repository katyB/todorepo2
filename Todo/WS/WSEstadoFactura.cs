﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSEstadoFactura
    {          
        //buscar listado de estdos desde el buho
        public async Task<List<EstadoFactura>> GetEstadosFactura<T>(string urlEstado)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(urlEstado);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var listaclientes = JsonConvert.DeserializeObject<List<EstadoFactura>>(content);
                return listaclientes;
            }
            else
            {
                return null;
            }
        }
    }
}
