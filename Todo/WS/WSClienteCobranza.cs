﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSClienteCobranza
    {
        HttpClient client = new HttpClient();

        //buscar listado de clientes  para cobranza
        public async Task<List<Cobranza>> GetClientesCobranza<T>(String url)
        {
            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var listaclientesrecibidos = JsonConvert.DeserializeObject<List<VMcobranzaRecibido>>(content);

                        List<Cobranza> listaclientescobranza = new List<Cobranza>();

                        foreach (var item in listaclientesrecibidos)
                        {
                            Cobranza cobranza = new Cobranza
                            {
                                idCliente = item.idCliente,
                                codCliente = item.codCliente,
                                nombreCliente = item.nombreCliente,
                                idZona = item.idZona,
                                saldo = (int)item.saldo,
                                domicilio = item.domicilio,
                                cuentaCorriente = item.cuentaCorriente,
                                idEstado = item.idEstado,
                                nombreEstado = item.nombreEstado,
                                monto = (int)item.monto,
                                fechaCobranza = item.fechaCobranza,
                                nroComprobante = item.nroComprobante,
                                cobrado = item.cobrado,
                                costoCapital = (int)item.costoCapital,
                                costoOperativo = (int)item.costoOperativo,
                                montoPrimerCuota = (int)item.montoPrimerCuota,
                            };
                            listaclientescobranza.Add(cobranza);
                        }

                        return listaclientescobranza;
                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;
                    }
                }
            }
            catch (JsonSerializationException e)
            {
                var error = e;
            }
            return null;
        }

        //buscar el listado de cuentas de cada cliente
        public async Task<List<CobranzaCuentaCorriente>> GeCobranzaCuentaCorriente<T>(String url)
        {
            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {               
                    try
                    {
                        await App.Database.DeleteCobranzaCuentaCorrienteAsync();

                        var content = await response.Content.ReadAsStringAsync();
                        var listarecibida = JsonConvert.DeserializeObject<List<VMCobranzaCuentaCorrienterecibido>>(content);

                        List<CobranzaCuentaCorriente> listados = new List<CobranzaCuentaCorriente>();

                        foreach (var item in listarecibida)
                        {
                            CobranzaCuentaCorriente cobranzaCuentaCorriente = new CobranzaCuentaCorriente
                            {
                                idCliente = item.idCliente,
                                codCliente = item.codCliente,
                                fechaVencimiento = item.fechaVencimiento,
                                idEstado = item.idEstado,
                                montoComprobante = (int)item.montoComprobante,
                            };
                            listados.Add(cobranzaCuentaCorriente);
                        }

                        return listados;
                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;
                    }

                }
            }
            catch (JsonSerializationException e)
            {
                var error = e;
            }
            return null;
        }

        // enviar datos de cobranza
        public async Task EnviarClienteCobranza()
        {
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);
            var listado = await App.Database.GetCobranzaAsync();

            var url = "http://biancogestion.com:3630/api/Cobranza/ActualizarCuentaCliente";
            //var url = "http://biancogestion.com/api/Cobranza/ActualizarCuentaCliente";

            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    if (element.sincronizado == false)
                    {
                        try
                        {
                            string sContentType = "application/json";
                            JObject parameters = new JObject();

                            parameters.Add("idCliente",(element.idCliente).ToString());
                            parameters.Add("idUsuario",(idUsuario).ToString());
                            parameters.Add("montoComprobante",(element.monto).ToString());
                            parameters.Add("costoOperativo", (element.costoOperativo).ToString());
                            parameters.Add("costoCapital", (element.costoCapital).ToString());
                            parameters.Add("fechaComprobante",(element.fechaCobranza).ToString());
                            parameters.Add("nroComprobante",(element.nroComprobante).ToString());

                            var respuesta = await client.PutAsync(url, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                            var json = await respuesta.Content.ReadAsStringAsync();
                            var cliente = JsonConvert.DeserializeObject<Cobranza>(json);

                            if (respuesta.IsSuccessStatusCode)
                            {
                                element.sincronizado = true;
                                await App.Database.SaveICobranzaAsync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveICobranzaAsync(element);
                            }
                           
                        }
                        catch (Exception e)
                        {
                            var error = e.Message;
                        }
                    }
                }

            }
        }

        //enviar datos de una cobranza 
        public async Task EnviarUnClienteCobranza(Cobranza element)
        {
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            var url = "http://biancogestion.com:3630/api/Cobranza/ActualizarCuentaCliente";
            //var url = "http://biancogestion.com/api/Cobranza/ActualizarCuentaCliente";

            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                    if (element.sincronizado == false)
                    {
                        try
                        {
                            string sContentType = "application/json";
                            JObject parameters = new JObject();

                        parameters.Add("idCliente", (element.idCliente).ToString());
                        parameters.Add("idUsuario", (idUsuario).ToString());
                        parameters.Add("montoComprobante", (element.monto).ToString());
                        parameters.Add("costoOperativo", (element.costoOperativo).ToString());
                        parameters.Add("costoCapital", (element.costoCapital).ToString());
                        parameters.Add("fechaComprobante", (element.fechaCobranza).ToString());
                        parameters.Add("nroComprobante", (element.nroComprobante).ToString());

                        var respuesta = await client.PutAsync(url, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                            var json = await respuesta.Content.ReadAsStringAsync();
                            //var cliente = JsonConvert.DeserializeObject<Cliente>(json);

                            if (respuesta.IsSuccessStatusCode)
                            {
                                element.sincronizado = true;
                                await App.Database.SaveICobranzaAsync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveICobranzaAsync(element);
                            }

                        }
                        catch (Exception e)
                        {
                            var error = e.Message;
                        }
                    }
            }
        }

        // buscaar listado de CobranzaTipoReclamo
        public async Task<List<CobranzaTipoReclamo>> GetCobranzaTipoReclamo<T>()
        {
            String url = "http://biancogestion.com:3630/api/Cobranza/buscarTipoInforme";
            //String url = "http://biancogestion.com/api/Cobranza/buscarTipoInforme";
            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var listadoCobranzaTipoReclamo = JsonConvert.DeserializeObject<List<CobranzaTipoReclamo>>(content);
                    return listadoCobranzaTipoReclamo;
                }
            }
            catch (JsonSerializationException e)
            {
                var error = e;
            }
            return null;
        }

        //enviar reclamo de cobranza
        public async Task EnviarUnReclamoDeCobranza(CobranzaReclamo element)
        {
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            var url = "http://biancogestion.com:3630/api/Cobranza/nuevoInforme";
            //var url = "http://biancogestion.com/api/Cobranza/nuevoInforme";
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                if (element.sincronizado == false)
                {
                    try
                    {
                        string sContentType = "application/json";
                        JObject parameters = new JObject();

                        parameters.Add("idTipoInforme", (element.idTipoInforme).ToString());
                        parameters.Add("idCliente", (element.idCliente).ToString());
                        parameters.Add("idEmpleado", (idUsuario).ToString());
                        parameters.Add("textoInforme", (element.observacion).ToString());
                        parameters.Add("fechaInforme", (element.fechaCobranzaReclamo).ToString());

                        var respuesta = await client.PostAsync(url, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                        var json = await respuesta.Content.ReadAsStringAsync();
                        //var cliente = JsonConvert.DeserializeObject<Cliente>(json);

                        if (respuesta.IsSuccessStatusCode)
                        {
                            element.sincronizado = true;
                            await App.Database.SaveCobranzaReclamoAsync(element);
                        }
                        else
                        {
                            element.sincronizado = false;
                            await App.Database.SaveCobranzaReclamoAsync(element);
                        }

                    }
                    catch (Exception e)
                    {
                        var error = e.Message;
                    }
                }
            }
        }
    }
}
