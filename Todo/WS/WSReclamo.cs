﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
//using static Android.Provider.CalendarContract;
using Xamarin.Essentials;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Todo.WS
{
    public class WSReclamo
    {
        String url = "http://biancogestion.com:3630/api/Reclamo/ActualizarApp";
        //String url = "http://biancogestion.com/api/Reclamo/ActualizarApp";
        HttpClient client = new HttpClient();

        //buscar listado de reclamos desde el buho
        public async Task<List<TodoItem>> GetReclamos<T>(string Url)
        {           
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var Items = JsonConvert.DeserializeObject<List<TodoItem>>(content);
                return Items;
            }
            else
            {
                return null;
            }
        }

        //metodo put de reclamos
        public async Task PutReclamo()
        {
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            var listado = await App.Database.GetItemsAsync();
                  
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    if (element.sincronizado == false & element.idEstado == 5)
                    {
                        try
                        {
                            //var parameters = new Dictionary<string, string> { { "idReclamo", (element.idReclamo).ToString() }, { "idEstado", (element.idEstado).ToString() }, { "observacionVenta", element.observacionVenta }, { "idUsuario", (element.codCliente).ToString() }, { "idSolucion", (element.CbrSolucion).ToString() }, { "referencia1", (element.Referencia1).ToString() }, { "referencia2", (element.Referencia2).ToString()} };
                            //var encodedContent = new FormUrlEncodedContent(parameters);
                            //var response = await client.PutAsync(url, encodedContent).ConfigureAwait(false);

                            string sContentType = "application/json";

                            JObject parameters = new JObject();
                            parameters.Add("idReclamo", (element.idReclamo).ToString());
                            parameters.Add("idEstado", (element.idEstado).ToString());
                            parameters.Add("observacionVenta", (element.observacionVenta).ToString());
                            parameters.Add("idUsuario", (idUsuario).ToString());
                            parameters.Add("idSolucion", (element.CbrSolucion).ToString());
                            parameters.Add("referencia1", (element.Referencia1).ToString());
                            parameters.Add("referencia2", (element.Referencia2).ToString());

                            var respuesta = await client.PutAsync(url, new StringContent(parameters.ToString(), Encoding.UTF8, sContentType));

                            //var json = await respuesta.Content.ReadAsStringAsync();
                            //var cliente = JsonConvert.DeserializeObject<Cliente>(json);

                            if (respuesta.IsSuccessStatusCode)
                            {
                                element.sincronizado = true;
                                await App.Database.DeleteItemAsync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveItemAsync(element);
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = ex.Message;
                        }
                    }
                    else
                    {
                        //eliminar
                    }
                }
            }
        }


        //Ejemplo metodo post
        //async void EnviarReclamo(string URL)
        //{
        //    var formContent = new FormUrlEncodedContent(new[]
        //        {
        //        new KeyValuePair<string, string>("somekey", "1"),
        //        new KeyValuePair<string, string>("somekey2", "2"),
        //    });

        //    var myHttpClient = new HttpClient();
        //    var response = await myHttpClient.PostAsync(URL, formContent);

        //    var json = await response.Content.ReadAsStringAsync();
        //    Events result = JsonConvert.DeserializeObject<Events>(json);
        //}

    }
}
