﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSUser
    {
        public async Task<T> Get<T>(string Url)
        {
            HttpClient client = new HttpClient();
            WSDeposito deposito = new WSDeposito();

            var response = await client.GetAsync(Url);
            var json = await response.Content.ReadAsStringAsync();
            var usuario = JsonConvert.DeserializeObject<T>(json);
            var user = JsonConvert.DeserializeObject<UserLogueado>(json);

            if (usuario != null)
            {
                Xamarin.Essentials.Preferences.Set("usuarioLogueado", user.nombreEmpleado);
                Xamarin.Essentials.Preferences.Set("idusuarioLogueado", user.idEmpleado);
                Xamarin.Essentials.Preferences.Set("telefono", user.nroTelefono);
            }
            return usuario;
        }
    }
}
