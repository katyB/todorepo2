﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.WS
{
    public class WSDeposito
    {
        //buscar datos de Deposito para la fecha actual
        public async Task<T> GetDeposito<T>(String url)
        {
            HttpClient client = new HttpClient();
            WSDeposito deposito = new WSDeposito();

            var response = await client.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var depositojson = JsonConvert.DeserializeObject<T>(json);
            var depositoDato = JsonConvert.DeserializeObject<DatoDeposito>(json);     

            if (depositojson != null)
                {
                Xamarin.Essentials.Preferences.Set("idDeposito", depositoDato.idDeposito);
                Xamarin.Essentials.Preferences.Set("nombreDeposito", depositoDato.nombreDeposito);
            }
            else
            {
                Xamarin.Essentials.Preferences.Set("idDeposito", 0);
                Xamarin.Essentials.Preferences.Set("nombreDeposito", "");
            }
            return depositojson;               
        }
    }
}
