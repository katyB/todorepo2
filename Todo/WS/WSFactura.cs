﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WSFactura
    {
        String url = "";
        HttpClient client = new HttpClient();

        // metodo get 
        public async Task<List<Talonario>> GetFacturass<T>(string Url)
        {
            var response = await client.GetAsync(Url);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var listado = JsonConvert.DeserializeObject<List<Talonario>>(content);
                return listado;
            }
            else
            {
                return null;
            }
        }

        //Enviar factura
        public async Task EnviarFctura()
        {

            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            var listado = await App.Database.GetFacturaAsync();
            var idVendedor1 = Preferences.Get("idusuarioLogueado", 0);

            int idVendedor = Convert.ToInt32(idVendedor1);
            var url = "http://biancogestion.com:3630/api/Factura/ActualizarApp";
            //var url = "http://biancogestion.com/api/Factura/ActualizarApp";
            var current = Connectivity.NetworkAccess;
            var idEstado = 14;
            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    var listaProducto = await App.Database.GetListFacturasProductosAsync(element.nroFactura);
                    var listaCouta = await App.Database.GetFacturaCuotaAsync(element.nroFactura);

                   
                    if (element.sincronizado == false & element.idEstado == 13)
                    {
                        try
                        {               
                            string sContentType = "application/json";

                            ViewsModels.VMFacturaEnviar model = new ViewsModels.VMFacturaEnviar();

                            model.nroFactura = element.nroFactura;
                            model.idFactura = element.idFactura;                         
                            model.idEstado = element.idEstado;
                            model.fechaFactura = element.fechaFactura;
                            model.nroCliente = element.nroCliente;
                            model.anulado = element.anulado;
                            model.idEstado2 = idEstado;
                            model.idVendedor = idVendedor;                      
                            model.montoFactura = element.montoFactura;
                            model.revisionAdm = element.revisionAdm;
                           
                            if (element.observacion != null)
                            {
                                model.observacion = element.observacion;
                            }
                            else
                            {
                                model.observacion = "";
                            }
                        
                            model.montoAnticipo = element.anticipo;
                            model.listaProducto = listaProducto;
                            model.listaCuota = listaCouta;
                            model.nombreCliente = element.nombreCliente;
                            model.dniCliente = Int32.Parse(element.dniCliente);
                            model.domicilioCliente = element.domicilioCliente;
                            model.idLocalidad = element.idLocalidad;
                            model.idDepartamento = element.idDepartamento;

                            var jsonstring = JsonConvert.SerializeObject(model);

                            var response = await client.PutAsync(url, new StringContent(jsonstring, Encoding.UTF8, sContentType));

                            try
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    try
                                    {
                                        element.sincronizado = true;
                                        element.nombreEstado = "Derivado a Administración";
                                        element.nombreImagenFactura = "DerivadoAdmin.png";
                                        await App.Database.SaveIFacturaAsync(element);
                                    }
                                    catch (Exception ex)
                                    {
                                        var error = ex.Message;
                                        ControlErrores control = new ControlErrores();
                                        DateTime fecha = DateTime.Now;
                                        control.codigoControlErrores = "011-FacturaAlta";
                                        control.sincronizado = false;
                                        control.nombreError = error;
                                        control.fecha = fecha;
                                        control.idUsuario = idUsuario;
                                        control.version = version;
                                        await App.Database.SaveIControlErroresAsync(control);
                                    }
                                }
                                else
                                {
                                    element.sincronizado = false;
                                    await App.Database.SaveIFacturaAsync(element);

                                }
                            }
                            catch (Exception ex)
                            {
                                var error = ex.Message;
                                ControlErrores control = new ControlErrores();
                                DateTime fecha = DateTime.Now;
                                control.codigoControlErrores = "011-FacturaAlta";
                                control.sincronizado = false;
                                control.nombreError = error;
                                control.fecha = fecha;
                                control.idUsuario = idUsuario;
                                control.version = version;
                                await App.Database.SaveIControlErroresAsync(control);
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = ex.Message;
                        }
                    }
                    else
                    {
                        //eliminar
                    }
                }
            }
        }
        //metodo put 
        public async Task PutFctura()
        {
            var listado = await App.Database.GetFacturaAsync();

            var Url = "";
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                foreach (var element in listado)
                {
                    if (element.sincronizado == false)
                    {
                        try
                        {
                            var parameters = new Dictionary<string, string> { { "id", (element.nroFactura).ToString() }, { "idEstado", (element.idEstado).ToString() }, { "observacion", element.observacion }};
                            var encodedContent = new FormUrlEncodedContent(parameters);

                            var response = await client.PutAsync(url, encodedContent).ConfigureAwait(false);

                            if (response.IsSuccessStatusCode)
                            {
                                element.sincronizado = true;
                                await App.Database.DeleteUnaFacturasync(element);
                            }
                            else
                            {
                                element.sincronizado = false;
                                await App.Database.SaveIFacturaAsync(element);
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = ex.Message;
                        }
                    }
                    else
                    {
                        //eliminar
                    }
                }
            }
        }
    }
}
