﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Todo.ViewsModels;
using Xamarin.Essentials;

namespace Todo.WS
{
    public class WMMontoMaximo
    {
        string url = "http://biancogestion.com:3630/api/Cliente/calcularMontoMaximo";
        //string url = "http://biancogestion.com/api/Cliente/calcularMontoMaximo";

        public async Task<MontoMaximo> GetMontoMaximo<T>()
        {
            var version = VersionTracking.CurrentVersion;
            var idUsuario = Preferences.Get("idusuarioLogueado", 0);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var montoMaximo = JsonConvert.DeserializeObject<VMMontoMaximo>(content);

                    MontoMaximo montosRecibidos = new MontoMaximo();
                    montosRecibidos.montoMaximo = montoMaximo.montoMaximo;
                    montosRecibidos.montoTotalMaximo = (int)(montoMaximo.montoTotalMaximo);
                    montosRecibidos.porcentajeDescuento = montoMaximo.porcentajeDescuento;

                    return montosRecibidos;
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    ControlErrores control = new ControlErrores();
                    DateTime fecha = DateTime.Now;
                    control.codigoControlErrores = "001-montoMaximo"; 
                    control.sincronizado = false;
                    control.nombreError = error;
                    control.fecha = fecha;
                    control.idUsuario = idUsuario;
                    control.version = version;
                    await App.Database.SaveIControlErroresAsync(control);
                    return null;
                }
            }
            else
            {
                var error = "No se puede acceder a la api, error 500";
                ControlErrores control = new ControlErrores();
                DateTime fecha = DateTime.Now;
                control.codigoControlErrores = "001-montoMaximo";
                control.sincronizado = false;
                control.nombreError = error;
                control.fecha = fecha;
                await App.Database.SaveIControlErroresAsync(control);
                return null;
            }
        }
    }
}
