﻿//using Android.Telephony.Data;
using System;
using System.IO;

namespace Todo
{
    public static class Constants
    {
        public const string DatabaseFilename = "BDBianco.db";

        public const SQLite.SQLiteOpenFlags Flags =
            SQLite.SQLiteOpenFlags.ReadWrite |
            SQLite.SQLiteOpenFlags.Create |
            SQLite.SQLiteOpenFlags.SharedCache;

        public static string DatabasePath
        {
            get
            {
                //var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                //return Path.Combine(basePath, DatabaseFilename);

                string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return Path.Combine(path, DatabaseFilename);
            }
        }
    }
}
