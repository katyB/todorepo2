﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using Todo.Models;
using Xamarin.Forms;

namespace Todo
{
    public class TodoItemDatabase
    {
        //En el TodoItemDatabase se usa la clase de Lazy .NET para retrasar la inicialización de la base de datos hasta que se tiene acceso por primera vez. 
        //El uso de la inicialización diferida evita que el proceso de carga de la base de datos retrase el inicio de la aplicación

        //El método InitializeAsync es responsable de comprobar si ya existe una tabla para almacenar objetos TodoItem.
        //Este método crea automáticamente la tabla si no existe.

        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            //Para habilitar WAL en SQLite.NET
            //await Database.EnableWriteAheadLoggingAsync();

            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public TodoItemDatabase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(TodoItem).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(TodoItem)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(EstadoReclamo)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(CausaReclamo)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(ClienteApp)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Solucion)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(UserLogueado)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(EstadoFactura)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Producto)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FacturaApp)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FormaPago)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FacturaProducto)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(ProductoFormaDePago)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Departamento)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Localidad)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Talonario)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FacturaCuota)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FinalizacionDia)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FinalizacionDiaCobranza)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Cobranza)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(CobranzaCuentaCorriente)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(TalonarioCobranza)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(MontoMaximo)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(CobranzaReclamo)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(CobranzaTipoReclamo)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(ControlErrores)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FacturaTemporal)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FacturaTemporalProducto)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(FaturaTemporalCuota)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Informacion)).ConfigureAwait(false);
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(InfoMonto)).ConfigureAwait(false);
                    initialized = true;
                }
            }
        }

        //La biblioteca SQLite.NET proporciona un mapa relacional de objetos simple (ORM) que permite almacenar y 
        //recuperar objetos sin escribir instrucciones SQL.

        public Task<List<TodoItem>> GetItemsAsync()
        {
            return Database.Table<TodoItem>().ToListAsync();
        }

        public Task<List<TodoItem>> GetItemsNotDoneAsync()
        {
            return Database.QueryAsync<TodoItem>("SELECT * FROM [TodoItem] ORDER BY NroReclamo DESC");
        }

        public Task<TodoItem> GetItemAsync(int NroReclamo)
        {
            return Database.Table<TodoItem>().Where(i => i.NroReclamo == NroReclamo).FirstOrDefaultAsync();
        }

        public Task<TodoItem> GetReclamoPorID(int id)
        {
            return Database.Table<TodoItem>().Where(i => i.idReclamo == id).FirstOrDefaultAsync();
        }


        internal object getDatabasePath(string v)
        {
            throw new NotImplementedException();
        }

        async public Task<int> SaveItemAsync(TodoItem item)
        {
            var consultar = await App.Database.GetReclamoPorID(item.idReclamo);

            if (consultar != null)
            {
                item.sincronizado = false;
                return await Database.UpdateAsync(item);
            }
            else
            {
                List<TodoItem> app = await App.Database.GetItemsNotDoneAsync();
                int nReclamo;
                if (app.Count() != 0)
                {
                    nReclamo = app[0].NroReclamo + 1;
                }
                else
                {
                    nReclamo = 1;
                }
                if (string.IsNullOrWhiteSpace(item.Estado))
                {
                    if (item.idEstado == 1)
                    {
                        item.Estado = "Sin Usar";
                        item.nombreImagen = "Ingresado.png";
                    }
                    else
                    {
                        if (item.idEstado == 3)
                        {
                            item.Estado = "Observada";
                            item.nombreImagen = "EnProceso.png";
                        }
                        else
                        {
                            if (item.idEstado == 5)
                            {
                                item.Estado = "Derivado a Administración";
                                item.nombreImagen = "DerivadoAdmin.png";
                            }
                        }
                    }
                }
                item.fechaReclamoMostrar = item.fechaInicio.ToString("dd/MM/yyyy");
                item.NroReclamo = nReclamo;
                item.sincronizado = false;

                return await Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(TodoItem item)
        {
            return Database.DeleteAsync(item);
        }

        public Task DeleteReclamoAsync()
        {
            return Database.QueryAsync<TodoItem>("DELETE FROM [TodoItem] ");
        }

        //Estados
        public Task<List<EstadoReclamo>> GetEstadosAsync()
        {
            return Database.Table<EstadoReclamo>().ToListAsync();
        }

        public Task<List<EstadoReclamo>> GetEstadoNotDoneAsync()
        {
            return Database.QueryAsync<EstadoReclamo>("SELECT * FROM [EstadoReclamo] ");
        }

        public Task<EstadoReclamo> GetEstadoAsync(int idEstado)
        {
            return Database.Table<EstadoReclamo>().Where(i => i.idEstado == idEstado).FirstOrDefaultAsync();
        }

        async public Task<int> SaveIEstadoAsync(EstadoReclamo item)
        {
            return await Database.InsertAsync(item);
        }

        //Solucion

        public Task<List<Solucion>> GetSolucionAsync()
        {
            return Database.Table<Solucion>().ToListAsync();
        }

        public Task<List<Solucion>> GetSolucionNotDoneAsync()
        {
            return Database.QueryAsync<Solucion>("SELECT * FROM [Solucion] ");
        }

        public Task<Solucion> GetSolucionAsync(int CbrSolucion)
        {
            return Database.Table<Solucion>().Where(i => i.CbrSolucion == CbrSolucion).FirstOrDefaultAsync();
        }

        async public Task<int> SaveSolucionAsync(Solucion item)
        {
            return await Database.InsertAsync(item);
        }

        //CausaReclamo

        public Task<List<CausaReclamo>> GetCausaReclamoAsync()
        {
            return Database.Table<CausaReclamo>().ToListAsync();
        }

        public Task<List<CausaReclamo>> GetCausaReclamoNotDoneAsync()
        {
            return Database.QueryAsync<CausaReclamo>("SELECT * FROM [CausaReclamo] ");
        }

        public Task<CausaReclamo> GetCausaReclamoAsync(int CausaReclamo)
        {
            return Database.Table<CausaReclamo>().Where(i => i.idCausa == CausaReclamo).FirstOrDefaultAsync();
        }

        async public Task<int> SaveCausaReclamoAsync(CausaReclamo item)
        {
            return await Database.InsertAsync(item);
        }

        //Cliente
        public Task<ClienteApp> GetClienteCodAsync(string nroCliente)
        {
            return Database.Table<ClienteApp>().Where(i => i.codigoCliente == nroCliente).FirstOrDefaultAsync();
        }

        public Task<List<FacturaApp>> GetClientesNuevos()
        {
            return Database.Table<FacturaApp>().Where(i => i.nuevoCliente == true).ToListAsync();
        }

        public Task<List<ClienteApp>> GetClientesPropios()
        {
            return Database.Table<ClienteApp>().Where(i => i.nuevoCliente == false).ToListAsync();
        }

        public Task<List<ClienteApp>> GetClientesAsync()
        {
            return Database.Table<ClienteApp>().ToListAsync();
        }

        public Task<List<ClienteApp>> GetClienteNotDoneAsync()
        {
            return Database.QueryAsync<ClienteApp>("SELECT * FROM [Cliente] ORDER BY codigoCliente ");
        }

        async public Task<int> SaveIClienteAsync(ClienteApp item)
        {
            item.sincronizado = false;

            switch (item.idEstado)
            {
                case 1:
                    item.imagenEstado = "Verde.png";
                    break;
                case 2:
                    item.imagenEstado = "Rojo.png";
                    break;
                case 3:
                    item.imagenEstado = "Amarillo.png";
                    break;
                case 7:
                    item.imagenEstado = "DerivadoAdmin.png";
                    break;
            }
            
            var buscarSiExisteCliente = await App.Database.GetClientePorDni(item.nroDocumento.Trim());

            if (buscarSiExisteCliente != null)
            {
                try
                {
                    return await Database.UpdateAsync(item);
                }
                catch (Exception ex)
                {
                    var e = ex.Message;
                }
                return 1;
            }
            else
            {
                return await Database.InsertAsync(item);
            }      
        }

        public Task<ClienteApp> GetClienteAsync(int codCliente)
        {
            return Database.Table<ClienteApp>().Where(i => i.idCliente == codCliente).FirstOrDefaultAsync();
        }

        public Task<ClienteApp> GetClientePorDni(string dniCliente)
        {
            return Database.Table<ClienteApp>().Where(i => i.nroDocumento == dniCliente).FirstOrDefaultAsync();
        }

        public Task DeleteCLIENTEAsync()
        {
            return Database.QueryAsync<ClienteApp>("DELETE FROM [ClienteApp] ");
        }

        public Task<int> DeleteUnClientesync(ClienteApp item)
        {
            return Database.DeleteAsync(item);
        }


        //usuario  logueado
        public Task<List<UserLogueado>> GetUserLogueadoAsync()
        {
            return Database.Table<UserLogueado>().ToListAsync();
        }

        public Task<List<UserLogueado>> GetUserLogueadoNotDoneAsync()
        {
            return Database.QueryAsync<UserLogueado>("SELECT * FROM [UserLogueado] ");
        }

        async public Task<int> SaveIUserLogueadoAsync(UserLogueado item)
        {
            return await Database.InsertAsync(item);
        }

        public Task<UserLogueado> GetUserLogueadoAsync(int idUsuario)
        {
            return Database.Table<UserLogueado>().Where(i => i.idUsuario == idUsuario).FirstOrDefaultAsync();
        }

        //EstadoFactura
        public Task<List<EstadoFactura>> GetEstadoFacturaAsync()
        {
            return Database.Table<EstadoFactura>().ToListAsync();
        }

        public Task<List<EstadoFactura>> GetEstadoFacturaNotDoneAsync()
        {
            return Database.QueryAsync<EstadoFactura>("SELECT * FROM [EstadoFactura] ORDER BY idEstadoFactura ");
        }

        async public Task<int> SaveIEstadoFacturaAsync(EstadoFactura item)
        {
            return await Database.InsertAsync(item);
        }

        public Task<EstadoFactura> GetEstadoFacturaAsync(int idEstadoFactura)
        {
            return Database.Table<EstadoFactura>().Where(i => i.idEstadoFactura == idEstadoFactura).FirstOrDefaultAsync();
        }

        public Task DeleteEstadoFacturasync()
        {
            return Database.QueryAsync<EstadoFactura>("DELETE FROM [EstadoFactura] ");
        }

        //Producto
        public Task<List<Producto>> GetProductoAsync()
        {
            return Database.Table<Producto>().Where(i => i.saldo != 0).ToListAsync();
        }

        public Task<List<Producto>> GetProductoTotalAsync()
        {
            return Database.Table<Producto>().ToListAsync();
        }

        public Task<List<Producto>> GetProductoNotDoneAsync()
        {
            return Database.QueryAsync<Producto>("SELECT * FROM [Producto] ORDER BY idProducto ");
        }

        async public Task<int> SaveIProductoAsync(Producto item)
        {

            //if (item.idinterno != 0)
            //{
            //    return await Database.UpdateAsync(item);
            //}

            //else
            //{
            //    List<Producto> app = await App.Database.GetProductoAsync();
            //    int nro;
            //    if (app.Count() != 0)
            //    {
            //        nro = app[0].idinterno + 1;
            //        item.idinterno = nro;
            //    }
            //    else
            //    {
            //        nro = 1;
            //        item.idinterno = nro;
            //    }
            //    return await Database.InsertAsync(item);
            //}
            var consultaUnProducto = await App.Database.GetProductoAsync(item.idProducto);

            if (consultaUnProducto != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task<Producto> GetProductoAsync(int idProducto)
        {
            return Database.Table<Producto>().Where(i => i.idProducto == idProducto).FirstOrDefaultAsync();
        }

        public Task DeleteProductosync()
        {
            return Database.QueryAsync<Producto>("DELETE FROM [Producto] ");
        }
        //Factura

        public Task<FacturaApp> GetFacturaPorDniYestado(string dni)
        {
            return Database.Table<FacturaApp>().Where(f => f.idEstado == 14 & f.dniCliente == dni).FirstOrDefaultAsync();
        }

        public Task<List<FacturaApp>> GetFacturaAsync()
        {
            return Database.Table<FacturaApp>().Where(f => f.idEstado == 13).ToListAsync();
        }

        public Task<List<FacturaApp>> GetFacturaTalonarioAsync()
        {
            return Database.Table<FacturaApp>().Where(p => p.idEstado == 1).ToListAsync();
        }

        public Task<List<FacturaApp>> GetFacturaPorclienteAsync(string nroDocumento, DateTime fecha)
        {
            string fechaString = fecha.ToString("dd/MM/yyyy");
            return Database.Table<FacturaApp>().Where(p => p.dniCliente == nroDocumento & p.fechaFacturaMostrar == fechaString ).ToListAsync();
        }

        public Task<List<FacturaApp>> GetFacturaNotDoneAsync()
        {
            //return Database.QueryAsync<Factura>("SELECT * FROM [Factura] WHERE [Factura].idEstado == 13");
            return Database.QueryAsync<FacturaApp>("SELECT * FROM [FacturaApp] ");
        }

        public Task<List<FacturaApp>> GetFacturasProductosAsync()
        {
            return Database.QueryAsync<FacturaApp>("SELECT * FROM [FacturaApp]  INNER JOIN [FacturaProducto] ON Factura.nroFactura = FacturaProducto.nroFactura");
        }

        public Task<List<FacturaProducto>> GetListFacturasProductosAsync(int nro)
        {
            return Database.Table<FacturaProducto>().Where(i => i.nroFactura == nro).ToListAsync();
        }

        async public Task<int> SaveIFacturaAsync(FacturaApp item)
        {
            if (item.idinterno != 0)

            //if (item.nroFactura != 0)
            {
                if (string.IsNullOrWhiteSpace(item.nombreEstado) || string.IsNullOrWhiteSpace(item.nombreImagenFactura))
                {

                    if (item.idEstado == 1)
                    {

                        item.nombreEstado = "Sin Usar";
                        item.nombreImagenFactura = "";
                    }
                    else
                    {
                        if (item.idEstado == 3)
                        {
                            item.nombreEstado = "En proceso";
                            item.nombreImagenFactura = "EnProceso.png";
                        }
                        else
                        {
                            if (item.idEstado == 13)
                            {
                                item.nombreEstado = "Derivado a Administración";
                                item.nombreImagenFactura = "DerivadoAdmin.png";

                            }
                            else
                            {
                                if (item.idEstado == 14)
                                {
                                    item.nombreEstado = "Ingresado";
                                    item.nombreImagenFactura = "Ingresado.png";
                                }

                            }
                        }
                    }
                }
                return await Database.UpdateAsync(item);
            }

            else
            {
                List<FacturaApp> app = await App.Database.GetFacturaNotDoneAsync();
                int nFactura;
                if (app.Count() != 0)
                {
                    nFactura = app[0].idinterno + 1;
                }
                else
                {
                    nFactura = 1;
                }

                if (string.IsNullOrWhiteSpace(item.nombreEstado) || string.IsNullOrWhiteSpace(item.nombreImagenFactura))
                {

                    if (item.idEstado == 1)
                    {

                        item.nombreEstado = "Sin Usar";
                        item.nombreImagenFactura = "";
                    }
                    else
                    {
                        if (item.idEstado == 3)
                        {
                            item.nombreEstado = "En proceso";
                            item.nombreImagenFactura = "EnProceso.png";
                        }
                        else
                        {
                            if (item.idEstado == 13)
                            {
                                item.nombreEstado = "Derivado a Administración";
                                item.nombreImagenFactura = "DerivadoAdmin.png";

                            }
                            else
                            {
                                if (item.idEstado == 14)
                                {
                                    item.nombreEstado = "Ingresado";
                                    item.nombreImagenFactura = "Ingresado.png";
                                }

                            }
                        }
                    }
                }


                item.fechaFacturaMostrar = item.fechaFactura.ToString("dd/MM/yyyy");
                item.horaFacturaMostrar = item.fechaFactura.ToString("HH:mm");
                item.fechaPrimerCuotaMostrar = item.fechaPrimerCuota.ToString("dd/MM/yyyy");
                item.idinterno = nFactura;
                item.sincronizado = false;
                return await Database.InsertAsync(item);
            }


        }

        public Task<FacturaApp> GetFacturaAsync(int nroFactura)
        {
            return Database.Table<FacturaApp>().Where(i => i.nroFactura == nroFactura).FirstOrDefaultAsync();
        }

        public Task<FacturaApp> GetFacturaPorIdAsync(int id)
        {
            return Database.Table<FacturaApp>().Where(i => i.idFactura == id).FirstOrDefaultAsync();
        }

        public Task DeleteFacturasync()
        {
            return Database.QueryAsync<FacturaApp>("DELETE FROM [FacturaApp] ");
        }

        public Task<int> DeleteUnaFacturasync(FacturaApp item)
        {
            return Database.DeleteAsync(item);
        }

        //FormaPago

        public Task<List<FormaPago>> GetFormaPagoAsync()
        {
            return Database.Table<FormaPago>().ToListAsync();
        }

        public Task<List<FormaPago>> GetFormaPagoNotDoneAsync()
        {
            return Database.QueryAsync<FormaPago>("SELECT * FROM [FormaPago] ");
        }


        public Task<List<FormaPago>> GetJoinFormaPago(int cantProductosSeleccionados)
        {
            return Database.QueryAsync<FormaPago>("SELECT fp.idFormaPago, fp.nombre FROM [FormaPago] fp  INNER JOIN [Producto] pp ON fp.idProducto = pp.idProducto  GROUP BY fp.idFormaPago, fp.nombre HAVING COUNT(fp.idFormaPago) >= 2");
        }

        async public Task<int> SaveIFormaPagoAsync(FormaPago item)
        {
            //List<FormaPago> app = await App.Database.GetFormaPagoAsync();
            //int nro;
            //if (app.Count() != 0)
            //{
            //    nro = app[0].idinterno + 1;
            //    item.idinterno = nro;
            //}
            //else
            //{
            //    nro = 1;
            //    item.idinterno = nro;
            //}

            //item.idProducto = prod;
            return await Database.InsertAsync(item);
        }

        public Task<List<FormaPago>> GetFormaPagoAsync(int idProducto)
        {
            return Database.Table<FormaPago>().Where(i => i.idProducto == idProducto).ToListAsync();

        }

        public Task<List<FormaPago>> GetFormaPagoYFormaPagoAsync(int idProducto, int idFormaPago)
        {
            return Database.Table<FormaPago>().Where(i => i.idProducto == idProducto & i.idFormaPago == idFormaPago).ToListAsync();
        }

        public Task<List<FormaPago>> GetFormaPagoPorProductoAsync(int id)
        {
            return Database.Table<FormaPago>().Where(i => i.idProducto == id).ToListAsync();
        }

        public Task DeleteFormaPagosync()
        {
            return Database.QueryAsync<FormaPago>("DELETE FROM [FormaPago] ");
        }

        //FacturaProducto
        public Task<FacturaProducto> GetFacturaProductooAsync()
        {
            return Database.Table<FacturaProducto>().FirstOrDefaultAsync();
        }

        async public Task<int> SaveIFacturaProductoAsync(FacturaProducto item)
        {
            item.sincronizado = false;
            return await Database.InsertAsync(item);
        }

        public Task<FacturaProducto> GetFacturaProductoAsync(int id)
        {
            return Database.Table<FacturaProducto>().Where(i => i.idFactura == id).FirstOrDefaultAsync();
        }

        public Task DeleteFacturaProductosync()
        {
            return Database.QueryAsync<FacturaProducto>("DELETE  FROM [FacturaProducto] ");
        }

        //Froma de Pago por Producto

        public Task<List<ProductoFormaDePago>> GetFormaPagoProductosAsync()
        {
            return Database.Table<ProductoFormaDePago>().ToListAsync();
        }

        public Task<List<ProductoFormaDePago>> GetFormaPagoProductoAsync(int idProducto)
        {
            return Database.Table<ProductoFormaDePago>().Where(i => i.idProducto == idProducto).ToListAsync();

        }


        public Task<List<ProductoFormaDePago>> GetProductoFormaDePagoAsync(int idProducto)
        {
            return Database.QueryAsync<ProductoFormaDePago>("SELECT * FROM [ProductoFormaDePago] WHERE idProducto = idProducto");
        }

        async public Task<int> SaveIProductoFormaDePagoAsync(ProductoFormaDePago item)
        {
            return await Database.InsertAsync(item);
        }

        //public Task<ProductoFormaDePago> GetProductoFormaDePagoAsync(int id)
        //{
        //    return Database.Table<ProductoFormaDePago>().Where(i => i.nroFactura == id).FirstOrDefaultAsync();
        //}

        public Task DeleteProductoFormaDePagosync()
        {
            return Database.QueryAsync<ProductoFormaDePago>("DELETE FROM [ProductoFormaDePago] ");
        }

        //Departamento
        public Task<List<Departamento>> GetDepartamentoAsync()
        {
            return Database.Table<Departamento>().ToListAsync();
        }

        public Task<List<Departamento>> GetDepartamentoNotDoneAsync()
        {
            return Database.QueryAsync<Departamento>("SELECT * FROM [Departamento] ORDER BY idDepartamento ");
        }

        async public Task<int> SaveIDepartamentoAsync(Departamento item)
        {
            var consultaUnDepartamento = await App.Database.GetDepartamentoAsync(item.id);

            if (consultaUnDepartamento != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task<Departamento> GetDepartamentoAsync(int id)
        {
            return Database.Table<Departamento>().Where(i => i.id == id).FirstOrDefaultAsync();
        }

        public Task DeleteDepartamentosync()
        {
            return Database.QueryAsync<Departamento>("DELETE FROM [Departamento] ");
        }
        //Localidades
        public Task<List<Localidad>> GetLocalidadAsync()
        {
            return Database.Table<Localidad>().ToListAsync();
        }

        public Task<List<Localidad>> GetLocalidadNotDoneAsync()
        {
            return Database.QueryAsync<Localidad>("SELECT * FROM [Localidad] ORDER BY idLocalidad");
        }

        async public Task<int> SaveILocalidadAsync(Localidad item, int id)
        {
            item.idDepartamento = id;

            var consultaUnaLocalidad = await App.Database.GetLocalidadAsync(item.id);

            if (consultaUnaLocalidad != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task<Localidad> GetLocalidadAsync(int id)
        {
            return Database.Table<Localidad>().Where(i => i.id == id).FirstOrDefaultAsync();
        }


        public Task<List<Localidad>> GetLocalidadesPorDepartamento(int idDepartamento)
        {
            return Database.Table<Localidad>().Where(i => i.idDepartamento == idDepartamento).ToListAsync();
        }


        public Task DeleteLocalidadsync()
        {
            return Database.QueryAsync<Localidad>("DELETE FROM [Localidad] ");
        }

        // Talonario
        public Task<List<Talonario>> GetTalonariosAsync()
        {
            return Database.Table<Talonario>().Where(i => i.facturado == false && i.anulado == false).ToListAsync();
        }

        public Task<List<Talonario>> GetTalonarios()
        {
            return Database.Table<Talonario>().ToListAsync();
        }

        public Task<List<Talonario>> GetTalonariosAnulados()
        {
            return Database.Table<Talonario>().Where(i => i.anulado == true).ToListAsync();
        }

        public Task<List<Talonario>> GetTalonariosFacturados()
        {
            return Database.Table<Talonario>().Where(i => i.facturado == true & i.anulado == false).ToListAsync();
        }

        public Task<List<Talonario>> GetTalonariosNotDoneAsync()
        {
            return Database.QueryAsync<Talonario>("SELECT * FROM [Talonario] ORDER BY idLocalidad");
        }

        async public Task<int> SaveITalonarioAsync(Talonario item)
        {
            var buscarSiExisteTalonario = await App.Database.GetTalonario(item.idFactura);

            if (buscarSiExisteTalonario != null)
            {
                return await Database.UpdateAsync(item);
            }
            else
            {
                return await Database.InsertAsync(item);
            }

        }

        public Task<Talonario> GetTalonario(int idFactura)
        {
            return Database.Table<Talonario>().Where(i => i.idFactura == idFactura).FirstOrDefaultAsync();
        }

        public Task<Talonario> GetTalonarioAsync(int idTalonario)
        {
            return Database.Table<Talonario>().Where(i => i.idTalonario == idTalonario).FirstOrDefaultAsync();
        }

        public Task DeleteTalonarios()
        {
            return Database.QueryAsync<Talonario>("DELETE FROM [Talonario] ");
        }
        public Task<int> DeleteTalonarioAsync(Talonario item)
        {
            return Database.DeleteAsync(item);
        }

        //FacturaCuota 
        public Task<List<FacturaCuota>> GetFacturaCuotaAsync()
        {
            return Database.Table<FacturaCuota>().ToListAsync();
        }

        public Task<List<FacturaCuota>> GetFacturaCuotaNotDoneAsync()
        {
            return Database.QueryAsync<FacturaCuota>("SELECT * FROM [FacturaCuota]");
        }

        async public Task<int> SaveIFacturaCuotaAsync(FacturaCuota item)
        {
            return await Database.InsertAsync(item);
        }

        public Task<List<FacturaCuota>> GetFacturaCuotaAsync(int nroFactura)
        {
            return Database.Table<FacturaCuota>().Where(i => i.nroFactura == nroFactura).ToListAsync();
        }

        public Task DeleteFacturaCuotas()
        {
            return Database.QueryAsync<FacturaCuota>("DELETE FROM [FacturaCuota] ");
        }
        public Task<int> DeleteFacturaCuotaAsync(FacturaCuota item)
        {
            return Database.DeleteAsync(item);
        }

        //consultas para armar el resumen del dia del vendedor

        async public Task<int> GetCantClientesFacturados()
        {
            List<ClienteApp> clientesPropios = await App.Database.GetClientesPropios();

            int cantidad = 0;
            foreach (var item in clientesPropios)
            {
                DateTime fecha = DateTime.Now;
                List<FacturaApp> factura = await App.Database.GetFacturaPorclienteAsync(item.nroDocumento, fecha);
                if (factura.Count() > 0)
                {
                    bool esNuevo = false;
                    foreach (var item2 in factura)
                    {
                        if (item2.nuevoCliente == true)
                        {
                            esNuevo = true;
                        }
                    }
                    if(esNuevo == false)
                    {
                        //cantidad = cantidad + factura.Count();
                        cantidad = cantidad + 1;
                    }

                }
            }

            return cantidad;
        }

        async public Task<int> GetCantClientesFacturadosNuevos()
        {
            List<FacturaApp> clientesnuevos = await App.Database.GetClientesNuevos();

            int cantidad = 0;
            foreach (var item in clientesnuevos)
            {
                DateTime fecha = DateTime.Now;
                List<FacturaApp> factura = await App.Database.GetFacturaPorclienteAsync(item.dniCliente, fecha);
                if (factura.Count() > 0)
                {
                    bool esNuevo = false;
                    foreach (var item2 in factura)
                    {
                        if (item2.nuevoCliente == true)
                        {
                            esNuevo = true;
                        }
                    }
                    if (esNuevo == true)
                    {
                        //cantidad = cantidad + factura.Count();
                        cantidad = cantidad + 1;
                    }

                }
            }
            return cantidad;
        }
        async public Task<int> GetCantFacturasAnuladas()
        {
            List<Talonario> facturas = await App.Database.GetTalonariosAnulados();
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var listaFactura = facturas.Where(f => f.fecha.ToString("dd/MM/yyyy") == fechaString);
            int cantidad = 0;

            cantidad = cantidad + listaFactura.Count();

            return cantidad;
        }

        async public Task<int> GetCantFacturasEmitidas()
        {
            List<Talonario> facturas = await App.Database.GetTalonariosFacturados();
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var listaFactura = facturas.Where(f => f.fecha.ToString("dd/MM/yyyy") == fechaString);
            int cantidad = 0;

            cantidad = cantidad + listaFactura.Count();

            return cantidad;
        }
        async public Task<Talonario> GetPrimerNrroFactura()
        {
            var listadoTalonario = await App.Database.GetTalonarios();
            var talonario = listadoTalonario.Where(p => p.facturado == true || p.anulado == true);
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var talonarioFac = talonario.Where(f => f.fecha.ToString("dd/MM/yyyy") == fechaString).ToList();
            var talonario1 = talonarioFac.FirstOrDefault();
            return talonario1;

        }

        async public Task<Talonario> GetUltimoNrroFactura()
        {
            var listadoTalonario = await App.Database.GetTalonarios();
            var talonario = listadoTalonario.Where(p => p.facturado == true || p.anulado == true);
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var talonarioFac = talonario.Where(f => f.fecha.ToString("dd/MM/yyyy") == fechaString).ToList();
            var talonario1 = talonarioFac.LastOrDefault();
            return talonario1;

        }
        public Task<List<FacturaApp>> GetFacturaDeldia()
        {
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var res = Database.Table<FacturaApp>().Where(f => f.fechaFacturaMostrar == fechaString & f.anulado == false).ToListAsync();
            return res;
        }


        async public Task<int> GetCantProductosVendidos()
        {
            var listado = await App.Database.GetFacturaDeldia();
            int cantidad = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    var listaProducto = await App.Database.GetListFacturasProductosAsync(element.nroFactura);
                    foreach (var item in listaProducto)
                    {
                        cantidad = cantidad + item.cantidadComprada;
                    }
                }

            }
            return cantidad;
        }

        async public Task<decimal> GetCantAnticipo()
        {
            var listado = await App.Database.GetFacturaDeldia();
            decimal anticipo = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    anticipo = anticipo + element.anticipo;
                }
            }
            return anticipo;
        }

        async public Task<decimal> GetCantContado()
        {
            var listado = await App.Database.GetFacturaDeldia();
            decimal anticipo = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    anticipo = anticipo + element.totalContado;
                }
            }
            return anticipo;
        }

        async public Task<decimal> GetCantDebito()
        {
            var listado = await App.Database.GetFacturaDeldia();
            decimal anticipo = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    anticipo = anticipo + element.totalDebito;

                }
            }
            return anticipo;
        }

        async public Task<decimal> GetCantCredito()
        {
            var listado = await App.Database.GetFacturaDeldia();
            decimal anticipo = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    anticipo = anticipo + element.totalCredito;
                }
            }
            return anticipo;
        }

        async public Task<decimal> GetCantFinanaciado()
        {
            var listado = await App.Database.GetFacturaDeldia();
            decimal anticipo = 0;
            foreach (var element in listado)
            {
                if (element.anulado == false)
                {
                    anticipo = anticipo + element.totalFinanciado;
                }
            }
            return anticipo;
        }


        async public Task<int> GetMontoVendido()
        {
            var listado = await App.Database.GetFacturaDeldia();
            int monto = 0;
            foreach (var element in listado)
            {
                if(element.anulado == false)
                {
                    monto = monto + element.montoFactura;
                }
            }
            return monto;
        }

        //finalizacion del dia

        public Task<List<FinalizacionDia>> FinalizacionDiaAsync()
        {
            return Database.Table<FinalizacionDia>().ToListAsync();
        }

        public Task<List<FinalizacionDia>> GetFinalizacionDiaNotDoneAsync()
        {
            return Database.QueryAsync<FinalizacionDia>("SELECT * FROM [FinalizacionDia] ORDER BY idFinalizacionDia");
        }

        async public Task<int> SaveIFinalizacionDiaAsync(FinalizacionDia item)
        {
            if (item.idFinalizacionDia != 0)
            {
                return await Database.UpdateAsync(item);
            }
            else
            {
                return await Database.InsertAsync(item);
            }

        }

        public Task<FinalizacionDia> GetFinalizacionDiaAsync(int idFinalizacionDia)
        {
            return Database.Table<FinalizacionDia>().Where(i => i.idFinalizacionDia == idFinalizacionDia).FirstOrDefaultAsync();
        }

        public Task DeleteFinalizacionDias()
        {
            return Database.QueryAsync<FinalizacionDia>("DELETE FROM [FinalizacionDia] ");
        }
        public Task<int> DeleteFinalizacionDiaAsync(FinalizacionDia item)
        {
            return Database.DeleteAsync(item);
        }

        //COBRANZA

        //talonario cobranza
        public Task<List<TalonarioCobranza>> GetTalonariosCobranzaAsync()
        {
            return Database.Table<TalonarioCobranza>().Where(i => i.usado == false).ToListAsync();
        }

        public Task<List<TalonarioCobranza>> GetTalonariosCobranzaAnulados()
        {
            return Database.Table<TalonarioCobranza>().Where(i => i.anulado == true).ToListAsync();
        }



        public Task<List<TalonarioCobranza>> GetTalonariosCobranzaUsado()
        {
            return Database.Table<TalonarioCobranza>().Where(i => i.usado == true).ToListAsync();
        }

        public Task<List<TalonarioCobranza>> GetTalonariosCobranzaNotDoneAsync()
        {
            return Database.QueryAsync<TalonarioCobranza>("SELECT * FROM [TalonarioCobranza] ORDER BY idTalonarioCobranza");
        }

        async public Task<int> SaveITalonarioCobranzaAsync(TalonarioCobranza item)
        {
            if (item.idTalonarioCobranza != 0)
            {
                return await Database.UpdateAsync(item);
            }
            else
            {
                return await Database.InsertAsync(item);
            }
        }



        public Task<TalonarioCobranza> GetTalonarioCobranzaAsync(int idTalonarioCobranza)
        {
            return Database.Table<TalonarioCobranza>().Where(i => i.idTalonarioCobranza == idTalonarioCobranza).FirstOrDefaultAsync();
        }

        public Task DeleteTalonarioCobranza()
        {
            return Database.QueryAsync<TalonarioCobranza>("DELETE FROM [TalonarioCobranza] ");
        }
        public Task<int> DeleteTalonarioCobranzaAsync(TalonarioCobranza item)
        {
            return Database.DeleteAsync(item);
        }

        //cobranza

        public Task<List<Cobranza>> GetCobranzaAsync()
        {
            return Database.Table<Cobranza>().ToListAsync();
        }

        public Task<List<Cobranza>> GetCobranzaNotDoneAsync()
        {
            return Database.QueryAsync<Cobranza>("SELECT * FROM [Cobranza] ORDER BY codCliente ");
        }

        public Task<List<Cobranza>> GetCobranzaDeldia()
        {
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var res = Database.Table<Cobranza>().Where(f => f.fechaCobranzaMostrar == fechaString).ToListAsync();
            return res;
        }

        public Task<List<Cobranza>> GetClienteCobradoAsync()
        {
            return Database.Table<Cobranza>().Where(i => i.cobrado == true).ToListAsync();
        }

        async public Task<int> SaveICobranzaAsync(Cobranza item)
        {
            item.sincronizado = false;

            switch (item.idEstado)
            {
                case 1:
                    item.imagenEstado = "Verde.png";
                    break;
                case 2:
                    item.imagenEstado = "Rojo.png";
                    break;
                case 3:
                    item.imagenEstado = "Amarillo.png";
                    break;
                case 7:
                    item.imagenEstado = "DerivadoAdmin.png";
                    break;
            }

            //if (item.idInterno != 0)
            //{
            //    try
            //    {
            //        return await Database.UpdateAsync(item);
            //    }
            //    catch (Exception ex)
            //    {
            //        var e = ex.Message;
            //    }
            //    return 1;
            //}

            //else
            //{
            //    return await Database.InsertAsync(item);
            //}

            var consultaUnCLienteCobranza = await App.Database.GetCobranzaClienteAsync(item.idCliente);

            if (consultaUnCLienteCobranza != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task DeleteCobranzaAsync()
        {
            return Database.QueryAsync<Cobranza>("DELETE FROM [Cobranza] ");
        }

        public Task<int> DeleteUnCobranzasync(Cobranza item)
        {
            return Database.DeleteAsync(item);
        }

        public Task<Cobranza> GetCobranzaCodAsync(string codigo)
        {
            return Database.Table<Cobranza>().Where(i => i.codCliente == codigo).FirstOrDefaultAsync();
        }

        public Task<Cobranza> GetCobranzaClienteAsync(int id)
        {
            return Database.Table<Cobranza>().Where(i => i.idCliente == id).FirstOrDefaultAsync();
        }

        //Cobranza Cuenta Corriente
        public Task<List<CobranzaCuentaCorriente>> GetCobranzaCuentaCorrienteAsync()
        {
            return Database.Table<CobranzaCuentaCorriente>().ToListAsync();
        }

        public Task<List<CobranzaCuentaCorriente>> GetCobranzaCuentaCorrientePorIdCliente(int idCliente)
        {
            return Database.Table<CobranzaCuentaCorriente>().Where(i => i.idCliente == idCliente).ToListAsync();
        }

        public Task<List<CobranzaCuentaCorriente>> GetCobranzaCuentaCorrienteNotDoneAsync()
        {
            return Database.QueryAsync<CobranzaCuentaCorriente>("SELECT * FROM [CobranzaCuentaCorriente] ORDER BY codCliente ");
        }

        async public Task<int> SaveCobranzaCuentaCorrienteAsync(CobranzaCuentaCorriente item)
        {
            item.sincronizado = false;

            switch (item.idEstado)
            {
                case 1:
                    item.imagenEstado = "Verde.png";
                    break;
                case 2:
                    item.imagenEstado = "Rojo.png";
                    break;
                case 3:
                    item.imagenEstado = "Amarillo.png";
                    break;
                case 7:
                    item.imagenEstado = "DerivadoAdmin.png";
                    break;
            }

            var consultaUnCLienteCtaCteCobranza = await App.Database.GetCobranzaCuentaCorrienteCodAsync(item.codCliente);

            var consulta = await App.Database.GetCobranzaCuentaCorrientePorIdCliente(item.idCliente);

            if (consultaUnCLienteCtaCteCobranza.Count() != 0)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }


        }

        public Task DeleteCobranzaCuentaCorrienteAsync()
        {
            return Database.QueryAsync<CobranzaCuentaCorriente>("DELETE FROM [CobranzaCuentaCorriente] ");
        }

        public Task<int> DeleteUnCobranzaCuentaCorrienteync(CobranzaCuentaCorriente item)
        {
            return Database.DeleteAsync(item);
        }

        public Task<List<CobranzaCuentaCorriente>> GetCobranzaCuentaCorrienteCodAsync(string codigo)
        {
            return Database.Table<CobranzaCuentaCorriente>().Where(i => i.codCliente == codigo).ToListAsync();
        }

        public Task<List<CobranzaCuentaCorriente>> GetClienteCobranzaCuentaCorrienteCodAsync(string codigo)
        {
            return Database.QueryAsync<CobranzaCuentaCorriente>("SELECT * FROM [CobranzaCuentaCorriente] ORDER BY fechaVencimiento ASC ");
        }

        //finalizacion dia de cobranza 
        async public Task<int> GetCantClientesCobrados()
        {
            List<Cobranza> lista = await App.Database.GetClienteCobradoAsync();
            DateTime fecha = DateTime.Now;
            string fechaString = fecha.ToString("dd/MM/yyyy");
            var listaCobrado = lista.Where(f => f.fechaCobranza.ToString("dd/MM/yyyy") == fechaString);
            int cantidad = 0;

            cantidad = cantidad + listaCobrado.Count();

            return cantidad;
        }

        async public Task<int> GetTotalCobrado()
        {
            var listado = await App.Database.GetCobranzaDeldia();
            int monto = 0;
            foreach (var element in listado)
            {
                monto = monto + element.monto;
            }
            return monto;
        }

        //monto mximo
        public Task<MontoMaximo> GetMontoMaximoAsync()
        {
            return Database.Table<MontoMaximo>().FirstOrDefaultAsync();
        }

        public Task<List<MontoMaximo>> GetMontoMaximoNotDoneAsync()
        {
            return Database.QueryAsync<MontoMaximo>("SELECT * FROM [MontoMaximo] ");
        }

        public Task<MontoMaximo> GetMontoMaximoAsync(int idMontoMaximo)
        {
            return Database.Table<MontoMaximo>().Where(i => i.idMontoMaximo == idMontoMaximo).FirstOrDefaultAsync();
        }

        async public Task<int> SaveMontoMaximoAsync(MontoMaximo item)
        {
            return await Database.InsertAsync(item);
        }

        public Task DeleteMontoMaximoAsync()
        {
            return Database.QueryAsync<MontoMaximo>("DELETE FROM [MontoMaximo] ");
        }

        // cobranza reclamo
        public Task<List<CobranzaReclamo>> GetCobranzaReclamoAsync()
        {
            return Database.Table<CobranzaReclamo>().ToListAsync();
        }

        public Task<List<CobranzaReclamo>> GetCobranzaReclamoNotDoneAsync()
        {
            return Database.QueryAsync<CobranzaReclamo>("SELECT * FROM [CobranzaReclamo] ORDER BY idCobranzaReclamo DESC");
        }

        public Task<CobranzaReclamo> GetCobranzaReclamoAsync(int idCobranzaReclamo)
        {
            return Database.Table<CobranzaReclamo>().Where(i => i.idCobranzaReclamo == idCobranzaReclamo).FirstOrDefaultAsync();
        }

        async public Task<int> SaveCobranzaReclamoAsync(CobranzaReclamo item)
        {
            item.sincronizado = false;

            switch (item.idEstado)
            {
                case 1:
                    item.imagenEstado = "Verde.png";
                    break;
                case 2:
                    item.imagenEstado = "Rojo.png";
                    break;
                case 3:
                    item.imagenEstado = "Amarillo.png";
                    break;
                case 7:
                    item.imagenEstado = "DerivadoAdmin.png";
                    break;
            }

            if (item.idCobranzaReclamo != 0)
            {
                try
                {
                    return await Database.UpdateAsync(item);
                }
                catch (Exception ex)
                {
                    var e = ex.Message;
                }
                return 1;
            }

            else
            {
                return await Database.InsertAsync(item);
            }

        }

        public Task<int> DeleteCobranzaReclamoAsync(CobranzaReclamo item)
        {
            return Database.DeleteAsync(item);
        }
        public Task DeleteCobranzaReclamo()
        {
            return Database.QueryAsync<CobranzaReclamo>("DELETE FROM [CobranzaReclamo]");
        }

        //CobranzaTipoReclamo
        public Task<List<CobranzaTipoReclamo>> GetCobranzaTipoReclamoAsync()
        {
            return Database.Table<CobranzaTipoReclamo>().ToListAsync();
        }

        public Task<List<CobranzaTipoReclamo>> GetCobranzaTipoReclamoNotDoneAsync()
        {
            return Database.QueryAsync<CobranzaTipoReclamo>("SELECT * FROM [CobranzaTipoReclamo] ");
        }

        public Task<CobranzaTipoReclamo> GetCobranzaTipoReclamoAsync(int idCobranzaTipoReclamo)
        {
            return Database.Table<CobranzaTipoReclamo>().Where(i => i.idCobranzaTipoReclamo == idCobranzaTipoReclamo).FirstOrDefaultAsync();
        }

        public Task<CobranzaTipoReclamo> GetCobranzaTipoReclamo(int idTipoInforme)
        {
            return Database.Table<CobranzaTipoReclamo>().Where(i => i.idTipoInforme == idTipoInforme).FirstOrDefaultAsync();
        }

        async public Task<int> SaveCobranzaTipoReclamoAsync(CobranzaTipoReclamo item)
        {
            var consultaUntipoReclamo = await App.Database.GetCobranzaTipoReclamo(item.idTipoInforme);

            if (consultaUntipoReclamo != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task DeleteCobranzaTipoReclamoAsync()
        {
            return Database.QueryAsync<CobranzaTipoReclamo>("DELETE FROM [CobranzaTipoReclamo] ");
        }

        //Control de errores
        public Task<List<ControlErrores>> GetControlErrores()
        {
            return Database.Table<ControlErrores>().ToListAsync();
        }


        public Task<ControlErrores> GetControlErroresAsync(int idControlErrores)
        {
            return Database.Table<ControlErrores>().Where(i => i.idControlErrores == idControlErrores).FirstOrDefaultAsync();
        }

        async public Task<int> SaveIControlErroresAsync(ControlErrores item)
        {
            return await Database.InsertAsync(item);
        }

        public Task DeleteControlErroresAsync()
        {
            return Database.QueryAsync<ControlErrores>("DELETE FROM [ControlErrores] ");
        }

        //Factura temporal
        public Task<List<FacturaTemporal>> GetFacturaTemporalAsync()
        {
            return Database.Table<FacturaTemporal>().ToListAsync();
        }

        public Task<List<FacturaTemporal>> GetFacturaTemporalotDoneAsync()
        {
            return Database.QueryAsync<FacturaTemporal>("SELECT * FROM [FacturaTemporal] ");
        }

        public Task<FacturaTemporal> GetFacturaTemporalAsync(int idFacturaTemporal)
        {
            return Database.Table<FacturaTemporal>().Where(i => i.idFacturaTemporal == idFacturaTemporal).FirstOrDefaultAsync();
        }

        public Task<FacturaTemporal> GetFacturaTemporal(int id)
        {
            return Database.Table<FacturaTemporal>().Where(i => i.idFactura == id).FirstOrDefaultAsync();
        }

        public Task<FacturaTemporal> GetFacturaTemporalPorCliente(string dniCliente)
        {
            return Database.Table<FacturaTemporal>().Where(i => i.dniCliente == dniCliente).FirstOrDefaultAsync();
        }

        async public Task<int> SaveFacturaTemporalAsync(FacturaTemporal item)
        {
            var consultaUnaFacturaTemporal = await App.Database.GetCobranzaTipoReclamo(item.idFacturaTemporal);

            if (consultaUnaFacturaTemporal != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task DeleteFacturaTemporalAsync()
        {
            return Database.QueryAsync<FacturaTemporal>("DELETE FROM [FacturaTemporal] ");
        }


        //Factura temporal couta
        public Task<List<FaturaTemporalCuota>> GetFaturaTemporalCuotaAsync()
        {
            return Database.Table<FaturaTemporalCuota>().ToListAsync();
        }

        public Task<List<FaturaTemporalCuota>> GetFaturaTemporalCuotaNotDoneAsync()
        {
            return Database.QueryAsync<FaturaTemporalCuota>("SELECT * FROM [FaturaTemporalCuota]");
        }

        public Task<List<FaturaTemporalCuota>> GetListFacturasTemporalCuotaAsync(int nro)
        {
            return Database.Table<FaturaTemporalCuota>().Where(i => i.nroFactura == nro).ToListAsync();
        }

        async public Task<int> SaveFaturaTemporalCuotaAsync(FaturaTemporalCuota item)
        {
            return await Database.InsertAsync(item);
        }

        public Task<List<FaturaTemporalCuota>> GetFaturaTemporalCuotaAsync(int nroFactura)
        {
            return Database.Table<FaturaTemporalCuota>().Where(i => i.nroFactura == nroFactura).ToListAsync();
        }

        public Task DeleteFaturaTemporalCuotas()
        {
            return Database.QueryAsync<FaturaTemporalCuota>("DELETE FROM [FaturaTemporalCuota] ");
        }

        //Factura temporal Producto
        public Task<FacturaTemporalProducto> GetFacturaTemporalProductooAsync()
        {
            return Database.Table<FacturaTemporalProducto>().FirstOrDefaultAsync();
        }

        async public Task<int> SaveIFacturaTemporalProductoAsync(FacturaTemporalProducto item)
        {
            item.sincronizado = false;
            return await Database.InsertAsync(item);
        }

        public Task<List<FacturaTemporalProducto>> GetListFacturasTemporalProductosAsync(int nro)
        {
            return Database.Table<FacturaTemporalProducto>().Where(i => i.nroFactura == nro).ToListAsync();
        }


        public Task<FacturaTemporalProducto> GetFacturaTemporalProductoAsync(int id)
        {
            return Database.Table<FacturaTemporalProducto>().Where(i => i.idFactura == id).FirstOrDefaultAsync();
        }

        public Task DeleteFacturaTemporalProductosync()
        {
            return Database.QueryAsync<FacturaTemporalProducto>("DELETE  FROM [FacturaTemporalProducto] ");
        }

        //servicio para los prodctos con saldo mayor a 1
        public Task<List<Producto>> GetProductosConSaldo()
        {
            return Database.Table<Producto>().Where(i => i.saldo >= 1).ToListAsync();
        }

        //Informacion
        public Task<List<Informacion>> GetInformacionAsync()
        {
            return Database.Table<Informacion>().ToListAsync();
        }

        public Task<List<Informacion>> GetInformacionNotDoneAsync()
        {
            return Database.QueryAsync<Informacion>("SELECT * FROM [Informacion] ORDER BY idInformacion");
        }

        async public Task<int> SaveIInformacionAsync(Informacion item, int id)
        {
            item.idDeposito = id;

            var consultaUnaInformacion = await App.Database.GetInformacionAsync(item.idDeposito);

            if (consultaUnaInformacion != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task<Informacion> GetInformacionAsync(int id)
        {
            return Database.Table<Informacion>().Where(i => i.idDeposito == id).FirstOrDefaultAsync();
        }

        public Task DeleteInformacionsync()
        {
            return Database.QueryAsync<Informacion>("DELETE FROM [Informacion] ");
        }
        //InfoMonto
        public Task<InfoMonto> GetInfoMontoAsync()
        {
            return Database.Table<InfoMonto>().FirstOrDefaultAsync();
        }

        public Task<List<InfoMonto>> GetInfoMontoNotDoneAsync()
        {
            return Database.QueryAsync<InfoMonto>("SELECT * FROM [InfoMonto] ORDER BY idInfoMontoSuperado");
        }

        async public Task<int> SaveIInfoMontoAsync(InfoMonto item)
        {

            var consultaUnaInfoMonto = await App.Database.GetInfoMontoAsync(item.idInfoMontoSuperado);

            if (consultaUnaInfoMonto != null)
            {
                return await Database.UpdateAsync(item);
            }

            else
            {

                return await Database.InsertAsync(item);
            }
        }

        public Task<InfoMonto> GetInfoMontoAsync(int id)
        {
            return Database.Table<InfoMonto>().Where(i => i.idInfoMontoSuperado == id).FirstOrDefaultAsync();
        }

        public Task DeleteInfoMontosync()
        {
            return Database.QueryAsync<InfoMonto>("DELETE FROM [InfoMonto] ");
        }
    }
}

