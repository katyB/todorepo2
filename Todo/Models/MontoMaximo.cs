﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class MontoMaximo
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idMontoMaximo { get; set; }
        public int porcentajeDescuento { get; set; }
        public int montoMaximo { get; set; }
        public int montoTotalMaximo { get; set; }
    }
}
