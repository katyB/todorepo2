﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class CobranzaCuentaCorriente
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idInterno { get; set; }

        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public int idEstado {get; set;}
        public int montoComprobante { get; set; }

        public string imagenEstado { get; set; }
        public bool sincronizado { get; set; }
    }
}
