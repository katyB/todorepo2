﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class EstadoFactura
    {
        [PrimaryKey]
        public int idEstadoFactura { get; set; }
        public string nombreEstadoFactura { get; set; }
    }
}
