﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class ProductoFormaDePago
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idProductoFormaDePago { get; set; }
        public int idFormaPago { get; set; }
        public string nombreFormaPago { get; set; }
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public int montoVenta { get; set; }
        public int cantidadCuotas { get; set; }
        public bool tieneFinanciacion { get; set; }
    }
}
