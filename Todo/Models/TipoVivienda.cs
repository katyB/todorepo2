﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class TipoVivienda
    {
        public int idTipoVivienda { get; set; }
        public string nombreTipoVivienda { get; set; }
    }
}
