﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Cobranza
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idInterno { get; set; }

        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public string nombreCliente { get; set; }
        public int idZona { get; set; }
        public int saldo { get; set; }
        public string domicilio { get; set; }
        public string cuentaCorriente { get; set; }
        public string nombreEstado { get; set; } //mensaje a mostrar
        public int idEstado { get; set; }

        public int monto { get; set; }
        public DateTime fechaCobranza { get; set; }
        public string fechaCobranzaMostrar { get; set; }
        public bool sincronizado { get; set; }
        public string imagenEstado { get; set; }
        public string nroComprobante { get; set; }

        public bool cobrado {get; set;}

        public int costoCapital { get; set; }
        public int costoOperativo { get; set; }

        public int montoPrimerCuota { get; set; }
    }

    }
