﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FaturaTemporalCuota
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFaturaTemporalCuota { get; set; }
        public string nombreCuota { get; set; }
        public decimal monto { get; set; }
        public DateTime fechaVto { get; set; }
        public int idFactura { get; set; }
        public int nroFactura { get; set; }
        public bool sincronizado { get; set; }
    }
}
