﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FacturaTemporalProducto
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFacturaTemporalProducto { get; set; }
        public int idFactura { get; set; }
        public int nroFactura { get; set; }
        public int idProducto { get; set; }
        public int cantidadComprada { get; set; }
        public decimal precio { get; set; }
        public int idFormaPago { get; set; }
        public decimal monto { get; set; }
        public bool sincronizado { get; set; }
        public string nombreProducto { get; set; }
        public decimal precioTotal { get; set; }
    }
}
