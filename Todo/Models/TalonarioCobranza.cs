﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class TalonarioCobranza
    {
        [PrimaryKey]
        [AutoIncrement]

        public int idTalonarioCobranza { get; set; }
        public int idTalonario { get; set; }
        public string numeroTalonario { get; set; }
        public DateTime fechaEntrega { get; set; }
        public int idEstado { get; set; }

        public bool usado { get; set; }
        public bool anulado { get; set; }
        public DateTime fecha { get; set; }
    }
}
