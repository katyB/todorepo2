﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Localidad
    {
        [PrimaryKey][AutoIncrement]
        public int idLocalidad { get; set; }
        public int id { get; set; }
        public string nombre { get; set; }
        public int idDepartamento { get; set; }
    }
}
