﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Todo.Models
{
    public class User
    {
        public int idUser;
        public int idUsuaio;
        public int idEmpleado;
        public string usuario = "";
        public string password = "";

        public User()
        {
            //if (Application.Current.Properties.ContainsKey("name"))
            if (Preferences.ContainsKey("Name"))
            {
                //var val = Application.Current.Properties["name"];
                //usuario = val.ToString();
                var val = Xamarin.Essentials.Preferences.Get("Name", "");
                usuario = val;
            }
        }
    }
}
