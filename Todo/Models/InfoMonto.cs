﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class InfoMonto
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idInfoMontoSuperado { get; set; }
        public int limiteVenta { get; set; }
        public string mensaje { get; set; }
    }
}
