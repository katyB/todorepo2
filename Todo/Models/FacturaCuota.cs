﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FacturaCuota
    {
        [PrimaryKey][AutoIncrement]
        public int idFacturaCouta { get; set; }
        public string nombreCuota { get; set; }
        public int monto { get; set; }
        public DateTime fechaVto { get; set; }
        public int idFactura { get; set; }
        public int nroFactura { get; set; }
        public bool sincronizado { get; set; }
    }
}
