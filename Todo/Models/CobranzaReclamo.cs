﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class CobranzaReclamo
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idCobranzaReclamo { get; set; }
        public int idCliente { get; set; }
        public string codCliente { get; set; }
        public string nombreCliente { get; set; }
        public int idTipoInforme { get; set; }
        public int idTipoReclamoCobranza { get; set; }
        public string nombreTipoReclamoCobranza { get; set; }
        public string observacion { get; set; }
        public bool sincronizado { get; set; }
        public int idEstado { get; set; }
        public string imagenEstado { get; set; }
        public DateTime fechaCobranzaReclamo { get; set; }
    }
}
