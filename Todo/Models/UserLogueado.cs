﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class UserLogueado
    {
        public int idUsuario { get; set; }
        public String nombreVendedor { get; set; }
        public String nombreEmpleado { get; set; }
        public int idPerfil { get; set; }
        public int idEmpleado { get; set; }
        public string nroTelefono { get; set; }
    }
}
