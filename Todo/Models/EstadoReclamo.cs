﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class EstadoReclamo
    {
        [PrimaryKey]
        public int idEstado { get; set; }
        public string nombreEstado { get; set; }
    }
}
