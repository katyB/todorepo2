﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class EstadoCivil
    {
        public int idEstadoCivil { get; set; }
        public string nombreEstadoCivil { get; set; }
    }
}
