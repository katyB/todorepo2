﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FinalizacionDiaCobranza
    {
        [PrimaryKey] [AutoIncrement]
        public int idFinalizacionDiaCobranza { get; set; }
        public string Cobrador { get; set; }
        public DateTime fechaHoraFinalizacion { get; set; }
        public string fechaFinalizacion { get; set; }
        public string horaFinalizacion { get; set; }
        public string zona {get; set;}
        public int cantRecibo { get; set; }
    }
}
