﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Departamento
    {
        [PrimaryKey][AutoIncrement]
        public int idDepartamento { get; set; }
        public int id { get; set; }
        public string nombre { get; set; }
    }
}
