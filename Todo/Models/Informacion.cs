﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Informacion
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idInformacion { get; set; }
        public int productoCargar { get; set; }
        public int idDeposito { get; set; }
    }
}
