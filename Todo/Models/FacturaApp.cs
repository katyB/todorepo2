﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FacturaApp
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFacturaApp { get; set; }
        public int nroFactura { get; set; }
        public int idFactura { get; set; }
        public int idEstado { get; set; }
        public DateTime fechaFactura { get; set; }
        public string fechaFacturaMostrar { get; set; }
        public string horaFacturaMostrar { get; set; }
        public string nroCliente { get; set; }
        public bool sincronizado { get; set; }
        public string nombreImagenFactura { get; set; }
        public string nombreCliente { get; set; }
        public string domicilioCliente { get; set; }
        public string dniCliente { get; set; }
        public int montoFactura { get; set; }
        //public int nroTalonario { get; set; }
        public string nombreEstado { get; set; }
        public int anticipo { get; set; }
        public bool financiacion { get; set; }
        public int idEstado2 { get; set; }
        public string nombreEstado2 { get; set; }
        public string nombreImagenestado2 { get; set; }
        public int idinterno { get; set; }
        public int montoDisponibleCliente { get; set; }
        public int montoTotalMaximoCliente { get; set; }
        public int totalContado { get; set; }
        public int totalCredito { get; set; }
        public int totalDebito { get; set; }
        public int totalFinanciado { get; set; }
        public int formaPago { get; set; }
        public string nombreTipoFactura { get; set; }
        public bool anulado { get; set; }

        public bool revisionAdm { get; set; }
        public string observacion { get; set; }
        public int idLocalidad { get; set; }
        public int idDepartamento { get; set; }

        //Datos que creo q no usamos
        public int cantProductos { get; set; }
        public string formaDePagoNombre { get; set; }
        public int idFormaPago { get; set; }
        public DateTime fechaPrimerCuota { get; set; }
        public string fechaPrimerCuotaMostrar { get; set; }
        public int cantidadCuotas { get; set; }
        public int valorCuota { get; set; }
        public bool nuevoCliente { get; set; }
    }
}
