﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FacturaProducto
    {
        [PrimaryKey] [AutoIncrement]
        public int idFacturaProducto { get; set; }
        public int idFactura { get; set; }
        public int nroFactura { get; set; }
        public int idProducto { get; set; }
        public int cantidadComprada { get; set; }
        public int precio { get; set; }
        public int idFormaPago { get; set; }
        public int monto { get; set; }
        public bool sincronizado { get; set; }
        public string nombreProducto { get; set; }
        public int precioTotal { get; set; }
    }

}
