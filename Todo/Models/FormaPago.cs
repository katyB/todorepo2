﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
   public class FormaPago
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idinterno { get; set; }

        public int idFormaPago { get; set; }
        public string nombreFormaPago { get; set; }
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public int montoVenta { get; set; }
        public int cantidadCuotas { get; set; }
        public bool tieneFinanciacion { get; set; }

        public string nombre { get; set; }
        public decimal  descuento { get; set; }
        public decimal aumento { get; set; }
        public string formaDePagoNombre { get; set; }
        public bool financiacion { get; set; }
        public int cantCuotas { get; set; }


        public int precioProducto { get; set; }




    }
}
