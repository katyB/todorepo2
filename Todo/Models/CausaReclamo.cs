﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class CausaReclamo
    {
        public int idCausa { get; set; }
        public string nombreCausa { get; set; }
    }
}
