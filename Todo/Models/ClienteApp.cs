﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Todo.Models
{
    public class ClienteApp
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idClienteApp { get; set; }
        public int idCliente { get; set; } //
        public string codigoCliente { get; set; } //
        public string nombreCliente { get; set; } //
        public string domicilioCliente { get; set; } // 
        public string localidadCliente { get; set; } //
        public string nombreDepartamento { get; set; } //
        public string actividad { get; set; } //
        public string tipoDocumento { get; set; }//
        public string nroDocumento { get; set; } //
        public int idEstado { get; set; } //
        public string nombreEstado { get; set; } //
        public int idDeposito { get; set; } //
        public DateTime fechaNacimiento { get; set; } //
        public string referencia { get; set; } //
        public string empresa { get; set; } //
        public double longitud { get; set; } //
        public double latitud { get; set; } //
        public string mensajeEstado { get; set; } //
        public string cantidadHijo { get; set; } //
        public string tarjeta { get; set; } //
        public string vivienda { get; set; } //
        public string genero { get; set; }
        public string codPostal { get; set; }
        
        public string referenciaVecinal { get; set; }
        public bool nuevoCliente { get; set; }

        public int idLocalidad { get; set; }
        public int idDepartamento { get; set; }
        public int idDomicilio { get; set; }

        public string imagenEstado { get; set; }
        public string fechaNacimientoMostrar { get; set; }
        public string telefonoFijo { get; set; }
        public string telefono { get; set; }
        public string telFamiliar { get; set; }
        public bool sincronizado { get; set; }
        public int idinterno { get; set; }
        public string actividad2 { get; set; }
        public string domicilioEmpresa { get; set; }
        public string Antiguedad { get; set; }
        public string telefonoEmpresa { get; set; }
        public string nombreConyugue { get; set; }
        public string nombreOtro { get; set; }
        public string dniConyugue { get; set; }
        public string dniOtro { get; set; }
        public string empresaConyugue { get; set; }
        public string empresaOtro { get; set; }

        public string antiguedadConyugue { get; set; }
        public string vinculoFamiliarOtro { get; set; }
        public string domicilioAlternativo { get; set; }
        public string departamentoAlternativo { get; set; }
        public string localidadAlternativa { get; set; }
        public int idDepartamentoAlternativo { get; set; }
        public int idLocalidadAlternativa { get; set; }
        public DateTime fechaDesdeInquilino { get; set; }
        public DateTime fechaHastaInquilino { get; set; }
        public string estadoCivil { get; set; }
        public string mensaje { get; set; }
        public int montoAutorizado { get; set; }
        public bool nuevoApp { get; set; }

        public bool autorizado { get; set; }
        public int cantidadCompra { get; set; }
        public int cantidadReclamo { get; set; }

        //ingresos
        public int ingresoConyogue { get; set; }
        public int ingresoOtro { get; set; }
        public int ingresoMensual { get; set; }
        public int ingresoComplementario { get; set; }
        public int ingresoComplementarios { get; set; }

        [DefaultValue("Sin datos")]
        public int distanciaMetros { get; set; }
        public string distanciaMetrosTexto { get; set; }


        //Montos 1° Control
        public int montoTotalAutorizadoVeraz { get; set; }
        public int montoCuotaAutorizadoVeraz { get; set; }
        //Montos 2° Control
        public int montoTotalFinal { get; set; }
        public int montoCuotaFinal { get; set; }

        //Montos Autorizados Venta
        public int montoDisponible { get; set; }
        public int montoTotalMaximo { get; set; }
    }
}
