﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FinalizacionDia
    {
        [PrimaryKey][AutoIncrement]
        public int idFinalizacionDia { get; set; }
        public string Ocupante { get; set; }
        public int nroOcupante { get; set; }
        public DateTime fechaHoraFinalizacion { get; set; }
        public string fechaFinalizacion { get; set; }
        public string horaFinalizacion { get; set; }
        public int clientesBianco { get; set; }
        public int clientesNuevos { get; set; }
        public int cantProdVendidos { get; set; }
        public int cantFacturasX { get; set; }
        public int cantFacturasXAnuladas { get; set; }
        public string desdeX { get; set; }
        public string hastaX { get; set; }
        public decimal totalVendido { get; set; }
    }
}
