﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class ControlErrores
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idControlErrores { get; set; }
        public string codigoControlErrores { get; set; }
        public string nombreError { get; set; }
        public bool sincronizado { get; set; }
        public DateTime fecha { get; set; }
        public string version { get; set; }
        public int idUsuario { get; set; }
    }
}
