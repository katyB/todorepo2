﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class CobranzaTipoReclamo
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idCobranzaTipoReclamo { get; set; }
        public int idTipoInforme { get; set; }
        public string nombreInforme { get; set; }
    }
}
