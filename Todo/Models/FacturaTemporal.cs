﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class FacturaTemporal
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFacturaTemporal { get; set; }
        public int nroFactura { get; set; }
        public int idFactura { get; set; }
        public string nombreEstado { get; set; }
        public string observacion { get; set; }
        public int idEstado { get; set; }
        public DateTime fechaFactura { get; set; }
        public string nroCliente { get; set; }
        public string nombreCliente { get; set; }
        public string dniCliente { get; set; }
        public string domicilioCliente { get; set; }
        public decimal montoFactura { get; set; }
        public int anticipo { get; set; }
        public bool financiacion { get; set; }
        public decimal montoDisponibleCliente { get; set; }
        public decimal totalContado { get; set; }
        public decimal totalCredito { get; set; }
        public decimal totalDebito { get; set; }
        public decimal totalFinanciado { get; set; }
        public int formaPago { get; set; }
        public string nombreTipoFactura { get; set; }
        public bool sincronizado { get; set; }

        public bool cancelado { get; set; }
    }
}
