﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Talonario
    {
        [PrimaryKey] [AutoIncrement]

        public int idTalonario { get; set; }
        public int idFactura { get; set; }
        public int nroFactura { get; set; }
        public string  nombreTipoFactura { get; set; }
        public bool facturado { get; set;  }
        public bool anulado { get; set; }
        public DateTime fecha { get; set; }
    }
}
