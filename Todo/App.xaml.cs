﻿using Todo.Services;
using Todo.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using System;
using System.Timers;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Todo
{
    public partial class App : Application, ILoginManager
    {
        static TodoItemDatabase database;
        static ILoginManager loginManager;
        public new static App Current;
        public static int val;
        //variables para el cierre de sesión automático
        private Timer _timer;
        private Int32 _hours = 0;
        private Int32 horaSeteada = 09;
        private Int32 minutosSeteada = 59;
        private Int32 segundosSeteada = 0;
        public App()
        {
            InitializeComponent();
            Current = this;
            bool isLoggedIn = Preferences.Get("IsLoggedIn", false);

            if (isLoggedIn)
            {
                MainPage = new MasterPageApp(); 
            }
            else
            {
                var nav = new NavigationPage(new LoginModalPage(this));
                nav.BarBackgroundColor = Color.Transparent;
                nav.BackgroundColor = Color.White;
                nav.BarTextColor = Color.White;
                nav.BackgroundColor = Color.FromHex("#f8f4fc");
                MainPage = nav;
            }    
        }

        public static TodoItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TodoItemDatabase();
                }
                return database;
            }
        }

        public void ShowMainPage()
        {
            MainPage = new MasterPageApp();
        }

        public void Logout()
        {
            var nav = new NavigationPage(new LoginModalPage(this));
            nav.BarBackgroundColor = Color.Transparent;
            nav.BackgroundColor = Color.White;
            nav.BarTextColor = Color.White;
            nav.BackgroundColor = Color.FromHex("#f8f4fc");
            MainPage = nav;
            Preferences.Clear();
            Preferences.Set("IsLoggedIn", false);

        }

        protected override void OnStart()
        {
            //Definición para levantar el proceso de cierre de sesión a una hora especifica
            //var horaReal = DateTime.Now;
            //var horaSetEnMS = horaSeteada * 60 * 60 * 1000 + minutosSeteada * 60 * 1000 + segundosSeteada * 1000;
            //var horaRelEnMS = horaReal.Hour * 60 * 60 * 1000 + horaReal.Minute * 60 * 1000 + horaReal.Second * 1000;
            //var unDiaEnMS = 24 * 60 * 60 * 1000;
            //_hours = horaSetEnMS - horaRelEnMS;

            //if (_hours <= 0)
            //{
            //    _hours = _hours + unDiaEnMS;
            //}
            //_timer = new Timer();
            //_timer.Interval = _hours;
            //string texto = (_hours / 1000).ToString();
            //Preferences.Set("Tiempo", texto);

            //_timer.Elapsed += new ElapsedEventHandler(Tick);
            //_timer.Start();
        }

        protected async override void OnSleep() { }

        protected override void OnResume(){}
        //Método para el cierre de sesión autmatico
        //void Tick(object sender, ElapsedEventArgs e)
        //{

        //    if (_hours != 24)
        //    {
        //        _hours = 24;
        //        _timer.Interval = _hours * 60 * 60 * 1000;
        //    }
        //    //_timer.Stop();
        //    App.Current.Logout();


        //}
    }
}