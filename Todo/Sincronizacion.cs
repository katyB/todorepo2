﻿//using Android.Widget;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Todo.WS;
using Todo.ViewsModels;

namespace Todo
{
    public class Sincronizacion
    {           
        HttpClient client = new HttpClient();  

        public async Task<int> btnSincronizarClick()
        {        
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;
            var idusuarioLogueado = Preferences.Get("idusuarioLogueado", 0);
            int avance = 1;
            int error = -1;

            if (current == NetworkAccess.Internet)
            // if (profiles.Contains(ConnectionProfile.WiFi)== true)
            {
                WSReclamo cliente = new WSReclamo();
                WSEstado estado = new WSEstado();
                WSCliente clientes = new WSCliente();
                WSCausa causa = new WSCausa();
                WSSolucion solucion = new WSSolucion();
                WSProducto producto = new WSProducto();
                WSFactura factura = new WSFactura();
                WSEstadoFactura estadoFactura = new WSEstadoFactura();
                WSFormaPago formaPago = new WSFormaPago();
                WSDepartamento departamento = new WSDepartamento();
                WSClienteCobranza cobranza = new WSClienteCobranza();
                WSTalonarioCobranza talonario = new WSTalonarioCobranza();
                WSDeposito deposito = new WSDeposito();
                WMMontoMaximo montoMaximo = new WMMontoMaximo();
                WSControlErrores control = new WSControlErrores();
                WSInformacion informacion = new WSInformacion();
                //WSFacturaTemporal facturaTemporal = new WSFacturaTemporal();

                //envio datos
                await cobranza.EnviarClienteCobranza();
                await cliente.PutReclamo();
                await clientes.EnviarCliente();
                await factura.EnviarFctura();
                await control.EnviarControlErrores();
                await App.Database.DeleteProductoFormaDePagosync();
                //await facturaTemporal.EnviarFcturaTemporal();
                MessagingCenter.Send(this, "Avance", avance);

                //deposito
                var Urldeposito = "http://biancogestion.com:3630/api/Vendedor/BuscarDepositoPorVendedor?";
                //var Urldeposito = "http://biancogestion.com/api/Vendedor/BuscarDepositoPorVendedor?";

                if (current == NetworkAccess.Internet)
                {
                    var datosDeposito = await deposito.GetDeposito<WSDeposito>(Urldeposito + "idVendedor=" + idusuarioLogueado);
                    if (datosDeposito != null)
                    {
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                var idDeposito = Preferences.Get("idDeposito", 0);

                if (current == NetworkAccess.Internet)
                {
                    //buscar monto maximo del monto disponible del cliente
                    await App.Database.DeleteMontoMaximoAsync();
                    var montoDisponible = await montoMaximo.GetMontoMaximo<WMMontoMaximo>();
                    if (montoDisponible != null)
                    {
                        await App.Database.SaveMontoMaximoAsync(montoDisponible);
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }



                if (current == NetworkAccess.Internet)
                {
                    //informacion
                    string urlInformacion = "http://biancogestion.com:3630/api/Producto/calcularTarjeta?";
                    //string urlInformacion = "http://biancogestion.com/api/Producto/calcularTarjeta?";

                    var info = await informacion.GetInformacion<WSInformacion>(urlInformacion + "idDeposito=" + idDeposito + "&&codProducto=");
                    if (info != null)
                    {

                        await App.Database.SaveIInformacionAsync(info, idDeposito);

                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //InfoMonto 

                    var info = await informacion.GetMontoSuperado<WSInformacion>();
                    if (info != null)
                    {
                        await App.Database.SaveIInfoMontoAsync(info);

                        MessagingCenter.Send(this, "Avance", avance);
                    }
                }


                if (current == NetworkAccess.Internet)
                {
                    //productos
                    string urlproducto = "http://biancogestion.com:3630/api/Producto/BuscarProductoPorDeposito?";
                    //string urlproducto = "http://biancogestion.com/api/Producto/BuscarProductoPorDeposito?";
                    var respuestaProductos = await producto.GetProductos<WSProducto>(urlproducto + "idDeposito=" + idDeposito);
                    if (respuestaProductos != null)
                    {
                        foreach (var element in respuestaProductos)
                        {
                            await App.Database.SaveIProductoAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }


                if (current == NetworkAccess.Internet)
                {
                    //get Facturas
                    string urlfacturas = "http://biancogestion.com:3630/api/Factura/ListarPendienteDeposito?";
                    //string urlfacturas = "http://biancogestion.com/api/Factura/ListarPendienteDeposito?";

                    var respuestaFactura = await factura.GetFacturass<WSFactura>(urlfacturas + "idDeposito=" + idDeposito);
                    if (respuestaFactura != null)
                    {
                        foreach (var element in respuestaFactura)
                        {
                            await App.Database.SaveITalonarioAsync(element);
                        }

                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //buscaar tipo reclamos corbanza               
                    var cobranzaListatipo = await cobranza.GetCobranzaTipoReclamo<WSClienteCobranza>();
                    if (cobranzaListatipo != null)
                    {
                        foreach (var element in cobranzaListatipo)
                        {
                            await App.Database.SaveCobranzaTipoReclamoAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }


                if (current == NetworkAccess.Internet)
                {
                    //listado de cliente cobranza COBRANZA
                    string urltClienteCobranza = "http://biancogestion.com:3630/api/Cobranza/listarClientesPorCobrador?";
                    //string urltClienteCobranza = "http://biancogestion.com/api/Cobranza/listarClientesPorCobrador?";
                    var respuestaClienteCobranza = await cobranza.GetClientesCobranza<Cobranza>(urltClienteCobranza + "idEmpleado=" + idusuarioLogueado);
                    if (respuestaClienteCobranza != null)
                    {
                        foreach (var element in respuestaClienteCobranza)
                        {
                            await App.Database.SaveICobranzaAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //getclientes por supervisor
                    string urlcliente = "http://biancogestion.com:3630/api/Cliente/BuscarClientePorDeposito?";
                    //string urlcliente = "http://biancogestion.com/api/Cliente/BuscarClientePorDeposito?";
                    var respuestacliente = await clientes.GetClientes<WSCliente>(urlcliente + "idDeposito=" + idDeposito);
                    if (respuestacliente != null)
                    {
                        foreach (var element in respuestacliente)
                        {
                            try
                            {

                                await App.Database.SaveIClienteAsync(element);
                            }
                            catch (Exception e)
                            {
                                var a = e;
                            }
                        }

                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //lista cuenta corriente de los clientes de cobranza COBRANZA
                    string urlCobranzaCtacte = "http://biancogestion.com:3630/api/Cobranza/listarCuentaCorrientePorCobrador?";
                    //string urlCobranzaCtacte = "http://biancogestion.com/api/Cobranza/listarCuentaCorrientePorCobrador?";
                    var respuestactaCte = await cobranza.GeCobranzaCuentaCorriente<CobranzaCuentaCorriente>(urlCobranzaCtacte + "idEmpleado=" + idusuarioLogueado);
                    if (respuestactaCte != null)
                    {
                        foreach (var element in respuestactaCte)
                        {
                            await App.Database.SaveCobranzaCuentaCorrienteAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    
                    //Forma de pago
                    var urlFP = "http://biancogestion.com:3630/api/Producto/BuscarProductoConFormaPagoPorDeposito?";
                    //var urlFP = "http://biancogestion.com/api/Producto/BuscarProductoConFormaPagoPorDeposito?";
                    
                    await App.Database.DeleteProductoFormaDePagosync();
                    var listadoProductos = await App.Database.GetProductoAsync();

                    if (listadoProductos.Count() != 0)
                    {                      
                        var respuestaWSFormaDePago = await formaPago.Get<WSFormaPago>(urlFP + "idDeposito=" + idDeposito);
                        foreach (var item in respuestaWSFormaDePago)
                        {
                            try
                            {
                                await App.Database.SaveIProductoFormaDePagoAsync(item);
                            }
                            catch (Exception e)
                            {
                                var a = e;
                            }
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //listado de talonario cobranza COBRANZA
                    string urltalonaarioCobranza = "http://biancogestion.com:3630/api/Talonario/listarTalonariosUnEmpleado?";
                    //string urltalonaarioCobranza = "http://biancogestion.com/api/Talonario/listarTalonariosUnEmpleado?";

                    var respuestaTalonarioCobranza = await talonario.GetListaTalonarioCobranza<WSTalonarioCobranza>(urltalonaarioCobranza + "idEmpleado=" + idusuarioLogueado);
                    if (respuestaTalonarioCobranza != null)
                    {
                        foreach (var element in respuestaTalonarioCobranza)
                        {
                            await App.Database.SaveITalonarioCobranzaAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //Departamentos
                    string urlDepartamento = "http://biancogestion.com:3630/api/General/listarDepartamento";
                    //string urlDepartamento = "http://biancogestion.com/api/General/listarDepartamento";
                    var respuestaDepartamento = await departamento.GetDepartamentos<WSDepartamento>(urlDepartamento);
                    if (respuestaDepartamento != null)
                    {
                        foreach (var element in respuestaDepartamento)
                        {
                            await App.Database.SaveIDepartamentoAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //localidades
                    String Urllocalidad = "http://biancogestion.com:3630/api/General/listarUnLocalidad?";
                    //String Urllocalidad = "http://biancogestion.com/api/General/listarUnLocalidad?";
                    var listadoDepartamentos = await App.Database.GetDepartamentoAsync();
                    if (listadoDepartamentos.Count() != 0)
                    {
                        foreach (var element in listadoDepartamentos)
                        {
                            var respuestaWSDepartamento = await departamento.GetLocalidades<WSDepartamento>(Urllocalidad + "idDepartamento=" + element.id);
                            var prod = element.idDepartamento;
                            foreach (var it in respuestaWSDepartamento)
                            {
                                await App.Database.SaveILocalidadAsync(it, prod);
                            }
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }



                if (current == NetworkAccess.Internet)
                {
                    //reclamos  
                    string Url2 = "http://biancogestion.com:3630/api/Reclamo/GetListarPorAsignado?";
                    //string Url2 = "http://biancogestion.com/api/Reclamo/GetListarPorAsignado?";

                    var response = await cliente.GetReclamos<WSReclamo>(Url2 + "idSupervisor=" + idusuarioLogueado + "&idEstado=" + 3);
                    if (response != null)
                    {
                        foreach (var element in response)
                        {
                            await App.Database.SaveItemAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //listado de estados de las Facturas
                    string UrlEstadoFactura = "http://biancogestion.com:3630/api/Factura/listarEstado";
                    //string UrlEstadoFactura = "http://biancogestion.com/api/Factura/listarEstado";

                    var respuestaestadoFactura = await estadoFactura.GetEstadosFactura<WSEstadoFactura>(UrlEstadoFactura);
                    if (respuestaestadoFactura != null)
                    {

                        foreach (var element in respuestaestadoFactura)
                        {
                            await App.Database.SaveIEstadoFacturaAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }


                if (current == NetworkAccess.Internet)
                {
                    //get estados

                    var respuestaEstado = await estado.GetEstados<WSEstado>();
                    if (respuestaEstado != null)
                    {
                        foreach (var element in respuestaEstado)
                        {
                            await App.Database.SaveIEstadoAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);

                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //get causas

                    var respuestacausas = await causa.GetCausas<WSCausa>();
                    if (respuestacausas != null)
                    {
                        foreach (var element in respuestacausas)
                        {
                            await App.Database.SaveCausaReclamoAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }

                if (current == NetworkAccess.Internet)
                {
                    //solucion

                    var listadosolucion = await solucion.GetSolucion<WSSolucion>();
                    if (listadosolucion != null)
                    {
                        foreach (var element in listadosolucion)
                        {
                            await App.Database.SaveSolucionAsync(element);
                        }
                        MessagingCenter.Send(this, "Avance", avance);
                    }
                    else
                    {
                        MessagingCenter.Send(this, "Avance", error);
                    }
                }
                else
                {
                    MessagingCenter.Send(this, "Avance", error);
                }



                return 1;
            }
            else
            {
                MessagingCenter.Send(this, "Avance", error);

                return 0;
            }
        }

        public async Task<int> btnSincronizarParcial()
        {
            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;
            int avance = 1;

            //if (current == NetworkAccess.Internet)
            if (profiles.Contains(ConnectionProfile.WiFi) == true)
            {
                WSReclamo cliente = new WSReclamo();
                WSEstado estado = new WSEstado();
                WSCliente clientes = new WSCliente();
                WSFactura factura = new WSFactura();
                WSFormaPago formaPago = new WSFormaPago();
                WSClienteCobranza cobranza = new WSClienteCobranza();
                WSControlErrores control = new WSControlErrores();
                //WSFacturaTemporal facturaTemporal = new WSFacturaTemporal();
                //enviar datos
                await control.EnviarControlErrores();
                //await facturaTemporal.EnviarFcturaTemporal();
                await cobranza.EnviarClienteCobranza();
                MessagingCenter.Send(this, "Avance", avance);
                await cliente.PutReclamo();
                MessagingCenter.Send(this, "Avance", avance);
                await clientes.EnviarCliente();
                MessagingCenter.Send(this, "Avance", avance);
                await factura.EnviarFctura();
                MessagingCenter.Send(this, "Avance", avance);

                //Limpiamos la base de datos completa
                await App.Database.DeleteInformacionsync();
                await App.Database.DeleteCobranzaCuentaCorrienteAsync();
                await App.Database.DeleteCobranzaTipoReclamoAsync();
                await App.Database.DeleteCobranzaReclamo();
                await App.Database.DeleteTalonarioCobranza();
                await App.Database.DeleteEstadoFacturasync();
                await App.Database.DeleteFacturasync();
                await App.Database.DeleteProductoFormaDePagosync();
                await App.Database.DeleteProductosync();
                await App.Database.DeleteCLIENTEAsync();
                await App.Database.DeleteFacturaProductosync();
                await App.Database.DeleteReclamoAsync();
                await App.Database.DeleteTalonarios();
                await App.Database.DeleteCobranzaAsync();
                await App.Database.DeleteCobranzaCuentaCorrienteAsync();
                await App.Database.DeleteCLIENTEAsync();
                await App.Database.DeleteMontoMaximoAsync();
                await App.Database.DeleteLocalidadsync();
                await App.Database.DeleteDepartamentosync();
                await App.Database.DeleteFacturaTemporalAsync();
                await App.Database.DeleteFacturaTemporalProductosync();
                await App.Database.DeleteFaturaTemporalCuotas();
                await App.Database.DeleteControlErroresAsync();
              
                MessagingCenter.Send(this, "Avance", avance);

                // sinronizamos datos
                btnSincronizarClick();


                return 1;
            }
            else
            {
                return 0;
            }

        }

 
    }
}

