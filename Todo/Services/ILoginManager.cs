﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Services
{
    public interface ILoginManager
    {
        void ShowMainPage();
        void Logout();
    }
}
