﻿using System;
using System.Collections.Generic;
using System.Text;
using Todo.Views;
using Xamarin.Forms;

namespace Todo.Services
{
    public class LoginModalPage : CarouselPage
    {
        ContentPage login;

        public LoginModalPage(ILoginManager ilm)
        {
            login = new LoginView(ilm);
            this.Children.Add(login);
            MessagingCenter.Subscribe<ContentPage>(this, "Login", (sender) =>
            {
                this.SelectedItem = login;
            });

        }

    
    }
}
